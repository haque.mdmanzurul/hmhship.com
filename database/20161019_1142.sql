CREATE TABLE  `business_signups` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`shipping_plan` VARCHAR( 500 ) NOT NULL ,
`insured` BOOL NOT NULL DEFAULT '0',
`economy` BOOL NOT NULL DEFAULT '0',
`expedited` BOOL NOT NULL DEFAULT '0',
`dhl` BOOL NOT NULL DEFAULT '0',
`ups` BOOL NOT NULL DEFAULT '0',
`usps` BOOL NOT NULL DEFAULT '0',
`fedex` BOOL NOT NULL DEFAULT '0',
`account_shipping_carrier` VARCHAR (500) NULL,
`account_number` VARCHAR (500) NULL,
`account_zip` VARCHAR(50) NULL,
`business` VARCHAR(500) NULL,
`primary_person` VARCHAR(1000) NULL,
`main_phone` VARCHAR(100) NULL,
`cell_phone` VARCHAR(200) NULL,
`fax` VARCHAR(100) NULL,
`website` VARCHAR(1000) NULL,
`street_address` VARCHAR(1000) NULL,
`street_address2` VARCHAR(1000) NULL,
`city` VARCHAR (1000) NULL,
`state` VARCHAR (100) NULL,
`zip` VARCHAR (100) NULL,
`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`read` BOOL NOT NULL DEFAULT  '0'
);

update database_version set version = '20161019_1142';