-- DB Version 20160928_1807

ALTER TABLE `transactions` 
ADD COLUMN `reference_tran` varchar(200),
ADD COLUMN `amount` decimal(10,2)
;

CREATE TABLE `database_version` (
  `version` varchar(500) NOT NULL COMMENT 'version string'
);

insert into database_version (version) values ('20160928_1807');