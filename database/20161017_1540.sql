CREATE TABLE  `messages` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`name` VARCHAR( 500 ) NOT NULL ,
`email` VARCHAR( 600 ) NOT NULL ,
`subject` VARCHAR( 500 ) NOT NULL ,
`message` TEXT NOT NULL ,
`tran_number` VARCHAR( 100 ) NOT NULL ,
`tran_type` VARCHAR( 50 ) NOT NULL,
`updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
`read` BOOL NOT NULL DEFAULT  '0'
);

update database_version set version = '20161017_1540';