ALTER TABLE  `users` ADD  `balance` DECIMAL( 10, 2 ) NULL ;


ALTER TABLE  `shipments` ADD  `balance_due` DECIMAL( 10, 2 ) NULL ;


ALTER TABLE  `messages` ADD  `user_id` INT NULL COMMENT  'id of User this message is associated with',
ADD  `from_hmh` BOOL NOT NULL DEFAULT  '0' COMMENT  'Was this message from HMH?';


ALTER TABLE  `messages` ADD  `created` DATETIME NULL ;


-- initialize new created field
update messages set created=updated;


CREATE TRIGGER `messages_insert` BEFORE INSERT ON `messages` 
FOR EACH ROW 
SET NEW.created = NOW();


ALTER TABLE `addresses` 
ADD COLUMN `default_billing` TINYINT(1) NULL DEFAULT 0 AFTER `modified`,
ADD COLUMN `default_shipping` TINYINT(1) NULL DEFAULT 0 AFTER `default_billing`;




update database_version set version = '20170424_1106';
