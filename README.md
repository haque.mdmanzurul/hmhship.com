# HMHShip

- Staging: [http://gettingitdone.121ecommerce.com/hmhship/staging/quickship](http://gettingitdone.121ecommerce.com/hmhship/staging/quickship)

## Initial setup


## Notes

Payment are done by Paypal

## Vagrant

VM config files has been generated by using puphpet.com

## Workflow

### Frontend

Javascript and CSS files are combined and minified by using a Bower task. 

- Source files are located at `/app/webroot/FRONTEND`.
- app/dev/css/styles.css is compiled from SASS sources under `css/dev/sass`

## Deployment

TODO: unknown deployment method, fill me

## External dependencies

### Grunt, Bower, Compass

- Install Ruby
- Install Compass with `gem update --system && gem install compass` (administrator permissions may be required)
- Install Compass for Grunt `npm install grunt-contrib-compass --save-dev`
- 

### CSS

- [Twitter Bootstrap v3.3.2](http://www.getbootstrap.com)

### Javascript

- [hjquery.wm-validate-1.0](http://welisonmenezes.com.br/extras/plugins/jquery/wm-validate/) (form validation)
- [jquery lightSlider.js v1.1.1](http://sachinchoolur.github.io/lightslider/) (section slide effects)
- [Kalypto v.0.2.2](https://github.com/localpcguy/Kalypto) (replace radio/checkbox with customizable CSS)

### PHP
- [Composer](http://getcomposer.org)
- Easy-Post


# CakePHP

[![Latest Stable Version](https://poser.pugx.org/cakephp/cakephp/v/stable.svg)](https://packagist.org/packages/cakephp/cakephp)
[![License](https://poser.pugx.org/cakephp/cakephp/license.svg)](https://packagist.org/packages/cakephp/cakephp)
[![Bake Status](https://secure.travis-ci.org/cakephp/cakephp.png?branch=master)](http://travis-ci.org/cakephp/cakephp)
[![Code consistency](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/grade.svg)](http://squizlabs.github.io/PHP_CodeSniffer/analysis/cakephp/cakephp/)

[![CakePHP](http://cakephp.org/img/cake-logo.png)](http://www.cakephp.org)

CakePHP is a rapid development framework for PHP which uses commonly known design patterns like Active Record, Association Data Mapping, Front Controller and MVC.
Our primary goal is to provide a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss to flexibility.


## Some Handy Links

[CakePHP](http://www.cakephp.org) - The rapid development PHP framework

[CookBook](http://book.cakephp.org) - THE CakePHP user documentation; start learning here!

[API](http://api.cakephp.org) - A reference to CakePHP's classes

[Plugins](http://plugins.cakephp.org/) - A repository of extensions to the framework

[The Bakery](http://bakery.cakephp.org) - Tips, tutorials and articles

[Community Center](http://community.cakephp.org) - A source for everything community related

[Training](http://training.cakephp.org) - Join a live session and get skilled with the framework

[CakeFest](http://cakefest.org) - Don't miss our annual CakePHP conference

[Cake Software Foundation](http://cakefoundation.org) - Promoting development related to CakePHP


## Get Support!

[#cakephp](http://webchat.freenode.net/?channels=#cakephp) on irc.freenode.net - Come chat with us, we have cake

[Google Group](https://groups.google.com/group/cake-php) - Community mailing list and forum

[GitHub Issues](https://github.com/cakephp/cakephp/issues) - Got issues? Please tell us!

[Roadmaps](https://github.com/cakephp/cakephp/wiki#roadmaps) - Want to contribute? Get involved!


## Contributing

[CONTRIBUTING.md](CONTRIBUTING.md) - Quick pointers for contributing to the CakePHP project

[CookBook "Contributing" Section (2.x)](http://book.cakephp.org/2.0/en/contributing.html) [(3.0)](http://book.cakephp.org/3.0/en/contributing.html) - Version-specific details about contributing to the project
