<?php

/**
 *
 */
namespace Hmhship\Quickship;

/**
 * Class Address
 *
 * @package Hmhship\Quickship
 */
class Address
{
	/**
	 * @var string
	 */
	public $first_name = '';

	/**
	 * @var string
	 */
	public $last_name = '';

	/**
	 * @var string
	 */
	public $address1 = '';

	/**
	 * @var array
	 */
	public $address2 = '';

	/**
	 * @var string
	 */
	public $city = '';
        
        public $state = '';

	/**
	 * @var string
	 */
	public $postal_code = '';

	/**
	 * @var null
	 */
	public $country = null;

	/**
	 * @var string
	 */
	public $phone = '';

	/**
	 * @param $data
	 */
	public function set($data)
	{
		$this->first_name = $data->first_name;
		$this->last_name = $data->last_name;
		$this->address1 = $data->address1;
		$this->address2 = $data->address2;
		$this->city = $data->city;
		$this->country = $data->country;
		$this->postal_code = $data->postal_code;
        if (isset($data->phone))		    
            $this->phone = $data->phone;
        if (isset($data->state))
             $this->state = $data->state;
	}

}