<?php

/**
 *
 */
namespace Hmhship\Quickship;


/**
 * Class Package
 *
 * @package Hmhship\Quickship
 */
class Package
{
    /**
     * @var string
     */
    public $size_unit = 'in';

    /**
     * @var string
     */
    public $weight_unit = 'lb';

    /**
     * @var string
     */
    public $tracking_code = '';

    /**
     * @var array
     */
    public $consolidated = array();

    /**
     * @var int
     */
    public $width = 0;

    /**
     * @var int
     */
    public $height = 0;

    /**
     * @var int
     */
    public $length = 0;

    /**
     * @var int
     */
    public $weight = 0;

    /**
     * @var array
     */
    public $items = array();

    /**
     * @var array
     */
    private $_item_data = array();

    /**
     * @var array
     */
    public $data = array();

    /**
     * @param $items
     *
     * @return float|int|null
     */
    public function getSumPackageItemsValues($items)
    {
        $total = null;
        foreach ($items as $item) {
            if (is_null($total)) $total = 0;
            $price = substr($item->price_value, 1, strlen($item->price_value));
            $total += floatval($price);
        }
        return $total;
    }
}