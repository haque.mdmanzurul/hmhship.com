<?php

/**
 *
 */
namespace Hmhship;

/**
 * Class Logger
 * @package Hmhship
 */
class Logger
{
	private static $instance = null;

	private $_log = null;

	public static function getInstance()
	{
		if (null === static::$instance) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	protected function __construct()
	{

	}

	private function __clone()
	{

	}

	private function __wakeup()
	{

	}

	/**
	 * @param string $message
	 * @param string $type
	 * @return mixed
	 */
	public function log($message = '', $type = 'error')
	{
		if ($this->_log === null) {
			$this->_log = new \Monolog\Logger('general');
			/*$browserHandler = new \Monolog\Handler\BrowserConsoleHandler(\Monolog\Logger::INFO);
			$streamHandler = new \Monolog\Handler\StreamHandler('php://stderr', \Monolog\Logger::ERROR);*/
			$slackHandler = new \Monolog\Handler\SlackHandler('xoxp-3765617742-4832290450-15851248243-849c842684', '#hmhship_phase2', 'HmhshipLogger');
			$slackHandler->setLevel(\Monolog\Logger::DEBUG);

			//$logger->pushHandler($browserHandler);
			//$logger->pushHandler($streamHandler);
			$this->_log->pushHandler($slackHandler); // TODO: remove in production

		}
		
		$message .= sprintf(" | Server: %s | Remote IP: %s", env('HTTP_HOST'), env('REMOTE_ADDR'));
		
		return $this->_log->{$type}($message);
	}
}