(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"row\">\n  <div class=\"col-md-4\">\n    <ng-select [(ngModel)]=\"selectedOption\" (change)=\"showOptionShipments(selectedOption)\"  [clearable]=\"false\" class=\"all_options\">\n      <ng-option [value]=\"999\">All Shipments</ng-option>\n      <ng-option [value]=\"4\">Pending Shipments</ng-option>\n      <ng-option [value]=\"3\">Awaiting Payment</ng-option>\n      <ng-option [value]=\"1\">Awaiting Packages</ng-option>\n      <ng-option [value]=\"2\">Packages Received</ng-option>\n      <ng-option [value]=\"5\">Shipment History</ng-option>\n    </ng-select>\n    <section id=\"innerpanel\" class=\"sortable-wrap\">\n      <div class=\"topicons\">\n        <span class=\"sort-opt-wrap\">\n         <a href=\"javascript:;\" (click)=\"reverseShipments($event)\">\n           <span class=\"icon-sort\"></span>\n         </a>\n       </span>\n      </div>\n      <div class=\"search-wrap\">\n        <input [(ngModel)]=\"searchText\" type=\"text\" (keyup)=\"searchTransactionList(searchText)\" placeholder=\"Search\"/>\n        <span class=\"icon-search\"></span>\n      </div>\n    </section>\n    <section class=\"shipments-container\">\n      <ul class=\"shipments\">\n        <li *ngIf=\"selectedOption != '999'\" style=\"background-color: #f3f3f3\">\n          <input type=\"checkbox\" (change)=\"selectAllTransactions($event)\" value=\"\" [(ngModel)]=\"bulkselect\" class=\"itemcheck\">\n          <label for=\"myid\" class=\"shipmentdetails\">\n            <div class=\"row\">\n              <div class=\"col-md-10\">\n                <div class=\"actionbuttons\" style=\"margin-top: 8px\">\n                  <a *ngIf=\"selectedOption == '2'\" href=\"{{apiurl}}/new-shipment\" class=\"action-button\">Ship Now</a>\n                  <a *ngIf=\"selectedOption == '3'\" href=\"javascript;\" (click)=\"processMakePayment($event)\" class=\"action-button\">Make Payment</a>\n                  <a href=\"javascript;\" (click)=\"requestShipmentPhoto($event)\" class=\"action-button\">Request Photo</a>\n                </div>\n              </div>\n            </div>\n          </label>\n        </li>\n      </ul>\n    </section>\n    <section class=\"shipments-container\">\n      <ul class=\"shipments\">\n        <li *ngFor=\"let shipmentobj of filteredShipments\">\n          <input *ngIf=\"selectedOption != '999'\" (change)=\"onTransactionSelection($event, shipmentobj)\" type=\"checkbox\" value=\"\" [(ngModel)]=\"shipmentobj.checked\" id=\"myid\" class=\"itemcheck\">\n          <label for=\"myid\" class=\"shipmentdetails\">\n            <div class=\"row\">\n              <div class=\"col-md-8\" *ngIf=\"shipmentobj?.Transaction?.type_id == '4'\">\n                <div class=\"cocode\">GP {{cocode}} {{shipmentobj?.Shipment[0].Package.in_tracking ? shipmentobj?.Shipment[0].Package.in_tracking : shipmentobj?.Transaction.id.toString() | uppercase}}</div>\n                <div class=\"receipt\">Receipt Date  {{shipmentobj?.Transaction.date}}</div>\n              </div>\n              <div class=\"col-md-8\" *ngIf=\"shipmentobj?.Transaction?.type_id != '4'\">\n                <div class=\"cocode\">NS# {{cocode}} #{{shipmentobj?.Transaction.id}}</div>\n                <div class=\"receipt\">Receipt Date  {{shipmentobj?.Transaction.date}}</div>\n              </div>\n              <div class=\"col-md-4\">\n                <a class=\"shipment-details\" href=\"javascript:;\" (click)=\"shipmentsDetails($event, shipmentobj)\">\n                  Details\n                </a>\n              </div>\n            </div>\n          </label>\n        </li>\n      </ul>\n    </section>\n  </div>\n  <div class=\"col-md-7\">\n    <div class=\"all-tabs\">\n      <section *ngIf=\"!transactionDetailsShown\" class=\"sortable-wrap\">\n        <div *ngIf=\"selectedOption !== 3 && selectedOption !== 2\" class=\"container-fluid\">\n          <h3 class=\"toptitle\">My Shipments</h3>\n          <section id=\"actionboxes\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n                <div class=\"actionbox\" rel=\"awaitingpaymaction\" (click)=\"showOptionShipments(3)\">\n                  <div class=\"icon-wrap\">\n                    <span class=\"icon-awaitingpaym\"></span>\n                  </div>\n                  <h4 class=\"title\">Awaiting Payment</h4>\n                  <h4 class=\"notification\">HMHShip is awaiting payment to<br/> ship <span class=\"primary\"> {{stats.count_awaiting_payment}}</span>\n                    {{ stats.count_awaiting_payment > 1 ? 'packages' : 'package'}}</h4>\n                  <h5 style=\"color: #0D3349\">Pay Now</h5>\n                </div>\n              </div>\n\n              <div class=\"col-md-6\">\n                <div class=\"actionbox\" rel=\"packreceived\" (click)=\"showOptionShipments(2)\">\n                  <div class=\"icon-wrap\">\n                    <span class=\"icon-packreceived\"></span>\n                  </div>\n                  <h4 class=\"title\">Package Received</h4>\n                  <h4 class=\"notification\">You have<span class=\"primary\"> {{stats.count_received_package}} </span> {{ stats.count_received_package > 1 ? 'packages' : 'package'}} that<br/> have been received at HMHShip\n                  </h4>\n                  <h5 style=\"color: #0D3349\">Ship Now</h5>\n                </div>\n              </div>\n            </div>\n            <div class=\"row\">\n\n              <div class=\"col-md-4\">\n                <div class=\"actionbox\" rel=\"waitpackaction\" (click)=\"showOptionShipments(1)\">\n                  <div class=\"icon-wrap\">\n                    <span class=\"icon-waitpack\"></span>\n                  </div>\n                  <h4 class=\"title\">Awaiting Packages</h4>\n                  <h4 class=\"notification\">HMHShip is waiting to <br/>receive <span class=\"primary\">{{stats.count_waiting_package}}</span> {{ stats.count_waiting_package > 1 ? 'packages' : 'package'}}\n                     </h4>\n                  <!--<h5><a style=\"cursor:pointer;\" (click)=\"showOptionShipments(1)\">Show me more</a></h5>-->\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"actionbox\" rel=\"paymreceived\" (click)=\"showOptionShipments(4)\">\n                  <div class=\"icon-wrap\">\n                    <span class=\"icon-paymreceived\"></span>\n                  </div>\n                  <h4 class=\"title\">Pending Shipments</h4>\n                  <h4 class=\"notification\">HMHShip has <span class=\"primary\">{{stats.count_received_payment}}</span><br> {{ stats.count_received_payment > 1 ? 'packages' : 'package'}}\n                     pending <br> shipment</h4>\n                  <!--<h5><a style=\"cursor:pointer;\" (click)=\"showOptionShipments(4)\">Show me more</a></h5>-->\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"actionbox\" rel=\"packsentaction\" (click)=\"showOptionShipments(5)\">\n                  <div class=\"icon-wrap\">\n                    <span class=\"icon-packsent\"></span>\n                  </div>\n                  <h4 class=\"title\">Shipment History</h4>\n                  <h4 class=\"notification\">HMHShip has shipped <br/> <span class=\"primary\">{{stats.count_package_sent}}</span> {{ stats.count_package_sent > 1 ? 'packages' : 'package'}}\n                  </h4>\n                  <!--<h5><a style=\"cursor:pointer;\" (click)=\"showOptionShipments(5)\">Show me more</a></h5>-->\n                </div>\n              </div>\n            </div>\n          </section>\n        </div>\n        <div *ngIf=\"selectedOption == '3'\" class=\"container-fluid\">\n          <h3 class=\"toptitle\">Pay Balance Due</h3>\n          <section id=\"actionboxes\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n                Please pay the shipping balance of ${{balanceDue.toFixed(2)}} and we'll have your package shipped to you right away!\n                <div class=\"actionbuttons\">\n                  <a  href=\"javascript;\" (click)=\"goToDashboard()\" class=\"action-button\">Go Back</a>\n                  <a href=\"javascript:;\" (click)=\"processMakePayment($event)\" *ngIf=\"balanceDue\" class=\"action-button\">Pay Now</a>\n\n                </div>\n              </div>\n            </div>\n          </section>\n        </div>\n\n        <div *ngIf=\"selectedOption == '2'\" class=\"container-fluid\">\n          <h3 class=\"toptitle\">Ship your packages</h3>\n          <section id=\"actionboxes\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n               Ship your packages right away!\n                <div class=\"actionbuttons\">\n                  <a  href=\"javascript;\" (click)=\"goToDashboard()\" class=\"action-button\">Go Back</a>\n                  <a *ngIf=\"selectedOption == '2'\" href=\"{{apiurl}}/new-shipment\" class=\"action-button\">Ship Now</a>\n\n                </div>\n              </div>\n            </div>\n          </section>\n        </div>\n      </section>\n\n\n      <div id=\"innertabs\" *ngIf=\"transactionDetailsShown\">\n        <div class=\"row\">\n          <div class=\"col-md-6\">\n            <h3 class=\"toptitle-detail-header\">\n              NS#{{cocode}} #{{selectedTransaction?.Shipment[0]?.Package.in_tracking ? selectedTransaction?.Shipment[0]?.Package.in_tracking : selectedTransaction?.Transaction.id.toString() | uppercase}}\n            </h3>\n          </div>\n          <div class=\"col-md-6\">\n            <div class=\"actionbuttons\">\n              <a  href=\"javascript;\" (click)=\"goBack()\" class=\"action-button\">Go Back</a>\n              <a *ngIf=\"selectedTransaction?.Transaction?.status == '2'\" href=\"{{apiurl}}/new-shipment\" class=\"action-button\">Ship Now</a>\n              <a *ngIf=\"selectedTransaction?.Transaction?.status == '3'\" href=\"{{apiurl}}/Users/payment\" class=\"action-button\">Pay Now</a>\n              <a href=\"javascript;\"  class=\"action-button\">Request Photo</a>\n            </div>\n          </div>\n        </div>\n\n        <div class=\"innersection\">\n          <h4 class=\"innersection-title\">Personal Information</h4>\n          <div class=\"row\">\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Name</h4>\n                <span >\n                        {{ (selectedTransaction?.AddressBilling[0]?.firstname || 'empty') }}\n                    </span>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.lastname || 'empty') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Address 1</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.address1 || 'empty') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>City</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.city || 'empty') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>State</h4>\n\n                <span >\n                        {{ (selectedTransaction?.AddressBilling[0]?.adm_division || 'empty') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Postcode</h4>\n\n                <span >\n                        {{ (selectedTransaction?.AddressBilling[0]?.postal_code || 'empty') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Country</h4>\n\n                <span>\n                {{ countries[selectedTransaction?.AddressBilling[0]?.country_id] }}\n              </span>\n              </div>\n            </div>\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Contact Information</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.email || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.phone || 'empty') }}\n                    </span>\n\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Address 2</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressBilling[0]?.address2 || 'None') }}\n                    </span>\n              </div>\n              <div class=\"clearfix\">&nbsp;\n                <div>\n                  <h4 *ngIf=\"selectedTransaction?.Photos.length\">Requested Photos</h4>\n                  <ul class=\"requested-photos\">\n                    <li *ngFor=\"let photo of selectedTransaction?.Photos\"><img style=\"max-width: 120px; max-height: 100px;\" src=\"/{{photo.requested_photo_url}}\"></li>\n                  </ul>\n                </div>\n              </div>\n            </div>\n            <!-- /.col-md-6 -->\n          </div>\n          <!-- /.row -->\n          <hr>\n          <h3 class=\"clearfix\">Shipping</h3>\n          <div class=\"row\">\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Name</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.firstname || 'empty') }}\n                    </span>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.lastname || 'empty') }}\n                    </span>\n              </div>\n\n              <div class=\"block hasPadding\">\n                <h4>Address 1</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.address1 || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.city || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.adm_division || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.postal_code || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n                <span>\n                  {{ countries[selectedTransaction?.AddressShipping[0]?.country_id] }}\n              </span>\n\n              </div>\n            </div>\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Contact Information</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.email || 'empty') }}\n                    </span>\n                <div class=\"clearfix\">&nbsp;</div>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.phone || 'empty') }}\n                    </span>\n\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Address 2</h4>\n\n                <span>\n                        {{ (selectedTransaction?.AddressShipping[0]?.address2 || 'None') }}\n                    </span>\n              </div>\n            </div>\n          </div>\n        </div>\n        <!-- /.innersection -->\n        <div class=\"innersection\">\n          <h4 class=\"innersection-title\">Additional Options</h4>\n          <div class=\"row\">\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Need these packages expedited</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.expedited && \"Yes\" || 'No') }}\n                    </span>\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Photo Requested</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.request_photo && \"Yes\" || 'No') }}\n                    </span>\n              </div>\n            </div>\n            <div class=\"col-md-6\">\n              <div class=\"block hasPadding\">\n                <h4>Insurance added</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.insured && \"Yes\" || 'No') }}\n                    </span>\n\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Repackage</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.repackaged && \"Yes\" || 'No') }}\n                    </span>\n\n              </div>\n              <div class=\"block hasPadding\">\n                <h4>Special Instructions</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.special_instructions || 'empty') }}\n                    </span>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <!-- /.innersection -->\n        <div class=\"innersection\">\n          <h4 class=\"innersection-title\">Package Details</h4>\n          <!-- Start Packages -->\n          <div *ngFor=\"let shipment of selectedTransaction?.Shipment; let $index = index\">\n            <div class=\"block hasPadding\">\n              <h3>Package {{$index + 1}}</h3>\n              <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <h4>Gross Value</h4>\n                  <p></p>\n                </div>\n                <div class=\"col-md-4\">\n                  <h4>Estimate Gross Weight</h4>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.weight_kg ? selectedTransaction?.Shipment[$index].Package.weight_kg : selectedTransaction?.Shipment[$index].Package.weight_kg) }}\n                        </span>\n\n                  <span>\n                  {{ selectedTransaction?.Shipment[$index].Package.weight_kg? 'kg' : 'lb' }}\n                </span>\n\n                </div>\n\n                <div class=\"col-md-4\">\n                  <h4>Gross Dimensions</h4>\n                  <label>Width</label>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.width_in ? selectedTransaction?.Shipment[$index].Package.width_in : selectedTransaction?.Shipment[$index].Package.width_cm) }}\n                        </span>\n                  <span>\n                  {{ selectedTransaction?.Shipment[$index].Package.width_in ? 'in': 'cm' }}\n                </span>\n                  <div class=\"clearfix\"></div>\n                  <label>Length</label>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.length_in ? selectedTransaction?.Shipment[$index].Package.length_in : selectedTransaction?.Shipment[$index].Package.length_cm) }}\n                        </span>\n                  <span>\n                  {{ selectedTransaction?.Shipment[$index].Package.length_in ? 'in' : 'cm' }}\n                </span>\n                  <div class=\"clearfix\"></div>\n                  <label>Height</label>\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.height_in ? selectedTransaction?.Shipment[$index].Package.height_in : selectedTransaction?.Shipment[$index].Package.height_cm) }}\n                        </span>\n                  <span>\n                  {{ selectedTransaction?.Shipment[$index].Package.height_in ? 'in' : 'cm' }}\n                </span>\n\n                </div>\n                <div class=\"col-md-4\" *ngIf=\"selectedTransaction?.Shipment[$index].Package.consolidated_packages\">\n                  <h4>Consolidated Packages</h4>\n                  {{ selectedTransaction?.Shipment[$index].Package.consolidated_packages }}\n                </div>\n              </div>\n            </div>\n            <div class=\"row\">\n              <div class=\"col-md-4\">\n                <div class=\"block hasPadding\">\n                  <h4>Inbound Tracking Number</h4>\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.in_tracking || 'Not set') }}\n                        </span>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"block hasPadding\">\n                  <h4>Inbound Carrier</h4>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.carrier  || 'Not set') }}\n                        </span>\n                </div>\n              </div>\n\n            </div>\n            <div class=\"row\">\n              <div class=\"col-md-4\">\n                <div class=\"block hasPadding\">\n                  <h4>Outbound Tracking Number</h4>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.out_tracking  || 'Not set') }}\n                        </span>\n                </div>\n              </div>\n              <div class=\"col-md-4\">\n                <div class=\"block hasPadding\">\n                  <h4>Outbound Carrier</h4>\n\n                  <span>\n                            {{ (selectedTransaction?.Shipment[$index].Package.out_carrier  || 'Not set') }}\n                        </span>\n                </div>\n              </div>\n            </div>\n            <h3>Item Details</h3>\n\n            <!-- Start Items -->\n            <div *ngFor=\"let item of selectedTransaction?.Shipment[$index].Package.Item\">\n              <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Description</h4>\n                    <span>\n                    {{ (item.description || 'Not set') }}\n                  </span>\n                  </div>\n                </div>\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Weight</h4>\n\n                    <span>\n\t\t\t\t\t\t{{ (item.weight_kg || 'Not set') }}\n\t\t\t\t\t\t\t</span>\n\n                    <span>\n\t\t\t\t\t\t{{ (item.weight_lb || 'Not set') }}\n\t\t\t\t\t\t\t</span>\n                    <span>\n                    {{ item.weight_unit || 'Not set' }}\n                  </span>\n\n                  </div>\n                </div>\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Quantity</h4>\n\n                    <span>\n                                {{ (item.quantity || 'Not set') }}\n                            </span>\n\n                  </div>\n                </div>\n              </div>\n              <div class=\"row\">\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Country of origin</h4>\n                    <span>\n                    <!--{{ showStatus(item.country_id, countries) }}-->\n                  </span>\n                  </div>\n                </div>\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Price</h4>\n\n                    <span>\n                                {{ ((item.value  || '0') | currency) }}\n                            </span>\n                  </div>\n                </div>\n                <div class=\"col-md-4\">\n                  <div class=\"block hasPadding\">\n                    <h4>Type of Content</h4>\n                    <span>\n                    <!--{{ showStatus(item.type_id, item_types) }}-->\n                  </span>\n                  </div>\n                </div>\n              </div>\n\n            </div>\n            <!-- End Items -->\n            <hr>\n            <button type=\"button\" class=\"hidden\">Add Package</button>\n          </div>\n          <!-- End Packages -->\n        </div>\n\n        <div class=\"innersection\">\n          <h4 class=\"innersection-title\">Shipment Rates</h4>\n          <div class=\"row\">\n            <div class=\"col-md-12 shippingRateMessage\" style=\"color: #FF0000\"></div>\n          </div>\n          <div class=\"row\">\n            <div class=\"col-md-6\" *ngFor=\"let shipment of selectedTransaction?.Shipment; let $index = index\">\n              <div class=\"block hasPadding\">\n                <h4>Package {{$index + 1}}</h4>\n                <p>{{shipment.Package.easypost_rate_description}}</p>\n\n                <p>Quoted Rate: ${{shipment.Package.easypost_rate_price }}</p>\n                <!--<p>EasyPost Rate: {{(shipment.Package.easypost_rate_price - (shipment.Package.easypost_rate_price * 0.15)).toFixed(2)}}</p>-->\n\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <!-- Payment options -->\n        <div class=\"innersection\">\n          <h4 class=\"innersection-title\">Payment Options</h4>\n          <div class=\"row\">\n            <div class=\"col-md-4\">\n              <div class=\"block hasPadding\">\n                <h4>Shipping Charges Authorized</h4>\n\n                <span>\n                        {{ (selectedTransaction.Transaction?.charge_now?'Yes':'No' ) }}\n                    </span>\n\n              </div>\n            </div>\n\n          </div>\n        </div>\n      </div><!-- /#innertabs -->\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var AppComponent = /** @class */ (function () {
    function AppComponent(httpclient) {
        this.httpclient = httpclient;
        this.title = 'dashboardapp';
        this.searchText = "";
        this.balanceDue = 0;
        this.apiurl = window.location.protocol + '//' + window.location.hostname;
        this.selectedOption = 999;
        this.cocode = '';
        this.shipments = [];
        this.filteredShipments = [];
        this.stats = {};
        this.countries = [];
        this.transactionDetailsShown = false;
        this.selectedTransaction = {};
        this.checkedTransactions = [];
        this.checkedShipments = [];
        this.bulkselect = false;
        //constructor
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpclient.post(this.apiurl + '/Users/myaccount', { params: { 'ajax_only': '1' } }).subscribe(function (data) {
            //console.log(data)
            _this.stats = data;
            _this.countries = data.countries;
            _this.cocode = data.co_code;
            _this.shipments = data.shipments;
            _this.shipments.map(function (item) { return item.checked = false; });
            _this.filteredShipments = Array.from(data.shipments);
            console.log(_this.filteredShipments);
        });
    };
    AppComponent.prototype.reverseShipments = function ($event) {
        this.filteredShipments.reverse();
        //return false;
    };
    AppComponent.prototype.showOptionShipments = function (selectedOption) {
        console.log(selectedOption);
        var bulkselect = this.bulkselect = false;
        var balanceDue = 0;
        var temp = [];
        var temp_trans = [];
        var temp_ships = [];
        this.selectedOption = selectedOption;
        //Filtering items to find out the selectedOption items
        this.shipments.forEach(function (item) {
            item.checked = bulkselect;
            if (item.Transaction.status == selectedOption && selectedOption != 999) {
                temp.push(item);
            }
            else if (selectedOption == 999) {
                temp.push(item);
            }
        });
        //Filtering items for option 'awaiting payment packages
        if (selectedOption == 3 || selectedOption == 2) {
            this.selectedOption = selectedOption;
            temp.forEach(function (titem) {
                titem.checked = true;
                temp_trans.push(titem.Transaction.id);
                temp_ships.push(titem);
                titem.Shipment.forEach(function (shp) {
                    balanceDue += (!shp.balance_due) ? 0 : parseFloat(shp.balance_due);
                });
            });
            this.checkedTransactions = temp_trans;
            this.checkedShipments = temp_ships;
        }
        //console.log('baclance due', balanceDue, temp)
        this.balanceDue = balanceDue;
        this.filteredShipments = temp;
        this.transactionDetailsShown = false;
    };
    AppComponent.prototype.shipmentsDetails = function ($event, transaction) {
        console.log(transaction);
        this.selectedTransaction = transaction;
        this.transactionDetailsShown = true;
    };
    AppComponent.prototype.goBack = function () {
        this.selectedTransaction = null;
        this.transactionDetailsShown = false;
        return false;
    };
    AppComponent.prototype.goToDashboard = function () {
        this.selectedTransaction = null;
        this.transactionDetailsShown = false;
        this.selectedOption = 999;
        var temp = [];
        this.shipments.forEach(function (item) {
            temp.push(item);
        });
        this.filteredShipments = temp;
        return false;
    };
    AppComponent.prototype.searchTransactionList = function ($event) {
        if (this.searchText.length < 2) {
            this.filteredShipments = Array.from(this.shipments);
            return false;
        }
        this.selectedOption = 999;
        var temp = [];
        this.shipments.forEach(function (item) {
            if ((item.Transaction.id.toString().lastIndexOf($event) >= 0 ||
                item.Transaction.date.toString().lastIndexOf($event) >= 0)) {
                temp.push(item);
            }
        });
        this.filteredShipments = temp;
    };
    AppComponent.prototype.onTransactionSelection = function ($event, Shipment) {
        var balanceDue = 0;
        if ($event.target.checked) {
            this.checkedTransactions.push(Shipment.Transaction.id);
            this.checkedShipments.push(Shipment);
        }
        else {
            this.checkedTransactions.splice(this.checkedTransactions.indexOf(Shipment.Transaction.id), 1);
            this.checkedShipments.splice(this.checkedShipments.indexOf(Shipment), 1);
        }
        if (this.checkedShipments.length) {
            this.checkedShipments.forEach(function (titem) {
                titem.Shipment.forEach(function (shp) {
                    balanceDue += parseFloat(shp.balance_due);
                });
            });
        }
        this.balanceDue = balanceDue;
    };
    AppComponent.prototype.selectAllTransactions = function ($event) {
        var _this = this;
        this.checkedTransactions.splice(0);
        this.checkedShipments.splice(0);
        this.filteredShipments.map(function (item) {
            item.checked = _this.bulkselect;
            if (_this.bulkselect) {
                _this.checkedTransactions.push(item.Transaction.id);
                _this.checkedShipments.push(item);
            }
            else {
                _this.checkedTransactions.splice(_this.checkedTransactions.indexOf(item.Transaction.id), 1);
                _this.checkedShipments.splice(_this.checkedTransactions.indexOf(item), 1);
            }
        });
        console.log($event, this.checkedTransactions, this.checkedShipments);
    };
    AppComponent.prototype.requestShipmentPhoto = function ($event) {
        if (this.checkedTransactions.length) {
            var ids = this.checkedTransactions.join(',');
            this.httpclient.post(this.apiurl + '/Users/RequestPhoto', { params: { 'ajax_only': '1', 'ids': ids } }).subscribe(function (data) {
                if (data.ok) {
                    alert('Got it, We’ll get a photo sent to you via email. You can also view the photo here in your account.');
                }
                else {
                    alert(data.message);
                }
            });
        }
        else {
            alert('Please select at least one');
            //window.location.href = this.apiurl + '/Users/payment';
        }
        return false;
    };
    AppComponent.prototype.processMakePayment = function ($event) {
        if (this.checkedTransactions.length) {
            var ids = this.checkedTransactions.join(',');
            window.location.href = this.apiurl + '/Users/payment/' + ids;
        }
        else {
            window.location.href = this.apiurl + '/Users/payment';
        }
        return false;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-select/ng-select */ "./node_modules/@ng-select/ng-select/fesm5/ng-select.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _ng_select_ng_select__WEBPACK_IMPORTED_MODULE_3__["NgSelectModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/manzurulhaque/Desktop/Projects/OwnProjects/hmhship.com/app/webroot/dashboardapp/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map