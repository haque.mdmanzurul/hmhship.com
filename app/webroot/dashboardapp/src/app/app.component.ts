import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
    title = 'dashboardapp';
    searchText: any = "";
    balanceDue: Number = 0;
    apiurl = window.location.protocol + '//' + window.location.hostname;
    selectedOption: number = 999;
    cocode: string = '';
    shipments: any = [];
    filteredShipments: any = [];

    stats: any = {}
    countries: any = []

    transactionDetailsShown: boolean = false;
    selectedTransaction: any = {};
    checkedTransactions: any = []
    checkedShipments: any = []
    bulkselect: any = false;

    constructor(private httpclient: HttpClient) {
        //constructor
    }

    ngOnInit() {

        this.httpclient.post(this.apiurl + '/Users/myaccount', { params: {'ajax_only' : '1'} }).subscribe((data : any) => {
            //console.log(data)
            this.stats = data;
            this.countries = data.countries;
            this.cocode = data.co_code;
            this.shipments = data.shipments;
            this.shipments.map(item => item.checked = false);
            this.filteredShipments = Array.from(data.shipments);
            console.log(this.filteredShipments)
        });
    }

    reverseShipments($event) {
        this.filteredShipments.reverse();
        //return false;
    }

    showOptionShipments(selectedOption) {
        console.log(selectedOption);
        let bulkselect = this.bulkselect = false;
        let balanceDue = 0;
        let temp: any = [];
        let temp_trans: any = []
        let temp_ships: any = []
        this.selectedOption = selectedOption;

        //Filtering items to find out the selectedOption items
        this.shipments.forEach(function(item) {
           item.checked = bulkselect;
           if (item.Transaction.status == selectedOption && selectedOption != 999) {
               temp.push(item);
           }
           else if (selectedOption == 999) {
               temp.push(item);
           }
        });

        //Filtering items for option 'awaiting payment packages
        if (selectedOption == 3 || selectedOption == 2) {
            this.selectedOption = selectedOption;

            temp.forEach(function(titem) {
                titem.checked = true;
                temp_trans.push(titem.Transaction.id);
                temp_ships.push(titem);
                titem.Shipment.forEach(function(shp) {
                    balanceDue += (!shp.balance_due) ? 0 : parseFloat(shp.balance_due);
                });
            });

            this.checkedTransactions = temp_trans;
            this.checkedShipments = temp_ships;
        }

        //console.log('baclance due', balanceDue, temp)
        this.balanceDue = balanceDue;
        this.filteredShipments = temp;
        this.transactionDetailsShown = false;
    }

    shipmentsDetails($event, transaction) {
        console.log(transaction);
        this.selectedTransaction = transaction;
        this.transactionDetailsShown = true;
    }

    goBack(){
        this.selectedTransaction = null;
        this.transactionDetailsShown = false;
        return false;
    }

    goToDashboard(){
        this.selectedTransaction = null;
        this.transactionDetailsShown = false;
        this.selectedOption = 999;
        let temp: any = [];
        this.shipments.forEach(function(item) {
            temp.push(item);
        });

        this.filteredShipments = temp;
        return false;
    }

    searchTransactionList($event) {
        if(this.searchText.length < 2){
            this.filteredShipments = Array.from(this.shipments);
            return false;
        }
        this.selectedOption = 999;
        let temp: any = [];

        this.shipments.forEach(function(item: any) {
            if (
                (item.Transaction.id.toString().lastIndexOf($event) >= 0 ||
                    item.Transaction.date.toString().lastIndexOf($event) >= 0))  {
                temp.push(item);
            }

        });

        this.filteredShipments = temp;

    }

    onTransactionSelection($event, Shipment) {
        let balanceDue: any = 0;
        if($event.target.checked) {
            this.checkedTransactions.push(Shipment.Transaction.id);
            this.checkedShipments.push(Shipment);
        }
        else {
            this.checkedTransactions.splice(this.checkedTransactions.indexOf(Shipment.Transaction.id), 1);
            this.checkedShipments.splice(this.checkedShipments.indexOf(Shipment), 1);
        }

        if (this.checkedShipments.length) {
            this.checkedShipments.forEach(function(titem) {
                titem.Shipment.forEach(function(shp) {
                    balanceDue += parseFloat(shp.balance_due);
                });
            });
        }


        this.balanceDue = balanceDue;

    }

    selectAllTransactions($event) {
        this.checkedTransactions.splice(0);
        this.checkedShipments.splice(0);
        this.filteredShipments.map(item => {
            item.checked = this.bulkselect;
            if(this.bulkselect) {
                this.checkedTransactions.push(item.Transaction.id);
                this.checkedShipments.push(item);
            }
            else{
                this.checkedTransactions.splice(this.checkedTransactions.indexOf(item.Transaction.id), 1);
                this.checkedShipments.splice(this.checkedTransactions.indexOf(item), 1);
            }
        });
        console.log($event, this.checkedTransactions, this.checkedShipments)
    }

    requestShipmentPhoto($event){
        if (this.checkedTransactions.length) {
            let ids = this.checkedTransactions.join(',')
            this.httpclient.post(this.apiurl + '/Users/RequestPhoto', { params: {'ajax_only': '1', 'ids': ids} }).subscribe((data : any) => {

                if(data.ok) {
                    alert('Got it, We’ll get a photo sent to you via email. You can also view the photo here in your account.');
                }
                else {
                    alert(data.message);
                }
            });
        }
        else {
            alert('Please select at least one');
            //window.location.href = this.apiurl + '/Users/payment';
        }

        return false;
    }

    processMakePayment($event) {
        if (this.checkedTransactions.length) {
            let ids = this.checkedTransactions.join(',')
            window.location.href = this.apiurl + '/Users/payment/' + ids;
        }
        else {
            window.location.href = this.apiurl + '/Users/payment';
        }
        return false;
    }
}
