<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'js' => array('//FRONTEND/_init/bower_components/angular/angular.js',
         '//FRONTEND/_init/bower_components/angular-route/angular-route.js',
         '//FRONTEND/_init/bower_components/angular-resource/angular-resource.js',
         '//FRONTEND/_init/bower_components/angular-sanitize/angular-sanitize.js',
         '//FRONTEND/_init/bower_components/angular-animate/angular-animate.js',
         '//FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.js',
         '//FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap.js',
         '//FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
         '//FRONTEND/_init/bower_components/ngstorage/ngStorage.js',
         '//FRONTEND/_init/bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.js',
         '//FRONTEND/_init/bower_components/ng-dialog/js/ngDialog.js',
         '//FRONTEND/js/dev/plugins/bootstrap.min.js',
         '//FRONTEND/js/modules/app.js',
         '//FRONTEND/js/modules/QuickshipSvc.js'
         ), 
    'css' => array('//css/app.min.css',
        '//css/angular-toggle-switch.min.css',
        '//css/select.min.css',
        '//css/ng-tags-input.min.css',
        '//css/ng-tags-input.bootstrap.min.css',
        '//css/ngDialog.min.css',
        '//css/ngDialog-theme-default.min.css'
        )
);