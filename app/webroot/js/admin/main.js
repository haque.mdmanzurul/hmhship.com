SmartAjax_load(BASE_URL + 'js/admin/', function () {

	// bind to all anchors in the menu and footer menu sections
	SmartAjax.bind('.menu-wrap a.ajaxify, .bottom-menu a');

	SmartAjax.setOptions({
		cache: false,
		reload: false,
		containers: [
			{selector: '#section'}
		],

		before: function () {
			$('body').addClass("panelloading");
			$loading = $("<div />");
			$loading.addClass("loading").addClass("loading1").html('<div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div>');


			SmartAjax.proceed();
			$("main").prepend($loading);
		},
		success: function (url) {
			$('#section').fadeOut(500, SmartAjax.proceed);
			$(".nav-wrap").removeClass("opened");
		},
		done: function () {
			$('#section').fadeIn(700);
			$(".menu-wrap .active").removeClass("active");
			$(".menu-wrap .ajaxActive").removeClass("ajaxActive").find('li').addClass("active");
			initFn();
			$('body').removeClass("panelloading").removeClass("innerloading").removeClass("tabloading");
		}
	});

	SmartAjax.bind('#innerpanel a,.innerlink', {
		cache: false,
		reload: false,
		containers: [
			{selector: '#innercontent'}
		],

		before: function () {
			$('body').addClass("innerloading");
			$loading = $("<div />");
			$loading.addClass("loading").addClass("loading2").html('<div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div>');
			$("#innerpanel").animate({"height": '50px'}, 500);

			SmartAjax.proceed();
			$("main #innercontent").prepend($loading);
		},
		success: function () {
			$(".panellist .active").removeClass("active");
			$(".panellist .ajaxActive").find(".new").remove();
			$(".panellist .ajaxActive").find(".icon-message").removeClass("icon-message").addClass("icon-messageread");
			$(".panellist .ajaxActive").removeClass("ajaxActive").parent('li').addClass("active");
			$('#innercontent').fadeOut(500, SmartAjax.proceed);
		},
		done: function () {

			$('#innercontent').fadeIn(700);
			initFn();
			$('body').removeClass("innerloading").removeClass("tabloading");
		}
	});

	SmartAjax.bind('.tabs a', {
		cache: false,
		reload: false,
		containers: [
			{selector: '#innertabs'}
		],

		before: function () {
			$('body').addClass("tabloading");
			$loading = $("<div />");
			$loading.addClass("loading").addClass("loading3").html('<div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div>');

			$("#innerpanel").animate({"height": '50px'}, 500);

			$("#innertabs").prepend($loading);
			SmartAjax.proceed();
		},
		success: function () {
			$(".tabs .active").removeClass("active");
			$(".tabs .ajaxActive").removeClass("ajaxActive").find('li').addClass("active");
			$('#innertabs').fadeOut(500, SmartAjax.proceed);
		},
		done: function () {

			$('#innertabs').fadeIn(700);
			initFn();
			$('body').removeClass("tabloading");
		}
	});
}, true);


$(document).ready(function () {
	$(document).on('click', ".tabs > li", function () {
		if (!$(this).hasClass("active")) {
			$filter = $(".filter-wrap .filter-options li.active");
			if ($filter.length > 0 && $filter.attr('rel') != 'all') {
				var filter = $(".filter-wrap .filter-options li.active").attr('rel');
			}

			$(".tabs > li.active").removeClass("active");
			$(this).addClass("active");
			if ($(this).data('type') == 'allshipment') {
				$(this).parents(".filter-wrap").find(".filter-list li:not(:visible):not('.filtered')").show();
				$(this).parents(".filter-wrap").find("li.topfiltered").removeClass("topfiltered");
			}
			else {

				var type = $(this).data("type");
				$(this).parents(".filter-wrap").find(".filter-list li:not([data-type=" + type + "])").hide().addClass("topfiltered");
				$(this).parents(".filter-wrap").find(".filter-list li[data-type=" + type + "]:not('.filtered')").each(function () {
					if ($(this).attr('rel') == filter || filter == null) {
						$(this).show();
					}
				});
				$(this).parents(".filter-wrap").find("li[data-type=" + type + "].topfiltered").removeClass("topfiltered");

			}
		}
	});
	$(document).on('click', ".filter-wrap .filter-options li", function () {
		if (!$(this).hasClass("active")) {
			if ($(this).parents('.filter-options').attr('data-type-active') != undefined && $(this).data('type') != $(this).parents('.filter-options').attr('data-type-active')) {
				var wrap = $(this).parents('.filter-wrap').find('.filter-list');
				var wrapType = $(this).parents('.filter-options').attr('data-type-active');
				var thisType = $(this).data('type');

				if ($(this).attr('rel') == 'all') {
					$(wrap).find('li[data-type=' + wrapType + ']').hide().addClass('filtered').addClass('typefiltered');
					$(wrap).find('li[data-type=' + thisType + ']').show().removeClass('filtered').removeClass('typefiltered');
				}
				else {
					var type = $(this).attr("rel");
					$(wrap).find('li[data-type=' + wrapType + ']').hide().addClass('filtered').addClass('typefiltered');
					$(wrap).find('li[data-type=' + thisType + ']+[rel=' + type + ']').show().removeClass('filtered').removeClass('typefiltered');
					$(wrap).find('li[data-type=' + thisType + ']+:not([rel=' + type + '])').hide().addClass('filtered').addClass('typefiltered');
				}
				if ($(this).hasClass("sub")) {
					var content = $(this).html();
				}
				else {
					var content = $(this).find(".top-title").html();
				}
				$(".filter-opt-wrap .filter-title").html(content);
				var type = $(this).data("type");
				$(this).parents('.filter-options').attr('data-type-active', type);

				type = "all" + type + "s";

				$(".filter-opt-wrap [class*=icon-]").first().removeClass("icon-users").removeClass("icon-alltransactions").addClass("icon-" + type);

			}
			else {
				if ($(this).attr('rel') == "all") {
					$(this).parents(".filter-wrap").find(".filter-list li:not(.topfiltered)").show().removeClass('filtered');
					$(this).parents(".filter-wrap").find("li.filtered").removeClass("filtered");
				}
				else {
					var type = $(this).attr("rel");
					$(this).parents(".filter-wrap").find(".filter-list li:not(.topfiltered):not([rel=" + type + "])").hide().addClass('filtered');
					$(this).parents(".filter-wrap").find(".filter-list li:not(.topfiltered)[rel=" + type + "]").show().removeClass('filtered');
					$(this).parents(".filter-wrap").find(".filtered[rel=" + type + "]").removeClass("filtered");
				}
			}
			$(".filter-wrap .filter-options li.active").removeClass("active");
			$(this).addClass("active");


			if ($(this).parents("#innerpanel").length > 0 && $(window).width() < 768) {
				expandeInnerPanel('resize');
			}
		}
	});

	$(document).on('keypress', '.reply-wrap textarea', function (e) {
		if (e.which == 13) {
			if (e.shiftKey) {
				e.stopPropagation();
			} else {
				e.preventDefault();
				var message = $(this).val().replace(/\r?\n/g, "<br>");
				sendmessage(message, $(this));
			}
		}
	});

	$(document).on('click', ".reply-wrap button", function () {
		var message = $(this).parents('.reply-wrap').find('textarea').val().replace(/\r?\n/g, "<br>");
		sendmessage(message, $(this).parents('.reply-wrap').find('textarea'));
	});

	$(document).on('click', "#editstatus", function () {
		if ($(this).hasClass("editing")) {
			$(this).removeClass("editing").html("Edit Status");
			$(".status-wrap").removeClass("editing");

			//send ajax request to edit status
			// $.post(....

			$(".sidenote").fadeOut();
		}
		else {
			$(this).addClass('editing').html("Save Status");
			$(".status-wrap").addClass("editing");
			$(".sidenote").fadeIn();
		}
	});

	$(document).on('click', ".status-wrap.editing .status:not('.active')", function () {
		$(".status-wrap .status.active").removeClass("active");
		$(this).addClass("active").addClass("checked");
		$(this).prevAll('.status').addClass("checked");
		$(this).nextAll('.status').removeClass("checked");


	});

	function sendmessage(message, field) {
		$(field).val("");
		$(field)[0].focus();
		var d = new Date();
		var hours = d.getHours();
		suffix = (hours >= 12) ? 'pm' : 'am';
		hours = (hours > 12) ? hours - 12 : hours;
		hours = (hours == '00') ? 12 : hours;
		var time = hours + ':' + d.getMinutes();
		var mymsg = '<div class="mymsg">' +
			'<div class="icon-wrap">' +
			'<span class="time-wrap">' + time + '</span>' +
			'</div>' +
			'<p>' + message + '</p>' +
			'</div>';

		//send ajax msg

		$(".msgs-wrap").append(mymsg).scrollTop($(".msgs-wrap")[0].scrollHeight);

	}

	$(document).on('keyup change', '#innerpanel .search-wrap input', function () {
		var val = this.value.toLowerCase();
	});
	$(document).on('keyup change', "input[name=refund-value]", function () {
		var value = parseFloat($(this).val());
		$(".requested").html((value).toFixed(2));
		var available = parseFloat($(".available").html());
		$(".remaining").html((available - value).toFixed(2));
	});
	$(".search-text-wrap").click(function () {
		$(this).parents('.search_bar').toggleClass('activated');
	});
	$(".search_bar .closebtn").click(function () {
		$(this).parents('.search_bar').removeClass("activated");
	});
	$(document).on("click", ".edit-btn", function () {
		$(this).parents('.block').addClass('editable-wrap');
		$(this).hide();
	});
	$(document).on("click", ".editable button", function () {
		var parent = $(this).parents(".block");
		$(parent).removeClass("editable-wrap");
		$(parent).find(".edit-btn").show();
		$(parent).find("fieldset > *").each(function () {
			var name = $(this).attr('name');
			$(this).parents(".editable").siblings(".non-editable").find("span[name=" + name + "]").html($(this).val());
		});

	});


	$(document).on("click", ".sort-options li", function () {
		var sortby = $(this).attr('rel');
		var parent = $(this).parents(".sortable-wrap").find(".sortable");

		//get the elements
		var arrayItems = [];
		$(parent).find("li").each(function (index) {
			var sortinfo = $(this).data(sortby);
			arrayItems.push([index, sortinfo]);
		});

		//sort them
		arrayItems.sort(function (a, b) {
			var a1 = a[1], b1 = b[1];
			if (a1 == b1) return 0;
			return a1 > b1 ? 1 : -1;
		});
		var neworder = "";

		//get the html from the elements in order
		$(arrayItems).each(function (index) {
			var id = $(this)[0];
			var el = $(parent).find("li").eq(id);
			if ($(el).parent().is("a")) {
				neworder += $(el).parent().wrap('<div>').parent().html();
				$(el).parent().unwrap();
			}
			else {
				neworder += $(el).wrap('<div>').parent().html();
				$(el).unwrap();
			}

		});

		//refresh the list
		$(parent).html(neworder);

	});

	$(".icon-menu").click(function () {
		$("aside .nav-wrap").toggleClass("opened");
	});
	$(".menu-wrap li").click(function (e) {
		if (e.target != "A") {
			$(this).parent('li').find('a').click();
		}
	});

	function expandeInnerPanel(event) {
		if (event == 'click') {
			if ($(".expander").hasClass("expanded")) {
				$("#innerpanel").animate({"height": '50px'}, 500).removeClass("expanded");
			}
			else {
				var newHeight = $("#innerpanel")[0].scrollHeight;
				$("#innerpanel").animate({"height": newHeight}, 500).addClass("expanded").css('height', 'auto');
			}
			$(".expander").toggleClass("expanded");
		}
		else {
			if ($(".expander").hasClass("expanded")) {
				var newHeight = $("#panellist").height() + 189;
				$("#innerpanel").animate({"height": newHeight}, 500).css('height', 'auto');

			}
			else {
				$("#innerpanel").animate({"height": '50px'}, 500);
			}
		}
	}

	$(document).on("click", ".expander", function () {
		expandeInnerPanel('click');
	});
	$(document).on("click", ".bottom-menu .icon-arrowdown", function () {
		$("aside").toggleClass("closed");
	});

	initFn();
});
$(document).ajaxComplete(function () {
	initFn();
});

function initFn() {

	if ($(".msgs-wrap").length > 0) {
		$(".msgs-wrap").scrollTop($(".msgs-wrap")[0].scrollHeight);
	}
	var allPanels = $('.accordion > dd').hide();


	$(".markunread").click(function () {
		$(".panellist input[type=checkbox]:checked").parent().find(".dblist-title .icon-messageread").removeClass("icon-messageread").addClass("icon-message");
		$(".panellist input[type=checkbox]:checked").parent().append("<span class='new'>N   </span>");

	});
	$(".deletemsg").click(function () {
		$(".panellist input[type=checkbox]:checked").parent('li').remove();

	});
	$(".multistatus").click(function () {


	});
	$("select[name=multistatus]").one("change", function () {
		$(this).parent().next().find('button').fadeIn();
	});


	if (!$(".dateselector").hasClass("init") && $(".dateselector").length > 0) {
		var now = moment();
		var monthago = moment().subtract(1, 'month');
		$(".dateselector").daterangepicker(
			{
				locale: {
					format: 'MMMM, Do, YYYY',
					applyLabel: 'Filter date'
				},
				startDate: monthago,
				endDate: now,
				maxDate: now

			},
			function (start, end, label) {
				$(".dateselector .datelabel").html(start.format('MMMM Do, YYYY') + ' - ' + end.format('MMMM Do, YYYY'));
			});
	}

	if ($("#innermsg").length > 0) {
		messageHeightMobile();
	}


}


//LOAD MORE
$(document).on('click', "#loadmore", function () {
	$loadmore = $(this);
	$(this).fadeOut('fast', function () {
		$("#innerpanel .sk-three-bounce").fadeIn(function () {

			//get more using ajax and return to side panel
			//$.get('page.php',function(data){

			//just as example data
			var exampleData = $(".panellist li").first()[0].outerHTML;

			$(".panellist").append(exampleData);

			$("#innerpanel .sk-three-bounce").fadeOut('fast', function () {
				$loadmore.fadeIn();
			});
			/*
			 });
			 */
		});
	});


});


//ADD NEW SHIPMENT -LOAD SUITES

$("select[name='suite-selector']").on('change', function () {
	var suite = $(this).val();
	$(this).parent('fieldset').next('a').attr('href', '?section=regcust&page=' + suite + '&tab=newshipment');
});


function messageHeightMobile() {
	if ($(window).width() < 768) {
		var top = $("#innermsg").offset().top;
		var bottom = $(".tabs").outerHeight();
		var windowH = $(window).height();
		var newheight = parseInt(windowH) - parseInt(top) - parseInt(bottom);
	}
	else {
		var newheight = '100%';
	}
	$("#innermsg").css('height', newheight);
}


$(window).resize(function () {
	if ($("#innermsg").length > 0) {
		messageHeightMobile();
	}
	if ($(window).width() < 768) {
		expandeInnerPanel('resize');
	}
});


// Lightweight Accordion 
(function ($) {
	//Hide all panels

	//Show first panel
	// $('.accordion > dd:first-of-type').show();
	//Add active class to first panel 
	// $('.accordion > dt:first-of-type').addClass('accordion-active');
	//Handle click function
	jQuery(document).on('click', '.accordion > dt', function (e) {
		//this clicked panel
		$this = $(this);
		//the target panel content
		$target = $this.next();

		//Only toggle non-displayed 
		if (!$this.hasClass('accordion-active')) {
			//slide up any open panels and remove active class
			$this.parent().children('dd').slideUp();

			//remove any active class
			jQuery('.accordion > dt').removeClass('accordion-active');
			//add active class
			$this.addClass('accordion-active');
			//slide down target panel
			$target.addClass('active').slideDown();

		}
		else {
			$this.removeClass('accordion-active');
			//slide down target panel
			$target.removeClass('active').slideUp();
		}

		return false;

	});


})(jQuery);


