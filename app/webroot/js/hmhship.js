hmhship.config = {
    steps: ""
};

hmhship.payment = {
    total_handling_fee: 0,
    
    paypal: {
        pay_handling: function () {
            $this = hmhship.payment;
            var params = {
                description: "Package handling fee",
                price: $this.total_handling_fee,
                steps: hmhship.config.steps
            };
            
            $("#paypal_pay").hide();
            $(".paypal_paying").show();
            $.post(Configure.base_url + 'quickship/step5.json', params, function (response) {
                if(response.ok) {
                    window.location = response.approval_link;
                }
                else {
                    alert("Error, please try again");
                    console.log(response);
                    $(".paypal_paying").hide();
                    $("#paypal_pay").show();
                }
            });
            
            return false;
        }
    }
};
