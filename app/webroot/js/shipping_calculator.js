hmhship.shipping_calculator = {
    initialized: false,
    
    templates: {
        heading: null,
        container: null
    },
    
    init: function () {
        var $this = hmhship.shipping_calculator;
        
        if(!$this.initialized) {
            $this.templates.heading = $("#package_rates div[data-attribute='heading']").detach();
            $this.templates.container = $("#package_rates div[data-attribute='container']").detach();
            
            $this.initialized = true;
        }
        
        $("#package_rates").empty();
    },
    
    get_button: function (idx) {
        var choose = $("<button/>").addClass("btn btn-default").text("Choose");
        
        choose.data("p", idx.p).data("r", idx.r);
        choose.click(function () {
            var package_container = ".package" + $(this).data("p") + "_rates";
            var rate_container = $("tr#" + $(this).data("r"));
            
            var service = $(".service", rate_container).text();
            var price = parseFloat($(".price span", rate_container).text());
            
            var title_selected = $(package_container + ".panel-heading .title-selected");
            title_selected.text(service + " ($" + price + ")");
            title_selected.data("price", price);
        });
        
        return $("<td/>").addClass("actions").append(choose);
    },
    
    load_rates: function(package_rates) {
        var $this = hmhship.shipping_calculator;
        var $tpl = hmhship.shipping_calculator.templates;
        hmhship.shipping_calculator.init();
        
        var pidx = 1;
	    for(var i in package_rates) {
	        var css_class = "package" + pidx + "_rates";
	        
	        // create package rates view from templates
	        var heading = $tpl.heading.clone().addClass(css_class);
	        $("*[data-attribute='package_name']", heading).attr("href", "#" + css_class).text("Package " + pidx);
	        $("#package_rates").append(heading);
	        
	        var container = $tpl.container.clone().addClass(css_class).attr("id", css_class);
	        if(pidx === 1) { container.addClass("in"); }
	        
	        $(".package_rate_list", container).empty();
	        var rates = package_rates[i];
	        for(var j in rates) {
	            var rate = rates[j];
	            
	            var tr = $("<tr/>").attr("id", j);
	            tr.append($('<td align="center"></td>')
                    .append($('<img height="30" />')
                    .attr("src", Configure.base_url + "img/carriers/" + rate.carrier.toLowerCase() + ".jpg")));
                
                tr.append($('<td class="title service">' + rate.service + '</td>'));
                
                if(rate.est_delivery_days !== "Varies by destination") {
                    rate.est_delivery_days += " days";
                }
                tr.append($('<td class="est_delivery_days">' + rate.est_delivery_days + '</td>'));
                
                tr.append($('<td class="price">EST <span>' + rate.rate + '</span></td>'));
                
                tr.append($this.get_button({ p: pidx, r: j }));
                
                $(".package_rate_list", container).append(tr);
            }
            $("#package_rates").append(container);
            
            pidx++;
        }
    }
};
