// BACKEND version of QuickshipSvc
angular.module('adminApp')
	.service('QuickshipSvc', ['$http', '$q', '$localStorage', '$rootScope',
		function ($http, $q, $localStorage, $rootScope) {

			var service = this;
			
			service.configure = null;


			/**
			 * Current step in the wizard. Defaults to 1 (Personal details)
			 * @type {number}
			 */
			service.currentStep = 3;

			/**
			 *
			 * @type {boolean}
			 */
			service.processingPayment = false;

			/**
			 * True if settings such as countries and item types were already loaded using loadSettings(), false otherwise
			 * @type {boolean}
			 */
			service.settingsLoaded = false;

			/**
			 * True if all validations are ok on Personal Detail, false otherwise. Defaults to true
			 * @type {boolean}
			 */
			service.validatesPersonalDetails = true;

			/**
			 * True if all validations are ok on Package Details, false otherwise. Defaults to true
			 * @type {boolean}
			 */
			service.validatesPackageDetails = true;

			/**
			 *
			 * @type {Array}
			 */
			service.shippingRatesSelected = [];

			/**
			 * Holds firstname and lastname for your-us-address page
			 * @type {{}}
			 */
			service.address = {};


			service.defaultOptions = {
				shippingRates: [], // Holds all shipping rates from EasyPost API for the current request
				shippingRatesSelected: [],
				billing_shipping: true,
				simpleSteps: true,
				personalDetailsCompleted: false,
				countries: null,
				itemTypes: null,
				transactionHash: null,
				payment_type: 'Paypal',
				billing: {
					first_name: '',
					last_name: '',
					phone: '',
					address2: ''
				},
				shipping: {
					address2: ''
				},
				quickship: {
					special_instructions: '',
					shipping_options: {
						cheapest: false,
						fastest: false
					},
					packagesExpedited: false,
					repackaged: false,
					packagesInsurance: false,
					consolidatePackages: false,
					amount: 1,
					packages: [
						{
							tracking_code: [],
							consolidated: [],
							weight_unit: 'lb',
							size_unit: 'in',
							items: [
								{
                                    description: 'na',
                                    quantity: 1,
                                    type_id: null,
                                    weight_unit: 'lb',
                                    price_value: null,
                                    info: ''
								}
							]
						}
					]
				}
			};


			/**
			 * Local storage to keep user data safe from browser reloads.
			 * IMPORTANT: Must be cleaned up after a successful submission on step 6.
			 */
			service.$storage = $localStorage.$default(service.defaultOptions);

			/**
			 * On 'simple steps' mode, it will update the amount of packages based on the 'How many packages' input field
			 */
			service.updateSimplePackages = function () {
				var currentPackagesNum = service.$storage.quickship.packages.length;
				var newPackagesNum = service.$storage.quickship.amount;
				var diff = newPackagesNum - currentPackagesNum;
				if (newPackagesNum < 1) {
					return false;
				}
				if (newPackagesNum < currentPackagesNum) {
					service.$storage.quickship.packages.splice(newPackagesNum, Math.abs(diff));
				} else if (diff > 0) {
					for (var i = 0; i < diff; i++) {
						service.$storage.quickship.packages.push({
							items: [
								{
									description: '',
									weight_unit: 'lb',
									price_value: null,
									info: ''
								}
							]
						});
					}
				}
			};

			
			service.loadSettings = function () {
                service.settingsLoaded = true;
                $rootScope.$broadcast('event:loaded-api');
			
			};

			/**
			 * Calls the backend API to fetch required app settings
			 * @returns {*}
			 */
			service.loadSettingsApi = function () {
				var defer = $q.defer();

				$http.post(service.configure.base_url + 'quickship/api_settings.json', null)
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 * Save personal information data after step 1. Required to calculate shipping rates and keep session data
			 * @returns {*}
			 */
			service.updatePersonalInfo = function () {
				var defer = $q.defer();
				$http.post(service.configure.base_url + 'quickship/step1.json', service.$storage)
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 *
			 * @returns {Number}
			 */
			service.addPackage = function () {
			    return service.$storage.quickship.packages.push({
			        consolidated: [],
			        weight_unit: 'lb',
			        size_unit: 'in',
			        items: [
						{
						    description: '',
						    weight_unit: 'lb',
						    price_value: null,
						    info: ''
						}
			        ]
			    });
			};

			/**
			 *
			 * @param package_index
			 */
			service.addConsolidatedPackage = function (package_index) {
				var currentItemsNum = service.$storage.quickship.packages[package_index].consolidated.length;
				var defaultData = {
					weight: null,
					unit: 'lb'
				};
				if (currentItemsNum < 3) {
					service.$storage.quickship.packages[package_index].consolidated.push(defaultData);
				} else {
					service.addPackage();
					service.$storage.quickship.packages[service.$storage.quickship.packages.length - 1].consolidated.push(defaultData);
				}
			};

			/**
			 * Remove consolidated packages on simple steps
			 */
			service.cleanupConsolidated = function () {
				angular.forEach(service.$storage.quickship.packages, function (value, index) {
					service.$storage.quickship.packages[index].consolidated = [];
				});
			};

			/**
			 *
			 */
			service.updateConsolidated = function () {
				if (service.$storage.quickship.consolidatePackages === false) {
					service.cleanupConsolidated();
				}
			};

			/**
			 * Load shipping rates using EasyPost API
			 * @returns {*}
			 */
			service.loadRates = function () {
				var defer = $q.defer();
				service.$storage.shippingRates = [];
				service.$storage.shippingRatesSelected = [];
				$http.post(service.configure.base_url + 'quickship/api_rates.json', service.$storage)
					.success(function (res) {
						if (typeof res == 'object') service.$storage.shippingRates = res;
						defer.resolve(res);
					}).
					error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			
			service.pay_handling = function () {

				var params = {
					description: "Package handling fee",
					price: service.$storage.handling_fee,
					steps: (service.simpleSteps === true) ? 'simple' : 'full',
					shipment: service.$storage
				};

				service.processingPayment = true;

				$http.post(service.configure.base_url + 'quickship/payment.json', params).success(function (res) {
					if (res.ok) {
						window.location = res.approval_link;
					} else {
						alert("Error, please try again");
						service.processingPayment = false;
					}
				}).error(function (err) {
					alert("Error, please try again");
					service.processingPayment = false;
				});
			};

			/**
			 *
			 * @param key
			 * @param selected_id
			 * @returns {boolean}
			 */
			service.isSelectedItem = function (key, selected_id) {
				if (key == selected_id) return true;
				return false;
			};

			/**
			 *
			 * @param amount
			 * @returns {*}
			 */
			service.getSimpleStepsAmountMin = function (amount) {
				if (service.$storage.simpleSteps === true) {
					return amount;
				}
				return false;
			};

			

			/**
			 *
			 * @returns {*}
			 */
			service.getShippingCharge = function () {
				if (service.$storage.simpleSteps === true) {
					return service.$storage.handling_fee;
				}
				var total = 0;
				if (service.$storage.shippingRatesSelected.length > 0) {
					angular.forEach(service.$storage.shippingRatesSelected, function (value) {
						var price = parseFloat(value.price.replace(' USD', ''));
						total += price;
					});
				}
				if (service.$storage.quickship.packagesInsurance === true) {
				    total += (total * 0.01);
				}
				return total;
			};

			/**
			 * Returns true if it has consolidated packages, false otherwise
			 * @returns {boolean}
			 */
			service.hasConsolidatedPackages = function (index) {
				var has_consolidated = false;
				if (typeof service.$storage.quickship.packages[index] !== 'undefined' &&
                  typeof service.$storage.quickship.packages[index].consolidated !== 'undefined' &&
                  service.$storage.quickship.packages[index].consolidated.length > 0) {
					has_consolidated = true;
				}
				return has_consolidated;
			};

			/**
			 * Resets everything except for personal information
			 */
			service.cleanup = function () {
				var personal_info = service.$storage.user;
				var billing = service.$storage.billing;
				var shipping = service.$storage.shipping;
				service.reset();
				service.$storage.user = personal_info;
				service.$storage.billing = billing;
				service.$storage.shipping = shipping;
			};

			/**
			 *
			 * @returns {number}
			 */
			service.calculateHandlingFee = function () {
				var amount = 0;
				if (service.$storage.simpleSteps === true) {
					amount = service.$storage.quickship.amount;
				} else {
					angular.forEach(service.$storage.quickship.packages, function (value, index) {
						amount++;
					});
				}
				return service.$storage.handling_fee * amount;
			};

			/**
			 * Converts between kilograms and pounds
			 * @param orig_weight
			 * @param orig_weight_unit
			 * @param dest_weight_unit
			 */
			service.calculateWeight = function (orig_weight, orig_weight_unit, dest_weight_unit) {
				if (!parseFloat(orig_weight)) return 0;
				orig_weight = parseFloat(orig_weight);
				if (orig_weight_unit === dest_weight_unit) return orig_weight;
				var new_amount = (dest_weight_unit === 'lb') ? orig_weight * 2.2046 : orig_weight / 2.2046;

				// Round up to 2 decimals
				new_amount = +(Math.round(new_amount + "e+" + 2) + "e-" + 2);
				return new_amount;
			};

			/**
			 *
			 */
			service.reset = function () {
				/*delete service.$storage.user;
				 delete service.$storage.quickship;
				 delete service.$storage.shippingRates;
				 delete service.$storage.shippingRatesSelected;
				 delete service.$storage.shipping;
				 delete service.$storage.billing;
				 delete service.$storage.simpleSteps;*/
				service.$storage.$reset();
				service.$storage = $localStorage.$default(service.defaultOptions);
			};

			return service;

		}]
);