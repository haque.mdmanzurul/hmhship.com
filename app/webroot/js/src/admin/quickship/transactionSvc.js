(function (angular) {
	'use strict';

	angular.module('adminApp')
		.service('TransactionSvc', ['$rootScope', '$http', '$q', '$window', function ($rootScope, $http, $q, $window) {
			var service = this;

			/**
			 *
			 * @param itemId
			 * @param data
			 * @returns {*}
			 */
			service.updateData = function (itemId, data) {
				var defer = $q.defer();

				$http.post($window.Configure.base_url + 'admin/transactions/api_update/' + itemId + '.json', data)
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 * 
			 * @param itemId
			 * @param data
			 * @returns {*}
			 */
			service.callGenerateLabel = function (itemId) {
				var defer = $q.defer();

				$http.post($window.Configure.base_url + 'admin/shipments/generate_label/' + itemId + '.json', {})
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};
			
			service.generateLabel = function (shipment, transaction) {

			    $("#btnGenerateLabel").prop("disabled", true);
			    $('.showLoad').show();

				service.callGenerateLabel(shipment.id).then(function(result) {
				   
				    if (transaction.Transaction.charge_now) {
				        $.ajax({
				            url: '/payments/reference_tran/' + transaction.Transaction.id + '/' + shipment.Package.easypost_rate_price,
				            success: function (result) {
				                if (result.indexOf("failed") > -1) {
				                    $('.showLoad').hide();
				                    $("#btnGenerateLabel").prop("disabled", false);
				                    alert("Error: " + result);
				                }
				                else {
				                    // refresh page to show label
				                    location.reload(true);
				                }
				            },
				            error: function (xhr) {
				                $('.showLoad').hide();
				                $("#btnGenerateLabel").prop("disabled", false);
				                alert("An error occured: " + xhr.status + " " + xhr.statusText);
				            }
				        });
				    } else {
				        // customer didn't authorize charges so add to Balancevar
						var easypost_rate = (shipment.Package.easypost_rate_price != 'undefined') ? parseFloat('' + shipment.Package.easypost_rate_price) : parseFloat('0');
						var due =  (shipment.balance_due != null) ? parseFloat('' + shipment.balance_due) : parseFloat('0');
						var balance_due = due + easypost_rate;
						//console.log(easypost_rate, due, balance_due, shipment.balance_due);
                        $.ajax({
                                url: '/shipments/saveBalanceDue/' + transaction.Transaction.id + '/' + balance_due,
				            success: function (result) {
				                // refresh page to show label
				                location.reload(true);
				            },
				            error: function (xhr) {
				                $('.showLoad').hide();
				                $("#btnGenerateLabel").prop("disabled", false);
				                alert("An error occured: " + xhr.status + " " + xhr.statusText);
				            }
				        });
				    }
                   
				}, function(err) {
				    console.log(err);
				    alert("Error occurred: " + err);
				});
			};

			service.postageButtonText = function (data, shipment) {
			    if (data.Transaction.charge_now)
			        return "Purchase Label for $" + (shipment.Package.easypost_rate_price / 1.13).toFixed(2) + " and Charge Customer $" + shipment.Package.easypost_rate_price;
			    else
			        return "Purchase Label for $" + (shipment.Package.easypost_rate_price / 1.13).toFixed(2) + " and Add to Balance Due $" + shipment.Package.easypost_rate_price;
			};

			return service;

		}]);
})(angular);

function chargeClicked() {
    // Admin would like to charge an Additional Amount to the Customer using the current Transaction as a Reference
    var amount = $("#txtChargeValue").val();
    
    if (!$.isNumeric(amount)) {
        alert('Please enter a number.');
        return;
    }    
    
    var id = $("#hdnTranID").val();
    $("#btnCharge").prop("disabled",true);
    $('.showLoad').show();
    $.ajax({
        url: '/payments/reference_tran/' + id + '/' + amount,
        success: function(result){
            if (result.indexOf("failed") > -1) {
                $('.showLoad').hide();
                $("#btnCharge").prop("disabled", false);
                alert("Error: " + result);
            }                
            else
                // refresh page
                location.reload(true);
        },
        error: function(xhr){
             $('.showLoad').hide();
             $("#btnCharge").prop("disabled",false);
            alert("An error occured: " + xhr.status + " " + xhr.statusText);
        }
    });   
}

function refundClicked() {
    // Admin would like to charge an Additional Amount to the Customer using the current Transaction as a Reference
    var amount = $("#txtRefundValue").val();
    
    if (!$.isNumeric(amount)) {
        alert('Please enter a number.');
        return;
    }
    
    var id = $("#hdnTranID").val();
    $("#btnRefund").prop("disabled",true);
    $('#divRefundSpinner').show();
    $.ajax({
        url: '/payments/refund_tran/' + id + '/' + amount,
        success: function(result){
            
            // refresh page
            location.reload(true);
        },
        error: function(xhr){
             $('#divRefundSpinner').hide();
             $("#btnRefund").prop("disabled",false);
            alert("An error occured: " + xhr.status + " " + xhr.statusText);
        }
    });   
}

function saveBalanceDue() {

    // Admin would like to save Balance Due for the current Tran/Shipment
    var amount = $("#txtBalanceDue").val();

    if (!$.isNumeric(amount)) {
        alert('Please enter a number.');
        return;
    }

    var id = $("#hdnTranID").val();
    $("#btnSaveBalanceDue").prop("disabled",true);
    $('#divBalanceDueSpinner').show();
    $.ajax({
        url: '/shipments/saveBalanceDue/' + id + '/' + amount,
        success: function(result){
        	console.log('Save Balance Due response', result);
            // refresh page
            location.reload(true);
        },
        error: function(xhr){
            $('#divBalanceDueSpinner').hide();
            $("#btnSaveBalanceDue").prop("disabled",false);
            alert("An error occured: " + xhr.status + " " + xhr.statusText);
        }
    });
}


function printDetails() {
    
    if (window.location.toString().indexOf("overview") < 0) {
        // switch to Overview
        window.location = "#/quickship/overview/" + $("#hdnTranID").val();
        setTimeout(function(){ printDetails(); }, 2000);
        return;
    }
    
    var mywindow = window.open('', 'Details', 'height=400,width=600');
    mywindow.document.write('<html><head><title>Transaction #' + $("#hdnTranID").val() + '</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write($("#innertabs").html());
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

}

