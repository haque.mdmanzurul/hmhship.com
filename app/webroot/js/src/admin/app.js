var app = angular.module('adminApp', [
	'ngRoute',
	'ngResource',
	'ngStorage',
	'ngAnimate',
	'ui.bootstrap',
	'toggle-switch',
	'ngSanitize',
	'ngTagsInput',
	'ngDialog',
	'filters',
	'xeditable'
]);
angular.module('adminApp')
	.config(
	['$routeProvider', '$httpProvider', '$anchorScrollProvider', 'tagsInputConfigProvider',
		function ($routeProvider, $httpProvider, $anchorScrollProvider, tagsInputConfigProvider) {
            $routeProvider
                .when('/quickship', {
                    title: 'Quickship ',
                    templateUrl: window.Configure.base_url + 'js/src/admin/quickship/index.html',
                    controller: 'QuickshipCtrl'
                })
				.when('/quickship/:itemAction/:itemId', {
                title: 'Quickship ',
                templateUrl: window.Configure.base_url + 'js/src/admin/quickship/quickship.html',
                controller: 'QuickshipOverviewCtrl'
            }).when('/quickship_registered', {
                title: 'New Shipments ',
                templateUrl: window.Configure.base_url + 'js/src/admin/quickship_registered/index.html',
                controller: 'QuickshipRegisteredCtrl'
            }).when('/quickship_registered/:itemAction/:itemId', {
                title: 'New Shipments ',
                templateUrl: window.Configure.base_url + 'js/src/admin/quickship_registered/quickship.html',
                controller: 'QuickshipRegisteredOverviewCtrl'
            }).when('/ghost', {
                title: 'Ghost Shipments ',
                templateUrl: window.Configure.base_url + 'js/src/admin/ghost/index.html',
                controller: 'QuickshipGhostCtrl'
            }).when('/ghost/:itemAction/:itemId', {
                title: 'Ghost Shipments ',
                templateUrl: window.Configure.base_url + 'js/src/admin/ghost/quickship.html',
                controller: 'QuickshipGhostOverviewCtrl'
            }).when('/messages', {
                title: 'Messages ',
                templateUrl: window.Configure.base_url + 'js/src/admin/messages/index.html',
                controller: 'MessagesCtrl'
            }).when('/messages/:itemAction/:itemId', {
                title: 'Messages ',
                templateUrl: window.Configure.base_url + 'js/src/admin/messages/message.html',
                controller: 'MessagesOverviewCtrl'
            }).when('/messages_registered', {
                title: 'Messages - Registered',
                templateUrl: window.Configure.base_url + 'js/src/admin/messages_registered/index.html',
                controller: 'MessagesRegisteredCtrl'
            }).when('/messages_registered/:itemAction/:itemId', {
                title: 'Messages - Registered',
                templateUrl: window.Configure.base_url + 'js/src/admin/messages_registered/message.html',
                controller: 'MessagesRegisteredOverviewCtrl'
            }).when('/business', {
                title: 'Business Signups ',
                templateUrl: window.Configure.base_url + 'js/src/admin/business/index.html',
                controller: 'BusinessCtrl'
            }).when('/business/:itemAction/:itemId', {
                title: 'Business Signup ',
                templateUrl: window.Configure.base_url + 'js/src/admin/business/business.html',
                controller: 'BusinessOverviewCtrl'
            }).when('/registered', {
                title: 'Registered Accounts ',
                templateUrl: window.Configure.base_url + 'js/src/admin/registered/index.html',
                controller: 'RegisteredCtrl'
            }).when('/registered/:itemAction/:itemId', {
                title: 'Registered Accounts ',
                templateUrl: window.Configure.base_url + 'js/src/admin/registered/registered.html',
                controller: 'RegisteredOverviewCtrl'
            }).otherwise({
                redirectTo: '/quickship'
            });

			// enable http caching
			$httpProvider.defaults.cache = false;

			// set headers for request detection
			$httpProvider.defaults.headers.common["X-From-Angularjs"] = true;

			$anchorScrollProvider.disableAutoScrolling();

			tagsInputConfigProvider
				.setDefaults('tagsInput', {
					minLength: 1
				});
		}
	]
);

app.run(function (editableOptions, editableThemes) {
	//editableThemes.bs3.inputClass = 'input-sm';
	//editableThemes.bs3.buttonsClass = 'btn-sm';
	editableOptions.theme = 'bs3';
});

angular.module('adminApp')
	.controller('MainCtrl', ['$scope', '$rootScope', '$http', '$q', '$window',
		function ($scope, $rootScope, $http, $q, $window) {

			$scope.selectedItem = null;

			$scope.transactionStatuses = {1: 'Awaiting payment'};

			$scope.batchSelection = [];

			$scope.listItems = [];

			$scope.countries = [];

			$scope.item_types = [];

			$scope.stats = {};

			$scope.loadingData = false;

            $scope.user_ids = [];
            $scope.Notes = [];
            $scope.newNote = null;
            $scope.noteAddResponse = null;

            $scope.saveNote = function (transaction_id, user_id, from) {
            	if ($('#noteDesc').val() == '') {
            		alert('Enter a note please');
                    return false;
				}
                var fd = new FormData();
                fd.append('transaction_id', transaction_id);
                fd.append('user_id', user_id);
                fd.append('note', $('#noteDesc').val());
                $http.post("/admin/quickship/add_note", fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function(response){
                    if (response.ok) {
                        $scope.noteAddResponse = response.message;
                        if (!from)
                            $scope.Notes.push(response.note);
                        else {
                            $('.notes-history-account').append('<div class="col-md-5">' + response.note.created + '</div><div class="col-md-7">' + response.note.note + '</div>');
                        }
                        $('#noteDesc').val('');
                    }
                }).error(function(error){
                	console.log(error);
				});

                return false;
            };

			$scope.loadMore = function () {
				alert('Loading more');
			};

			$scope.changeSorting = function (orderBy) {
				alert(orderBy);
			};

			$scope.loadAllData = function () {
				var defer = $q.defer();

				$http.get($window.Configure.base_url + 'admin/transactions/api_list.json')
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

            $scope.find_user_count = function (user_id) {
            	var count = 0;
            	for(var i = 0; i < $scope.user_ids.length; i++) {
            		if ($scope.user_ids[i] == user_id)
            			count =+ 1;
				}

				return count;
            };

			$scope.init = function () {
			    $scope.loadingData = true;


				$scope.loadAllData().then(function (result) {
					console.log(result);

				    $scope.transactionStatuses = result.transaction_statuses;

				    // convert listItems (QuickShips) to array so it can be sorted by Angular
				    var listItems_array = [];

				    angular.forEach(result.transactions, function (item, key) {
				        listItems_array.push({ value: key, obj: item });
				    });
				    $scope.listItems = listItems_array;
				    $scope.transactions_sort_reverse = true;
				    $scope.transactions_sort_reverseit = function () {
				        $scope.transactions_sort_reverse = !$scope.transactions_sort_reverse;
				    };
				    // used for QuickShip filtering
				    $scope.transactions_filter_status = "";

                    // convert listItems_registered (Shipments) to array so it can be sorted by Angular
				    var shipments_array = [];
				    angular.forEach(result.transactions_registered, function (item, key) {
				        shipments_array.push({ value: key, obj: item });
				    });
				    $scope.listItems_registered = shipments_array;
				    $scope.shipments_sort_reverse = true;
				    $scope.shipments_sort_reverseit = function () {
				        $scope.shipments_sort_reverse = !$scope.shipments_sort_reverse;
				    };


                    // convert Ghost item (Shipments) to array so it can be sorted by Angular
                    var ghost_shipments_array = [];
                    angular.forEach(result.transactions_ghost, function (item, key) {
                        ghost_shipments_array.push({ value: key, obj: item });
                    });
                    console.log(ghost_shipments_array);
                    $scope.listItems_ghost = ghost_shipments_array;
				    // used for Shipment filtering
				    $scope.shipments_filter_status = "";

					$scope.stats.count_waiting_package = result.count_waiting_package;
					$scope.stats.count_package_received = result.count_package_received;
					$scope.stats.count_awaiting_payment = result.count_awaiting_payment;
					$scope.stats.count_payment_received = result.count_payment_received;
					$scope.stats.count_package_sent = result.count_package_sent;
					$scope.stats.count_waiting_package_registered = result.count_waiting_package_registered;
					$scope.stats.count_package_received_registered = result.count_package_received_registered;
					$scope.stats.count_awaiting_payment_registered = result.count_awaiting_payment_registered;
					$scope.stats.count_payment_received_registered = result.count_payment_received_registered;
					$scope.stats.count_package_sent_registered = result.count_package_sent_registered;
					$scope.stats.count_waiting_package_ghost = result.count_waiting_package_ghost;


				    // convert registereds to array so it can be Sorted by Angular
					var registereds_array = [];
					angular.forEach(result.registereds, function (item, key) {
					    registereds_array.push({ value: key, obj: item });
					});
					//alert('registereds: ' + registereds_array);
					$scope.registereds = registereds_array;

					$scope.registereds_sort_reverse = true;
					$scope.registereds_sort_reverseit = function () {
					    $scope.registereds_sort_reverse = !$scope.registereds_sort_reverse;
					};

					var countries = [];
					angular.forEach(result.countries, function (item, key) {
						countries.push({value: key, text: item});
					});
					$scope.countries = countries;

				    // convert messages to array so it can be Sorted by Angular
					var messages_array = [];
					angular.forEach(result.messages, function (item, key) {
					    messages_array.push({ value: key, obj: item });
					});
					$scope.messages = messages_array;

					$scope.messages_registered = result.messages_registered;
                    $scope.businesssignups = result.businesssignups;
					var item_types = [];
					angular.forEach(result.item_types, function (item, key) {
						item_types.push({value: key, text: item});
					});
					$scope.item_types = item_types;
					$scope.loadingData = false;

				}, function (err) {
					$scope.loadingData = false;
				});


			};
			$scope.init();
		}
	]
);

angular.module('adminApp')
	.controller('QuickshipCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

            $scope.testClick = function () {
                console.log('Loading more');
                return false;
            };

		}
	]
);

angular.module('adminApp')
	.controller('QuickshipRegisteredCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

		}
	]
);


angular.module('adminApp')
    .controller('QuickshipGhostCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
            function ($scope, $rootScope, $http, $q, $routeParams, $window) {

            }
        ]
    );




angular.module('adminApp')
	.controller('MessagesCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

		}
	]
);

angular.module('adminApp')
        .controller('MessagesOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window', '$filter', 'TransactionSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc) {
                        $scope.itemId = null;

			$scope.itemAction = 'overview';

			$scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/messages/';

			$scope.templateUrl = '';

			$scope.message = {};

			//$scope.transactionApi = TransactionSvc;

			$scope.loadMessage = function (id) {
				var defer = $q.defer();
				$http.get($window.Configure.base_url + 'admin/messages/api_read/' + id + '.json')
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};



			/**
			 *
			 */
			$scope.init = function () {
				if (typeof $routeParams.itemId !== null) {
					$scope.itemId = $routeParams.itemId;

					$scope.$parent.loadingData = true;
					$scope.loadMessage($scope.itemId).then(function (result) {
						$scope.message = result.detail;
						$scope.$parent.selectedItem = $scope.message.Message.id;

						$scope.$parent.loadingData = false;
					}, function (err) {
						$scope.$parent.loadingData = false;
						alert('error');
					});
				}
				if (typeof $routeParams.itemAction !== null) {
					$scope.itemAction = $routeParams.itemAction;
					$scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
				}
			};

			$scope.init();



	}
	]
);

angular.module('adminApp')
	.controller('MessagesRegisteredCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

		}
	]
);

angular.module('adminApp')
        .controller('MessagesRegisteredOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window', '$filter', 'TransactionSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc) {
		    $scope.itemId = null;

		    $scope.itemAction = 'overview';

		    $scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/messages_registered/';

		    $scope.templateUrl = '';

		    $scope.message = {};


		    $scope.loadMessage = function (id) {
		        var defer = $q.defer();
		        $http.get($window.Configure.base_url + 'admin/messages/api_read/' + id + '.json')
					.success(function (res) {
					    defer.resolve(res);
					})
					.error(function (err) {
					    defer.reject(err);
					});

		        return defer.promise;
		    };



		    /**
			 *
			 */
		    $scope.init = function () {
		        if (typeof $routeParams.itemId !== null) {
		            $scope.itemId = $routeParams.itemId;

		            $scope.$parent.loadingData = true;
		            $scope.loadMessage($scope.itemId).then(function (result) {
		                $scope.message = result.detail;
                        $scope.$parent.Notes = result.detail.Notes;
		                $scope.$parent.selectedItem = $scope.message.Message.id;

		                $scope.$parent.loadingData = false;
		            }, function (err) {
		                $scope.$parent.loadingData = false;
		                alert('error');
		            });
		        }
		        if (typeof $routeParams.itemAction !== null) {
		            $scope.itemAction = $routeParams.itemAction;
		            $scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
		        }
		    };

		    $scope.init();
		}
        ]
);


angular.module('adminApp')
	.controller('BusinessCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

		}
	]
);

angular.module('adminApp')
	.controller('RegisteredCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window',
		function ($scope, $rootScope, $http, $q, $routeParams, $window) {

            $scope.saveNoteAccount = function (transaction_id, user_id) {
                var fd = new FormData();
                fd.append('transaction_id', transaction_id);
                fd.append('user_id', user_id);
                fd.append('note', $('#noteDesc').val());
                $http.post("/admin/quickship/add_note", fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                }).success(function(response){
                    if (response.ok) {
                        $scope.noteAddResponse = response.message;
                        $scope.$parent.Notes.push(response.note);
                        console.log(response.note);
                        $('#noteDesc').val('');
                    }
                }).error(function(response){
                });

                return false;
            };

		}
	]
);

angular.module('adminApp')
        .controller('BusinessOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window', '$filter', 'TransactionSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc) {
        	$scope.itemId = null;

			$scope.itemAction = 'overview';

			$scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/business/';

			$scope.templateUrl = '';

			$scope.businesssignup = {};

			//$scope.transactionApi = TransactionSvc;

			$scope.loadBusiness = function (id) {
				var defer = $q.defer();
				$http.get($window.Configure.base_url + 'admin/business/api_read/' + id + '.json')
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};



			/**
			 *
			 */
			$scope.init = function () {
				if (typeof $routeParams.itemId !== null) {
					$scope.itemId = $routeParams.itemId;

					$scope.$parent.loadingData = true;
					$scope.loadBusiness($scope.itemId).then(function (result) {
						$scope.businesssignup = result.detail;
						$scope.$parent.selectedItem = $scope.businesssignup.BusinessSignup.id;

						$scope.$parent.loadingData = false;
					}, function (err) {
						$scope.$parent.loadingData = false;
						alert('error');
					});
				}
				if (typeof $routeParams.itemAction !== null) {
					$scope.itemAction = $routeParams.itemAction;
					$scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
				}
			};

			$scope.init();
	}
	]
);

angular.module('adminApp')
   .controller('RegisteredOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window', '$filter', 'TransactionSvc', 'QuickshipSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc, QuickshipSvc) {

		    $scope.title = 'RegisteredOverviewCtrl';

		    $scope.itemId = null;

		    $scope.itemAction = 'overview';

		    $scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/registered/';

		    $scope.templateUrl = '';

		    $scope.registered = {};


            $scope.loadRegistered = function (id) {
		        var defer = $q.defer();
		        $http.get($window.Configure.base_url + 'admin/users/api_read/' + id + '.json')
					.success(function (res) {
					    defer.resolve(res);
					})
					.error(function (err) {
					    defer.reject(err);
					});

		        return defer.promise;
		    };

		    $scope.quickship = QuickshipSvc;

		    $scope.quickship.$storage.quickship.consolidatePackages = false;
            /**
             *
             * @type {number}
             */
            $scope.activeDetailTab = 0;

            $scope.validationStarted = false;
		    $scope.personalFrmdata = $scope.quickship.$storage.quickship.packages;

            /**
             *
             * @param form
             * @param index
             * @returns {boolean}
             */
            $scope.$watch('personalFrmdata', function() {
                console.log('Ghost', $scope.quickship.$storage.quickship.packages);
                for(var i=0; i < $scope.quickship.$storage.quickship.packages.length; i++) {

                    if ($scope.quickship.$storage.quickship.packages[i].weight === 'undefined' ||
                        parseFloat($scope.quickship.$storage.quickship.packages[i].weight) < 0.5)
                        $scope.quickship.$storage.quickship.packages[i].weight = null;

                    if ($scope.quickship.$storage.quickship.packages[i].length === 'undefined' ||
                        parseFloat($scope.quickship.$storage.quickship.packages[i].length) < 0.5)
                        $scope.quickship.$storage.quickship.packages[i].length = null;

                    if ($scope.quickship.$storage.quickship.packages[i].width === 'undefined' ||
                        parseFloat($scope.quickship.$storage.quickship.packages[i].width) < 0.5)
                        $scope.quickship.$storage.quickship.packages[i].width = null;

                    if ($scope.quickship.$storage.quickship.packages[i].height === 'undefined' ||
                        parseFloat($scope.quickship.$storage.quickship.packages[i].height) < 0.5)
                        $scope.quickship.$storage.quickship.packages[i].height = null;

                }

            }, true);


            $scope.hasValidWeight = function (form, index) {
		        if ($scope.quickship.$storage.quickship.consolidatePackages) return true;
		        if ($scope.validationStarted === false) return true;
		        if ($scope.quickship.$storage.quickship.packages[index].weight < 0.01) return false;
		        //if (form['package_' + index + '_weight'].$error.required) return false;
		        return true;
		    };


            $scope.validateWeights = function () {
                var is_valid = true;
                if ($scope.quickship.$storage.simpleSteps === true) return true;
                if ($scope.quickship.$storage.quickship.consolidatePackages) return true;
                angular.forEach($scope.quickship.$storage.quickship.packages, function (item) {
                    if (parseFloat(item.weight) < 0.01) return false;
                });
                return is_valid;
            };
            /**
             * Set active tab for package items
             * @param tab_index
             */
            $scope.setActiveTab = function (tab_index) {
                $scope.activeDetailTab = tab_index;
            };

            /**
             * Checks and highlights a tab for package items with invalid data
             * @param package_index
             * @param form
             * @returns {boolean}
             */
            $scope.hasInvalidTabData = function (package_index, form) {
            	if ($scope.quickship.$storage.quickship.packages[package_index].items == 'undefined') return;
                var count_items = $scope.quickship.$storage.quickship.packages[package_index].items.length;
                var hasErrors = false;
                for (var i = 0; i < count_items; i++) {
                    if (typeof form['packages_' + package_index + '_items_' + i + '_country_id'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_country_id'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_description'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_description'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_quantity'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_quantity'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_type_id'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_type_id'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_value'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_value'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_weight'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_weight'].$invalid === true) hasErrors = true;
                    if (typeof form['packages_' + package_index + '_items_' + i + '_weight_unit'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_weight_unit'].$invalid === true) hasErrors = true;
                }

                return hasErrors;
            };
            /**
			 *
			 * @param package_index
			 * @param index
			 */
			$scope.deleteItem = function (package_index, index) {
				$scope.quickship.$storage.quickship.packages[package_index].items.splice(index, 1);
		    };

			/**
			 *
			 * @param index
			 */
			$scope.addItem = function (index) {
				$scope.quickship.$storage.quickship.packages[index].items.push({
			     description: '',
				 quantity: 0,
				 weight_unit: 'lb',
			     price_value: null
			 });
			};

			/**
			 *
			 * @returns {Number}
			 */
			$scope.addPackage = function () {
			    return $scope.quickship.addPackage();
			 };


			$scope.updateSimplePackages = function () {
			    $scope.quickship.updateSimplePackages();
			    $scope.activeDetailTab = 0;
			};

			 /**
               *
               * @param package_index
             */
             $scope.deletePackage = function (package_index) {
                $scope.quickship.$storage.quickship.packages.splice(package_index, 1);
				$scope.activeDetailTab = 0;
		    };

            $scope.updateWeight = function (package_index) {
                var sum_weight = 0;
                var sum_weight_unit = $scope.quickship.$storage.quickship.packages[package_index].weight_unit;
                angular.forEach($scope.quickship.$storage.quickship.packages[package_index].consolidated, function (value, index) {
                    if (parseFloat(value.weight)) {
                        sum_weight += $scope.quickship.calculateWeight(value.weight, value.unit, sum_weight_unit);
                    }
                });
                $scope.quickship.$storage.quickship.packages[package_index].weight = sum_weight;
            };

            $scope.saveNewShipment = function (frm) {
                $scope.validationStarted = true;

                console.log($scope.personalFrmdata);
                // TODO: check if all packages weight are valid and above 0
                if (frm.$valid === false) {
                    $scope.quickship.validatesPackageDetails = false;
                    return false;
                }
                if ($scope.validateWeights() === false) {
                    $scope.quickship.validatesPackageDetails = false;
                    return false;
                }
                $scope.quickship.validatesPackageDetails = true;

                // Post the Package data
                var params = {
                    shipment: $scope.quickship.$storage,
                    user_id: $scope.itemId
                };

                $("#divPleaseWait").show();
                $http.post($window.Configure.base_url + 'quickship/new_shipment.json', params)
                    .success(function (res) {
                        $("#divPleaseWait").hide();
                        if (res.ok) {
                            alert("Ghost Shipment #" + res.shipment_id + " created and email sent.");
                            $scope.quickship.$storage.quickship.packages.splice(0);
                            $scope.quickship.$storage.quickship.packages = [{
                                tracking_code: [],
                                consolidated: [],
                                weight_unit: 'lb',
                                size_unit: 'in',
                                items: [
                                    {
                                        description: 'na',
										quantity: 1,
										type_id: null,
                                        weight_unit: 'lb',
                                        price_value: null,
                                        info: ''
                                    }
                                ]
                            }];
                        } else {
                            alert("Error, please try again");
                        }
                    }).error(function (err) {
                        $("#divPleaseWait").hide();
                        alert("Error: " + err);
                    });
            };

            $scope.saveGhostShipment = function (frm) {
                $scope.validationStarted = true;

                console.log($scope.personalFrmdata);
                // TODO: check if all packages weight are valid and above 0
                if (frm.$valid === false) {
                    $scope.quickship.validatesPackageDetails = false;
                    return false;
                }
                if ($scope.validateWeights() === false) {
                    $scope.quickship.validatesPackageDetails = false;
                    return false;
                }
                $scope.quickship.validatesPackageDetails = true;

                // Post the Package data
                var params = {
                    shipment: $scope.quickship.$storage,
                    user_id: $scope.itemId,
                    type_id: 4
                };

                $("#divPleaseWait").show();
                $http.post($window.Configure.base_url + 'quickship/new_shipment.json', params)
                    .success(function (res) {
                        $("#divPleaseWait").hide();
                        if (res.ok) {
                            alert("Ghost Shipment #" + res.shipment_id + " created and email sent.");
                            $scope.quickship.$storage.quickship.packages.splice(0);
                            $scope.quickship.$storage.quickship = {
                                special_instructions: '',
                                shipping_options: {
                                    cheapest: false,
                                    fastest: false
                                },
                                packagesExpedited: false,
                                repackaged: false,
                                packagesInsurance: false,
                                consolidatePackages: false,
                                amount: 1,
                                packages: [
                                    {
                                        tracking_code: [],
                                        consolidated: [],
                                        weight_unit: 'lb',
                                        size_unit: 'in',
                                        items: [
                                            {
                                                description: 'na',
                                                quantity: 1,
                                                type_id: null,
                                                weight_unit: 'lb',
                                                price_value: null,
                                                info: ''
                                            }
                                        ]
                                    }
                                ]
                            };
                        } else {
                            alert("Error, please try again");
                        }
                    }).error(function (err) {
                    $("#divPleaseWait").hide();
                    alert("Error: " + err);
                });
            };

		    $scope.init = function () {
		    	console.log($scope.quickship);
		        if (typeof $routeParams.itemId !== null) {
		            $scope.itemId = $routeParams.itemId;

		            $scope.$parent.loadingData = true;
		            $scope.loadRegistered($scope.itemId).then(function (result) {
		                $scope.registered = result.detail;
                        $scope.$parent.Notes = result.detail.Notes;
		                $scope.$parent.selectedItem = $scope.registered.User.id;

		                $scope.$parent.loadingData = false;
		            }, function (err) {
		                $scope.$parent.loadingData = false;
		                alert('error');
		            });
		        }
		        if (typeof $routeParams.itemAction !== null) {
		            $scope.itemAction = $routeParams.itemAction;
		            $scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
		        }

		        $scope.quickship.configure = $scope.Configure;
		        $scope.quickship.loadSettings();
		    };

		    $scope.init();

		}
        ]
);





angular.module('adminApp')
	.controller('QuickshipOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams', '$window', '$filter', 'TransactionSvc', 'QuickshipSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc, QuickshipSvc) {
			$scope.itemId = null;

			$scope.itemAction = 'overview';

			$scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/quickship/';

			$scope.base_url = $window.Configure.base_url;

			$scope.templateUrl = '';

			$scope.transaction = {};

			$scope.edit_status = false;

			$scope.transactionDirty = false;

			$scope.transactionStatus = 1;

			$scope.transactionApi = TransactionSvc;

			$scope.packageIndexEditingRate = 0;

			$scope.transactionDirtyFromGetRates = false;
			
			$scope.currently_selected = null;



			$scope.weightUnits = [
				{value: 'lb', text: 'lb'},
				{value: 'kg', text: 'kg'}
			];

			$scope.sizeUnits = [
				{value: 'cm', text: 'cm'},
				{value: 'in', text: 'in'}
			];

			$scope.quickship = QuickshipSvc;

            $scope.editBalanceDue = 0;

			$scope.editStatus = function () {
				$scope.edit_status = !$scope.edit_status;
			};

			$scope.$watch('transaction.Shipment', function (newv, previous) {
				if (!previous || Object.keys(previous).length < 1) return false;
				$scope.transactionDirty = true;
				$scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
                    if (!$scope.transactionDirtyFromGetRates && $scope.transactionDirty) {
                        alert('We have detected changes in your package details. \n' +
                            'Please update shipping quote for this package from "Shipment Rates" section. \n ' +
                            'Before updating rate you wont be able to print labels.');
                    }
                    $('.shippingRateMessage').html('We have detected changes in your package details.' +
                        'Please update shipping quote to print labels correctly.');
				}, function (err) {
					//console.log(err);
					//alert('Unable to save changes');
				});
			}, true);

			$scope.showStatus = function (field, options) {
				if (typeof field === 'undefined') return false;
				var selected = $filter('filter')(options, {value: field});
				return (field && selected.length) ? selected[0].text : 'Not set';
			};

			$scope.changeStatus = function (status) {
				if ($scope.edit_status !== true) return false;
				$scope.updateStatus($scope.itemId, status).then(function (result) {
					$scope.transactionStatus = status;
				}, function (err) {
					alert('Unable to change status');
				});
			};

			$scope.updateStatus = function (id, status) {
				var defer = $q.defer();

				$http.post($window.Configure.base_url + 'admin/transactions/api_status/' + id + '.json', {Transaction: {status: status}})
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			$scope.loadTransaction = function (transactionId) {
				var defer = $q.defer();
				$http.get($window.Configure.base_url + 'admin/transactions/api_read/' + transactionId + '.json')
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			$scope.updateTransaction = function () {

			};

			$scope.getPackageValue = function (index) {
				var total = 0;
				angular.forEach($scope.transaction.Shipment[index].Package.Item, function (item, key) {
					total += parseFloat('' + item.value);
				});
				return total;
			};

			$scope.getEasyPostRate = function (rate) {
			    return (rate / 1.13).toFixed(2);
			};

			$scope.fieldEmpty = function (field) {
			    return (field === null || field === "");
			};

			$scope.findRates = function (shipment, index) {
                $scope.transactionDirtyFromGetRates = true;

			    // Admin would like to search for a new rate for the passed-in Package
				$scope.packageIndexEditingRate = index;
				$scope.currently_selected = shipment.Package.easypost_rate_description;

				console.log($scope.currently_selected);

			    // $storage will be passed to QuickshipController.php:api_rates()
			    // api_rates pulls from $storage->quickship->packages, $storage->billing_shipping, $storage->billing, $storage->shipping
			    $scope.quickship.$storage.quickship.packages = [
						{
						    tracking_code: [],
						    consolidated: [],
						    weight_unit: shipment.Package.weight_unit,
						    weight: shipment.Package.weight,
						    size_unit: shipment.Package.height_unit,
						    width: shipment.Package.width,
						    height: shipment.Package.height,
						    length: shipment.Package.length,
						    items: shipment.Package.Item
						}
			    ];

			    // add price_value field to items (required by ShippingComponent.php)
			    angular.forEach($scope.quickship.$storage.quickship.packages[0].items, function (item, key) {
			        item.price_value = item.value;
			    });

			    $scope.quickship.$storage.billing_shipping = true;

			    $scope.quickship.$storage.billing = {
			        first_name: shipment.Address.firstname,
			        last_name: shipment.Address.lastname,
			        address1: shipment.Address.address1,
			        address2: shipment.Address.address2,
			        city: shipment.Address.city,
			        state: shipment.Address.adm_division,
			        postal_code: shipment.Address.postal_code,
			        country: shipment.Address.country_id,
                    phone: shipment.Address.phone
			    };

                // Show Please Wait spinner...
			    $("#divRatesWait").show();
			    $("#divRatesTable").hide();
			    $("#divRates").dialog();
                // call api_rates() and rates will be placed in $scope.quickship.$storage.shippingRates
			    var loadRatesPromise = $scope.quickship.loadRates();
			    loadRatesPromise.then(function (data) {
			        $("#divRatesWait").hide();
					$("#divRatesTable").show();
					$('.ui-dialog').find('.ui-dialog-title').html('Customer Previously Selected: '+ $scope.currently_selected);
			        $("#divRates").dialog("close");
			        $("#divRates").dialog({ width: 800 });
			    }, function (error) {
			        $("#divRatesWait").hide();
			        $("#divRatesTable").hide();
                    alert('Error occurred: ' + error);
		        });

			};

            // Admin selected a Rate
			$scope.selectService = function (shipping_rate) {

			    $("#spanWaitText").text("Saving selected rate...");
			    $("#divRatesWait").show();

			    $scope.quickship.$storage.shippingRatesSelected[$scope.packageIndexEditingRate] = {
			        package: $scope.packageIndexEditingRate,
			        shipping_rate_id: shipping_rate.rate_id,
			        shipment_id: shipping_rate.shipment_id,
			        rate: shipping_rate,
			        price: shipping_rate.rate,
			        price_num: shipping_rate.rate_num
			    };

			    // Save selected Rate
			    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_description = shipping_rate.carrier + ' ' + shipping_rate.service;
			    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_id = shipping_rate.rate_id;
			    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_price = shipping_rate.rate_num;
			    $scope.transaction.Shipment[$scope.packageIndexEditingRate].easypost_shipment_id = shipping_rate.shipment_id;

			    $scope.transactionDirty = true;
			    $scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
			        // refresh page to show changes
			        location.reload(true);
			    }, function (err) {
			        $("#divRatesWait").hide();
			        console.log(err);
			        //alert('Unable to save changes: ' + err);
			    });
			};

			//Print label
            $scope.printLabel = function(shipment) {
                var innerContents = document.getElementById(shipment.easypost_shipment_id).innerHTML;
                var popupWinindow = window.open('', '_blank', 'width=1000,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                popupWinindow.document.open();
                popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
                popupWinindow.document.close();
            };

			/**
			 *
			 */
			$scope.init = function () {
				if (typeof $routeParams.itemId !== null) {
					$scope.itemId = $routeParams.itemId;

					$scope.$parent.loadingData = true;
					$scope.loadTransaction($scope.itemId).then(function (result) {
						$scope.transaction = result.transaction_detail;
                        $scope.$parent.Notes = $scope.transaction.Notes;
						if ($scope.$parent !== null) {
                            $scope.$parent.selectedItem = $scope.transaction.Transaction.id;
                            $scope.$parent.loadingData = false;
						}

						$scope.transactionStatus = $scope.transaction.Transaction.status;

                        // add price_value field to items (required by ShippingComponent.php)
						var price = 0.00, fee = 0.00;
                        angular.forEach($scope.transaction.Shipment, function (item, key) {
                        	console.log('Shipment', item.Package.easypost_rate_price);
                            price +=  (item.balance_due == null || item.balance_due == '0.00') ? parseFloat(item.Package.easypost_rate_price) : parseFloat(item.balance_due);
                            fee += parseFloat('7.25');
                        });

                        $scope.editBalanceDue = price - fee;

					}, function (err) {
						$scope.$parent.loadingData = false;
						alert('error');
					});
				}
				if (typeof $routeParams.itemAction !== null) {
					$scope.itemAction = $routeParams.itemAction;
					$scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
				}

				$scope.quickship.configure = { base_url: $scope.base_url };

			};

			$scope.init();
		}
	]
);

angular.module('adminApp')
	.controller('QuickshipRegisteredOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams',
		'$window', '$filter', 'TransactionSvc', 'QuickshipSvc',
		function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc, QuickshipSvc) {
		    $scope.itemId = null;

		    $scope.itemAction = 'overview';

		    $scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/quickship_registered/';

		    $scope.base_url = $window.Configure.base_url;

		    $scope.templateUrl = '';

		    $scope.transaction = {};

		    $scope.edit_status = false;

		    $scope.transactionDirty = false;

            $scope.transactionDirtyFromGetRates = false;

		    $scope.transactionStatus = 1;

		    $scope.transactionApi = TransactionSvc;

			$scope.packageIndexEditingRate = 0;
			
			$scope.currently_selected = null;

		    $scope.weightUnits = [
				{ value: 'lb', text: 'lb' },
				{ value: 'kg', text: 'kg' }
		    ];

		    $scope.sizeUnits = [
				{ value: 'cm', text: 'cm' },
				{ value: 'in', text: 'in' }
		    ];

		    $scope.quickship = QuickshipSvc;

		    $scope.editStatus = function () {
		        $scope.edit_status = !$scope.edit_status;
		    };

		    $scope.$watch('transaction.Shipment', function (newv, previous) {
		        if (!previous || Object.keys(previous).length < 1) return false;
		        $scope.transactionDirty = true;
		        $scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
		        	if (!$scope.transactionDirtyFromGetRates) {
                        alert('We have detected changes in your package details. \n' +
                            'Please update shipping quote for this package from "Shipment Rates" section. \n ' +
                            'Before updating rate you wont be able to print labels.');
					}

                    $('.shippingRateMessage').html('We have detected changes in your package details.' +
                        'Please update shipping quote to print labels correctly.');
		        }, function (err) {
		            //console.log(err);
		            //alert('Unable to save changes');
		        });
		    }, true);

		    $scope.showStatus = function (field, options) {
		        if (typeof field === 'undefined') return false;
		        var selected = $filter('filter')(options, { value: field });
		        return (field && selected.length) ? selected[0].text : 'Not set';
		    };

		    $scope.changeStatus = function (status) {
		        if ($scope.edit_status !== true) return false;
		        $scope.updateStatus($scope.itemId, status).then(function (result) {
		            $scope.transactionStatus = status;
		        }, function (err) {
		            alert('Unable to change status');
		        });
		    };

		    $scope.updateStatus = function (id, status) {
		        var defer = $q.defer();

		        $http.post($window.Configure.base_url + 'admin/transactions/api_status/' + id + '.json', { Transaction: { status: status } })
					.success(function (res) {
					    defer.resolve(res);
					})
					.error(function (err) {
					    defer.reject(err);
					});

		        return defer.promise;
		    };

		    $scope.loadTransaction = function (transactionId) {
		        var defer = $q.defer();
		        $http.get($window.Configure.base_url + 'admin/transactions/api_read/' + transactionId + '.json')
					.success(function (res) {
					    defer.resolve(res);
					})
					.error(function (err) {
					    defer.reject(err);
					});

		        return defer.promise;
		    };

		    $scope.updateTransaction = function () {

		    };

		    $scope.getPackageValue = function (index) {
		        var total = 0;
		        angular.forEach($scope.transaction.Shipment[index].Package.Item, function (item, key) {
                    total += parseFloat('' + item.value);
		        });
		        return total;
		    };

		    $scope.getEasyPostRate = function (rate) {
		        return (rate / 1.13).toFixed(2);
		    };

		    $scope.fieldEmpty = function (field) {
		        return (field === null || field === "");
		    };

		    $scope.findRates = function (shipment, index) {

                $scope.transactionDirtyFromGetRates = true;
		        // Admin would like to search for a new rate for the passed-in Package
				$scope.packageIndexEditingRate = index;
				$scope.currently_selected = shipment.Package.easypost_rate_description;

		        // $storage will be passed to QuickshipController.php:api_rates()
		        // api_rates pulls from $storage->quickship->packages, $storage->billing_shipping, $storage->billing, $storage->shipping
		        $scope.quickship.$storage.quickship.packages = [
						{
						    tracking_code: [],
						    consolidated: [],
						    weight_unit: shipment.Package.weight_unit,
						    weight: shipment.Package.weight,
						    size_unit: shipment.Package.height_unit,
						    width: shipment.Package.width,
						    height: shipment.Package.height,
						    length: shipment.Package.length,
						    items: shipment.Package.Item
						}
		        ];

		        // add price_value field to items (required by ShippingComponent.php)
		        angular.forEach($scope.quickship.$storage.quickship.packages[0].items, function (item, key) {
		            item.price_value = item.value;
		        });

		        $scope.quickship.$storage.billing_shipping = true;

		        $scope.quickship.$storage.billing = {
		            first_name: shipment.Address.firstname,
		            last_name: shipment.Address.lastname,
		            address1: shipment.Address.address1,
		            address2: shipment.Address.address2,
		            city: shipment.Address.city,
		            state: shipment.Address.adm_division,
		            postal_code: shipment.Address.postal_code,
		            country: shipment.Address.country_id,
		            phone: shipment.Address.phone
		        };

		        // Show Please Wait spinner...
		        $("#divRatesWait").show();
		        $("#divRatesTable").hide();
		        $("#divRates").dialog();
		        // call api_rates() and rates will be placed in $scope.quickship.$storage.shippingRates
		        var loadRatesPromise = $scope.quickship.loadRates();
		        loadRatesPromise.then(function (data) {
		            $("#divRatesWait").hide();
					$("#divRatesTable").show();
					$('.ui-dialog').find('.ui-dialog-title').html('Customer Previously Selected: '+ $scope.currently_selected);
		            $("#divRates").dialog("close");
		            $("#divRates").dialog({ width: 800 });
		        }, function (error) {
		            $("#divRatesWait").hide();
		            $("#divRatesTable").hide();
		            alert('Error occurred: ' + error);
		        });

		    };

		    // Admin selected a Rate
		    $scope.selectService = function (shipping_rate) {

		        $("#spanWaitText").text("Saving selected rate...");
		        $("#divRatesWait").show();

		        $scope.quickship.$storage.shippingRatesSelected[$scope.packageIndexEditingRate] = {
		            package: $scope.packageIndexEditingRate,
		            shipping_rate_id: shipping_rate.rate_id,
		            shipment_id: shipping_rate.shipment_id,
		            rate: shipping_rate,
		            price: shipping_rate.rate,
		            price_num: shipping_rate.rate_num
		        };

		        // Save selected Rate
		        $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_description = shipping_rate.carrier + ' ' + shipping_rate.service;
		        $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_id = shipping_rate.rate_id;
		        $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_price = shipping_rate.rate_num;
		        $scope.transaction.Shipment[$scope.packageIndexEditingRate].easypost_shipment_id = shipping_rate.shipment_id;

		        $scope.transactionDirty = true;
		        $scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
		            // refresh page to show changes
		            location.reload(true);
		        }, function (err) {
		            $("#divRatesWait").hide();
		            console.log(err);
		            //alert('Unable to save changes: ' + err);
		        });
		    };

		    //Print the Label
            $scope.printLabel = function(shipment) {
                console.log('printSectionId', printSectionId);
                var innerContents = $('#'+shipment.easypost_shipment_id).html();
                var popupWinindow = window.open('', '_blank', 'width=1000,height=700,scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                popupWinindow.document.open();
                popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
                popupWinindow.document.close();
            };

		    /**
			 *
			 */
		    $scope.init = function () {
		        if (typeof $routeParams.itemId !== null) {
		            $scope.itemId = $routeParams.itemId;

		            $scope.$parent.loadingData = true;
		            $scope.loadTransaction($scope.itemId).then(function (result) {
		                $scope.transaction = result.transaction_detail;
		                $scope.$parent.Notes = $scope.transaction.Notes;
		                if ($scope.$parent !== null) {
		                    $scope.$parent.selectedItem = $scope.transaction.Transaction.id;
		                    $scope.$parent.loadingData = false;
		                }

		                $scope.transactionStatus = $scope.transaction.Transaction.status;

                        // add price_value field to items (required by ShippingComponent.php)
                        var price = 0.00;
                        console.log('Transacton details',result.transaction_detail.Shipment);
                        angular.forEach(result.transaction_detail.Shipment, function (item) {
                            console.log('Shipment', item.Package.easypost_rate_price);
                            price +=  (item.balance_due != null) ? parseFloat(item.balance_due) : 0.00;
                        });

                        $scope.editBalanceDue = price;

		            }, function (err) {
		                $scope.$parent.loadingData = false;
		                alert('error');
		            });
		        }
		        if (typeof $routeParams.itemAction !== null) {
		            $scope.itemAction = $routeParams.itemAction;
		            $scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
		        }

		        $scope.quickship.configure = { base_url: $scope.base_url };

		    };

		    $scope.init();
		}
	]
);

angular.module('adminApp')
    .controller('QuickshipGhostOverviewCtrl', ['$scope', '$rootScope', '$http', '$q', '$routeParams',
            '$window', '$filter', 'TransactionSvc', 'QuickshipSvc',
            function ($scope, $rootScope, $http, $q, $routeParams, $window, $filter, TransactionSvc, QuickshipSvc) {
                $scope.itemId = null;

                $scope.itemAction = 'overview';

                $scope.templateUrlPrefix = $window.Configure.base_url + 'js/src/admin/ghost/';

                $scope.base_url = $window.Configure.base_url;

                $scope.templateUrl = '';

                $scope.transaction = {};

                $scope.edit_status = false;

                $scope.transactionDirty = false;

                $scope.transactionDirtyFromGetRates = false;

                $scope.transactionStatus = 1;

                $scope.transactionApi = TransactionSvc;

                $scope.packageIndexEditingRate = 0;

                $scope.currently_selected = null;

                $scope.weightUnits = [
                    { value: 'lb', text: 'lb' },
                    { value: 'kg', text: 'kg' }
                ];

                $scope.sizeUnits = [
                    { value: 'cm', text: 'cm' },
                    { value: 'in', text: 'in' }
                ];

                $scope.quickship = QuickshipSvc;

                $scope.editStatus = function () {
                    $scope.edit_status = !$scope.edit_status;
                };

                $scope.$watch('transaction.Shipment', function (newv, previous) {
                    if (!previous || Object.keys(previous).length < 1) return false;
                    $scope.transactionDirty = true;
                    $scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
                        if (!$scope.transactionDirtyFromGetRates) {
                            alert('We have detected changes in your package details. \n' +
                                'Please update shipping quote for this package from "Shipment Rates" section. \n ' +
                                'Before updating rate you wont be able to print labels.');
                        }

                        $('.shippingRateMessage').html('We have detected changes in your package details.' +
                            'Please update shipping quote to print labels correctly.');
                    }, function (err) {
                        //console.log(err);
                        //alert('Unable to save changes');
                    });
                }, true);

                $scope.showStatus = function (field, options) {
                    if (typeof field === 'undefined') return false;
                    var selected = $filter('filter')(options, { value: field });
                    return (field && selected.length) ? selected[0].text : 'Not set';
                };

                $scope.changeStatus = function (status) {
                    if ($scope.edit_status !== true) return false;
                    $scope.updateStatus($scope.itemId, status).then(function (result) {
                        $scope.transactionStatus = status;
                    }, function (err) {
                        alert('Unable to change status');
                    });
                };

                $scope.updateStatus = function (id, status) {
                    var defer = $q.defer();

                    $http.post($window.Configure.base_url + 'admin/transactions/api_status/' + id + '.json', { Transaction: { status: status } })
                        .success(function (res) {
                            defer.resolve(res);
                        })
                        .error(function (err) {
                            defer.reject(err);
                        });

                    return defer.promise;
                };

                $scope.loadTransaction = function (transactionId) {
                    var defer = $q.defer();
                    $http.get($window.Configure.base_url + 'admin/transactions/api_read/' + transactionId + '.json')
                        .success(function (res) {
                            defer.resolve(res);
                        })
                        .error(function (err) {
                            defer.reject(err);
                        });

                    return defer.promise;
                };

                $scope.updateTransaction = function () {

                };

                $scope.getPackageValue = function (index) {
                    var total = 0;
                    angular.forEach($scope.transaction.Shipment[index].Package.Item, function (item, key) {
                        total += parseFloat('' + item.value);
                    });
                    return total;
                };

                $scope.getEasyPostRate = function (rate) {
                    return (rate / 1.13).toFixed(2);
                };

                $scope.fieldEmpty = function (field) {
                    return (field === null || field === "");
                };

                $scope.findRates = function (shipment, index) {

                    $scope.transactionDirtyFromGetRates = true;
                    // Admin would like to search for a new rate for the passed-in Package
                    $scope.packageIndexEditingRate = index;
                    $scope.currently_selected = shipment.Package.easypost_rate_description;

                    // $storage will be passed to QuickshipController.php:api_rates()
                    // api_rates pulls from $storage->quickship->packages, $storage->billing_shipping, $storage->billing, $storage->shipping
                    $scope.quickship.$storage.quickship.packages = [
                        {
                            tracking_code: [],
                            consolidated: [],
                            weight_unit: shipment.Package.weight_unit,
                            weight: shipment.Package.weight,
                            size_unit: shipment.Package.height_unit,
                            width: shipment.Package.width,
                            height: shipment.Package.height,
                            length: shipment.Package.length,
                            items: shipment.Package.Item
                        }
                    ];

                    // add price_value field to items (required by ShippingComponent.php)
                    angular.forEach($scope.quickship.$storage.quickship.packages[0].items, function (item, key) {
                        item.price_value = item.value;
                    });

                    $scope.quickship.$storage.billing_shipping = true;

                    $scope.quickship.$storage.billing = {
                        first_name: shipment.Address.firstname,
                        last_name: shipment.Address.lastname,
                        address1: shipment.Address.address1,
                        address2: shipment.Address.address2,
                        city: shipment.Address.city,
                        state: shipment.Address.adm_division,
                        postal_code: shipment.Address.postal_code,
                        country: shipment.Address.country_id,
                        phone: shipment.Address.phone
                    };

                    // Show Please Wait spinner...
                    $("#divRatesWait").show();
                    $("#divRatesTable").hide();
                    $("#divRates").dialog();
                    // call api_rates() and rates will be placed in $scope.quickship.$storage.shippingRates
                    var loadRatesPromise = $scope.quickship.loadRates();
                    loadRatesPromise.then(function (data) {
                        $("#divRatesWait").hide();
                        $("#divRatesTable").show();
                        $('.ui-dialog').find('.ui-dialog-title').html('Customer Previously Selected: '+ $scope.currently_selected);
                        $("#divRates").dialog("close");
                        $("#divRates").dialog({ width: 800 });
                    }, function (error) {
                        $("#divRatesWait").hide();
                        $("#divRatesTable").hide();
                        alert('Error occurred: ' + error);
                    });

                };

                // Admin selected a Rate
                $scope.selectService = function (shipping_rate) {

                    $("#spanWaitText").text("Saving selected rate...");
                    $("#divRatesWait").show();

                    $scope.quickship.$storage.shippingRatesSelected[$scope.packageIndexEditingRate] = {
                        package: $scope.packageIndexEditingRate,
                        shipping_rate_id: shipping_rate.rate_id,
                        shipment_id: shipping_rate.shipment_id,
                        rate: shipping_rate,
                        price: shipping_rate.rate,
                        price_num: shipping_rate.rate_num
                    };

                    // Save selected Rate
                    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_description = shipping_rate.carrier + ' ' + shipping_rate.service;
                    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_id = shipping_rate.rate_id;
                    $scope.transaction.Shipment[$scope.packageIndexEditingRate].Package.easypost_rate_price = shipping_rate.rate_num;
                    $scope.transaction.Shipment[$scope.packageIndexEditingRate].easypost_shipment_id = shipping_rate.shipment_id;

                    $scope.transactionDirty = true;
                    $scope.transactionApi.updateData($scope.itemId, $scope.transaction).then(function (result) {
                        // refresh page to show changes
                        location.reload(true);
                    }, function (err) {
                        $("#divRatesWait").hide();
                        console.log(err);
                        //alert('Unable to save changes: ' + err);
                    });
                };

                //Print the Label
                $scope.printLabel = function(shipment) {
                    console.log('printSectionId', printSectionId);
                    var innerContents = $('#'+shipment.easypost_shipment_id).html();
                    var popupWinindow = window.open('', '_blank', 'width=1000,height=700,scrollbars=yes,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                    popupWinindow.document.open();
                    popupWinindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + innerContents + '</html>');
                    popupWinindow.document.close();
                };

                /**
                 *
                 */
                $scope.init = function () {
                    if (typeof $routeParams.itemId !== null) {
                        $scope.itemId = $routeParams.itemId;

                        $scope.$parent.loadingData = true;
                        $scope.loadTransaction($scope.itemId).then(function (result) {
                            $scope.transaction = result.transaction_detail;
                            $scope.$parent.Notes = $scope.transaction.Notes;
                            if ($scope.$parent !== null) {
                                $scope.$parent.selectedItem = $scope.transaction.Transaction.id;
                                $scope.$parent.loadingData = false;
                            }

                            $scope.transactionStatus = $scope.transaction.Transaction.status;

                            // add price_value field to items (required by ShippingComponent.php)
                            var price = 0.00;
                            angular.forEach($scope.transaction.Shipment, function (item, key) {
                                console.log('Shipment', item.Package.easypost_rate_price);
                                price +=  (item.balance_due != null) ? parseFloat(item.balance_due) : 0.00;
                            });

                            $scope.editBalanceDue = price;

                        }, function (err) {
                            $scope.$parent.loadingData = false;
                            alert('error');
                        });
                    }
                    if (typeof $routeParams.itemAction !== null) {
                        $scope.itemAction = $routeParams.itemAction;
                        $scope.templateUrl = $scope.templateUrlPrefix + $scope.itemAction + '.html';
                    }

                    $scope.quickship.configure = { base_url: $scope.base_url };

                };

                $scope.init();
            }
        ]
    );


angular.module('filters', []).filter('zpad', function () {
	return function (input, n) {
		if (input === undefined)
			input = "";
		if (input.length >= n)
			return input;
		var zeros = "0".repeat(n);
		return (zeros + input).slice(-1 * n);
	};
}).filter('orderObjectBy', function () {
    return function (items, field, reverse) {

        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a[field] > b[field] ? 1 : -1);
        });
        if (reverse) filtered.reverse();
        return filtered;
    };
});


angular.module('adminApp')
	.directive('quickshipTabs', function () {
		return {
			restrict: 'A',
			replace: true,
			controller: 'quickshipTabsCtrl',
			templateUrl: window.Configure.base_url + 'js/src/admin/quickship/quickship-tabs.html'
		};
	});

angular.module('adminApp')
	.controller('quickshipTabsCtrl', ['$scope', '$rootScope', '$routeParams', '$window',
		function ($scope, $rootScope, $routeParams, $window) {
			$scope.path = $routeParams;
		}
	]
);


function isEmpty(value) {
    return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

angular.module('adminApp').directive('ngMin', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attr, ctrl) {
            scope.$watch(attr.ngMin, function () {
                ctrl.$setViewValue(ctrl.$viewValue);
            });
            var minValidator = function (value) {
                var min = scope.$eval(attr.ngMin) || 0;
                if (!isEmpty(value) && value < min) {
                    ctrl.$setValidity('ngMin', false);
                    return undefined;
                } else {
                    ctrl.$setValidity('ngMin', true);
                    return value;
                }
            };

            ctrl.$parsers.push(minValidator);
            ctrl.$formatters.push(minValidator);
        }
    };
});

/**
 * Directive to add special effect for placeholders/labels on input fields
 */
angular.module('adminApp').directive('magicField', function ($compile, $timeout) {
    return {
        scope: {},
        template: '<div class="field"></div>',
        transclude: true,
        replace: true,
        restrict: 'E',
        controller: function ($scope, $element) {
            $scope.showLabel = false;
        },
        link: function (scope, element, attrs, ctrl, transclude) {
            transclude(scope, function (clone, scope) {
                element.append(clone);
            });
            var input = element.find('input')[0] || element.find('textarea')[0] || element.find('select')[0];
            var label = element.find('label')[0];
            input = angular.element(input);
            input.on('focus blur keyup', function (e) {
                if (e.type == 'focus') {
                    scope.showLabel = true;
                    input.attr('placeholder', '');
                } else if (e.type === 'blur') {
                    scope.showLabel = true;
                    //input.attr('placeholder', label.innerHTML);
                } else {
                    if (input.val().length > 0) {
                        scope.showLabel = true;
                        input.attr('placehoder', '');
                    } else {
                        scope.showLabel = true;
                        //input.attr('placeholder', label.innerHTML);
                    }
                }

                $timeout(function () {
                    scope.$apply();
                }, 0);

            });
        }
    };
});

/**
 * Directive for phone numbers, to prefix always with a plus sign
 */
angular.module('adminApp').directive('plusPrefix', function () {
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        link: function (scope, element, attrs, controller) {
            element.bind('keyup change focus', function () {
                scope.$apply(function () {
                    if (typeof scope.ngModel === 'undefined') scope.ngModel = '';
                    scope.ngModel = scope.ngModel.replace('+', '');
                    if (scope.ngModel.length >= 0) {
                        scope.ngModel = '+' + scope.ngModel.replace(/[^\d]/g, '');
                    }
                });
            });
        }
    };
});

/**
 * Directive for masking money
 */
angular.module('adminApp').directive('moneyPrefix', function () {
    return {
        restrict: 'A',
        scope: {
            ngModel: '='
        },
        link: function (scope, element, attrs, controller) {
            element.bind('keyup change focus', function () {
                scope.$apply(function () {
                    if (typeof scope.ngModel === 'undefined' || scope.ngModel === null) {
                        scope.ngModel = '';
                        return false;
                    }
                    scope.ngModel = scope.ngModel.replace('$', '');
                    if (scope.ngModel.length >= 0) {
                        scope.ngModel = '$' + scope.ngModel.replace(/[^\d.]/g, '');
                    }
                });
            });
        }
    };
});

/**
 * Small directive to allow dynamic names on name property for input fields
 */
angular.module('adminApp').directive('dynamicName', function ($compile, $parse) {
    return {
        restrict: 'A',
        terminal: true,
        priority: 100000,
        link: function (scope, elem) {
            var name = $parse(elem.attr('dynamic-name'))(scope);
            // $interpolate() will support things like 'skill'+skill.id where parse will not
            elem.removeAttr('dynamic-name');
            elem.attr('name', name);
            $compile(elem)(scope);
        }
    };
});

/**
 * Small directive to upload file for Your'e Request Photo from registered account
 */

angular.module('adminApp').controller('addRegisteredRequestedPhotoCtrl', ['$scope', 'YourRegisterRequestedPhoto', function($scope, YourRegisterRequestedPhoto){
    $scope.addPhoto = false;
    $scope.uploadFile = function(){
        document.getElementById('btnUploadPhoto').innerText = "Uploading... Please wait";
        var file = $scope.myFile;
        var uploadUrl = "/admin/quickship/add_registered_requested_photo";
        var user_id = document.getElementById('btnUploadPhoto').getAttribute("user_id");
        YourRegisterRequestedPhoto.uploadFileToUrl(file, user_id, uploadUrl);
    };

    $scope.triggerAddRequestedPhoto = function() {
        $('#addRequestedPhoto').trigger('click');
        setTimeout(function(){
            $scope.addPhoto = true;
        }, 2000);
    };

    $scope.$watch('myFile', function(){
        var file = document.getElementById('addRequestedPhoto').files[0];
        if (!file) return false;
        var imgSrc = '';
        var myReader = new FileReader();
        myReader.onloadend = function(e){
            var is_image = (file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png');
            if (is_image) {
                document.getElementById('previewRequestedPhoto').src = imgSrc = myReader.result;
                document.getElementById('previewRequestedPhoto').style.display = 'block';
                document.getElementById('addPhotoError').style.display = 'none';
                document.getElementById('btnUploadPhoto').style.display = 'block';
                document.getElementById('btnAddPhoto').style.display = 'none';
            }
            else {
                document.getElementById('addPhotoError').innerHTML = "Invalid file type, please select only jpg, jpeg, gif, png images.";
                document.getElementById('addPhotoError').style.display = 'block';
                document.getElementById('btnUploadPhoto').style.display = 'none';
                document.getElementById('btnAddPhoto').style.display = 'block';
            }
        };

        myReader.readAsDataURL(file);

        $scope.addRequestedPhotoPreviewSrc = imgSrc;

    });

}]);

/**
 * Small directive to upload file for Your'e Request Photo
 */

angular.module('adminApp').controller('addRequestedPhotoCtrl', ['$scope', 'YourRequestedPhoto', function($scope, YourRequestedPhoto){
    $scope.addPhoto = false;
    $scope.uploadFile = function(){
        document.getElementById('btnUploadPhoto').innerText = "Uploading... Please wait";
        var file = $scope.myFile;
        var uploadUrl = "/admin/quickship/add_requested_photo";
        var transaction_id = document.getElementById('btnUploadPhoto').getAttribute("transaction_id");
        YourRequestedPhoto.uploadFileToUrl(file, transaction_id, uploadUrl);
    };

    $scope.triggerAddRequestedPhoto = function() {
        $('#addRequestedPhoto').trigger('click');
        setTimeout(function(){
            $scope.addPhoto = true;
		}, 2000);
    };

    $scope.$watch('myFile', function(){
        var file = document.getElementById('addRequestedPhoto').files[0];
        if (!file) return false;
        var imgSrc = '';
        var myReader = new FileReader();
        myReader.onloadend = function(e){
            var is_image = (file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png');
            if (is_image) {
            	document.getElementById('previewRequestedPhoto').src = imgSrc = myReader.result;
                document.getElementById('previewRequestedPhoto').style.display = 'block';
                document.getElementById('addPhotoError').style.display = 'none';
                document.getElementById('btnUploadPhoto').style.display = 'block';
                document.getElementById('btnAddPhoto').style.display = 'none';
            }
            else {
                document.getElementById('addPhotoError').innerHTML = "Invalid file type, please select only jpg, jpeg, gif, png images.";
                document.getElementById('addPhotoError').style.display = 'block';
                document.getElementById('btnUploadPhoto').style.display = 'none';
                document.getElementById('btnAddPhoto').style.display = 'block';
            }
        };

        myReader.readAsDataURL(file);

        $scope.addRequestedPhotoPreviewSrc = imgSrc;

    });

}]);

/**
 * Small directive to upload file for Add Photo of ShippingBox
 */

angular.module('adminApp').controller('addShippingBoxPhotoCtrl', ['$scope', 'AddShippingBoxPhoto', function($scope, AddShippingBoxPhoto){
    $scope.addShippingPhoto = false;
    $scope.uploadFile = function(){
        document.getElementById('btnUploadShippingBoxPhoto').innerText = "Uploading... Please wait";
        var file = $scope.myShippingBoxFile;
        var uploadUrl = "/admin/quickship/add_shippingbox_photo";
        var transaction_id = document.getElementById('btnUploadShippingBoxPhoto').getAttribute("transaction_id");
        AddShippingBoxPhoto.uploadFileToUrl(file, transaction_id, uploadUrl);
    };

    $scope.triggerAddShippingPhoto = function() {
        $('#addShippingBoxPhoto').trigger('click');
        setTimeout(function(){
            $scope.addShippingPhoto = true;
		}, 2000);
    };

    $scope.$watch('myShippingBoxFile', function(){
        var file = document.getElementById('addShippingBoxPhoto').files[0];
        if (!file) return false;
        var imgSrc = '';
        var myReader = new FileReader();
        myReader.onloadend = function(e){
            var is_image = (file.type == 'image/jpeg' || file.type == 'image/jpg' || file.type == 'image/png');
            if (is_image) {
            	document.getElementById('previewShippingBoxPhoto').src = imgSrc = myReader.result;
                document.getElementById('previewShippingBoxPhoto').style.display = 'block';
                document.getElementById('uploadShippingBoxPhotoError').style.display = 'none';
                document.getElementById('btnUploadShippingBoxPhoto').style.display = 'block';
                document.getElementById('btnAddShippingBoxPhoto').style.display = 'none';
            }
            else {
                document.getElementById('uploadShippingBoxPhotoError').innerHTML = "Invalid file type, please select only jpg, jpeg, gif, png images.";
                document.getElementById('uploadShippingBoxPhotoError').style.display = 'block';
                document.getElementById('btnUploadShippingBoxPhoto').style.display = 'none';
                document.getElementById('btnAddShippingBoxPhoto').style.display = 'block';
            }
        };

        myReader.readAsDataURL(file);

        $scope.previewShippingBoxPhotoSrc = imgSrc;

    });

}]);

angular.module('adminApp').directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {

            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){

                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });


        }
    };
}]);

angular.module('adminApp').service('YourRegisterRequestedPhoto', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, user_id, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        fd.append('user_id', user_id);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(response){
            if (response.ok) {
                window.location.reload();
            }
            else {
                document.getElementById('addPhotoError').innerHTML = response.message;
                document.getElementById('addPhotoError').style.display = 'block';
            }
        }).error(function(response){
            document.getElementById('addPhotoError').innerHTML = response.message;
            document.getElementById('addPhotoError').style.display = 'block';
        });
    };
}]);

angular.module('adminApp').service('YourRequestedPhoto', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, transaction_id, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        fd.append('transaction_id', transaction_id);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(response){
			if (response.ok) {
				window.location.reload();
			}
			else {
                document.getElementById('addPhotoError').innerHTML = response.message;
                document.getElementById('addPhotoError').style.display = 'block';
			}
        }).error(function(response){
            document.getElementById('addPhotoError').innerHTML = response.message;
            document.getElementById('addPhotoError').style.display = 'block';
        });
    };
}]);

angular.module('adminApp').service('AddShippingBoxPhoto', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, transaction_id, uploadUrl){
        var fd = new FormData();
        fd.append('file', file);
        fd.append('transaction_id', transaction_id);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).success(function(response){
			if (response.ok) {
				window.location.reload();
			}
			else {
                document.getElementById('uploadShippingBoxPhotoError').innerHTML = response.message;
                document.getElementById('uploadShippingBoxPhotoError').style.display = 'block';
			}
        }).error(function(response){
            document.getElementById('uploadShippingBoxPhotoError').innerHTML = response.message;
            document.getElementById('uploadShippingBoxPhotoError').style.display = 'block';
        });
    };
}]);

angular.module('adminApp').config(['$provide', function ($provide) {
    $provide.decorator('ngModelDirective', function ($delegate) {
        var ngModel = $delegate[0], controller = ngModel.controller;
        ngModel.controller = ['$scope', '$element', '$attrs', '$injector', function (scope, element, attrs, $injector) {
            var $interpolate = $injector.get('$interpolate');
            attrs.$set('name', $interpolate(attrs.name || '')(scope));
            $injector.invoke(controller, this, {
                '$scope': scope,
                '$element': element,
                '$attrs': attrs
            });
        }];
        return $delegate;
    });
    $provide.decorator('formDirective', function ($delegate) {
        var form = $delegate[0], controller = form.controller;
        form.controller = ['$scope', '$element', '$attrs', '$injector', function (scope, element, attrs, $injector) {
            var $interpolate = $injector.get('$interpolate');
            attrs.$set('name', $interpolate(attrs.name || attrs.ngForm || '')(scope));
            $injector.invoke(controller, this, {
                '$scope': scope,
                '$element': element,
                '$attrs': attrs
            });
        }];
        return $delegate;
    });
}]);
