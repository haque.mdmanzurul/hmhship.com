/**
 *
 * @type {{index: number, consolidated: boolean, templates: {package: null, item_container: null, item: null}, get_package: Function, get_item_container: Function, get_item: Function, init: Function}}
 */
hmhship.packages = {

    index: 1,

    allowConsolidated: false,

    /**
     * Array data model for consolidated packages.
     * Every package is added as an object
     * Ex. {unit: 'value', weight: 'value', price: 'value'}
     */
    consolidated: [],

    /**
     *
     */
    consolidatedPackages: [],


    /**
     * Amount of consolidated groups
     */
    consolidate_groups_count: 0,


    /**
     * Keeps track of consolidated packages created since start
     */
    consolidated_index: 1,

    domElements: {
        hardStepsConsolidatedSelector: null
    },

    /**
     * Templates
     */
    templates: {
        package: null,
        item_container: null,
        item: null,
        newPack: null
    },

    /**
     * Reset consolidated packages
     */
    resetConsolidatedPackages: function () {

        $('.lslide', '#quickshipFrm').addClass('active');

        // Removes all consolidated packages boxes except first one
        hmhship.packages.domElements.hardStepsConsolidatedSelector.find('.box-item').not(':eq(0)').remove();

        hmhship.packages.consolidated = [];
        hmhship.packages.consolidated_index = 1;

        // Remove existing packages from 'package details'
        $('.wm3por1-added-consolidated', '#quickshipFrm').remove();

        hmhship.packages._updatePackagesLabels();

    },

    /**
     * Add consolidated package
     */
    add_consolidated: function () {

        var $pkg = hmhship.packages;

        // add to consolidated array
        $pkg.consolidated[$pkg.consolidated_index] = {
            weight: null,
            unit: null,
            price: null
        };

        var box_html = $pkg.getConsolidatePackageTpl();

        if (hmhship.config.steps == 'hardSteps') {
            // Add to "additional packages" on step 2

            if ($pkg.consolidated_index > 1) { // Only append after 1 package
                box_html.attr('data-package-index', $pkg.consolidated_index);
                hmhship.packages.domElements.hardStepsConsolidatedSelector.append(box_html);
                $('.step-2 .tmp-required', '#quickshipFrm').addClass('required');
                hmhship.packages.domElements.hardStepsConsolidatedSelector.find('.s2_delPackage').show();
            } else {
                var box_html = hmhship.packages.domElements.hardStepsConsolidatedSelector.find('.box-item:first');
                box_html.attr('data-package-index', $pkg.consolidated_index);
            }
        }

        // Handle DOM events for consolidated package
        $pkg.handleEventsConsolidated(
            box_html,
            $pkg.consolidated_index
        );

        // Consider as an auto-increment value
        hmhship.packages.consolidated_index++;
    },

    /**
     * Removes a consolidated package by index
     *
     * @param step
     * An integer value for current step (valid options 2 or 3)
     * @param elem
     * A jQuery element
     */
    remove_consolidated: function (elem) {
        if (hmhship.config.steps == 'hardSteps') {
            var step = '2';
        } else {
            var step = '3';
        }

        var index = elem.closest('.box-item').data('package-index');

        // Remove item from packages array
        delete hmhship.packages.consolidated[index];


        // Remove from DOM
        $('.s' + step + '_boxPackages .box-item[data-package-index=' + index + ']').remove();

        // Remove delete button if there's only one package
        if ($('.s' + step + '_boxPackages .box-item').length <= 1) {
            $('.s' + step + '_delPackage').hide();
        }
    },

    /**
     * One-way binding for consolidated packages
     * @param box_html
     * @param package_number
     */
    handleEventsConsolidated: function (box_html, package_number) {

        box_html.find('div.col-sm-12 > h3 > span').html(package_number);

        // Bind events to weight field
        box_html.find("input[name*='s2_adPackageWeight']")
            .bind('keyup change', function () {
                hmhship.packages.consolidated[package_number].weight = $(this).val();
                hmhship.packages.manageConsolidatedPackages();
            });

        // Bind events to unit field
        box_html.find("select[name*='s2_adPackageUnit']")
            .bind('change', function () {
                hmhship.packages.consolidated[package_number].unit = $(this).val();
                hmhship.packages.manageConsolidatedPackages();
            });
        box_html.find("select[name*='s2_adPackageUnit']").trigger('change');

        // Bind events to price field
        box_html.find("input[name*='s2_adPackageValue']")
            .bind('keyup change', function () {
                hmhship.packages.consolidated[package_number].price = $(this).val();
                hmhship.packages.manageConsolidatedPackages();
            });
    },

    /**
     * Group consolidated packages inside 'Package details'
     * @returns {boolean}
     */
    manageConsolidatedPackages: function () {

        var wm3por1 = 1;

        hmhship.packages.consolidate_groups_count = 0;
        var packages_groups = Math.ceil(Object.keys(hmhship.packages.consolidated).length / 3);
        if (packages_groups < 1) return false;

        var groups = [];
        var packages_keys = Object.keys(hmhship.packages.consolidated);

        for (var i = 1; i <= packages_groups; i++) {
            var keys_counter = 1;
            for (var package_key = 0; package_key < packages_keys.length; package_key++) {
                if (keys_counter > 3) break;
                if (typeof groups[i] === 'undefined') groups[i] = [];
                groups[i].push(packages_keys[package_key]);
                keys_counter++;
            }
            packages_keys.splice(0, 3);
        }

        $('.wm3por1-added-consolidated', '#quickshipFrm').remove();

        for (var group in groups) {
            var weight = 0;
            for (var key = 0; key < groups[group].length; key++) {
                if (hmhship.packages.consolidated.hasOwnProperty(groups[group][key])) {
                    if (parseFloat(hmhship.packages.consolidated[groups[group][key]].weight)) weight += parseFloat(hmhship.packages.consolidated[groups[group][key]].weight);
                }
            }
            hmhship.packages._addPkgDetailsConsolidated({
                weight: weight
            });
        }

        hmhship.packages._updatePackagesLabels();
    },

    /**
     *
     */
    enableSimpleStepsPackagesValidation: function () {
        $('.s3_boxPkgDetailSimple input[name=packages_simple]', '#quickshipFrm').addClass('required integer');

        // Hide dimensions on simplesteps
        $('.hs-packages-dimensions', '#quickshipFrm').hide();
        $('.hs-packages-dimensions input, .hs-packages-dimensions select', '#quickshipFrm').removeClass('required');
        $('.s3_boxPkgDetailSimple').removeClass('hidden');
        $('.s3_boxPkgDetail').parent().addClass('hidden');
    },

    /**
     *
     */
    disableSimpleStepsPackagesValidation: function () {
        $('.s3_boxPkgDetailSimple input[name=packages_simple]', '#quickshipFrm').removeClass('required').removeClass('integer');
    },

    /**
     * Add a consolidated package to the list of global packages
     * @param args
     * @private
     */
    _addPkgDetailsConsolidated: function (args) {
        var $pkgs = hmhship.packages;

        hmhship.packages.consolidate_groups_count++;

        //var indLabel = parseInt($('.s3_boxPkgDetail .box-item').length) + 1;
        var indLabel = hmhship.packages.consolidate_groups_count;

        var css_class = 'wm3por1-added-consolidated';
        var style = 'style="display:none !important;"';
        var tab_title = 'Package';

        var newDetailTab = '<li class="' + css_class + '"><a href="#consolidated_pkg' + hmhship.packages.consolidate_groups_count + '" data-toggle="tab">' + tab_title + ' <span class="label-tab">' + indLabel + '</span></a></li>';

        var packageTpl = $pkgs.get_package();

        // Add package to package list

        packageTpl.find('.hs-package-title').html('Package ' + indLabel + ' (Consolidated Package ' + indLabel + ')');

        packageTpl.find('input.s3_copyWeight')
            .val(args.weight)
            .prop('readonly', true);


        var weight_unit_holder_readonly = packageTpl.find('select.s3_copyUnit');
        var weight_unit_holder_hidden = $('<input type="hidden">');
        weight_unit_holder_hidden
            .attr('name', weight_unit_holder_readonly.attr('name'))
            .attr('value', weight_unit_holder_readonly.val());
        weight_unit_holder_hidden.insertAfter(weight_unit_holder_readonly);
        weight_unit_holder_readonly
            .attr('disabled', true)
            .removeAttr('name');

        packageTpl.addClass('wm3por1-added-consolidated');

        var tab_content = $pkgs.get_item_container();
        tab_content
            .addClass(css_class)
            .attr('id', 'consolidated_pkg' + hmhship.packages.consolidate_groups_count);
        if ($('.s3_boxPkgDetail > .' + css_class).length > 0) {
            packageTpl.insertAfter($('.s3_boxPkgDetail > .' + css_class + ':last'));
            $(newDetailTab).insertAfter($('.s3_itemDetailWrapTabs .nav-tabs > li.' + css_class + ':last'));
            tab_content.insertAfter($('.s3_itemDetailWrapTabs .tab-content > div.' + css_class + ':last'));
        } else {
            $('.s3_boxPkgDetail').prepend(packageTpl);
            $('.s3_itemDetailWrapTabs .nav-tabs').prepend(newDetailTab);
            $('.s3_itemDetailWrapTabs .tab-content').prepend(tab_content);
        }


        $('.step-3 .tmp3-required').addClass('required');

        $('.s3_delPkgDetail:last').parent().parent().show();
        $('.s3_delPkgDetail:last').show();

        $('.hs-tracking-codes').select2({
            tags: true,
            allowClear: true,
            tokenSeparators: [',', ' '],
            width: '100%'
        });
    },

    /**
     * Update
     * @private
     */
    _updatePackagesLabels: function () {

        var contLabelTab = 1;
        $('.step-3 .label-tab', '#quickshipFrm').each(function () {
            $(this).html(contLabelTab);
            contLabelTab++;
        });
        var contLabelTitle = 1;
        $('.step-3 .label-title', '#quickshipFrm').each(function () {
            $(this).html(contLabelTitle);
            contLabelTitle++;
        });
        var packageLabel = 1;
        $('.s3_boxPkgDetail .box-item .hs-package-title', '#quickshipFrm').each(function () {
            if ($(this).html().indexOf('Consolidated') === -1) {
                $(this).html('Package ' + packageLabel);
            }
            packageLabel++;
        });

        var packageLabel = 1;
        if (hmhship.config.steps == 'hardSteps') {
            $('.s2_boxPackages .col-sm-12 h3 span').each(function () {
                $(this).html(packageLabel);
                packageLabel++;
            });
        }

        // Make first tab active
        $('#tabs > li:first', '#quickshipFrm').find('a').trigger('click');
    },

    /**
     *
     * @returns {*}
     */
    get_package: function () {
        var $tpl = hmhship.packages.templates;
        var view = $tpl.package.clone();
        var idx = ++hmhship.packages.index;

        $('*[name^="packages"]', view).each(function () {
            var old_name = $(this).attr("name");
            var new_name = old_name.replace("[1]", "[" + idx + "]");
            $(this).attr("name", new_name);
        });

        if (hmhship.config.steps == 'simpleSteps') {
            $(view).find('.hs-packages-dimensions').hide();
        } else {
            $(view).find('.hs-packages-dimensions').show();
        }

        $(view).find('h3.hs-package-title').html('Package ' + idx);

        return view;
    },

    get_item_container: function () {
        var $tpl = hmhship.packages.templates;

        var view = $tpl.item_container.clone();
        view.attr("id", "pkg" + hmhship.packages.index);
        view.removeClass("active");

        $('*[name^="packages"]', view).each(function () {
            var old_name = $(this).attr("name");
            var new_name = old_name.replace("[1]", "[" + hmhship.packages.index + "]");

            $(this).attr("name", new_name);
        });

        return view;
    },

    get_item: function () {
        var $tpl = hmhship.packages.templates;

        var pid = $('.quickship3 .s3_itemDetailWrapTabs .tab-pane.active').index() + 1;
        var iid = $('.quickship3 .s3_itemDetailWrapTabs .tab-pane.active .s3_itemDetail').length + 1;
        var view = $tpl.item.clone();

        $('*[name^="packages"]', view).each(function () {
            var old_name = $(this).attr("name");

            var new_name = old_name;
            new_name = new_name.replace("packages[1]", "packages[" + pid + "]");
            new_name = new_name.replace("[items][1]", "[items][" + iid + "]");

            $(this).attr("name", new_name);
        });

        return view;
    },

    getConsolidatePackageTpl: function () {
        var $tpl = hmhship.packages.templates;
        var $view = $tpl.newPack.clone();
        $view
            .find('.s2_copyWeight')
            .attr('name', 's2_adPackageWeight[' + hmhship.packages.consolidated_index + ']');

        return $view;
    },

    /**
     * Create basic templates for packages on initialization
     */
    init: function () {
        var $tpl = hmhship.packages.templates;

        $tpl.package = $(".s3_boxPkgDetail div.box-item").first().clone();
        $tpl.item_container = $(".s3_itemDetailWrapTabs #my-tab-content .tab-pane").first().clone();
        $tpl.item = $(".quickship3 #pkg1 .s3_itemDetail").first().clone();

        $tpl.newPack = $('<div class="box-item"><div class="row"><div class="col-sm-12"><h3>Consolidated package <span>1</span></h3></div><div class="col col-md-3"><input type="number" min="1" max="999" name="s2_adPackageWeight[' + hmhship.packages.consolidated_index + ']" class="s2_copyWeight form-control tmp-required" placeholder="Weight"><span class="msg-error"></span></div><div class="col col-md-3"><select name="s2_adPackageUnit[' + hmhship.packages.consolidated_index + ']" class="s2_copyUnit form-control tmp-required"><option value="lb">lb</option><option value="kg">kg</option></select><span class="msg-error"></span></div><div class="col col-md-3"><input type="number" min="1" name="s2_adPackageValue[' + hmhship.packages.consolidated_index + ']" class="s2_copyValue form-control tmp-required" placeholder="U$xx.xx"><span class="msg-error"></span></div><div class="col col-md-3"><button type="button" class="btn btn-danger s2_delPackage">Delete</button></div></div></div>');

        hmhship.packages.domElements.hardStepsConsolidatedSelector = $('.s2_boxPackages', '#quickshipFrm');
    }
};
