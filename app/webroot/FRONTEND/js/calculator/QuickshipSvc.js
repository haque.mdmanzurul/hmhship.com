// FRONTEND version of QuickshipSvc
angular.module('calculator')
	.service('QuickshipSvc', ['$http', '$q', '$localStorage', '$rootScope',
		function ($http, $q, $localStorage, $rootScope) {

			var service = this;

			service.configure = null;


			/**
			 * Current step in the wizard. Defaults to 1 (Personal details)
			 * @type {number}
			 */
			service.currentStep = 3;

			/**
			 *
			 * @type {boolean}
			 */
			service.processingPayment = false;

			/**
			 * True if settings such as countries and item types were already loaded using loadSettings(), false otherwise
			 * @type {boolean}
			 */
			service.settingsLoaded = false;

			/**
			 * True if all validations are ok on Personal Detail, false otherwise. Defaults to true
			 * @type {boolean}
			 */
			service.validatesPersonalDetails = true;

			/**
			 * True if all validations are ok on Package Details, false otherwise. Defaults to true
			 * @type {boolean}
			 */
			service.validatesPackageDetails = true;

			/**
			 *
			 * @type {Array}
			 */
			service.shippingRatesSelected = [];

			/**
			 * Holds firstname and lastname for your-us-address page
			 * @type {{}}
			 */
			service.address = {};


			service.defaultOptions = {
				shippingRates: [], // Holds all shipping rates from EasyPost API for the current request
				shippingRatesSelected: [],
				billing_shipping: true,
				simpleSteps: true,
				personalDetailsCompleted: false,
				countries: null,
				itemTypes: null,
				transactionHash: null,
				payment_type: 'Paypal',
				billing: {
					first_name: '',
					last_name: '',
					phone: '',
					address2: ''
				},
				shipping: {
					address2: ''
				},
				quickship: {
					special_instructions: '',
					shipping_options: {
						cheapest: false,
						fastest: false
					},
					packagesExpedited: false,
					repackaged: false,
					packagesInsurance: false,
					consolidatePackages: false,
                    requestPhoto: false,
					amount: 1,
					packages: [
						{
							tracking_code: '',
							consolidated: [],
							weight_unit: 'lb',
							size_unit: 'in',
							items: [
								{
									description: '',
									weight_unit: 'lb',
									price_value: null,
									info: ''
								}
							]
						}
					]
				}
			};

			/**
			 * Local storage to keep user data safe from browser reloads.
			 * IMPORTANT: Must be cleaned up after a successful submission on step 6.
			 */
			service.$storage = $localStorage.$default(service.defaultOptions);

			/**
			 * service.$storage.quickship.amount should only be used in Simple Steps mode (Dimensions not known)
             * If in Simple Steps mode, update the amount of packages based on the 'How many packages' input field
             * If they chose Consolidated, then there should only be 1 Package for every 5
			 */
			service.updateSimplePackages = function () {

			    if (!service.$storage.simpleSteps)
			        return; // does not apply

				var currentPackagesNum = service.$storage.quickship.packages.length;
				var newPackagesNum = service.$storage.quickship.amount;

				var diff = newPackagesNum - currentPackagesNum;
				if (newPackagesNum < 1) {
					return false;
				}
				if (newPackagesNum < currentPackagesNum) {
					service.$storage.quickship.packages.splice(newPackagesNum, Math.abs(diff));
				} else if (diff > 0) {
					for (var i = 0; i < diff; i++) {
						service.$storage.quickship.packages.push({
							items: [
								{
									description: '',
									weight_unit: 'lb',
									price_value: null,
									info: ''
								}
							]
						});
					}
				}
			};

			/**
			 * Load app settings from backend API, such as countries, item types and other required settings
			 * @returns {boolean}
			 */
			service.loadSettings = function () {
                // call loadSettingsApi() because we need to get any logged-in user's info
                // also need to get the value of read_only_packages
				service.loadSettingsApi().then(function (res) {

				    //alert(res.read_only_packages);
				    service.$storage.read_only_packages = false;
				    if (res.read_only_packages && null != res.transaction && res.transaction.Transaction.id != "") {
				        // populate $storage packages with passed packages
				        service.$storage.quickship.packages = [];
				        for (i = 0; i < res.transaction.Shipment.length; i++) {
				            service.$storage.quickship.packages.push(
						        {
						            tracking_code: res.transaction.Shipment[i].Package.in_tracking,
                                    consolidated: false,
						            carrier: res.transaction.Shipment[i].Package.carrier,
						            weight_unit: (res.transaction.Shipment[i].Package.weight_lb != "") ? "lb" : "kg",
                                    size_unit: (res.transaction.Shipment[i].Package.width_in != "") ? "in" : "cm",
                                    weight: (res.transaction.Shipment[i].Package.weight_lb != "") ? res.transaction.Shipment[i].Package.weight_lb : res.transaction.Shipment[i].Package.weight_in,
                                    width: (res.transaction.Shipment[i].Package.width_in != "") ? res.transaction.Shipment[i].Package.width_in : res.transaction.Shipment[i].Package.width_cm,
                                    height: (res.transaction.Shipment[i].Package.height_in != "") ? res.transaction.Shipment[i].Package.height_in : res.transaction.Shipment[i].Package.height_cm,
                                    length: (res.transaction.Shipment[i].Package.length_in != "") ? res.transaction.Shipment[i].Package.length_in : res.transaction.Shipment[i].Package.length_cm,
                                    items: [{ "description": "", "weight_unit": "lb", "price_value": null, "info": "" }]
						        });
				        }
				    }

                    if (null != res.user && res.user.User.id != "") {
                        // load Registered User info
                        service.$storage.billing.first_name = res.user.User.first_name;
                        service.$storage.billing.last_name = res.user.User.last_name;
                        service.$storage.billing.email = res.user.User.email;
                        service.$storage.billing.phone = res.user.User.phone;

                        var billing_index = null;
                        var shipping_index = null;
                        if (res.user.Address.length === 1) {
                            billing_index = 0;
                            shipping_index = null;
                        }
                        else if (res.user.Address.length > 1) {
                            if (res.user.Address[0].address_type_id === 2) {
                                billing_index = 0;
                                shipping_index = 1;
                            }
                            else {
                                shipping_index = 0;
                                billing_index = 1;
                            }

                            // look for Default Billing and Default Shipping
                            for (i = 0; i < res.user.Address.length; i++) {
                                if (res.user.Address[i].default_billing == 1)
                                    billing_index = i;
                                if (res.user.Address[i].default_shipping == 1)
                                    shipping_index = i;
                            }

                        }
                        if (null != billing_index) {
                            service.$storage.billing.address1 = res.user.Address[billing_index].address1;
                            service.$storage.billing.address2 = res.user.Address[billing_index].address2;
                            service.$storage.billing.city = res.user.Address[billing_index].city;
                            service.$storage.billing.state = res.user.Address[billing_index].adm_division;
                            service.$storage.billing.postal_code = res.user.Address[billing_index].postal_code;
                            service.$storage.billing.country = res.user.Address[billing_index].country_id;

                            service.$storage.billing_shipping = (shipping_index == null);

                            if (null != shipping_index) {
                                service.$storage.shipping.first_name = res.user.Address[shipping_index].firstname;
                                service.$storage.shipping.last_name = res.user.Address[shipping_index].lastname;
                                service.$storage.shipping.address1 = res.user.Address[shipping_index].address1;
                                service.$storage.shipping.address2 = res.user.Address[shipping_index].address2;
                                service.$storage.shipping.city = res.user.Address[shipping_index].city;
                                service.$storage.shipping.state = res.user.Address[shipping_index].adm_division;
                                service.$storage.shipping.postal_code = res.user.Address[shipping_index].postal_code;
                                service.$storage.shipping.country = res.user.Address[shipping_index].country_id;
                                // there is currently no way for the user to specify a Shipping Email and Phone so use Billing
                                service.$storage.shipping.email = res.user.User.email;
                                service.$storage.shipping.phone = res.user.User.phone;
                            }
                        }


                    }


                    service.$storage.countries = res.countries;
                    service.$storage.itemTypes = res.item_types;
                    service.$storage.handling_fee = res.handling_fee;
                    service.settingsLoaded = true;
                    $rootScope.$broadcast('event:loaded-api');
				}, function (err) {
				    // error
				    alert("Error occurred: " + err);
				});
			};

			/**
			 * Calls the backend API to fetch required app settings
			 * @returns {*}
			 */
			service.loadSettingsApi = function () {
				var defer = $q.defer();

				$http.post(service.configure.base_url + 'quickship/api_settings.json', null)
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 * Save personal information data after step 1. Required to calculate shipping rates and keep session data
			 * @returns {*}
			 */
			service.updatePersonalInfo = function () {
				var defer = $q.defer();
				$http.post(service.configure.base_url + 'quickship/step1.json', service.$storage)
					.success(function (res) {
						defer.resolve(res);
					})
					.error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 *
			 * @returns {Number}
			 */
			service.addPackage = function () {
				return service.$storage.quickship.packages.push({
					consolidated: [],
					weight_unit: 'lb',
					size_unit: 'in',
					items: [
						{
							description: '',
							weight_unit: 'lb',
							price_value: null,
							info: ''
						}
					]
				})
			};

			/**
			 *
			 * @param package_index
			 */
			service.addConsolidatedPackage = function (package_index) {
				var currentItemsNum = service.$storage.quickship.packages[package_index].consolidated.length;
				var defaultData = {
					weight: null,
					unit: 'lb'
				};
				if (currentItemsNum < 5) {
					service.$storage.quickship.packages[package_index].consolidated.push(defaultData);
				} else {
					service.addPackage();
					service.$storage.quickship.packages[service.$storage.quickship.packages.length - 1].consolidated.push(defaultData);
				}
			};

			/**
			 * Remove consolidated packages on simple steps
			 */
			service.cleanupConsolidated = function () {
				angular.forEach(service.$storage.quickship.packages, function (value, index) {
					service.$storage.quickship.packages[index].consolidated = [];
				});
			};

			/**
			 *
			 */
			service.updateConsolidated = function () {
				if (service.$storage.quickship.consolidatePackages === false) {
					service.cleanupConsolidated();
				}
			};

			/**
			 * Load shipping rates using EasyPost API
			 * @returns {*}
			 */
			service.loadRates = function () {
				var defer = $q.defer();
				service.$storage.shippingRates = [];
				service.$storage.shippingRatesSelected = [];
				$http.post(service.configure.base_url + 'pages/shipping-calculator', service.$storage)
					.success(function (res) {
						if (typeof res == 'object') service.$storage.shippingRates = res;
						defer.resolve(res);
					}).
					error(function (err) {
						defer.reject(err);
					});

				return defer.promise;
			};

			/**
			 * Handle payment
			 */
			service.pay_handling = function () {

				var params = {
					description: "Package handling fee",
					price: service.$storage.handling_fee,
					steps: (service.simpleSteps === true) ? 'simple' : 'full',
					shipment: service.$storage
				};

				service.processingPayment = true;

				$http.post(service.configure.base_url + 'quickship/payment.json', params).success(function (res) {
					if (res.ok) {
						window.location = res.approval_link;
					} else {
						alert("Error, please try again");
						service.processingPayment = false;
					}
				}).error(function (err) {
					alert("Error, please try again");
					service.processingPayment = false;
				});
			};

			/**
			 *
			 * @param key
			 * @param selected_id
			 * @returns {boolean}
			 */
			service.isSelectedItem = function (key, selected_id) {
				if (key == selected_id) return true;
				return false;
			};

			/**
			 *
			 * @param amount
			 * @returns {*}
			 */
			service.getSimpleStepsAmountMin = function (amount) {
				if (service.$storage.simpleSteps === true) {
					return amount;
				}
				return false;
			};

			/**
			 *
			 * @param package_index
			 * @param shipping_rate
			 */
			service.selectService = function (package_index, shipping_rate) {
				service.$storage.shippingRatesSelected[package_index] = {
					package: package_index,
					shipping_rate_id: shipping_rate.rate_id,
					shipment_id: shipping_rate.shipment_id,
					rate: shipping_rate,
					price: shipping_rate.rate,
					price_num: shipping_rate.rate_num
				};

                // are there more packages?
				if (service.$storage.quickship.packages.length > (package_index + 1)) {
				    // scroll to the next package
				    document.getElementById('divPackage' + (package_index + 1)).scrollIntoView();
				    angular.element($(".step-4")).scope().currentRateGroup = package_index + 1;
				}
				else { // show Next button
				    $('.btn-success')[0].scrollIntoView();
					}
			};

			/**
			 *
			 * @returns {*}
			 */
			service.getShippingCharge = function () {
				if (service.$storage.simpleSteps === true) {
					return service.$storage.handling_fee;
				}
				var total = 0;
				if (service.$storage.shippingRatesSelected.length > 0) {
					angular.forEach(service.$storage.shippingRatesSelected, function (value) {
						var price = parseFloat(value.price.replace(' USD', ''));
						total += price;
					});
				}
				if (service.$storage.quickship.packagesInsurance === true) {
					total += (total * 0.01)
				}
				return total;
			};

			/**
			 * Returns true if it has consolidated packages, false otherwise
			 * @returns {boolean}
			 */
			service.hasConsolidatedPackages = function (index) {
				var has_consolidated = false;
				if (typeof service.$storage.quickship.packages[index] !== 'undefined'
					&& typeof service.$storage.quickship.packages[index].consolidated !== 'undefined'
					&& service.$storage.quickship.packages[index].consolidated.length > 0) {
					has_consolidated = true;
				}
				return has_consolidated;
			};

			/**
			 * Resets everything except for personal information
			 */
			service.cleanup = function () {
				var personal_info = service.$storage.user;
				var billing = service.$storage.billing;
				var shipping = service.$storage.shipping;
				service.reset();
				service.$storage.user = personal_info;
				service.$storage.billing = billing;
				service.$storage.shipping = shipping;
			};

			/**
			 *
			 * @returns {number}
			 */
			service.calculateHandlingFee = function () {
				var amount = 0;

				angular.forEach(service.$storage.quickship.packages, function (value, index) {
					amount++;
				});

                // Charge for the # of Packages shipped from HMH (may be consolidated packages)
				return service.$storage.handling_fee * amount;
			};

			/**
			 * Converts between kilograms and pounds
			 * @param orig_weight
			 * @param orig_weight_unit
			 * @param dest_weight_unit
			 */
			service.calculateWeight = function (orig_weight, orig_weight_unit, dest_weight_unit) {
				if (!parseFloat(orig_weight)) return 0;
				orig_weight = parseFloat(orig_weight);
				if (orig_weight_unit === dest_weight_unit) return orig_weight;
				var new_amount = (dest_weight_unit === 'lb') ? orig_weight * 2.2046 : orig_weight / 2.2046;

				// Round up to 2 decimals
				new_amount = +(Math.round(new_amount + "e+" + 2) + "e-" + 2);
				return new_amount;
			};

			/**
			 *
			 */
			service.reset = function () {
				/*delete service.$storage.user;
				 delete service.$storage.quickship;
				 delete service.$storage.shippingRates;
				 delete service.$storage.shippingRatesSelected;
				 delete service.$storage.shipping;
				 delete service.$storage.billing;
				 delete service.$storage.simpleSteps;*/
				service.$storage.$reset();
				service.$storage = $localStorage.$default(service.defaultOptions);
			};

			return service;

		}]
);
