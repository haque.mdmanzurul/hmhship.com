var app = angular.module('calculator', [
	'ngRoute',
	'ngResource',
	'ngStorage',
	'ngAnimate',
	'ui.bootstrap',
	'toggle-switch',
	'ngSanitize',
	'ngTagsInput',
	'ngDialog'
]);

angular.module('calculator')
	.config(
	['$routeProvider', '$httpProvider', '$anchorScrollProvider', 'tagsInputConfigProvider', '$locationProvider',
		function ($routeProvider, $httpProvider, $anchorScrollProvider, tagsInputConfigProvider, $locationProvider) {
			$routeProvider
                .when('/shipping-calculator', {
                    title: 'Package Details',
                    templateUrl: window.Configure.base_url + 'FRONTEND/js/calculator/package_details.html',
                    controller: 'PackageDetailsCtrl'
                }).when('/calculator-rates', {
					title: 'Shipment Rates',
					templateUrl: window.Configure.base_url + 'FRONTEND/js/calculator/shipment_rates.html',
					controller: 'ShipmentRatesCtrl'
            	}).otherwise({
					redirectTo: '/shipping-calculator'
				});

			// enable http caching
			$httpProvider.defaults.cache = false;

			// set headers for request detection
			$httpProvider.defaults.headers.common["X-From-Angularjs"] = true;

			$anchorScrollProvider.disableAutoScrolling();

			tagsInputConfigProvider
				.setDefaults('tagsInput', {
					minLength: 1
				});

			$locationProvider.html5Mode(true);
		}
	]
);

//angular.module('calculator').constant('Configure', typeof Configure === 'undefined' ? null : Configure);

angular.module('calculator')
	.controller('MainCtrl', ['$scope', '$rootScope', '$window', 'QuickshipSvc',
		function ($scope, $rootScope, $window, QuickshipSvc) {

			$scope.title = 'MainCtrl';

			$scope.Configure = $window.Configure;

			/**
			 *
			 * @type {QuickshipSvc|*}
			 */
			$scope.quickship = QuickshipSvc;
            $scope.quickship.$storage.simpleSteps = false;

			/**
			 * Used to detect if the application is executed for the first time
			 * @type {boolean}
			 */
			$rootScope.firstLoad = true;

			/**
			 *
			 */
			$scope.init = function () {
				$scope.quickship.configure = $scope.Configure;
				if ($rootScope.firstLoad) {
					//$scope.quickship.cleanup(); // TODO: uncomment on production
				}
				$scope.quickship.loadSettings();
				$rootScope.firstLoad = false;
			};

			$scope.init();

		}]);

/**
 * Personal details
 */
angular.module('calculator')
	.controller('PersonalCtrl', ['$scope', '$location', '$window',
		function ($scope, $location, $window) {

			/**
			 *
			 * @type {string}
			 */
			$scope.title = 'PersonalCtrl';

			/**
			 *
			 * @type {number}
			 */
			$scope.$parent.quickship.currentStep = 1;

			/**
			 *
			 * @param simpleSteps
			 * @param valid
			 * @returns {boolean}
			 */
			$scope.goNext = function (theForm) {
			    if (theForm.$valid === false) {
					$scope.$parent.quickship.validatesPersonalDetails = false;
					$('input[name="'+$scope.personalFrm.$error.required[0].$name+'"').focus();
					return false;
				} else {
					$scope.$parent.quickship.validatesPersonalDetails = true;
				}

				$scope.$parent.quickship.updatePersonalInfo().then(function (res) {
					$scope.$parent.quickship.$storage.personalDetailsCompleted = true;
					return $location.path('/additional-options');
				}, function (err) {
				    alert("Error occurred: " + err);
				});

			};

			/**
			 *
			 */
			$scope.init = function () {
				$window.scrollTo(0, 0);
			};

			$scope.init();
		}]);

/**
 * Additional options
 */
angular.module('calculator')
	.controller('AdditionalOptionsCtrl', ['$scope', '$location', '$window',
		function ($scope, $location, $window) {

			$scope.title = 'AdditionalOptionsCtrl';

			$scope.$parent.quickship.currentStep = 2;

			$scope.goPrev = function () {
				return $location.path('/');
			};

			$scope.goNext = function (simpleSteps) {

			    $scope.$parent.quickship.$storage.simpleSteps = simpleSteps;

			    // reset packages
			    $scope.$parent.quickship.$storage.quickship.amount = 1;
			    $scope.$parent.quickship.$storage.quickship.packages = [
                    {
                        tracking_code: '',
                        carrier: '',
                        consolidated: [],
                        weight_unit: 'lb',
                        size_unit: 'in',
                        items: [
                            {
                                description: '',
                                weight_unit: 'lb',
                                price_value: null,
                                info: ''
                            }
                        ]
                    }
			    ];


                return $location.path('/package-details');

			};

			$scope.init = function () {
				/*if ($scope.$parent.quickship.$storage.simpleSteps === null) {
					$location.path('/');
					return true;
				}
				$window.scrollTo(0, 0);*/
			};

			$scope.init();

		}]);

/**
 * Packages details
 */
angular.module('calculator')
	.controller('PackageDetailsCtrl', ['$scope', '$location', '$window', 'ngDialog', 'QuickshipSvc',
		function ($scope, $location, $window, ngDialog, QuickshipSvc) {

			$scope.title = 'PackageDetailsCtrl';

			$scope.$parent.quickship.currentStep = 3;

			$scope.countriesOfOrigin = [];

			$scope.packagesFrmData = $scope.$parent.quickship.$storage.quickship.packages;

			//console.log($scope.packagesFrmData)

			/**
			 *
			 * @type {number}
			 */
			$scope.activeDetailTab = 0;

			$scope.validationStarted = false;


			/**
			 *
			 */
			$scope.getWeightTitle = function () {
				return ($scope.$parent.quickship.consolidatePackages === true) ? 'Total Weight *' : 'Weight *';
			};

			/**
			 *
			 * @returns {*}
			 */
			$scope.goNext = function (frm) {

			    $scope.validationStarted = true;

				// TODO: check if all packages weight are valid and above 0
				if (frm.$valid === false) {
					$scope.$parent.quickship.validatesPackageDetails = false;
					return false;
				}

				if ($scope.validateWeights() === false) {
					$scope.$parent.quickship.validatesPackageDetails = false;
					return false;
				}

				$scope.$parent.quickship.validatesPackageDetails = true;

				return $location.path('/calculator-rates');

			};

			/**
			 *
			 * @returns {*}
			 */
			$scope.goBack = function () {

				return $location.path('/shipping-calculator');

			};

			/**
			 *
			 * @param form
			 * @param index
			 * @returns {boolean}
			 */
			$scope.hasValidWeight = function (form, index) {
				if ($scope.$parent.quickship.$storage.quickship.consolidatePackages) return true;
				if ($scope.validationStarted === false) return true;
				if (Math.round($scope.$parent.quickship.$storage.quickship.packages[index].weight)  > 0) return true;
				if ($scope.$parent.quickship.$storage.quickship.packages[index].weight < 0.01) return false;
				if (form['package_' + index + '_weight'].$error.required) return false;
			};


            /**
             *
             * @param form
             * @param index
             * @returns {boolean}
             */
            $scope.$watch('packagesFrmData', function() {

                for(var i=0; i < $scope.$parent.quickship.$storage.quickship.packages.length; i++) {

                    if ($scope.$parent.quickship.$storage.quickship.packages[i].weight === 'undefined' ||
                        parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].weight) < 0.5)
                        $scope.$parent.quickship.$storage.quickship.packages[i].weight = null;

                    if ($scope.$parent.quickship.$storage.quickship.packages[i].length === 'undefined' ||
                        parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].length) < 0.5)
                        $scope.$parent.quickship.$storage.quickship.packages[i].length = null;

                    if ($scope.$parent.quickship.$storage.quickship.packages[i].width === 'undefined' ||
                        parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].width) < 0.5)
                        $scope.$parent.quickship.$storage.quickship.packages[i].width = null;

                    if ($scope.$parent.quickship.$storage.quickship.packages[i].height === 'undefined' ||
                        parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].height) < 0.5)
                        $scope.$parent.quickship.$storage.quickship.packages[i].height = null;




                	//For items
                    for(var j = 0; j < $scope.$parent.quickship.$storage.quickship.packages[i].items.length; j++) {
                    	 if ($scope.$parent.quickship.$storage.quickship.packages[i].items[j].weight === 'undefined' ||
                             parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].items[j].weight) < 0.5)
                            $scope.$parent.quickship.$storage.quickship.packages[i].items[j].weight = null;

                        if ($scope.$parent.quickship.$storage.quickship.packages[i].items[j].quantity === 'undefined' ||
                            parseFloat($scope.$parent.quickship.$storage.quickship.packages[i].items[j].quantity) < 0.5)
                            $scope.$parent.quickship.$storage.quickship.packages[i].items[j].quantity = null;
                    }
                }

            }, true);

			/**
			 *
			 * @returns {boolean}
			 */
			$scope.validateWeights = function () {
				var is_valid = true;
				if ($scope.$parent.quickship.$storage.simpleSteps === true) return true;
				if ($scope.$parent.quickship.$storage.quickship.consolidatePackages) return true;
				angular.forEach($scope.$parent.quickship.$storage.quickship.packages, function (item) {
					if (parseFloat(item.weight) < 0.01) return false;
				});
				return is_valid;
			};

			/**
			 *
			 * @param package_index
			 * @param index
			 */
			$scope.deleteItem = function (package_index, index) {
				$scope.$parent.quickship.$storage.quickship.packages[package_index].items.splice(index, 1);
			};

			/**
			 *
			 * @param index
			 */
			$scope.addItem = function (index) {
				$scope.$parent.quickship.$storage.quickship.packages[index].items.push({
					description: '',
					weight_unit: 'lb',
					price_value: null
				});
			};

			/**
			 *
			 * @returns {Number}
			 */
			$scope.addPackage = function () {
				return $scope.$parent.quickship.addPackage();
			};

			/**
			 *
			 */
			$scope.updateSimplePackages = function () {
				$scope.$parent.quickship.updateSimplePackages();
				$scope.activeDetailTab = 0;
			}

			/**
			 *
			 * @param package_index
			 */
			$scope.deletePackage = function (package_index) {
				$scope.$parent.quickship.$storage.quickship.packages.splice(package_index, 1);
				$scope.activeDetailTab = 0;
			};

			/**
			 * Set active tab for package items
			 * @param tab_index
			 */
			$scope.setActiveTab = function (tab_index) {
				$scope.activeDetailTab = tab_index;
			};

			/**
			 * Checks and highlights a tab for package items with invalid data
			 * @param package_index
			 * @param form
			 * @returns {boolean}
			 */
			$scope.hasInvalidTabData = function (package_index, form) {
				var count_items = $scope.$parent.quickship.$storage.quickship.packages[package_index].items.length;
				var hasErrors = false;
				for (var i = 0; i < count_items; i++) {
					if (typeof form['packages_' + package_index + '_items_' + i + '_country_id'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_country_id'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_description'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_description'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_quantity'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_quantity'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_type_id'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_type_id'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_value'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_value'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_weight'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_weight'].$invalid === true) hasErrors = true;
					if (typeof form['packages_' + package_index + '_items_' + i + '_weight_unit'] !== 'undefined' && form['packages_' + package_index + '_items_' + i + '_weight_unit'].$invalid === true) hasErrors = true;
				}

				return hasErrors;
			};

			/**
			 *
			 * @param package_index
			 */
			$scope.addConsolidatedPackage = function (package_index) {
				return $scope.$parent.quickship.addConsolidatedPackage(package_index);
			};

			/**
			 *
			 * @param package_index
			 * @param index
			 */
			$scope.deleteConsolidatedPackage = function (package_index, index) {
				$scope.$parent.quickship.$storage.quickship.packages[package_index].consolidated.splice(index, 1);
				$scope.updateWeight(package_index);
			};

			/**
			 *
			 * @param package_index
			 */
			$scope.updateWeight = function (package_index) {
				var sum_weight = 0;
				var sum_weight_unit = $scope.$parent.quickship.$storage.quickship.packages[package_index].weight_unit;
				angular.forEach($scope.$parent.quickship.$storage.quickship.packages[package_index].consolidated, function (value, index) {
					if (parseFloat(value.weight)) {
						sum_weight += $scope.$parent.quickship.calculateWeight(value.weight, value.unit, sum_weight_unit);
					}
				});
				$scope.$parent.quickship.$storage.quickship.packages[package_index].weight = sum_weight;
			};


			/**
			 * Constructor
			 * @returns {*}
			 */
			$scope.init = function () {
				if ($scope.$parent.quickship.$storage.personalDetailsCompleted === false) {
					//return $location.path('/');
				}
				if (typeof $scope.$parent.quickship.$storage.countries === 'undefined' || $scope.$parent.quickship.$storage.countries === null) {
					$scope.$parent.quickship.loadSettings();
				}

			    // make sure $storage.quickship.packages.length == $storage.quickship.amount
				$scope.updateSimplePackages();

				$window.scrollTo(0, 0);
			};

			$scope.init();

		}]);

angular.module('calculator')
	.controller('ShipmentRatesCtrl', ['$scope', '$location', '$window',
		function ($scope, $location, $window) {

			/**
			 * Controllers name (for internal use)
			 * @type {string}
			 */
			$scope.title = 'ShipmentRatesCtrl';

			/**
			 *
			 * @type {number}
			 */
			$scope.currentRateGroup = 0;

			/**
			 *
			 * @type {null}
			 */
			$scope.orderBy = null;

			/**
			 *
			 * @type {boolean}
			 */
			$scope.orderByReverse = false;

			/**
			 * Current step
			 * @type {number}
			 */
			$scope.$parent.quickship.currentStep = 4;

			/**
			 * True if loading shipping rates from backend, false otherwise
			 * @type {boolean}
			 */
			$scope.loadingRates = false;

			/**
			 * Display only one item at a time in accordion for shipping rates
			 * @type {boolean}
			 */
			$scope.oneAtTime = false;

			/**
			 * Show first item as open in accordion for shipping rates
			 * @type {boolean}
			 */
			$scope.isFirstOpen = true;

			/**
			 * Load shipping rates
			 */
			$scope.loadRates = function () {

				//if ($scope.$parent.quickship.$storage.quickship.packages[0].items[0].price_value === null) return $location.path('/');

				$scope.loadingRates = true;

				$scope.$parent.quickship.loadRates().then(function () {
					$scope.loadingRates = false;
				}, function () {
					alert('ERROR: Unable to get shipping rates. Try again in a few seconds');
					$scope.loadingRates = false;
					return $location.path('/package-details');
				});
			};

			/**
			 *
			 * @returns {*}
			 */
			$scope.goNext = function () {
				return $location.path('/payment-options');
			};

			/**
			 *
			 * @returns {*}
			 */
			$scope.goBack = function () {

			    if ($scope.$parent.quickship.$storage.simpleSteps && $scope.$parent.quickship.$storage.quickship.consolidatePackages) {
			        // Customer does NOT know dimensions and the Packages array has been Consolidated
			        // put the array back the way it was input for editing
			        $scope.$parent.quickship.$storage.quickship.packages = $scope.$parent.quickship.$storage.quickship.small_packages;
			    }


				return $location.path('/shipping-calculator');

			};

			/**
			 * Constructor
			 * @returns {*}
			 */
			$scope.init = function () {

				$scope.loadRates();

				$window.scrollTo(0, 0);
			};

			$scope.init();
		}]);

/**
 *
 */
angular.module('calculator')
	.controller('PaymentOptionsCtrl', ['$scope', '$location', '$window', 'ngDialog',
		function ($scope, $location, $window, ngDialog) {

			/**
			 * @type {string}
			 */
			$scope.title = 'PaymentOptionsCtrl';

			/**
			 *
			 * @type {number}
			 */
			$scope.$parent.quickship.currentStep = 5;

			/**
			 * Opens up the terms and conditions dialog
			 */
			$scope.openTermsConditionsDialog = function () {
				ngDialog.open({
					template: window.Configure.base_url + 'FRONTEND/js/modules/terms_and_conditions.html',
					className: 'ngdialog-theme-default custom-width'
				});
			};

			/**
			 *
			 * @returns {*}
			 */
			$scope.goBack = function () {
				if ($scope.$parent.quickship.$storage.simpleSteps) {
					return $location.path('/package-details');
				} else {
					return $location.path('/shipment-rates');
				}
			};

			$scope.init = function () {
				if ($scope.$parent.quickship.$storage.quickship.packages[0].items[0].price_value === null) return $location.path('/');
				$window.scrollTo(0, 0);
			};

			$scope.init();

		}]);

angular.module('calculator').controller('YourUsAddressCtrl', ['$scope', '$window',
	function ($scope, $window) {

		/**
		 *
		 * @type {string}
		 */
		$scope.name = 'YourUsAddressCtrl';

		/**
		 *
		 * @type {number}
		 */
		$scope.$parent.quickship.currentStep = 6;

		/**
		 *
		 */
		$scope.init = function () {
			$scope.$parent.quickship.address.first_name = $scope.$parent.quickship.$storage.user.first_name;
			$scope.$parent.quickship.address.last_name = $scope.$parent.quickship.$storage.user.last_name;
			$scope.$parent.quickship.address.address1 = $scope.$parent.quickship.$storage.address1;
			$scope.$parent.quickship.address.address2 = $scope.$parent.quickship.$storage.address2;
			$scope.$parent.quickship.address.address3 = $scope.$parent.quickship.$storage.address3;
			$scope.$parent.quickship.reset();
			$window.scrollTo(0, 0);
		};

		$scope.init();
	}]);


function isEmpty(value) {
	return angular.isUndefined(value) || value === '' || value === null || value !== value;
}

angular.module('calculator').directive('ngMin', function () {
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function (scope, elem, attr, ctrl) {
			scope.$watch(attr.ngMin, function () {
				ctrl.$setViewValue(ctrl.$viewValue);
			});
			var minValidator = function (value) {
				var min = scope.$eval(attr.ngMin) || 0;
				if (!isEmpty(value) && value < min) {
					ctrl.$setValidity('ngMin', false);
					return undefined;
				} else {
					ctrl.$setValidity('ngMin', true);
					return value;
				}
			};

			ctrl.$parsers.push(minValidator);
			ctrl.$formatters.push(minValidator);
		}
	};
});



        /**
 * Directive to add special effect for placeholders/labels on input fields
 */
angular.module('calculator').directive('magicField', function ($compile, $timeout) {
	return {
		scope: {},
		template: '<div class="field"></div>',
		transclude: true,
		replace: true,
		restrict: 'E',
		controller: function ($scope, $element) {
			$scope.showLabel = false;
		},
		link: function (scope, element, attrs, ctrl, transclude) {
			transclude(scope, function (clone, scope) {
				element.append(clone);
			});
			var input = element.find('input')[0] || element.find('textarea')[0] || element.find('select')[0];
			var label = element.find('label')[0];
			input = angular.element(input);
			input.on('focus blur keyup', function (e) {
				if (e.type == 'focus') {
					scope.showLabel = true;
					input.attr('placeholder', '');
				} else if (e.type === 'blur') {
					scope.showLabel = true;
					//input.attr('placeholder', label.innerHTML);
				} else {
					if (input.val().length > 0) {
						scope.showLabel = true;
						input.attr('placehoder', '');
					} else {
						scope.showLabel = true;
						//input.attr('placeholder', label.innerHTML);
					}
				}

				$timeout(function () {
					scope.$apply();
				}, 0);

			});
		}
	}
});

/**
 * Directive for phone numbers, to prefix always with a plus sign
 */
angular.module('calculator').directive('plusPrefix', function () {
	return {
		restrict: 'A',
		scope: {
			ngModel: '='
		},
		link: function (scope, element, attrs, controller) {
			element.bind('keyup change focus', function () {
				scope.$apply(function () {
					if (typeof scope.ngModel === 'undefined') scope.ngModel = '';
					scope.ngModel = scope.ngModel.replace('+', '');
					if (scope.ngModel.length >= 0) {
						scope.ngModel = '+' + scope.ngModel.replace(/[^\d]/g, '');
					}
				});
			});
		}
	};
});

/**
 * Directive for masking money
 */
angular.module('calculator').directive('moneyPrefix', function () {
	return {
		restrict: 'A',
		scope: {
			ngModel: '='
		},
		link: function (scope, element, attrs, controller) {
			element.bind('keyup change focus', function () {
				scope.$apply(function () {
					if (typeof scope.ngModel === 'undefined' || scope.ngModel == null) {
						scope.ngModel = '';
						return false;
					}
					scope.ngModel = scope.ngModel.replace('$', '');
					if (scope.ngModel.length >= 0) {
						scope.ngModel = '$' + scope.ngModel.replace(/[^\d.]/g, '');
					}
				});
			});
		}
	};
});

/**
 * Small directive to allow dynamic names on name property for input fields
 */
angular.module('calculator').directive('dynamicName', function ($compile, $parse) {
	return {
		restrict: 'A',
		terminal: true,
		priority: 100000,
		link: function (scope, elem) {
			var name = $parse(elem.attr('dynamic-name'))(scope);
			// $interpolate() will support things like 'skill'+skill.id where parse will not
			elem.removeAttr('dynamic-name');
			elem.attr('name', name);
			$compile(elem)(scope);
		}
	};
});

/**
 * Small directive to check some field value if less than .5
 */


// Workaround for bug #1404
// https://github.com/angular/angular.js/issues/1404
// Source: http://plnkr.co/edit/hSMzWC?p=preview
/**
 * Required configuration service for handling dynamic names on form fields (for validations)
 */
angular.module('calculator').config(['$provide', function ($provide) {
	$provide.decorator('ngModelDirective', function ($delegate) {
		var ngModel = $delegate[0], controller = ngModel.controller;
		ngModel.controller = ['$scope', '$element', '$attrs', '$injector', function (scope, element, attrs, $injector) {
			var $interpolate = $injector.get('$interpolate');
			attrs.$set('name', $interpolate(attrs.name || '')(scope));
			$injector.invoke(controller, this, {
				'$scope': scope,
				'$element': element,
				'$attrs': attrs
			});
		}];
		return $delegate;
	});
	$provide.decorator('formDirective', function ($delegate) {
		var form = $delegate[0], controller = form.controller;
		form.controller = ['$scope', '$element', '$attrs', '$injector', function (scope, element, attrs, $injector) {
			var $interpolate = $injector.get('$interpolate');
			attrs.$set('name', $interpolate(attrs.name || attrs.ngForm || '')(scope));
			$injector.invoke(controller, this, {
				'$scope': scope,
				'$element': element,
				'$attrs': attrs
			});
		}];
		return $delegate;
	});
}]);
//jQuery("dl.accordion dt").click(function(){jQuery(this).toggleClass('active');})