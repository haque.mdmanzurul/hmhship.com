(function(){
	$(document).ready(function(){
		var funcs = {
			init : function(){
				funcs.initPlugins();
				funcs.showPackagesList();
				funcs.middlePrice();
				funcs.validateFormFront();
				funcs.maxColMission();
				funcs.maxColPhilosophy();

				$(window).resize(function(){
					funcs.middlePrice();
					funcs.maxColMission();
					funcs.maxColPhilosophy();
				});

				$(window).load(function(){
					funcs.showAfterLoad();
					funcs.maxColMission();
					funcs.maxColPhilosophy();
				});
			},
			initPlugins : function(){
				$('.customCheckbox').kalypto({toggleClass:'wmcheckbox'});
				$('.customSelect').customSelect();
				$('input, textarea').placeholder();
			},
			showAfterLoad : function(){
				$('.show-after-load').fadeIn(600, function(){
					$('.box-plane').animate({
						'left' : '-275px'
					}, 500);
				});
			},
			showPackagesList : function(){
				$('.show-packages-list').on('click', function(){
					var t = $(this);
					var p = t.parent().find('.packages-list');
					if(t.hasClass('active'))
					{
						t.removeClass('active');
						p.stop().slideUp();
					}
					else
					{
						t.addClass('active');
						p.stop().slideDown();
					}
					return false;
				});
			},
			middlePrice : function(){
				$('.wm-packages .price').each(function(){
					var t = $(this);
					var target = t.parent().parent().find('.text');
					t.css({
						'height' : target.outerHeight()+'px'
					});
				});
			},
			validateFormFront : function(){
				$('.form-valid').wmValidate({
					selectors : {
						submit : 'btn-send'
					},
					callbacks : {
						CB_Ok : function(){
							$(this.element).submit();
						}
					}
				});
			},
			maxColMission : function(){
				var arr = [], cont = 0;
				$('.info-mission .getmaxcol .content').each(function(){
					arr[cont] = $(this).outerHeight();
					cont++;
				});
				var colMax = Math.max.apply(Math,arr);
				$('.info-mission .getmaxcol').css({
					'height' : colMax+'px'
				});
			},
			maxColPhilosophy : function(){
				var arr = [], cont = 0;
				$('.info-philosophy .getmaxcol .content').each(function(){
					arr[cont] = $(this).outerHeight();
					cont++;
				});
				var colMax = Math.max.apply(Math,arr);
				$('.info-philosophy .getmaxcol').css({
					'height' : colMax+'px'
				});
			}
		};
		funcs.init();
	});
})();