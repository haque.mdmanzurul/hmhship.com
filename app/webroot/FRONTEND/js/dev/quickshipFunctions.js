var hmhship = {};

(function (quickshipUi) {
    var indAddPackage = 2;
    var total3por1 = 1;

    $(document).ready(function () {
        // init slider

        quickshipUi.init();

        var funcs = {

            // constructor
            init: function () {
                funcs.addDeleteMorePackages_2();
                funcs.addDeleteMorePackages_3();
                funcs.addDeletePkgDetail_3();
                funcs.addDeleteTrackCode_3();

                funcs.stepsNavigation();

                funcs.interactiveStep_1();
                funcs.interactiveStep_2();
                funcs.interactiveStep_3();
                funcs.interactiveStep_5();

                funcs.chooseShip();

                quickshipUi.handleLabelEffects();

                hmhship.packages.init();

                $(window).resize(function () {
                });
                $(window).load(function () {
                });

                quickshipUi.handleToggles();
                quickshipUi.handleDropdowns();

                // binding paypal button to paypal payment process
                $("#paypal_pay").click(hmhship.payment.paypal.pay_handling);

                if (window.ACTIVE_STEPS) {
                    $("." + ACTIVE_STEPS).show();
                }
            },
            /*
             * navegacao entre os passos, tambem ocorre a validacao
             */
            stepsNavigation: function () {
                //$('.hideLoad').show();
                //$('.showLoad').hide();
                //quickshipUi.slider.goToSlide(1);

                // go to step 1
                $('.go-to-slide-1').on('click', function () {

                    // hide steps menu
                    $('.hardSteps').hide();
                    $('.simpleSteps').hide();

                    quickshipUi.slider.goToSlide(0);
                    funcs.setWizardActive(1);

                    //$('.wm3por1-added').remove();

                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                });

                // go from step 1 to step 2 or step 3 and validade step 1
                funcs._validateSetp_1();

                // go from step 1 to step 3 and validate step 2
                funcs._validateSetp_2();

                // go from step 3 to step 4 and validate step 3
                funcs._validateSetp_3();

                // go from step 3 to step 2
                $('.step-3 .go-to-slide-2').on('click', function () {
                    quickshipUi.slider.goToSlide(1);
                    funcs.setWizardActive(2);
                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                });

                // go from step 4 to step 3
                $('.step-4 .go-to-slide-3').on('click', function () {
                    quickshipUi.slider.goToSlide(2);
                    funcs.setWizardActive(3);
                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                });

                // go from step 4 to step 5
                $('.step-4 .go-to-slide-5').on('click', function () {
                    var estimated_shipping_cost = 0;
                    var total_handling_fee = 0;
                    var allSelected = true;

                    $('.panel').removeClass('error');
                    $('.panel-group .title-selected').each(function () {
                        var t = $(this);
                        var ind = $('.panel-group .title-selected').index(t);
                        if (t.text() === '') {
                            $('.panel').eq(ind).addClass('error');
                            allSelected = false;
                        }
                        else {
                            estimated_shipping_cost += parseFloat(t.data("price"));
                            total_handling_fee += Configure.handling_fee;
                        }
                    });

                    /*var price = $('.panel-group').find('td.price.selected span');
                     var vP = 0;
                     price.each(function(){
                     var t = $(this);
                     vP = vP+parseFloat(t.text());
                     });
                     var totalPrice = vP+7.25;
                     $('.s5_valuePrice').html('$ '+vP.toFixed(2));
                     $('.s5_totalPrice').html('$ '+totalPrice.toFixed(2));*/

                    if (allSelected) {
                        $(".estimated_shipping_cost").text("$" + estimated_shipping_cost.toFixed(2));
                        $(".total_handling_fee").text("$" + total_handling_fee.toFixed(2));
                        hmhship.payment.total_handling_fee = total_handling_fee;

                        // for now just charge handling
                        $(".s5_totalPrice").text("$" + total_handling_fee);

                        quickshipUi.slider.goToSlide(4);
                        funcs.setWizardActive(5);
                        jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                        $('.step-4 .msg-error').html('');
                    }
                    else {
                        $('.step-4 .msg-error').html('Choose service for all packages.');
                    }

                    quickshipUi.slider.refresh();
                });

                // go from step 5 to step 3
                $('.step-5 .go-to-slide-3').on('click', function () {
                    quickshipUi.slider.goToSlide(2);
                    funcs.setWizardActive(3);
                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                });

                // go from step 5 to step 4
                $('.step-5 .go-to-slide-4').on('click', function () {
                    quickshipUi.slider.goToSlide(3);
                    funcs.setWizardActive(4);
                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                });

                // go from step 3 to step 4 and validate step 3
                funcs._validateSetp_5();

            },
            /*
             * valida o passo 1 tanto para o passo 2 ou para o passo 3
             */
            _validateSetp_1: function () {
                // validate step 1 to slide 2
                // Do you know your package dimensions? YES
                $('.wm-step.step-1').wmValidate({
                    selectors: {submit: 'go-to-slide-2'},
                    callbacks: {
                        CB_Ok: function () {
                            $.post(Configure.base_url + 'quickship/step1.json', $("form.quickship1").serializeJSON(), function (response) {
                                if (response.ok) {
                                    // show steps menu
                                    $('.hardSteps').show();
                                    hmhship.config.steps = "hardSteps";

                                    hmhship.packages.disableSimpleStepsPackagesValidation();

                                    quickshipUi.slider.goToSlide(1);
                                    funcs.setWizardActive(2);

                                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                                }
                            });
                        },
                        CB_AfterValidate: function () {
                            quickshipUi.slider.refresh();
                        }
                    }
                });

                // validate step 1 to slide 3
                // Do you know your package dimensions? NO
                $('.wm-step.step-1').wmValidate({
                    selectors: {submit: 'go-to-slide-3'},
                    callbacks: {
                        CB_Ok: function () {
                            $.post(Configure.base_url + 'quickship/step1.json', $("form.quickship1").serializeJSON(), function (response) {
                                if (response.ok) {
                                    $('.simpleSteps').show(); // show steps menu
                                    hmhship.config.steps = "simpleSteps";

                                    hmhship.packages.enableSimpleStepsPackagesValidation();

                                    $('.s3_boxPkgDetailSimple input[name=packages_simple]').bind('change keyup', function () {
                                        var amount = $(this).val();
                                        var existing_details = $('#tabs > li').length;
                                        if (!parseInt(amount) || parseInt(amount) < 1) return false;
                                        if (!parseInt(existing_details) || parseInt(existing_details) < 1) return false;

                                        // if greater, remove extra items
                                        if (parseInt(existing_details) > parseInt(amount)) {
                                            $.each($('.s3_boxPkgDetail > .box-item'), function (idx, elem) {
                                                if (idx >= amount) {
                                                    $(elem).remove();
                                                }
                                            });
                                            $.each($('#tabs > li.wm3por1-added'), function (idx, elem) {
                                                if (idx >= amount) {
                                                    $(elem).remove();
                                                }
                                            });
                                            $.each($('#my-tab-content > div'), function (idx, elem) {
                                                if (idx >= amount) {
                                                    $(elem).remove();
                                                }
                                            });
                                        }

                                        for (var i = existing_details; i < parseInt(amount); i++) {
                                            funcs._addPkgDetails(false, false);
                                        }
                                    });

                                    quickshipUi.slider.goToSlide(2);
                                    funcs.setWizardActive(2, true);

                                    jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                                }
                            });
                        },
                        CB_AfterValidate: function () {
                            quickshipUi.slider.refresh();
                        }
                    }
                });
            },
            /*
             * valida o passo 2 para o passo 3
             */
            /**
             * Validate data from step 2, and go to step 3
             * @private
             */
            _validateSetp_2: function () {
                // validate step 2
                $('.wm-step.step-2').wmValidate({
                    selectors: {submit: 'go-to-slide-3'},
                    callbacks: {
                        CB_Ok: function () {
                            quickshipUi.slider.goToSlide(2);
                            funcs.setWizardActive(3);

                            if (hmhship.config.steps == 'hardSteps') {
                                $('.hs-packages-dimensions').show();
                                $('.hs-packages-dimensions input, .hs-packages-dimensions select').addClass('required');
                                $('.s3_boxPkgDetailSimple').addClass('hidden');
                                $('.s3_boxPkgDetail').parent().removeClass('hidden');
                            } else {
                                $('.s3_boxPkgDetailSimple').removeClass('hidden');
                            }

                            jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                        },
                        CB_AfterValidate: function () {
                            quickshipUi.slider.refresh();
                        }
                    }
                });
            },
            /*
             * valida o passo 3 para o passo 5
             */
            _validateSetp_3: function () {
                // validate step 2
                $('.wm-step.step-3').wmValidate({
                    selectors: {
                        submit: 'go-to-slide-5'
                    },
                    callbacks: {
                        CB_Ok: function () {

                            quickshipUi.slider.goToSlide(4);
                            funcs.setWizardActive(3, true);

                            //$('.simpleSteps').show();

                            jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                        },
                        CB_AfterValidate: function () {

                            // add classe de erro nas tabs cujos campos sao criticados
                            $('.s3_itemDetailWrapTabs .tab-pane').each(function () {
                                var t = $(this);
                                var ind = $('.s3_itemDetailWrapTabs .tab-pane').index(t);
                                var ers = t.find('.error').length;
                                if (ers > 0) {
                                    $('.messageTabs').html('verify that all fields are valid');
                                    $('.s3_itemDetailWrapTabs .nav-tabs li').eq(ind).addClass('errorTab');
                                }
                                else {
                                    $('.messageTabs').html('');
                                    $('.s3_itemDetailWrapTabs .nav-tabs li').eq(ind).removeClass('errorTab');
                                }
                            });

                            quickshipUi.slider.refresh();
                        }
                    }
                });

                // validate step 2
                $('.wm-step.step-3').wmValidate({
                    selectors: {submit: 'go-to-slide-4'},
                    callbacks: {
                        CB_Ok: function () {
                            quickshipUi.slider.goToSlide(3);
                            $('.hideLoad').hide();
                            $('.showLoad').show();
                            jQuery('html, body').stop().animate({scrollTop: 0}, 300);

                            var packagesJson = $("form.quickship3").serialize();

                            $.post(Configure.base_url + 'quickship/step3.json', packagesJson, function (response) {
                                hmhship.shipping_calculator.load_rates(response);

                                $('.wm-step.step-4 .hideLoad').show();
                                $('.wm-step.step-4 .showLoad').hide();
                                funcs.setWizardActive(4);
                                quickshipUi.handleTableSort();
                                quickshipUi.slider.refresh();
                            });
                        },
                        CB_AfterValidate: function () {

                            $('.s3_itemDetailWrapTabs .tab-pane').each(function () {
                                var t = $(this);
                                var ind = $('.s3_itemDetailWrapTabs .tab-pane').index(t);
                                var ers = t.find('.error').length;
                                if (ers > 0) {
                                    $('.messageTabs').html('verify that all fields are valid');
                                    $('.s3_itemDetailWrapTabs .nav-tabs li').eq(ind).addClass('errorTab');
                                }
                                else {
                                    $('.messageTabs').html('');
                                    $('.s3_itemDetailWrapTabs .nav-tabs li').eq(ind).removeClass('errorTab');
                                }
                            });

                            quickshipUi.slider.refresh();
                        }
                    }
                });
            },
            /*
             * valida o passo 2 para o passo 3
             */
            _validateSetp_5: function () {
                // validate step 2
                $('.wm-step.step-5').wmValidate({
                    selectors: {
                        submit: 'go-to-slide-6'
                    },
                    callbacks: {
                        CB_Ok: function () {

                            quickshipUi.slider.goToSlide(5);

                            // fake load ajax
                            $('.hideLoad').hide();
                            $('.showLoad').show();
                            setTimeout(function () {
                                $('.wm-step.step-6 .hideLoad').show();
                                $('.wm-step.step-6 .showLoad').hide();

                                funcs.setWizardActive(4, true);
                                quickshipUi.slider.refresh();
                            }, 2000);

                            jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                        },
                        CB_AfterValidate: function () {
                            quickshipUi.slider.refresh();
                        }
                    }
                });

                $('.wm-step.step-5').wmValidate({
                    selectors: {
                        submit: 'go-to-slide-6b',
                        required: 'pay-required'
                    },
                    callbacks: {
                        CB_Ok: function () {

                            quickshipUi.slider.goToSlide(5);

                            // fake load ajax
                            $('.hideLoad').hide();
                            $('.showLoad').show();
                            setTimeout(function () {
                                $('.wm-step.step-6 .hideLoad').show();
                                $('.wm-step.step-6 .showLoad').hide();

                                funcs.setWizardActive(4, true);
                                quickshipUi.slider.refresh();
                            }, 2000);

                            jQuery('html, body').stop().animate({scrollTop: 0}, 300);
                        },
                        CB_AfterValidate: function () {
                            quickshipUi.slider.refresh();
                        }
                    }
                });
            },
            /**
             * Add and remove packages functionality on step 2
             *
             * Only happens on hardSteps
             */
            addDeleteMorePackages_2: function () {

                /**
                 * Add packages to consolidated package
                 */
                $('.s2_addMorePackages', '#quickshipFrm').on('click', function () {
                    console.log('click addDeleteMorePackages_2-s2_addMorePackages ');

                    hmhship.packages.add_consolidated();
                    hmhship.packages.manageConsolidatedPackages($('.s2_boxPackages', '#quickshipFrm'));

                    // Handle consolidated package logic
                    //funcs.manage3por1($('.s2_boxPackages'));

                    quickshipUi.slider.refresh();

                    return false;
                });

                // Remove a consolidated package
                $(document).on('click', '.s2_delPackage', function () {
                    var t = $(this);
                    hmhship.packages.remove_consolidated(t);

                    // Handle consolidated package logic
                    //funcs.manage3por1($('.s2_boxPackages'));

                    hmhship.packages.manageConsolidatedPackages($('.s2_boxPackages', '#quickshipFrm'));
                    quickshipUi.slider.refresh();

                    return false;
                });
            },
            /**
             * Manage consolidated packages
             * @param boxPackages
             * @returns {number}
             */
            manage3por1: function (boxPackages) {
                var cont = 0;
                var wm3por1 = 1;

                // Cleanup tabs for consolidated packages
                $('.wm3por1-added-consolidated').remove();

                // TODO: sum weight (only on hardSteps)
                // TODO: sum by quantiy (only on simpleSteps)


                // Lookup through all packages and 
                boxPackages.find('.box-item').each(function () {
                    var weight = 0;
                    if (cont > 0 && (parseInt(cont) % 3 === 0)) {
                        wm3por1++;
                        if (wm3por1 > 1) {
                            funcs._addPkgDetails(true, wm3por1, {
                                weight: weight
                            });
                            weight = 0; // reset weight for next package
                        }
                    }
                    cont++;
                });
                total3por1 = wm3por1;
                return wm3por1;
            },
            /**
             * Add and remove packages on step 3
             *
             * Only for consolidated packages
             */
            addDeleteMorePackages_3: function () {
                var indAddPackage = 2;

                // Add a package
                $('.s3_addMorePackages').on('click', function () {
                    var newPack = '<div class="track-item"><div class="row"><div class="col col-md-6"><input type="text" name="packages[' + indAddPackage + '][tracking_code]" class="form-control" placeholder="Tracking code"></div><div class="col col-md-6"><input type="text" name="packages[' + indAddPackage + '][carrier]" class="form-control" placeholder="Tracking code"></div></div><div class="row action"><div class="col col-md-12"><button type="button" class="btn btn-danger s3_delTrackCode">Delete</button></div></div></div>';
                    $('.s3_boxTrackCode').append(newPack);
                    $('.s3_boxPackages').append(newPack);
                    //funcs.effectLabel();
                    $('.step-3 .tmp-required').addClass('required');
                    $('.s3_delPackage').show();
                    quickshipUi.slider.refresh();
                    indAddPackage++;

                    // Handle consolidated package logic
                    funcs.manage3por1($('.s3_boxPackages'));

                    return false;
                });

                // delete button
                $(document).on('click', '.s3_delPackage', function () {
                    var t = $(this);
                    hmhship.packages.remove_consolidated(3, t);

                    // Handle consolidated package logic
                    funcs.manage3por1($('.s3_boxPackages'));

                    return false;
                });
            },
            /*
             * adicionar ou deletar packages no passo 3
             */
            addDeletePkgDetail_3: function () {

                var indItemDetail = 2;

                /**
                 * add a package
                 */
                $('.s3_addPkgDetail').on('click', function () {
                    funcs._addPkgDetails(false, false);

                    return false;
                });

                // deletar
                $(document).on('click', '.s3_delPkgDetail', function () {
                    var t = $(this);
                    var ind = $('.s3_delPkgDetail').index(t);

                    $('.s3_boxPkgDetail .box-item').get(ind).remove();
                    $('.s3_itemDetailWrapTabs .nav-tabs li').get(ind).remove();
                    $('.s3_itemDetailWrapTabs .tab-content .tab-pane').get(ind).remove();

                    $('.s3_itemDetailWrapTabs .nav-tabs li').removeClass('active');
                    $('.s3_itemDetailWrapTabs .tab-content .tab-pane').removeClass('active');
                    $('.s3_itemDetailWrapTabs .nav-tabs li').first().addClass('active');
                    $('.s3_itemDetailWrapTabs .tab-content .tab-pane').first().addClass('active');

                    if ($('.s3_boxPkgDetail .box-item').length <= 1) {
                        $('.s3_delPkgDetail').hide();
                        $('.s3_delItemDetail').hide();
                    }

                    hmhship.packages._updatePackagesLabels();

                    return false;
                });

                $('.s3_addItemDetail').on('click', function () {
                    var $pkgs = hmhship.packages;
                    //var newItemDetail = '<div class="s3_itemDetail s3_itemDetailAdd"><div class="row"><div class="col col-md-5"><input type="text" name="s3_itemDetailDesc['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tmp-required"placeholder="Item description"><span class="msg-error"></span></div><div class="col col-md-2"><select name="s3_itemDetailUnit['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tmp-required"><option value="">Qty</option><option value="1">1</option></select><span class="msg-error"></span></div><div class="col col-md-3"><input name="s3_itemDetailWeight['+(indAddPackage-1)+']['+indItemDetail+']" type="number"  min="1" class="form-control tmp-required" placeholder="Weight"><span class="msg-error"></span></div><div class="col col-md-2"><select name="s3_itemDetailUnit2['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tmp-required"><option value="lb">lb</option><option value="kg">kg</option></select><span class="msg-error"></span></div></div><div class="row"><div class="col col-md-5"><select name="s3_itemDetailType['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tmp-required"><option value="">Type of content</option><option value="teste">1</option></select><span class="msg-error"></span></div><div class="col col-md-4"><input type="text" name="s3_itemDetailCountry['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tmp-require" placeholder="Country of origin"><span class="msg-error"></span></div><div class="col col-md-3"><input type="number"  min="1" name="s3_itemDetailValue['+(indAddPackage-1)+']['+indItemDetail+']" class="form-control tm-required" placeholder="U$xx.xx"><span class="msg-error"></span></div></div><div class="row action"><div class="col col-md-12"><button type="button" class="btn btn-danger s3_delItemDetail">Delete</button></div></div></div>';
                    //var newItemDetail = '<div class="s3_itemDetail s3_itemDetailAdd"><div class="row"><div class="col col-md-5"><input type="text" name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][description]" class="form-control tmp-required" placeholder="Item description"><span class="msg-error"></span></div><div class="col col-md-2"><select name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][quantity]" class="form-control tmp-required"><option value="">Qty</option><option value="1">1</option></select><span class="msg-error"></span></div><div class="col col-md-3"><input name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][weight]" type="number"min="1" class="form-control tmp-required" placeholder="Weight"><span class="msg-error"></span></div><div class="col col-md-2"><select name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][weight_unit]" class="form-control tmp-required"><option value="lb">lb</option><option value="kg">kg</option></select><span class="msg-error"></span></div></div><div class="row"><div class="col col-md-5"><select name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][type_id]" class="form-control tmp-required"><option selected="selected" value="">-- Type of content * --</option></select><span class="msg-error"></span></div><div class="col col-md-4"><input type="text" name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][country_id]" class="form-control tmp-require" placeholder="Country of origin"><span class="msg-error"></span></div><div class="col col-md-3"><input type="number"min="1" name="packages['+(indAddPackage-1)+'][items]['+indItemDetail+'][value]" class="form-control tm-required" placeholder="U$xx.xx"><span class="msg-error"></span></div></div><div class="row action"><div class="col col-md-12"><button type="button" class="btn btn-danger s3_delItemDetail">Delete</button></div></div></div>';

                    $('.tab-pane.active .s3_boxItemDetail').append($pkgs.get_item());
                    $('.s3_delItemDetail').show();
                    quickshipUi.slider.refresh();
                    indItemDetail++;
                    return false;
                });

                $(document).on('click', '.s3_delItemDetail', function () {
                    var t = $(this);
                    var ind = $('.s3_delPkgDetail').index(t);

                    $('.tab-pane.active .s3_itemDetail').eq(ind).remove();

                    if ($('.tab-pane.active .s3_itemDetail').length <= 1) {
                        $('.s3_delItemDetail').hide();
                    }
                    quickshipUi.slider.refresh();
                    return false;
                });

            },

            /**
             *
             * @param byManage3por1
             * @param wm3por1
             * @param args
             * @private
             */
            _addPkgDetails: function (byManage3por1, wm3por1, args) {
                var $pkgs = hmhship.packages;

                //var indLabel = parseInt($('.s3_boxPkgDetail .box-item').length) + 1;
                var indLabel = parseInt($('#tabs > li').length) + 1;

                var css_class = 'wm3por1-added', style = '', tab_title = 'Package';
                if (byManage3por1 === true) {
                    css_class = 'wm3por1-added-consolidated';
                    style = 'style="display:none !important;"';
                    tab_title = 'Consolidated Package';
                }

                var classe = '';
                if (wm3por1 !== false) classe = 'wm3por1Index_' + wm3por1;

                var newDetailTab = '<li class="' + css_class + '"><a href="#pkg' + indAddPackage + '" data-toggle="tab">' + tab_title + ' <span class="label-tab">' + indLabel + '</span></a></li>';

                var packageTpl = $pkgs.get_package();
                packageTpl.removeClass('wm3por1-added-consolidated');
                var tab_content = $pkgs.get_item_container();
                tab_content.attr('id', 'pkg' + indAddPackage);

                $('.s3_boxPkgDetail').append(packageTpl);
                $('.s3_itemDetailWrapTabs .nav-tabs').append(newDetailTab);
                $('.s3_itemDetailWrapTabs .tab-content').append(tab_content);

                $('.step-3 .tmp3-required').addClass('required');

                $('.s3_delPkgDetail').parent().parent().show();
                $('.s3_delPkgDetail').show();
                //$('.s3_delItemDetail').show();

                quickshipUi.slider.refresh();
                indAddPackage++;

                $('.hs-tracking-codes:last').select2({
                    tags: true,
                    allowClear: true,
                    tokenSeparators: [',', ' '],
                    width: '100%'
                });
            },
            /*
             * adicionar ou deletar packages no passo 2
             */
            addDeleteTrackCode_3: function () {
                var indAddPackage = 2;

                // adicionar
                $('.s3_addTrackCode').on('click', function () {
                    // this line below could have lost code from prev versions, compare and merge
                    //var newPack = '<div class="track-item"><div class="row"><div class="col col-md-6"><input type="text" name="packages['+indAddPackage+'][tracking_code]" class="form-control" placeholder="Tracking code"></div><div class="col col-md-6"><input type="text" name="packages['+indAddPackage+'][carrier]" class="form-control" placeholder="Tracking code"></div></div><div class="row action"><div class="col col-md-12"><button type="button" class="btn btn-danger s3_delTrackCode">Delete</button></div></div></div>';
                    var newPack = '<div class="track-item"><div class="row"><div class="col col-md-6"><input type="text" name="s3_trackCode1[' + indAddPackage + ']" class="form-control" placeholder="Tracking code"></div><div class="col col-md-6"><input type="text" name="s3_carrier[' + indAddPackage + ']" class="form-control" placeholder="Carrier"></div></div><div class="row action"><div class="col col-md-12"><button type="button" class="btn btn-danger s3_delTrackCode">Delete</button></div></div></div>';
                    $('.s3_boxTrackCode').append(newPack);
                    $('.step-2 .tmp4-required').addClass('required');
                    $('.s3_delTrackCode').show();
                    quickshipUi.slider.refresh();
                    indAddPackage++;
                    return false;
                });

                // deletar
                $(document).on('click', '.s3_delTrackCode', function () {
                    var t = $(this);
                    var ind = $('.s3_delTrackCode').index(t);
                    $('.s3_boxTrackCode .track-item').get(ind).remove();

                    if ($('.s3_boxTrackCode .track-item').length <= 1) {
                        $('.s3_delTrackCode').hide();
                    }

                    return false;
                });
            },
            /*
             *	interatividade entre campos no passo um
             */
            interactiveStep_1: function () {
                funcs._sameShipAddress($('#s1_isSameShipAddressYes'));
                $('.isSameShipAddress').on('change', function () {
                    funcs._sameShipAddress($(this));

                    quickshipUi.slider.refresh();
                });
            },
            _sameShipAddress: function (el) {
                var t = el, v = t.val(), watch = (v === 'yes') ? true : false;

                funcs._watchFieldsToCopy($('#s1_personalFirstName'), $('#s1_shipFirstName'), watch);
                funcs._watchFieldsToCopy($('#s1_personalSecondName'), $('#s1_shipSecondName'), watch);
                funcs._watchFieldsToCopy($('#s1_personalEmail'), $('#s1_shipEmail'), watch);
                funcs._watchFieldsToCopy($('#s1_personalPhone'), $('#s1_shipPhone'), watch);
                funcs._watchFieldsToCopy($('#s1_personalAddress1'), $('#s1_shipAddress1'), watch);
                funcs._watchFieldsToCopy($('#s1_personalAddress2'), $('#s1_shipAddress2'), watch);
                funcs._watchFieldsToCopy($('#s1_personalCity'), $('#s1_shipCity'), watch);
                funcs._watchFieldsToCopy($('#s1_personalState'), $('#s1_shipState'), watch);
                funcs._watchFieldsToCopy($('#s1_personalPostalcode'), $('#s1_shipPostalcode'), watch);
                funcs._watchFieldsToCopy($('#s1_personalCountry'), $('#s1_shipCountry'), watch);

                if (watch) {
                    $('#s1_shipEmail').removeClass('required');
                    $('.shipInformation').hide();
                }
                else {
                    $('#s1_shipEmail').addClass('required');
                    $('.shipInformation').show();
                }
            },
            /*
             * interatividade no passo 2
             */
            interactiveStep_2: function () {
                /**
                 * If customer wants a consolidated package
                 */
                $('.s2_haveOther').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'yes') ? true : false;

                    if (watch) {
                        //funcs.carryingValues();

                        hmhship.packages.allowConsolidated = true;
                        console.log('changing consolidated to true');

                        $('.step-2 .tmp-required').addClass('required');
                        //$('.haveOtherHide').show();
                        /*$('.s2_boxPackages > .box-item').each(function (idx, elem) {
                            hmhship.packages.add_consolidated($(this));
                        });*/

                    } else {
                        
                        hmhship.packages.allowConsolidated = false;
                        
                        $('.step-2 .tmp-required').removeClass('required');
                        $('.haveOtherHide').hide();
                        hmhship.packages.resetConsolidatedPackages();

                    }

                    quickshipUi.slider.refresh();
                });

                $('.s2_wantRepackage').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'yes') ? true : false;

                    if (watch) {
                        $('.step-2 .tmp2-required').addClass('required');
                        //$('.s2_wantRepackageHide').show(); // removed on template
                    }
                    else {
                        $('.step-2 .tmp2-required').removeClass('required');
                        //$('.s2_wantRepackageHide').hide(); // removed on template
                    }

                    quickshipUi.slider.refresh();
                });
            },

            /**
             * interatividade entre campos no passo 3
             */
            interactiveStep_3: function () {

                // Allow consolidated packages on simpleSteps
                $('.s3_haveOther').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'yes') ? true : false;

                    if (watch) {
                        //$('.step-3 .tmp-required').addClass('required');
                        //$('.haveOtherHide').show();
                        hmhship.packages.allowConsolidated = true;
                        console.log('changing consolidated to true');
                    }
                    else {
                        //$('.step-3 .tmp-required').removeClass('required');
                        //$('.haveOtherHide').hide();
                        hmhship.packages.allowConsolidated = false;
                        console.log('changing consolidated to false');
                    }

                    quickshipUi.slider.refresh();

                    return false;
                });

                $('.s3_wantRepackage').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'yes') ? true : false;
                    /*
                     if(watch)
                     {
                     $('.step-3 .tmp2-required').addClass('required');
                     $('.s3_wantRepackageHide').show();
                     }
                     else
                     {
                     $('.step-3 .tmp2-required').removeClass('required');
                     $('.s3_wantRepackageHide').hide();
                     }
                     */
                    quickshipUi.slider.refresh();

                    return false;
                });

            },
            /*
             * interatividade entre campos no passo 3
             */
            interactiveStep_5: function () {

                console.log('running interactiveStep_5');

                $('input[name=payment_action]').kalypto({toggleClass: 'wmcheckbox'});

                $('.s5_paymentType').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'CreditCard') ? true : false;

                    if (watch) {
                        //$('.step-5 .tmp-required').addClass('required');
                        $('.s5_hidePaymentPaypal').show();
                        $('.s5_hidePaymentCreditCard').hide();
                    }
                    else {
                        //$('.step-5 .tmp-required').removeClass('required');
                        $('.s5_hidePaymentCreditCard').show();
                        $('.s5_hidePaymentPaypal').hide();
                    }

                    quickshipUi.slider.refresh();
                });

                $('.s5_allowShipCost').on('change', function () {
                    var t = $(this), v = t.val(), watch = (v === 'yes') ? true : false;

                    if (watch) {
                        $('.step-5 .tmp-required').addClass('required');
                        $('.s5_hideAllowShipCost').show();
                    }
                    else {
                        $('.step-5 .tmp-required').removeClass('required');
                        $('.s5_hideAllowShipCost').hide();
                    }

                    quickshipUi.slider.refresh();
                });
            },
            /*
             * funcao auxiliar para transfirir valor de um campo para outro ou limpar
             *
             * @param from - elemento de valor de origem
             * @param to - elemento de destino
             * @param watch - boleano - (se botao foi sim=true e nao=false)
             */
            _watchFieldsToCopy: function (from, to, watch) {
                var f = from, t = to;

                if (watch) {
                    t.val(f.val());
                    if (f.is('select') && t.is('select')) {
                        t.find('option:selected').text(f.find('option:selected').text());
                        t.find('option:selected').html(f.find('option:selected').html());
                    }

                    f.on('change', function () {
                        t.val(f.val());
                        if (f.is('select') && t.is('select')) {
                            t.find('option:selected').text(f.find('option:selected').text());
                            t.find('option:selected').html(f.find('option:selected').html());
                        }
                    });
                }
                else {
                    t.val('');
                    if (f.is('select') && t.is('select')) {
                        t.attr('selected', 'selected');
                    }
                    f.unbind('change');
                }

            },
            setWizardActive: function (step, isSimple) {
                var scont = 0;
                var box = (isSimple) ? $('.simpleSteps .wizard-steps') : $('.hardSteps .wizard-steps');

                box.find('li').removeClass('active');
                box.find('li').each(function () {
                    if (scont < step) {
                        $(this).addClass('active');
                    }
                    scont++;
                });
            },
            chooseShip: function () {
                $(document).on('click', '.btnChoose', function () {
                    var t = $(this);
                    var ind = t.parent().parent().parent().find('.btnChoose').index(t);
                    var ti = t.parent().parent().find('td.title').html();
                    var vl = t.parent().parent().parent().find('td.price');
                    var ts = t.attr('rel');

                    $('.btnChoose').removeClass('selected');
                    vl.removeClass('selected');

                    vl.eq(ind).addClass('selected');
                    $('.' + ts).html(ti).addClass('selected');

                    t.addClass('selected');
                    return;
                });
            },
            isNumeric: function (input) {
                var RE = /^-{0,1}\d*\.{0,1}\d+$/;
                return (RE.test(input));
            },

            /**
             * TODO: document me
             */
            carryingValues: function () {

                $(document).on('change', '.s2_copyWeight, .s2_copyUnit, .s2_copyValue', function () {
                    var arr = [];
                    var c_weight = 0;
                    var c_unit = [];
                    var c_value = 0;
                    var fake_wm3por1 = 0;
                    var cont2 = 0;
                    $('.s2_copyWeight').each(function () {
                        var t = $(this);
                        var ind = $('.s2_copyWeight').index(t);

                        var weight = (!isNaN(parseInt(t.val()))) ? parseInt(t.val()) : 0;
                        var unit = $('.s2_copyUnit').eq(ind).val();
                        var value = (!isNaN(parseInt($('.s2_copyValue').eq(ind).val()))) ? parseInt(t.val()) : 0;

                        if (cont2 >= 3) {
                            fake_wm3por1++;
                            cont2 = 0;
                            c_weight = 0;
                            c_unit = [];
                            c_value = 0;
                        }

                        c_weight = c_weight + weight;
                        c_value = c_value + value;
                        c_unit[cont2] = unit;
                        arr[fake_wm3por1] = {
                            'weight': c_weight,
                            'unit': c_unit,
                            'value': c_value
                        };

                        cont2++;
                    });

                    var index;
                    for (index = 0; index < arr.length; ++index) {
                        var ind = index + 1;
                        var a = arr[index];
                        var wei = a.weight;
                        if (jQuery.inArray('kg', a.unit) == -1) {
                            $('.wm3por1Index_' + ind + ' .s3_copyUnit').first().val('lb');
                            $('.wm3por1Index_' + ind + ' .s3_copyWeight').first().val(wei.toFixed(2));
                        }
                        else {
                            wei = (wei * 2.2);

                            $('.wm3por1Index_' + ind + ' .s3_copyUnit').first().val('kg');
                            $('.wm3por1Index_' + ind + ' .s3_copyWeight').first().val(wei.toFixed(2));
                        }
                    }

                });
            }
        };
        funcs.init();
    });
})(quickshipUi);
