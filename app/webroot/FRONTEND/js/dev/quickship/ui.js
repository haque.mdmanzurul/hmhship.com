/**
 *
 * @type {{slider: null, handleTableSort: Function, handleDropdowns: Function, handleToggles: Function, handleLabelEffects: Function, handleSlider: Function, init: Function}}
 */
var quickshipUi = {

    slider: null,

    /**
     * Used mostly for sorting data for shipping providers
     */
    handleTableSort: function () {
        $("table.wm-sort-table", '#quickshipFrm').tablesorter({
            headers: {
                0: {sorter: false},
                1: {sorter: false},
                2: {sorter: false},
                3: {sorter: false},
                4: {sorter: false}
            }
        });
        $("#orderByMostPopular", '#quickshipFrm').click(function () {
            var sorting = [[5, 0]];
            $("table.wm-sort-table", '#quickshipFrm').trigger("sorton", [sorting]);
            return false;
        });
        $("#orderByLowestPrice", '#quickshipFrm').click(function () {
            var sorting = [[6, 0]];
            $("table.wm-sort-table", '#quickshipFrm').trigger("sorton", [sorting]);
            return false;
        });
        $("#orderByFastestDelivery", '#quickshipFrm').click(function () {
            var sorting = [[7, 0]];
            $("table.wm-sort-table", '#quickshipFrm').trigger("sorton", [sorting]);
            return false;
        });
    },

    /**
     * Used mostly for tagging tracking codes on Quickship shipments
     */
    handleDropdowns: function () {
        $('.hs-tracking-codes:first-child', '#quickshipFrm').select2({
            tags: true,
            allowClear: true,
            tokenSeparators: [',', ' '],
            width: '100%'
        });
    },

    /**
     * Handle toggles on Quickship forms
     */
    handleToggles: function () {
        $('.toggles', '#quickshipFrm').toggles({
            width: 55,
            text: {
                on: 'YES',
                off: 'NO'
            }
        });
        $('.toggles', '#quickshipFrm').on('toggle', function (e, active) {
            if (active) {
                $(this).parent().parent().find('.toogle-yes').prop('checked', true).change();
            }
            else {
                $(this).parent().parent().find('.toogle-no').prop('checked', true).change();
            }
        });
    },

    /**
     * Handle effects on labels
     */
    handleLabelEffects: function () {
        $('input[type=text],input[type=email],input[type=number]', '#quickshipFrm').label_better({
            position: "top",
            animationTime: 500,
            easing: "ease-in-out",
            offset: 40,
            hidePlaceholderOnFocus: true
        });
        $('#s1_personalPhone, #s1_shipPhone').bind('focus keydown', function () {
            if ($(this).val().indexOf('+') !== 0) {
                $(this).val('+' + $(this).val());
            }
        });
    },

    /**
     * Handle slider on Quickship for moving between different steps
     */
    handleSlider: function () {
        quickshipUi.slider = $('.wm-slider', '#quickshipFrm').lightSlider({
            item: 1,
            autoWidth: false,
            adaptiveHeight: true,
            pager: false,
            controls: false,
            enableTouch: false,
            enableDrag: false,
            onAfterSlide: function () {
                quickshipUi.slider.refresh();
            }
        });
    },

    // constructor
    init: function () {
        quickshipUi.handleSlider();
    }
};

    

