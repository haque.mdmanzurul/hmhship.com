	</div>
	
	<footer class="footer">
        <div class="circle"></div>
		<div class="opac-bar"></div>
		<div class="container">
            <a href="#" class="logo">
                <img src="../img/logo.png" alt="HMH Ship Parcel Forwarding Simplified">
			</a>
			
			<div class="foot-sitemap">
                <ul class="social">
					<li>
						<a href="#" title="Blog"><i class="icon-blog"></i> Blog</a>
					</li>
					<li>
						<a href="#" title="Facebook"><i class="icon-fb"></i></a>
					</li>
					<li>
						<a href="#" title="Twitter"><i class="icon-tw"></i></a>
					</li>
				</ul>
				
				<ul class="menu menu-1">
					<li>
                        <a href="#" title="Home">Home</a>
					</li>
					<li>
						<a href="#" title="What we do &amp; rate">What we do &amp; rate</a>
					</li>
					<li>
						<a href="#" title="How we do it">How we do it</a>
					</li>
					<li>
						<a href="#" title="Business">Business</a>
					</li>
					<li>
						<a href="#" title="hmhship bio">hmhship bio</a>
					</li>
					<li>
						<a href="#" title="Talk to us">Talk to us</a>
					</li>
				</ul>
				
				<ul class="menu menu-2">
					<li>
						<a href="#" title="Shipping calculation">Shipping calculation</a>
					</li>
					<li>
						<a href="#" title="Quick Ship">Quick Ship</a>
					</li>
					<li>
						<a href="#" title="Login">Login</a>
					</li>
					<li>
						<a href="#" title="Create an account">Create an account</a>
					</li>
				</ul>
				
				<ul class="menu menu-3">
					<li>
						<a href="#" title="Ship to address">Ship to address</a>
					</li>
					<li>
						<a href="#" title="Account settings">Account settings</a>
					</li>
					<li>
						<a href="#" title="User preference">User preference</a>
					</li>
					<li>
						<a href="#" title="Uploads">Uploads</a>
					</li>
				</ul>
				<div class="clear"></div>
			</div><!-- foot-sitemap -->
			
			<ul class="contact">
				<li>
					<i class="icon-phone"></i> 99999999
				</li>
				<li>
					<i class="icon-location"></i> Mercantile Rd. Beachwood 44122
				</li>
			</ul>
		</div>
		<div class="foot-bar foot-bar-1"></div>
		<div class="foot-bar foot-bar-2"></div>		
    </footer>
    
    <script type="text/javascript" src="../js/app.min.js"></script>
    <script type="text/javascript" src="../js/jquery.serialize-object.min.js"></script>
    <script type="text/javascript">
    <?php
    $front_url = '/app/webroot/FRONTEND/quickship/';
    $base_url = str_replace($front_url, '', $_SERVER['REQUEST_URI']);
    echo "var BASE_URL = '$base_url';";
    ?>
    
    $(document).ready(function() {});
    </script>
    </body>
</html>