<?php require_once('header.php'); ?>
<section class="form-quickship">
	<div class="container">
		<div class="top-box-circle">
			<div class="circle"></div>
			<div class="icon">
				<i class="icon-quickship"></i>
			</div>
		</div>
		<div class="wrap">


			<div class="wizard row wizard-box simpleSteps">
                <ul class="wizard-steps">
                    <li data-target="#step1" class="active">
                        <span class="step">1</span>
                        <span class="title">Personal <br> information</span>
                    </li>
                    <li data-target="#step2">
                        <span class="step">2</span>
                        <span class="title">Shipment <br> Details</span>
                    </li>
                    <li data-target="#step3">
                        <span class="step">3</span>
                        <span class="title">Payment <br> Options</span>
                    </li>
                    <li data-target="#step4">
                        <span class="step">4</span>
                        <span class="title">Your <br> US Address</span>
                    </li>
                </ul>
            </div>

            <div class="wizard row wizard-box wizard-big hardSteps">
                <ul class="wizard-steps">
                    <li data-target="#step1" class="active">
                        <span class="step">1</span>
                        <span class="title">Personal <br> information</span>
                    </li>
                    <li data-target="#step2">
                        <span class="step">2</span>
                        <span class="title">Additional <br> Options</span>
                    </li>
                    <li data-target="#step3">
                        <span class="step">3</span>
                        <span class="title">Package <br> Details</span>
                    </li>
                    <li data-target="#step4">
                        <span class="step">4</span>
                        <span class="title">Shipment <br> Rates</span>
                    </li>
                    <li data-target="#step5">
                        <span class="step">5</span>
                        <span class="title">Payment <br> Options</span>
                    </li>
                    <li data-target="#step6">
                        <span class="step">6</span>
                        <span class="title">Your <br> US Address</span>
                    </li>
                </ul>
            </div>
            
            <ul class="wm-slider">
                <li>
                    <form class="quickship1">
    				<?php require_once('steps/step_1.php'); ?>
    				</form>
    			</li>
    			<li>
    				<?php require_once('steps/step_2.php'); ?>
    			</li>
    			<li>
                    <form class="quickship3">
    				<?php require_once('steps/step_3.php'); ?>
    				</form>
    			</li>
    			<li>
    				<?php require_once('steps/step_4.php'); ?>
    			</li>
    			<li>
    				<?php require_once('steps/step_5.php'); ?>
    			</li>
    			<li>
    				<?php require_once('steps/step_6.php'); ?>
    			</li>
            </ul>
		</div>
	</div>
</section>
<?php require_once('footer.php'); ?>