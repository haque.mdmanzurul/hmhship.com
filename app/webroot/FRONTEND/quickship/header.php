<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
		<title></title>
		<link rel="stylesheet" type="text/css" href="../css/app.min.css">
		<!--[if lt IE 9]>
			<script type="text/javascript" src="../js/html5shiv.js"></script>
			<script type="text/javascript" src="../js/respond.min.js"></script>
		<![endif]-->
		<!--[if lte IE 8]> 
			<script type="text/javascript" src="../js/json3.min.js"></script>
		<![endif]-->
	</head>
	<body>

		<nav class="navbar navbar-fixed-top">
		<div class="container">
			<a href="#" class="logo">
				<img src="../img/logo.png" alt="HMH Ship Parcel Forwarding Simplified">
			</a>
			<ul class="top-menu">
				<li>
					<a href="#" title="Shipping calculator">
						<i class="icon-calculator"></i>
						Shipping calculator
					</a>
				</li>
				<li>
					<a href="index.php" title="Quick ship">
						<i class="icon-quickship"></i>
						Quick ship
					</a>
				</li>
				<li>
					<a href="#" title="My account">
						<i class="icon-account"></i>
						My account
					</a>
				</li>
				<li>
					<a href="#" title="Log in">
						<i class="icon-login"></i>
						Log in
					</a>
				</li>
				<li>
					<a href="#" title="Create an account">
						<i class="icon-createaccount"></i>
						Create an account
					</a>
				</li>
				<li class="last">
					<a href="#" title="Blog">
						<i class="icon-blog"></i>
						Blog
					</a>
				</li>
				<li class="only-icon">
					<a href="#" title="FacebookTwitter">
						<i class="icon-fb"></i>
					</a>
				</li>
				<li class="last only-icon">
					<a href="#" title="Twitter">
						<i class="icon-tw"></i>
					</a>
				</li>
			</ul>
			<ul class="main-menu">
				<li>
					<a href="../index.php" title="Home">
						<span class="txt">Home</span>
						<span class="bg"></span>
					</a>
				</li>
				<li>
					<a href="../what-we-do.php" title="What we do &amp; rate">
						<span class="txt">What we do &amp; rate</span>
						<span class="bg"></span>
					</a>
				</li>
				<li>
					<a href="../how-we-do-it.php" title="How we do it">
						<span class="txt">How we do it</span>
						<span class="bg"></span>
					</a>
				</li>
				<li>
					<a href="../business.php" title="Business">
						<span class="txt">Business</span>
						<span class="bg"></span>
					</a>
				</li>
				<li>
					<a href="../hmhship-bio.php" title="hmhship bio">
						<span class="txt">hmhship bio</span>
						<span class="bg"></span>
					</a>
				</li>
				<li>
					<a href="../talk-to-us.php" title="Talk to us">
						<span class="txt">Talk to us</span>
						<span class="bg"></span>
					</a>
				</li>
			</ul>
		</div>
		<div class="opac-bar"></div>
	</nav>

	<div class="wm-main">