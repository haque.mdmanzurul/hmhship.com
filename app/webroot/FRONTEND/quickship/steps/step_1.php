<div class="wm-step step-1">
	<small>*required</small>
	<div class="group">
		<div class="row">
			<h2>Personal Information</h2>
			<div class="col col-md-3">
				<span>
					Let's get to know first *
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="user[first_name]" id="s1_personalFirstName" class="form-control required" placeholder="First name *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="user[last_name]" id="s1_personalSecondName" class="form-control required" placeholder="Last name *">
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 1 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					What is your address *
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="billing[address1]" id="s1_personalAddress1" class="form-control required" placeholder="Address 1 *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="billing[address2]" id="s1_personalAddress2" class="form-control" placeholder="Address 2 *">
						<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="billing[city]" id="s1_personalCity" class="form-control required" placeholder="City *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<div class="row row-inner">
							<div class="col col-md-6">
								<input type="text" name="billing[state]" id="s1_personalState" class="form-control required" placeholder="State/Province *">
								<span class="msg-error"></span>
							</div>
							<div class="col col-md-6 no-margin">
								<input type="text" name="billing[postal_code]" id="s1_personalPostalcode" class="form-control required" placeholder="Postal Code *">
								<span class="msg-error"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<select name="billing[country]" id="s1_personalCountry" class="form-control required">
							<option value="">Country *</option>
							<option value="1">Argentina</option>
							<option value="2">Brazil</option>
						</select>
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 3 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					How can we get in touch with you? *
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="user[email]" id="s1_personalEmail" class="form-control required email" placeholder="Email *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="user[phone]" id="s1_personalPhone" class="form-control required integer" placeholder="Phone *">
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 2 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					Is this the same as the shipping address?
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="billing_shipping" id="s1_isSameShipAddressNo" value="no" class="toogle-no isSameShipAddress required"> no
				        	<input type="radio" name="billing_shipping" id="s1_isSameShipAddressYes" value="yes" class="toogle-yes isSameShipAddress required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="true"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 4 -->
	</div>
	<div class="group last shipInformation">
		<div class="row">
			<h2>Shipping information</h2>
			<div class="col col-md-3">
				<span>
					Who is going to receive? *
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="shipping[first_name]" id="s1_shipFirstName" class="form-control required" placeholder="First name *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="shipping[last_name]" id="s1_shipSecondName" class="form-control required" placeholder="Last name *">
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 1 -->


		<div class="row">

			<div class="col col-md-3">
				<span>
					What is the address you want your package to be sent? *
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="shipping[address1]" id="s1_shipAddress1" class="form-control required" placeholder="Address 1 *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="shipping[address2]" id="s1_shipAddress2" class="form-control" placeholder="Address 2 *">
						<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="shipping[city]" id="s1_shipCity" class="form-control required" placeholder="City *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<div class="row row-inner">
							<div class="col col-md-6">
								<input type="text" name="shipping[state]" id="s1_shipState" class="form-control required" placeholder="State/Province *">
								<span class="msg-error"></span>
							</div>
							<div class="col col-md-6">
								<input type="text" name="shipping[postal_code]" id="s1_shipPostalcode" class="form-control required" placeholder="Postal Code *">
								<span class="msg-error"></span>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-6">
						<select name="shipping[country]" id="s1_shipCountry" class="form-control required">
							<option value="">Country</option>
							<option value="1">Argentina</option>
							<option value="2">Brazil</option>
						</select>
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 5 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					How can we get in touch? *
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
						<input type="text" name="shipping[email]" id="s1_shipEmail" class="form-control required email" placeholder="Email *">
						<span class="msg-error"></span>
					</div>
					<div class="col col-md-6">
						<input type="text" name="shipping[phone]" id="s1_shipPhone" class="form-control required integer" placeholder="Phone *">
						<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 2 -->

	</div>

	<div class="group">
		<div class="row text-center">
			<div class"col col-md-12">
				<p class="last-p">
					Do you know your package dimensions? *
				</p>
				<button type="button" class="btn btn-danger go-to-slide-3">No</button>
				<button type="button" class="btn btn-success go-to-slide-2">Yes</button>
				<br><br>

			</div>
		</div><!-- row 6 -->
	</div>

</div><!-- step-1 -->
