<div class="wm-step step-4">

	<div class="showLoad text-center">
		<img src="../img/loading.gif" />
		loading...
	</div>
	<div class="group hideLoad">
		<small>*required</small>
		<div class="row top-list">
			<div class="col col-md-12">
				<h2 class="nopadding">Shipping options</h2>
			</div>

			<ul class="list-inline">
				<li>
					<strong>Sort by</strong>
				</li>
				<li>
					<a href="#" id="orderByMostPopular" title="Most popular">Most popular</a>
				</li>
				<li>
					<a href="#" id="orderByLowestPrice" title="Lowest price">Lowest price</a>
				</li>
				<li>
					<a href="#" id="orderByFastestDelivery" title="Fastest delivery">Fastest delivery</a>
				</li>
			</ul>
		</div>
		
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading_1">
					<div class="row">
						<div class="col col-md-4">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse_1">
								Package 1 *
							</a>
						</div>
						<div class="col col-md-8">
							<span class="title-selected_1 title-selected"></span>
						</div>
					</div>
				</div>
				<div id="collapse_1" class="panel-collapse collapse in" role="tabpanel">
					<div class="row">
						<div class="col col-md-12">
							<table class="table wm-sort-table">
								<thead>
									<tr>
										<th>Carrier</th>
										<th>Service</th>
										<th>Estimated delivery</th>
										<th>Price</th>
										<th>&nbsp;</th>
										<th class="hide">Most popular</th>
										<th class="hide">Lowest price</th>
										<th class="hide">Fastest delivery</th>
									</tr>
								</thead>
								<tbody id="rate_list">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<span class="msg-error"></span>
	</div>
	
	<div class="row text-center hideLoad">
		<div class"col col-md-12">
			<button type="button" class="btn btn-default go-to-slide-3">Back</button>
			<button type="button" class="btn btn-success go-to-slide-5">Next</button>
			<br><br>
		</div>
	</div><!-- row 6 -->

</div><!-- step-1 -->