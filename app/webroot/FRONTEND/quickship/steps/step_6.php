<div class="wm-step step-6">
	<div class="showLoad text-center">
		<img src="../img/loading.gif" />
		loading...
	</div>
	<div class="row hideLoad">
		<div class="col col-md-12">
			<p class="text-center">
				We are done! You can start sending your packages to your US Address. <br>
				This address was also sent to your email. (not used)
			</p>
		</div>
	</div><!-- row 1 -->

</div><!-- step-1 -->