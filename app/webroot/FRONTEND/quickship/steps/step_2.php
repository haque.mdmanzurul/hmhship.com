<div class="wm-step step-2">
	<small>*required</small>
	<div class="group">
		<div class="row">
			<h2>Additional options</h2>
			<div class="col col-md-3">
				<span>
					Do you have other packages you would like to be shipped consolidated into 1 larger package? (Up to 3 packages and gross dimensions up to 24"x14"x12")
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-2">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s2_haveOther" id="s2_haveOtherNo" class="toogle-no s2_haveOther required" value="no"> no
				        	<input type="radio" name="s2_haveOther" id="s2_haveOtherYes" class="toogle-yes s2_haveOther required" value="yes"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
				<div class="row haveOtherHide">
					<div class="box-aditional">
						<div class="box-title">
							<div class="row">
								<div class="col col-md-12">
									<h3>Additional Packages *</h3>
								</div>
							</div>
						</div>
						<div class="s2_boxPackages">
							<div class="box-item">
								<div class="row">
									<div class="col col-md-3">
										<input type="number"  min="1" name="s2_adPackageWeight[1]" class="s2_copyWeight form-control tmp-required" placeholder="Weight *">
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<select name="s2_adPackageUnit[1]" class="s2_copyUnit form-control tmp-required">
											<option value="lb">lb</option>
											<option value="kg">kg</option>
										</select>
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<input type="number"  min="1" name="s2_adPackageValue[1]" class="s2_copyValue form-control tmp-required" placeholder="U$xx.xx *">
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<button type="button" class="btn btn-danger s2_delPackage">Delete</button>
									</div>
								</div>
							</div>
						</div>
						<div class="action text-right">
							<div class="col col-md-12">
								<button type="button" class="btn btn-success s2_addMorePackages">Add more</button>
								<p class="text-right">
									if the gross dimensions exceed 24"x14"x12", you will be charged for an additional package
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- row 1 -->

		<div class="row"><!-- .haveOtherHide -->
			<div class="col col-md-3">
				<span>
					Do you need these packages expedited?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s2_packagesExpedited" value="no" class="toogle-no tmp-required"> no
				        	<input type="radio" name="s2_packagesExpedited" value="yes" class="toogle-yes tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 2 -->

		<div class="row"><!-- .haveOtherHide -->
			<div class="col col-md-3">
				<span>
					Do you want to add insurance?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s2_addInsurance" value="no" class="toogle-no tmp-required"> no
				        	<input type="radio" name="s2_addInsurance" value="yes" class="toogle-yes tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 3 -->

		<div class="row"><!-- .haveOtherHide -->
			<div class="col col-md-3">
				<span>
					Do you want repackage in case of shipping delicate, fragile, high-value, etc. merchandise?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s2_wantRepackage" value="no" class="toogle-no s2_wantRepackage tmp-required"> no
				        	<input type="radio" name="s2_wantRepackage" value="yes" class="toogle-yes s2_wantRepackage tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
				<?php /* REMOVED
				<div class="row s2_wantRepackageHide">
					<div class="col col-md-3">
						<input type="checkbox" name="s2_repackageType" value="Lower weight" class="tmp2-required"> Lower weight
					</div>
					<div class="col col-md-3">
						<input type="checkbox" name="s2_repackageType" value="Fragile" class="tmp2-required"> Fragile
					</div>
					<span class="msg-error"></span>
				</div>
				*/ ?>
			</div>
		</div><!-- row 4 -->

		<div class="row"><!-- .haveOtherHide -->
			<div class="col col-md-3">
				<span>
					Leave special instructions for us<br>
					<small>(Optional)</small>
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-16">
						<textarea name="s2_specialInstructions" class="form-control" rows="3" placeholder="Write your notes"></textarea>
						<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
				</div>
			</div>
		</div><!-- row 5 -->

		<div class="row text-center">
			<div class"col col-md-12">
				<button type="button" class="btn btn-default go-to-slide-1">Back</button>
				<button type="button" class="btn btn-success go-to-slide-3">Next</button>
				<br><br>
			</div>
		</div><!-- row 6 -->

	</div>

</div><!-- step-1 -->
