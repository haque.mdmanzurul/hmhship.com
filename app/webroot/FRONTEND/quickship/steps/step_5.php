<div class="wm-step step-5">
	<small>*required</small>
	<div class="group">
		<div class="row">
			<h2>Review and pay</h2>
			<div class="col col-md-12 info-pay">
				<div class="row first">
					<div class="col col-md-8">
						EST Shipping cost
					</div>
					<div class="col col-md-4 text-right">
						<strong class="s5_valuePrice"></strong>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-8">
						Handing fee
					</div>
					<div class="col col-md-4 text-right">
						<strong>$7.25</strong>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-8">
						Now, you only be charged for the shipping handing
					</div>
					<div class="col col-md-4 text-right highlight">
						<strong>Total charge now</strong><br>
						<strong class="s5_totalPrice"></strong>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col col-md-6">
				<input type="radio" name="s5_paymentType" class="s5_paymentType required" value="CreditCard"> Credit card (Visa, MasterCard, Amex)
			</div>
			<div class="col col-md-6">
		        <input type="radio" name="s5_paymentType" class="s5_paymentType required" value="Paypal"> Paypal
			</div>
			<span class="msg-error"></span>
		</div>
	</div>

	<div class="group s5_hidePaymentCreditCard">
		<div class="row text-center">
			<div class="col col-md-12">
				<a href="#" class="btn btn-success" title="">botão paypal</a>
			</div>
		</div>
	</div>

	<div class="group s5_hidePaymentPaypal">
		<div class="row">
			<div class="col col-md-3">
				<span>
					Enter your credit card details *
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<input type="text" name="s5_nameOnCard" class="form-control required" placeholder="Name on card">
				      	<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-4">
				      	<input type="text" name="s5_cardNumber" class="form-control required" placeholder="Card number">
				      	<span class="msg-error"></span>
					</div>
					<div class="col col-md-2">
				      	<input type="text" name="s5_CVV" class="form-control required" placeholder="CVV">
				      	<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-3">
				      	<input type="text" name="s5_cardMonth" class="form-control required" placeholder="MM">
				      	<span class="msg-error"></span>
					</div>
					<div class="col col-md-3">
				      	<input type="text" name="s5_cardYear" class="form-control required" placeholder="YYYY">
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 1 -->
	</div>
	<div class="group">
		<div class="row">
			<div class="col col-md-3">
				<span>
					Do you allow us to charge you the shipping cost before package has been received?
				</span>
				<br>
				<small>
					If the shipping cost is different, you'll be refunded or charged at a later date
				</small>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s5_allowShipCost" id="s5_allowShipCostNo" value="no" class="toogle-no s5_allowShipCost required"> no
				        	<input type="radio" name="s5_allowShipCost" id="s5_allowShipCostYes" value="yes" class="toogle-yes s5_allowShipCost required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 2 -->

		<div class="row s5_hideAllowShipCost">
			<div class="col col-md-3">
				<span>
					Do you authorize us to make the charge for shipping upon receipt?
				</span>
				<br>
				<small>
					Otherwise, will be notified of the shipping charge, and you will only be charged upon agreement
				</small>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s5_authCharge" value="no" class="toogle-no tmp-required"> no
				        	<input type="radio" name="s5_authCharge" value="yes" class="toogle-yes tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 3 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					Do you agree to be charged at a later date if necessary before payment is processed? *
				</span>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s5_agreeCharge" value="no" class="toogle-no required"> no
				        	<input type="radio" name="s5_agreeCharge" value="yes" class="toogle-yes required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 4 -->

		<div class="row">
			<div class="col col-md-3">
				<span>
					Do you agree to terms and conditions?
				</span>
				<br>
				<a href="#" title="Read our Terms and Conditions">Read our Terms and Conditions</a>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<!--<input type="radio" checked="checked" name="s5_agreeTerms" value="" class="toogle-no required pay-required"> no-->
				        	<input type="radio" name="s5_agreeTerms" value="yes" class="toogle-yes required pay-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 5 -->
	</div>

	<div class="group">
		<div class="row text-center">
			<div class"col col-md-12">
				<button type="button" class="btn btn-default go-to-slide-3 simpleSteps">Back 1</button>
				<button type="button" class="btn btn-default go-to-slide-4 hardSteps">Back 2</button>
				<!--button type="button" class="btn btn-default go-to-slide-2 hardSteps hideBackBtn">Back 2</button-->
				<button type="button" class="btn btn-success go-to-slide-6 s5_hidePaymentPaypal">Confirm</button>
				<button type="button" class="btn btn-success go-to-slide-6b s5_hidePaymentCreditCard">Confirm b</button>
				<br><br>
			</div>
		</div><!-- row 6 -->
	</div>

</div><!-- step-1 -->