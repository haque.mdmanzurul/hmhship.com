module.exports = function(grunt) {

	// ===========================================================================
	// CONFIGURE GRUNT ===========================================================
	// ===========================================================================
	grunt.initConfig({

		// get the configuration info from package.json ----------------------------
		// this way we can use things like name and version (pkg.name)
		pkg: grunt.file.readJSON('package.json'),

		// configure jshint to validate js files -----------------------------------
		jshint: {
			options: {
				reporter: require('jshint-stylish')
			},
			all: ['Gruntfile.js', 'js/src/**/*.js']
		},

		// configure uglify to minify js files -------------------------------------
		uglify: {
			angular: {
				files: {
					'js/angular-bundle.min.js': ['FRONTEND/_init/bower_components/angular/angular.min.js',
                                            'FRONTEND/_init/bower_components/angular-route/angular-route.min.js',
                                            'FRONTEND/_init/bower_components/angular-resource/angular-resource.min.js',
                                            'FRONTEND/_init/bower_components/angular-sanitize/angular-sanitize.min.js',
                                            'FRONTEND/_init/bower_components/angular-animate/angular-animate.min.js',
                                            'FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.min.js',
                                            'FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap.min.js',
                                            'FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
                                            'FRONTEND/_init/bower_components/ngstorage/ngStorage.min.js',
                                            'FRONTEND/_init/bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.min.js',
                                            'FRONTEND/_init/bower_components/ng-dialog/js/ngDialog.min.js',
                                            'FRONTEND/js/dev/plugins/bootstrap.min.js'                                           
                                        ]
				}
			},
                        custom: {
                            options: {                                 
                                mangle: false // mangle breaks it                                
                            },  
                            files: {
                                'js/app.min.js': [ 'FRONTEND/js/modules/app.js', 'FRONTEND/js/modules/QuickshipSvc.js'],
                                'js/calculator.min.js': [ 'FRONTEND/js/calculator/app.js', 'FRONTEND/js/calculator/QuickshipSvc.js']
                            }
                        }
		},


		// compile less stylesheets to css -----------------------------------------
	    /*  
            // 12/18/2017 we are going to start just modifying app.css directly
            less: {
			compileCore: {
				options: {
					strictMath: true,
					sourceMap: true,
					outputSourceFiles: true,
					sourceMapURL: 'app.css.map',
					sourceMapFilename: 'css/app.css.map'
				},
				files: {
					'css/app.css': 'src/less/app.less'
				}
			}
		},  */

		// configure cssmin to minify css files ------------------------------------
		cssmin: {
			build: {
				files: {
					'css/app.min.css': 'css/app.css',
					'css/angular-toggle-switch.min.css': 'FRONTEND/css/dev/angular-toggle-switch.css',
					'css/select.min.css': 'FRONTEND/_init/bower_components/angular-ui-select/dist/select.css',
					'css/ng-tags-input.min.css': 'FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.css',
					'css/ng-tags-input.bootstrap.min.css': 'FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.bootstrap.css',
					'css/ngDialog.min.css': 'FRONTEND/_init/bower_components/ng-dialog/css/ngDialog.css',
					'css/ngDialog-theme-default.min.css': 'FRONTEND/_init/bower_components/ng-dialog/css/ngDialog-theme-default.css'
				}
			}
		},

		// configure watch to auto update ------------------------------------------
		watch: {
			stylesheets: {
				files: ['css/**/*.css', 'src/less/**/*.less'],
				tasks: ['less', 'cssmin']
			},
			scripts: {
				files: 'js/src/**/*.js',
				tasks: ['jshint', 'uglify']
			}
		}

	});

	// ===========================================================================
	// LOAD GRUNT PLUGINS ========================================================
	// ===========================================================================
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// ===========================================================================
	// CREATE TASKS ==============================================================
    // ===========================================================================
	grunt.registerTask('default', ['jshint', 'uglify', 'cssmin']);
	//grunt.registerTask('default', ['jshint', 'uglify', 'less', 'cssmin']);
	//grunt.registerTask('default', ['cssmin', 'less']);

};