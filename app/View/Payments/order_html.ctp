﻿<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="282" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 0px 9px 18px;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">
                        
                            <span style="font-size:14px"><strong>Personal Information:</strong></span><br>
<?php echo $BILLING_FIRSTNAME; ?>&nbsp;<?php echo $BILLING_LASTNAME; ?><br>
<?php echo $BILLING_ADDRESS1; ?>&nbsp;<?php echo $BILLING_ADDRESS2; ?><br>
<?php echo $BILLING_CITY; ?>&nbsp;<?php echo $BILLING_STATE; ?>&nbsp;<?php echo $BILLING_POSTAL_CODE; ?><br>
<?php echo $BILLING_COUNTRY; ?><br>
<?php echo $BILLING_EMAIL; ?>&nbsp;<?php echo $BILLING_PHONE; ?>
                        </td>
                    </tr>
                </tbody></table>
                
                <table align="right" border="0" cellpadding="0" cellspacing="0" width="282" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 18px;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">
                        
                            <span style="font-size:14px"><strong>Shipping Information:</strong></span><br>
<?php echo $SHIPPING_FIRSTNAME; ?>&nbsp;<?php echo $SHIPPING_LASTNAME; ?><br>
<?php echo $SHIPPING_ADDRESS1; ?>&nbsp;<?php echo $SHIPPING_ADDRESS2; ?><br>
<?php echo $SHIPPING_CITY; ?>&nbsp;<?php echo $SHIPPING_STATE; ?>&nbsp;<?php echo $SHIPPING_POSTAL_CODE; ?><br>
<?php echo $SHIPPING_COUNTRY; ?><br>
<?php echo $SHIPPING_EMAIL; ?>&nbsp;<?php echo $SHIPPING_PHONE; ?>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width:100%;" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 18px;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">
                        
                            <?php echo $PACKAGES; ?>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table><table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
    <tbody class="mcnTextBlockOuter">
        <tr>
            <td valign="top" class="mcnTextBlockInner">
                
                <table align="left" border="0" cellpadding="0" cellspacing="0" width="282" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 0px 9px 18px;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">
                        
                            <span style="font-size:14px"><strong>Additional Options:</strong></span><br>
<br>
Consolidate&nbsp;<?php echo $SHIPMENT_CONSOLIDATE; ?><br>
Expedite&nbsp;<?php echo $SHIPMENT_EXPEDITED; ?><br>
Insurance&nbsp;<?php echo $SHIPMENT_INSURED; ?><br>
Repack&nbsp;<?php echo $SHIPMENT_REPACKAGED; ?><br>
Special Instructions&nbsp;<?php echo $SHIPMENT_SPECIAL_INSTRUCTIONS; ?>
                        </td>
                    </tr>
                </tbody></table>
                
                <table align="right" border="0" cellpadding="0" cellspacing="0" width="282" class="mcnTextContentContainer">
                    <tbody><tr>
                        
                        <td valign="top" class="mcnTextContent" style="padding: 9px 18px;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">
                        
                            <span style="font-size:14px"><strong>Payment Details:</strong></span><br>
<br>
Shipping charge&nbsp;<?php echo $CHARGE_NOW; ?><br>
Handling fee&nbsp;<?php echo $HANDLING_FEE; ?><br>
EST shipment cost&nbsp;<?php echo $EST_SHIPPING_COST; ?><br>
Payment method&nbsp;<?php echo $PAYMENT_METHOD; ?>
                        </td>
                    </tr>
                </tbody></table>
                
            </td>
        </tr>
    </tbody>
</table>