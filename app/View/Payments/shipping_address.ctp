<section class="form-quickship" id="quickship">
	<div class="container">

		<div class="top-box-circle">
			<div class="circle"></div>
			<div class="icon">
				<i class="icon-quickship"></i>
			</div>
		</div>
		<div class="wrap" id="quickshipFrm">
			<p class="text-center">
				<strong>
          <?php echo $msg; ?>

				</strong>
			</p>
			<br><br>

			<p class="text-center" style="font-size: 20px;">
				<?php echo Hash::get($this->Transaction->extractAddress($data, 'billing'), 'firstname') . ' ' . Hash::get($this->Transaction->extractAddress($data, 'billing'), 'lastname'); ?>
                                 c/o <?php echo $co_code; ?>
				<br>
				<?php echo $address1 . " " .$address2; ?><br>
				<?php echo $address3 . " USA"; ?><br>

        <br/><br/><br/>
          <?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>
          <span style="text-align:middle;">
            <a href="/Users/myaccount">
              <input class="btn-send" type="button" value="Access My Account" id="btnMyAccount">
            </a>
          </span>
          <?php } ?>
			</p>
		</div>
	</div>
</section>
