<?php foreach ($data['Shipment'] as $package_number => $shipment): ?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width:100%;">
		<tbody class="mcnTextBlockOuter">
		<tr>
			<td valign="top" class="mcnTextBlockInner">

				<table align="left" border="0" cellpadding="0" cellspacing="0" width="282"
					   class="mcnTextContentContainer">
					<tbody>
					<tr>

						<td valign="top" class="mcnTextContent"
							style="padding: 9px 0px 9px 0;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">

							<strong><span style="font-size:14px">Package Details:</span></strong><br/>
							<span
								style="color:#FF8C00"><?php echo sprintf(__('Package %s'), $package_number + 1); ?></span><br/>
							<?php echo(is_numeric($shipment['Package']['weight_kg']) ? $shipment['Package']['weight_kg'] . ' kg' : $shipment['Package']['weight_lb'] . ' lb'); ?>
							<br/>
							<?php echo(is_numeric($shipment['Package']['length_cm']) ? $shipment['Package']['length_cm'] . ' cm'  . 'x' .   $shipment['Package']['width_cm'] . 'x' . $shipment['Package']['height_cm']  : $shipment['Package']['length_in'] . ' in' . 'x' . $shipment['Package']['width_in'] . 'x' . $shipment['Package']['height_in'] ); ?>
							<br/>
							Inbound Tracking Number: <?php echo $shipment['Package']['in_tracking']; ?>
                  <br/>
							Inbound Carrier: <?php echo $shipment['Package']['carrier']; ?>
                <br/>
              Outbound Tracking Number: <?php echo $shipment['Package']['out_tracking']; ?>
                  <br/>
               Outbound Carrier: <?php echo $shipment['Package']['out_carrier']; ?>
						</td>
					</tr>
					</tbody>
				</table>

				<table align="right" border="0" cellpadding="0" cellspacing="0" width="282"
					   class="mcnTextContentContainer">
					<tbody>
					<tr>

						<td valign="top" class="mcnTextContent"
							style="padding: 9px 0;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">

							<span style="font-size:14px"><strong>Shipping Method:</strong></span><br>
							<span
								style="color:#FF8C00"><?php echo sprintf(__('Package %s'), $package_number + 1); ?></span><br>
							<?php echo $shipment['Package']['easypost_rate_description']; ?><br>
							<?php echo $shipment['Package']['easypost_rate_price']; ?><br>
							&nbsp;
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<?php foreach ($shipment['Package']['Item'] as $item): ?>
		<table align="left" border="0" cellpadding="0" cellspacing="0" width="282" class="mcnTextContentContainer">
			<tbody>
			<tr>

				<td valign="top" class="mcnTextContent"
					style="padding: 9px 0;color: #000000;font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;font-size: 13px;line-height: 125%;text-align: left;">

					<strong><span style="font-size:14px">Item Details:</span></strong><br>
					<?php echo $item['description']; ?>&nbsp;<?php echo $item['quantity']; ?>&nbsp;<?php echo (is_numeric($item['weight_kg']) ? $item['weight_kg'] .' kg' : $item['weight_lb'].' lb'); ?><br>
					<?php echo $item['ItemType']['description']; ?>&nbsp;<?php echo $item['Country']['name']; ?>&nbsp;<?php echo sprintf(__('%s USD'), $item['value']); ?><br>
					<?php echo $item['info']; ?>
				</td>
			</tr>
			</tbody>
		</table>
	<?php endforeach; ?>
<?php endforeach; ?>