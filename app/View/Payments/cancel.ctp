<div class="container">
    <div class="top-box-circle">
        <div class="circle"></div>
        <div class="icon">
            <i class="icon-quickship"></i>
        </div>
    </div>
    <div class="wrap">
        <div class="row">
            <div class="col-sm-12">
                <h3>Payment cancelled</h3>
            </div>
        </div>
    </div>
</div>