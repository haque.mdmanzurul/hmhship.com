<div class="login-box row">
    <!-- /.login-logo -->
    <div class="login-box-body col-sm-4">
		<?php echo $this->Flash->render('auth'); ?>
        <p class="login-box-msg">Sign in to start your session</p>
        <?php echo $this->Form->create('Admin', array(
            'role' => 'form',
            'novalidate' => true,
            'inputDefaults' => array(
                'div' => array(
                    'class' => 'form-group has-feedback'
                ),
                'label' => false,
                'class' => 'form-control',
                'format' => array(
                    'before', 'label', 'between', 'input', 'error', 'after'
                ),
            )
        )); ?>

        <?php echo $this->Form->input('username', array(
            'placeholder' => __('Username'),
            'after' => '<span class="glyphicon glyphicon-envelope form-control-feedback"></span>'
        )); ?>

        <?php echo $this->Form->input('password', array(
            'placeholder' => __('Password'),
            'after' => '<span class="glyphicon glyphicon-lock form-control-feedback"></span>'
        )); ?>

        <div class="row">
            <!-- /.col -->
            <div class="col-xs-4">
                <?php echo $this->Form->submit(__('Sign In'), array('class' => 'btn btn-primary btn-block btn-flat', 'div' => false)); ?>
            </div>
            <!-- /.col -->
        </div>
        <?php echo $this->Form->end(); ?>


    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->

<script type="text/javascript">
    localStorage.clear();
</script>

