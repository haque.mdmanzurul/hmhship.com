<?php
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = "";
}
?>
<section id="innerpanel" class="filter-wrap">

  <div class="topicons">
    
    <span class="sort-opt-wrap">
      <a href="javascript:;" ng-click="registereds_sort_reverseit()"><span class="icon-sort"  ></span>
      </a>
     
    </span>
  </div>
  <div class="search-wrap">
    <input ng-model="query" type="text" placeholder="Search" style="height:35px;"/>
    <span class="icon-search"></span>
  </div>
  <div class="visible-mobile expander">
          <span class="icon-arrowdown"></span>
      </div>
  <form name="batchSelection">
	<ul class="panellist sortable">
    <li ng-repeat="registered in registereds | filter: {$: query} | orderBy:'obj.User.created':registereds_sort_reverse"
        ng-class="{'active': selectedItem == registered.obj.User.id, 'inactive': selectedItem != registered.obj.User.id}" 
        data-date="{{registered.obj.User.created}}">

		<a ng-href="#/registered/overview/{{registered.obj.User.id}}">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Registered Acct #{{registered.obj.User.id}}
                </span>
                <span class="date">
                  {{registered.obj.User.created | date:'MM/dd/yyyy'}}
                </span>

        		<span ng-hide="registered.obj.User.read === true" class="new">N</span>
		</a>
     </li>   
   </ul>

   </form>
    
   
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>

<section id="innercontent" data-ng-view="">
	<div class="loading loading1"><div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div></div>
</section>