<?php
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = "";
}
?>
<section id="innerpanel" class="filter-wrap">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <!--<div class="topicons">
        <a href="#/messages">
            <span class="icon-home"></span>
            <span class="innertitle">Messages</span>
        </a>
        <span class="markunread wrap">
            <span class="icon-message lefticon"></span>Mark as Unread
        </span>
        <span class="deletemsg wrap">
            <span class="icon-delete lefticon"></span>Delete
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search" />
        <span class="icon-search"></span>
    </div>-->
   
   <form name="batchSelection">
	<ul class="panellist sortable">
    <li ng-repeat="business in businesssignups" ng-class="{'active': selectedItem == business.BusinessSignup.id, 'inactive': selectedItem != business.BusinessSignup.id}" data-date="{{business.BusinessSignup.created}}">

		<a ng-href="#/business/overview/{{business.BusinessSignup.id}}">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Signup #{{business.BusinessSignup.id}}
                </span>
                <span class="date">
					{{business.BusinessSignup.created | date:'MM/dd/yyyy'}}
                </span>

        		<span ng-hide="business.BusinessSignup.read === true" class="new">N</span>
		</a>
    </li>   
        </ul>
   </form>
    
   
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>

<section id="innercontent" data-ng-view="">
	<div class="loading loading1"><div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div></div>
</section>