<section id="innerpanel" class="filter-wrap sortable-wrap">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <div class="topicons">
        <a href="?section=regcust">
            <span class="icon-home"></span>
            <span class="innertitle">Registered Customers</span>
        </a>
        <span class="wrap">
            <span class="icon-status lefticon"></span>Edit Multiple Status
        </span>
       <span class="sort-opt-wrap">
            <span class="icon-sort"></span>
            <ul class="sort-options">
                <li class="active" rel="suite">Suite #</li>
                <li rel="date">Latest updated</li>
                <li rel="name">Name</li>
            </ul>
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search" />
        <span class="icon-search"></span>
    </div>
    <div class="filter-opt-wrap">
        <span class="icon-users lefticon"></span><span class="filter-title">All Users</span><span class="icon-arrowdown"></span>
        <ul class="filter-options" data-type-active="user">
            <li rel="all" data-type="user"><span class="icon-user lefticon"></span><span class="top-title">All Users</span></li>
            <li class="sub" rel="newshipment" data-type="user">New Shipment</li>
            <li class="sub" rel="assistus" data-type="user">AssistUS Shopper</li>
            <li class="sub" rel="preferred" data-type="user">PreferredPack</li>
            <li rel="all" data-type="transaction"><span class="icon-alltransactions lefticon"></span><span class="top-title">All Transactions</span></li>
            <li class="sub" rel="newshipment" data-type="transaction">New Shipment</li>
            <li class="sub" rel="assistus" data-type="transaction">AssistUS Shopper</li>
            <li class="sub" rel="preferred" data-type="transaction">PreferredPack</li>
        </ul>
    </div>
    <ul class="panellist filter-list sortable">
        <li <?php if($page == 335){ echo "class='active'"; } ?> data-suite="335" data-name="Michael" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=regcust&page=335">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #335
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Michael</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 397){ echo "class='active'"; } ?> rel="assistus" data-suite="397" data-name="John Doe" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=regcust&page=397">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #397
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>John Doe</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 355){ echo "class='active'"; } ?> rel="assistus" data-suite="355" data-name="Alan Turing" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=regcust&page=355">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #355
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Alan Turing</p>
            </a>
        </li>
        <li <?php if($page == 895){ echo "class='active'"; } ?> rel="assistus" data-suite="895" data-name="John Doe" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=regcust&page=895">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #895
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>John Doe</p>
            </a>
        </li>
        <li <?php if($page == 695){ echo "class='active'"; } ?> rel="preferred"  data-suite="695" data-name="Alan Turing" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=regcust&page=695">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #695
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Alan Turing</p>
            </a>
        </li>
        <li <?php if($page == 12335){ echo "class='active'"; } ?> rel="preferred" class="typefiltered" data-status="paymreceived" data-date="2015-02-05" data-transaction="12335" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=regcust&type=transaction&page=12335">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#12335
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 11397){ echo "class='active'"; } ?> rel="preferred" class="typefiltered" data-status="paymreceived" data-date="2015-02-05" data-transaction="11397" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=regcust&type=transaction&page=11397">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#11397
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 12355){ echo "class='active'"; } ?> rel="preferred" class="typefiltered" data-status="paymreceived" data-date="2015-02-05" data-transaction="12355" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=regcust&type=transaction&page=12355">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#12355
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
            </a>
        </li>
        <li <?php if($page == 18895){ echo "class='active'"; } ?> rel="assistus" class="typefiltered" data-status="awaitingpack" data-date="2015-02-05" data-transaction="18895" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=regcust&type=transaction&page=18895">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#18895
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="awaitingpack"></span>Awaiting Package</p>
            </a>
        </li>
        <li <?php if($page == 18695){ echo "class='active'"; } ?>  rel="newshipment" class="typefiltered" data-status="awaitingpack" data-date="2015-02-05" data-transaction="18695" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=regcust&type=transaction&page=18695">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#18695
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="awaitingpack"></span>Awaiting Package</p>
            </a>
        </li>
    </ul>
    <button id="loadmore">Load more</button>
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>
<section id="innercontent">

    <?php if ($page == "" || $page == 'regdashboard') : ?>
        <?php echo $this->element('admin/registered_customers/dashboard'); ?>
    <?php elseif ($type == 'transaction') : ?>
        <?php echo $this->element('admin/registered_customers/regtransinner'); ?>
    <?php else : ?>
        <?php echo $this->element('admin/registered_customers/reginner'); ?>
    <?php endif; ?>

</section>