<div class="container-fluid">
  <section id="dateselect" class="hidden">
    <div class="dateselector">
      <span class="icon-calendar lefticon"></span>
      <span class="datelabel">June 2nd,2015 - July 1st, 2015</span>
      <span class="icon-arrowdown righticon"></span>
    </div>
    <hr />
  </section>
  <section id="actionboxes">
    <div class="row">
      <div class="col-md-4">
        <div class="actionbox" rel="qsactions">
          <div class="icon-wrap">
            <span class="icon-quickship"></span>
          </div>
          <h4 class="title">QuickShip</h4>
          <h4 class="notification">
            You have <span class="primary">
              <?php echo $quickship_unread_count; ?>
            </span> notifications
          </h4>
          <a href="<?php echo $this->Html->url(array('controller' => 'quickship', 'admin' => true, 'action' => 'index')); ?>">
            <h5>Show me more</h5>
          </a>
        </div>
      </div>

      <div class="col-md-4">
        <div class="actionbox" rel="qsactions">
          <div class="icon-wrap">
            <span class="icon-message"></span>
          </div>
          <h4 class="title">Messages</h4>
          <h4 class="notification">
            You have <span class="primary">
              <?php echo $messages_unread_count; ?>
            </span> notifications
          </h4>         
          <a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'messages')); ?>#/messages">
            <h5>Show me more</h5>
            </a>          
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="actionbox" rel="qsactions">
          <div class="icon-wrap">
            <span class="icon-message"></span>
          </div>
          <h4 class="title">Messages - Registered</h4>
          <h4 class="notification">
            You have <span class="primary">
              <?php echo $messages_registered_unread_count; ?>
            </span> notifications
          </h4>
          <a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'messages_registered')); ?>#/messages_registered">
            <h5>Show me more</h5>
          </a>
        </div>
      </div>

      <div class="col-md-4">
        <div class="actionbox" rel="qsactions">
          <div class="icon-wrap">
            <span class="icon-user"></span>
          </div>
          <h4 class="title">Business Customers</h4>
          <h4 class="notification">
            You have <span class="primary">
              <?php echo $business_unread_count; ?>
            </span> notifications
          </h4>
          <a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'business')); ?>#/business">
            <h5>Show me more</h5>
          </a>
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-md-4">
        <div class="actionbox" rel="qsactions">
          <div class="icon-wrap">
            <span class="icon-quickship"></span>
          </div>
          <h4 class="title">New Shipments</h4>
          <h4 class="notification">
            You have <span class="primary">
              <?php echo $quickship_registered_unread_count; ?>
            </span> notifications
          </h4>
          <a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'quickship', 'action' => 'registered')); ?>#/quickship_registered">
            <h5>Show me more</h5>
          </a>
        </div>
      </div>
    </div>
    
  </section>
 
</div>