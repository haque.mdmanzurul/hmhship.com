<?php
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = "";
}
?>
<section id="innerpanel" class="filter-wrap sortable-wrap">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <div class="topicons">
        <a href="?section=allusers&type=reg">
            <span class="icon-home"></span>
            <span class="innertitle">Registered Customers</span>
        </a>
       <span class="sort-opt-wrap">
            <span class="icon-sort"></span>    
            <ul class="sort-options">
                <li class="active" rel="suite">Suite #</li>
                <li rel="date">Latest updated</li>
                <li rel="name">Name</li>
            </ul>
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search" />
        <span class="icon-search"></span>
    </div>
    <div class="filter-opt-wrap">
        <span class="icon-users lefticon"></span><span class="filter-title">All Users</span><span class="icon-arrowdown"></span>
        <ul class="filter-options">
            <li rel="regcust" data-type="user">Registered Customers</li>
            <li rel="quickship" data-type="user">QuickShip</li>
        </ul>
    </div>
    <ul class="panellist filter-list sortable">
        <li <?php if($page == 335){ echo "class='active'"; } ?> rel='regcust' data-suite="335" data-name="Michael" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=allusers&type=reg&page=335">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #335
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Michael</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 397){ echo "class='active'"; } ?> rel="quickship" data-suite="397" data-name="John Doe" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=allusers&type=QS&page=397">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>QS#39748
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>John Doe</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 355){ echo "class='active'"; } ?> rel="quickship" data-suite="355" data-name="Alan Turing" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=allusers&type=QS&page=355">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>QS#35543
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Alan Turing</p>
            </a>
        </li>
        <li <?php if($page == 895){ echo "class='active'"; } ?> rel="regcust" data-suite="895" data-name="John Doe" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=allusers&type=reg&page=895">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #895
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>John Doe</p>
            </a>
        </li>
        <li <?php if($page == 695){ echo "class='active'"; } ?> rel="regcust"  data-suite="695" data-name="Alan Turing" data-date="2015-02-05" data-type="user">
            <input type="checkbox" />
            <a href="?section=allusers&type=reg&page=695">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>Suite #695
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p>Alan Turing</p>
            </a>
        </li>
    </ul>
    <button id="loadmore">Load more</button>
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>
<section id="innercontent">

    <?php
    if($page == "" || $page == 'allusersdb')
    {
        echo $this->element('admin/allusers/allusersdb');
    }
    else
    {
        echo $this->element('admin/allusers/allusersinner');
    }
    ?>
</section>