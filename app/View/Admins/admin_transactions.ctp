<?php
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = "";
}
?>
<section id="innerpanel" class="filter-wrap sortable-wrap">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <div class="topicons">
        <a href="?section=regcust">
            <span class="icon-home"></span>
            <span class="innertitle">Registered Customers</span>
        </a>  
       <span class="sort-opt-wrap">
            <span class="icon-sort"></span>    
            <ul class="sort-options">
                <li class="active" rel="transaction">Transaction #</li>
                <li rel="date">Latest updated</li>
                <li rel="name">Name</li>
            </ul>
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search" />
        <span class="icon-search"></span>
    </div>
    <div class="filter-opt-wrap">
        <span class="icon-alltransactions lefticon"></span><span class="filter-title">All Transactions</span><span class="icon-arrowdown"></span>
        <ul class="filter-options" data-type-active="user">
            <li rel="newshipment" data-type="transaction">New Shipment</li>
            <li rel="assistus" data-type="transaction">AssistUS Shopper</li>
            <li rel="preferred" data-type="transaction">PreferredPack</li>
        </ul>
    </div>
    <ul class="panellist filter-list sortable">

        <li <?php if($page == 12335){ echo "class='active'"; } ?> data-status="paymreceived" data-date="2015-02-05" data-transaction="12335" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=alltransactions&type=NS&page=12335">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#12335
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 11397){ echo "class='active'"; } ?> data-status="paymreceived" data-date="2015-02-05" data-transaction="11397" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=alltransactions&type=NS&page=11397">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#11397
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
                <span class="new">N</span>
            </a>
        </li>
        <li <?php if($page == 12355){ echo "class='active'"; } ?> data-status="paymreceived" data-date="2015-02-05" data-transaction="12355" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=alltransactions&type=NS&page=12355">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#12355
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="paymreceived"></span>Payment received</p>
            </a>
        </li>
        <li <?php if($page == 18895){ echo "class='active'"; } ?> data-status="awaitingpack" data-date="2015-02-05" data-transaction="18895" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=alltransactions&type=NS&page=18895">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#18895
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="awaitingpack"></span>Awaiting Package</p>
            </a>
        </li>
        <li <?php if($page == 18695){ echo "class='active'"; } ?> data-status="awaitingpack" data-date="2015-02-05" data-transaction="18695" data-type="transaction">
            <input type="checkbox" />
            <a href="?section=alltransactions&type=NS&page=18695">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>NS#18695
                </span>
                <span class="date">
                    01/04/2015
                </span>
                <p><span class="awaitingpack"></span>Awaiting Package</p>
            </a>
        </li>
    </ul>
    <button id="loadmore">Load more</button>
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>

<section id="innercontent">
    <?php
    if($page == "" || $page == 'alltransdb')
    {
        echo $this->element('admin/alltransactions/alltransdb');
    }
    else
    {
        echo $this->element('admin/alltransactions/alltransinner');
    }
    ?>
</section>