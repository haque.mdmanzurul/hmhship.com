<section id="innerpanel" class="sortable-wrap">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <div class="topicons">
        <a href="?section=qscust">
            <span class="icon-home"></span>
            <span class="innertitle">QuickShip Customers</span>
        </a>
        <a href="?section=qscust&page=multistatus">
            <span class="wrap">
                <span class="icon-status lefticon"></span>Edit Multiple Status
            </span>
        </a>
       <span class="sort-opt-wrap">
            <span class="icon-sort"></span>
            <ul class="sort-options">
                <li class="active" rel="transaction">Transaction #</li>
                <li rel="date">Latest updated</li>
                <li rel="status">Status</li>
            </ul>
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search"/>
        <span class="icon-search"></span>
    </div>


    <div class="list-title">
        <span class="active-opt"><span class="icon-alltransactions lefticon"></span>All Transactions</span>
    </div>

    <ul class="panellist sortable">
		<?php foreach($transactions as $transaction): ?>
		<li class="<?php echo ($page === $transaction['Transaction']['id'] ? 'active' : 'inactive'); ?>" data-status="paymreceived" data-date="<?php echo date('Y-m-d', strtotime($transaction['Transaction']['date'])); ?>">
			<input type="checkbox"/>
			<a href="?section=qscust&page=<?php echo $transaction['Transaction']['id']; ?>">
                <span class="dblist-title">
                    <span class="icon-user lefticon"></span>QS#<?php echo sprintf('%05s', $transaction['Transaction']['id']); ?>
                </span>
                <span class="date">
                    <?php echo date('m/d/Y', strtotime($transaction['Transaction']['date'])); ?>
                </span>

				<p><span class="paymreceived"></span><?php echo $transaction_statuses[$transaction['Transaction']['status']]; ?></p>
				<span class="new">N</span>
			</a>
		</li>
		<?php endforeach; ?>
       
    </ul>
    <button id="loadmore">Load more</button>
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>
<section id="innercontent">
    <?php if ($page == "" || $page == 'qsdashboard'): ?>
        <?php echo $this->element('admin/quickship_customers/qsdashboard'); ?>
    <?php else: ?>
        <?php if ($page == "multistatus"): ?>
            <?php echo $this->element('admin/quickship_customers/multistatus'); ?>
        <?php else: ?>
            <?php echo $this->element('admin/quickship_customers/qsinner'); ?>
        <?php endif; ?>
    <?php endif; ?>
</section>