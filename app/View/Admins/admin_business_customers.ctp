<?php
if(isset($_GET['page']))
{
    $page = $_GET['page'];
}
else
{
    $page = "";
}
?>
<section id="innerpanel">
    <div class="visible-mobile expander">
        <span class="icon-arrowdown"></span>
    </div>
    <div class="topicons">
        <a href="?section=bizcust">
            <span class="icon-home"></span>
            <span class="innertitle">Business Customers</span>
        </a>
        <span class="markunread wrap">
            <span class="icon-message lefticon"></span>Mark as Unread
        </span>
        <span class="deletemsg wrap">
            <span class="icon-delete lefticon"></span>Delete
        </span>
    </div>
    <div class="search-wrap">
        <input type="text" placeholder="Search" />
        <span class="icon-search"></span>
    </div>
    <ul class="panellist hidden">

        <li <?php if($page == 12335){ echo "class='active'"; } ?>>
            <input type="checkbox" />
            <a href="?section=bizcust&page=12335">
                <span class="dblist-title">
                    <span class="icon-message lefticon"></span>Michael
                </span>
                <span class="date">
                    01:25
                </span>
                <p>Lorem ipsum ut duo...</p>
                <span class="new">N</span>
            </a>
        </li>


        <li <?php if($page == 11397){ echo "class='active'"; } ?>>
            <input type="checkbox" />
            <a href="?section=bizcust&page=11397">
                <span class="dblist-title">
                    <span class="icon-message lefticon"></span>John Doe
                </span>
                <span class="date">
                    01:25
                </span>
                <p>Lorem ipsum ut duo...</p>
                <span class="new">N</span>
            </a>
        </li>


        <li <?php if($page == 12355){ echo "class='active'"; } ?>>
            <input type="checkbox" />
            <a href="?section=bizcust&page=12355">
                <span class="dblist-title">
                    <span class="icon-message lefticon"></span>Alan Turing
                </span>
                <span class="date">
                    01:25
                </span>
                <p>Lorem ipsum ut duo...</p>
            </a>
        </li>


        <li <?php if($page == 18895){ echo "class='active'"; } ?>>
            <input type="checkbox" />
            <a href="?section=bizcust&page=18895">
                <span class="dblist-title">
                    <span class="icon-messageread lefticon"></span>John Doe
                </span>
                <span class="date">
                    01:25
                </span>
                <p>Lorem ipsum ut duo...</p>
            </a>
        </li>


        <li <?php if($page == 18695){ echo "class='active'"; } ?> >
            <input type="checkbox" />
            <a href="?section=bizcust&page=18695">
                <span class="dblist-title">
                    <span class="icon-messageread lefticon"></span>Alan Turing
                </span>
                <span class="date">
                    01:25
                </span>
                <p>Lorem ipsum ut duo...</p>
            </a>
        </li>

    </ul>
    <button id="loadmore">Load more</button>
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</section>
<section id="innercontent">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="text-muted">Not implemented</p>
            </div>
        </div>
    </div>
    <?php
    if($page == "" || $page == 'bizdashboard')
    {
        //echo $this->element('admin/bizcust/bizdashboard');
    }
    else
    {
        //echo $this->element('admin/bizcust/bizinner');
    }
    ?>
</section>