
<section class="form-quickship">
	<div class="container">
		<div class="top-box-circle">
			<div class="circle"></div>
			<div class="icon">
				<i class="icon-quickship"></i>
			</div>
		</div>
		<div class="wrap">
			<div class="wizard row wizard-box simpleSteps hidden">
                <ul class="wizard-steps">
                    <li data-target="#step1">
                        <span class="step">1</span>
                        <span class="title">Personal <br> information</span>
                    </li>
                    <li data-target="#step2">
                        <span class="step">2</span>
                        <span class="title">Shipment <br> Details</span>
                    </li>
                    <li data-target="#step3">
                        <span class="step">3</span>
                        <span class="title">Payment <br> Options</span>
                    </li>
                    <li data-target="#step4" class="active">
                        <span class="step">4</span>
                        <span class="title">Your <br> US Address</span>
                    </li>
                </ul>
            </div>

            <div class="wizard row wizard-box wizard-big hardSteps hidden">
                <ul class="wizard-steps">
                    <li data-target="#step1">
                        <span class="step">1</span>
                        <span class="title">Personal <br> information</span>
                    </li>
                    <li data-target="#step2">
                        <span class="step">2</span>
                        <span class="title">Additional <br> Options</span>
                    </li>
                    <li data-target="#step3">
                        <span class="step">3</span>
                        <span class="title">Package <br> Details</span>
                    </li>
                    <li data-target="#step4">
                        <span class="step">4</span>
                        <span class="title">Shipment <br> Rates</span>
                    </li>
                    <li data-target="#step5">
                        <span class="step">5</span>
                        <span class="title">Payment <br> Options</span>
                    </li>
                    <li data-target="#step6" class="active">
                        <span class="step">6</span>
                        <span class="title">Your <br> US Address</span>
                    </li>
                </ul>
            </div>
            
            <ul class="wm-slider">
    			<li>
    				<?php require_once('step_6.ctp'); ?>
    			</li>
            </ul>
		</div>
	</div>
</section>
