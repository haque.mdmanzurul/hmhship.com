<div class="wm-step step-3">
	<small>*required</small>
	<div class="group">
		<div class="row">
			<h2>Packages details</h2>
			<a href="#" class="link-top" title="Check prohibited itens">Check prohibited items</a>
			<div class="col col-md-3">
				<span>Please enter your package details here *</span><br><br>
				<small>(All consolidated shipments - up to 3 packages and gross dimensions up to 24"x14"x12" - are considered as one outgoing package)</small>
			</div>
			<div class="col-md-9">
				<div class="row">
					<div class="box-aditional">
						<div class="s3_boxPkgDetail">
                            <?php echo $this->element('add_package_form', array()); ?>
						</div>
						<div class="text-right">
							<div class="col col-md-12">
								<button type="button" class="btn btn-success s3_addPkgDetail">Add more</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="group">
		<div class="row">
			<h2>Item Details </h2>
			<div class="col col-md-3">
				Enter details for each item coming to us that you want to include in each package added *
			</div>
			<div class="col col-md-9">
				<div class="row s3_itemDetailWrapTabs">
					<div class="messageTabs"></div>
					<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
						<li class="active"><a href="#pkg1" data-toggle="tab">Package <span class="label-tab">1</span></a></li>
					</ul>
					<div id="my-tab-content" class="tab-content">
				        <?php echo $this->element('packages_items_container', array()); ?>
				    </div>
				</div>
				<div class="row text-right">
					<div class="col col-md-12">
						<button type="button" class="btn btn-success s3_addItemDetail">Add more</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="group simpleSteps">
		<div class="row">
			<h2>Additional options</h2>
			<div class="col col-md-3">
				<span>
					Do you have other packages you would like to be shipped consolidated into 1 larger package? (Up to 3 packages and gross dimensions up to 24"x14"x12")
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s3_haveOther" id="s3_haveOtherNo" class="toogle-no required s3_haveOther" value="no"> no
				        	<input type="radio" name="s3_haveOther" id="s3_haveOtherYes" class="toogle-yes required s3_haveOther" value="yes"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
				<div class="row haveOtherHide">
					<div class="box-aditional">
						<div class="box-title">
							<div class="row">
								<div class="col col-md-12">
									<h3>Additional Packages</h3>
								</div>
							</div>
						</div>
						<div class="s3_boxPackages">
							<div class="box-item">
								<div class="row">
									<div class="col col-md-3">
										<input type="text" name="s3_adPackageWeight[1]" class="form-control tmp-required" placeholder="Weight *">
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<select name="s3_adPackageUnit[1]" class="form-control tmp-required">
											<option value="lb">lb</option>
											<option value="kg">kg</option>
										</select>
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<input type="number"  min="1" name="s3_adPackageValue[1]" class="form-control tmp-required" placeholder="$xx.xx *">
										<span class="msg-error"></span>
									</div>
									<div class="col col-md-3">
										<button type="button" class="btn btn-danger s3_delPackage">Delete</button>
									</div>
								</div>
							</div>
						</div>
						<div class="action text-right">
							<div class="col col-md-12">
								<button type="button" class="btn btn-success s3_addMorePackages">Add more</button>
								<p class="text-right">
									if the gross dimensions exceed 24"x14"x12", you will be charged for an additional package
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- row 1 -->

		<div class="row haveOtherHide">
			<div class="col col-md-3">
				<span>
					Do you need these packages expedited?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s3_packagesExpedited" value="no" class="toogle-no tmp-required"> no
				        	<input type="radio" name="s3_packagesExpedited" value="yes" class="toogle-yes tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 2 -->

		<div class="row haveOtherHide">
			<div class="col col-md-3">
				<span>
					Do you want to add insurance?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s3_addInsurance" value="no" class="toogle-no tmp-required"> no
				        	<input type="radio" name="s3_addInsurance" value="yes" class="toogle-yes tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
			</div>
		</div><!-- row 3 -->

		<div class="row haveOtherHide">
			<div class="col col-md-3">
				<span>
					Do you want repackage in case of shipping delicate, fragile, high-value, etc. merchandise?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-6">
				      	<label class="radio-inline hide">
				        	<input type="radio" checked="checked" name="s3_wantRepackage" value="no" class="toogle-no s3_wantRepackage tmp-required"> no
				        	<input type="radio" name="s3_wantRepackage" value="yes" class="toogle-yes s3_wantRepackage tmp-required"> yes
				      	</label>
				      	<div class="toogle-parent">
				      		<div class="toggles toggle-soft" data-toggle-on="false"></div>
				      	</div>
				      	<span class="msg-error"></span>
					</div>
				</div>
				<?php /*
				<div class="row s3_wantRepackageHide">
					<div class="col col-md-3">
						<input type="checkbox" name="s3_repackageType" value="Lower weight" class="tmp2-required"> Lower weight
					</div>
					<div class="col col-md-3">
						<input type="checkbox" name="s3_repackageType" value="Fragile" class="tmp2-required"> Fragile
					</div>
					<span class="msg-error"></span>
				</div>
				*/ ?>
			</div>
		</div><!-- row 4 -->

		<div class="row haveOtherHide">
			<div class="col col-md-3">
				<span>
					Leave special instructions for us
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-16">
						<textarea name="s3_specialInstructions" class="form-control" rows="3" placeholder="Write your notes"></textarea>
						<span class="msg-error"></span>
					</div>
				</div>
				<div class="row">
				</div>
			</div>
		</div><!-- row 5 -->

		<div class="row haveOtherHide">
			<h2>Shipping options</h2>
			<div class="col col-md-3">
				<span>
					Which Shipping options would you like us to use?
				</span>
			</div>
			<div class="col col-md-9">
				<div class="row">
					<div class="col col-md-4">
						<input type="checkbox" name="s3_ShipOptUse[]" value="teste" class="tmp-required"> Cheapest
					</div>
					<div class="col col-md-4">
						<input type="checkbox" name="s3_ShipOptUse[]" value="teste" class="tmp-required"> Fastest
					</div>
					<span class="msg-error"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="group">
		<div class="row text-center">
			<div class"col col-md-12">
				<button type="button" class="btn btn-default go-to-slide-1 simpleSteps">Back 1</button>
				<button type="button" class="btn btn-default go-to-slide-2 hardSteps">Back 2</button>
				<button type="button" class="btn btn-success go-to-slide-5 simpleSteps">Next 1</button>
				<button type="button" class="btn btn-success go-to-slide-4 hardSteps">Next 2</button>
				<br><br>
			</div>
		</div><!-- row 6 -->
	</div>

</div><!-- step-3 -->