<div class="wm-step step-6">
	<div class="row">
		<div class="col col-md-12">
			<p class="text-center">
                <strong>
				We are done! You can start sending your packages to your US Address.<br>
				This address was also sent to your email. (not used)
                </strong>
			</p>
			<br><br>
			<p class="text-center" style="font-size: 20px;">
			<?php echo $name; ?><br>
			<?php echo $address1; ?><br>
			<?php echo $address2; ?><br>
			</p>
		</div>
	</div>
</div>
<script>
ACTIVE_STEPS = "<?php echo $active_steps; ?>";
</script>
