<section id="innerpanel" class="sortable-wrap">
	<div class="visible-mobile expander">
		<span class="icon-arrowdown"></span>
	</div>
	<div class="topicons">
		<a ng-click="shipments_filter_status=''">
			<span class="icon-home"></span>
		</a>

       <span class="sort-opt-wrap">
         <a href="javascript:;" ng-click="shipments_sort_reverseit()">
           <span class="icon-sort"></span>
         </a>
       </span>
	</div>
	<div class="search-wrap">
    <input ng-model="query" type="text" placeholder="Search"/>
		<span class="icon-search"></span>
	</div>

	<form name="batchSelection">
	<ul class="panellist sortable">
    <li ng-repeat="transaction in listItems_ghost | filter: {obj: {Transaction: {status:shipments_filter_status}}} | filter: {$: query} | orderBy:'obj.Shipment[0].created':shipments_sort_reverse"
         ng-class="{'active': selectedItem == transaction.obj.Transaction.id, 'inactive': selectedItem != transaction.obj.Transaction.id}"
         data-status="paymreceived" data-date="{{transaction.obj.Transaction.date_utc | date:'Y-m-d'}}">

      			<a ng-href="#/ghost/overview/{{transaction.obj.Transaction.id}}">
                <span class="dblist-title">
                      <span class="icon-user lefticon"></span>GS#{{ (transaction.obj.AddressBilling[0].firstname | limitTo:1) | uppercase }}{{ (transaction.obj.AddressBilling[0].lastname | limitTo:1) | uppercase }}{{transaction.obj.Transaction.user_id}}-{{transaction.obj.Transaction.user_id_repeat | zpad: 3}}


        </span>
                <div class="date">
                    {{(transaction.obj.Shipment[0].created | date:'MM/dd/yyyy') || ""}}

        </div>

        <div style="color:#444;">{{transactionStatuses[transaction.obj.Transaction.status]}}</div>
        <span ng-hide="transaction.obj.Transaction.unread !== true" class="new">N</span>
      </a>
    </li>
	</ul>
	</form>
	<!--<button id="loadmore" ng-click="loadMore()">Load more</button>-->
	<div class="sk-three-bounce">
		<div class="sk-child sk-bounce1"></div>
		<div class="sk-child sk-bounce2"></div>
		<div class="sk-child sk-bounce3"></div>
	</div>
</section>

<!-- admin_quickship_customers.ctp -->
<section id="innercontent" data-ng-view="">
	<div class="loading loading1"><div class="sk-three-bounce"><div class="sk-child sk-bounce1"></div><div class="sk-child sk-bounce2"></div><div class="sk-child sk-bounce3"></div></div></div>
</section>
