<section class="form-quickship" data-ng-app="quickShip" id="quickship" data-ng-controller="MainCtrl">
    <div class="container">
      
        <div class="top-box-circle">
            <div class="circle"></div>
            <div class="icon">
                <i class="icon-quickship"></i>
            </div>
        </div>
        
       
        
        <div class="wrap" id="quickshipFrm" data-ng-cloak>
            
            <div class="text top-box-text">
                <h1>Ship US parcels anywhere in the world, for a great price!</h1>
            </div>           
          

            <div class="wizard row wizard-box wizard-big hardSteps" data-ng-class="{'hidden': $parent.firstLoad !== false}" >
                <ul class="wizard-steps">
                    <li data-target="#step1" ng-class="{'active': quickship.currentStep >= 1}">
                        <span class="step">1</span>
                        <span class="title">Personal <br> information</span>
                    </li>
                    <li data-target="#step2" ng-class="{'active': quickship.currentStep >= 2}">
                        <span class="step">2</span>
                        <span class="title">Additional <br> Options</span>
                    </li>
                    <li data-target="#step3" ng-class="{'active': quickship.currentStep >= 3}">
                        <span class="step">3</span>
                        <span class="title">Package <br> Details</span>
                    </li>
                    <li data-target="#step4" ng-class="{'active': quickship.currentStep >= 4}">
                        <span class="step">4</span>
                        <span class="title">Shipment <br> Rates</span>
                    </li>
                    <li data-target="#step5" ng-class="{'active': quickship.currentStep >= 5}">
                        <span class="step">5</span>
                        <span class="title">Payment <br> Options</span>
                    </li>
                    <li data-target="#step6" ng-class="{'active': quickship.currentStep >= 6}">
                        <span class="step">6</span>
                        <span class="title">Your <br> US Address</span>
                    </li>
                </ul>
            </div>

            <div data-ng-view></div>
            
            <br/>
            
            <div class="text top-box-text"><h2>Questions before You Ship?</h2></div>
            
            <div class="qs-guide">
				<div class="row">
				   <div class="col-md-4">
						<h3><b>Who is HMHShip?</b></h3>
					   <p>HMHShip is a company devoted to making international shipping possible wherever it is not inherently provided. There are several obstacles that you as an international consumer face while engaging in cross-border commerce, including shipping, and paying for orders.</p>

					   <p>For instance, US online sites are popular worldwide, but many of them only ship to US addresses or they have high international shipping rates. We look to improve the international shipping experience for you by doing things differently.</p>
					</div>
					<div class="col-md-4">
						<h3><b>How are we different?</b></h3>
						<p>We can forward packages to almost any country in the world, and we can do it for a great price. We also do what we can to make our services more convenient. Unlike the standard model where you need to go through the tedious process of setting up an account before you can even begin, HMHShip seeks to stand out in this industry for its accessibility and unique business model. We have eliminated the requirement to create an account to access our service, a feature unmatched in the industry.</p>

						<p>We have also eliminated the monthly fee that many of our competitors' charge. Simplicity and expediency are the names of the game.</p>
					</div>                            
					<div class="col-md-4">
						<h3><b>How does it work?</b></h3>
						<p>Maybe you are living out of the US and hear of a deal on a product available on a US based e-commerce site? Getting what you want is normally a hassle and major expense, but with Quickship, all you need to do is:</p>

						 <ul>
							 <li>·         Fill out the required information,</li>
							 <li>·         Pay the low per package processing fee (irrespective of their weight) and</li>
							 <li>·         Ship your package to the U.S. address we provide to you.</li>
						 </ul>
						<p>This will have your parcel shipped to us for forwarding to the overseas address of your choice. HMHShip allows you to take advantage of our pre-negotiated volume discounted shipping rates with the top international shipping companies in the industry - including DHL, FedEx, UPS & the USPS. We will notify you via email when we receive your parcel and when your parcel is shipped to you. Now you can make US based purchases with confidence!</p>
					</div>                     
				</div>
			</div>
            
        </div>
        <!-- /.wrap -->
    </div>
    <!-- /.container -->
</section><!-- /.form-quickship -->
