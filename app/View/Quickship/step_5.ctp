<div class="wm-step step-5">

    <div class="group">
        <div class="row">
            <h2>Payment Page</h2>

            <div class="col-sm-6 text-left">
                <h4>Handling <span
                        class="total_handling_fee hs-text-light">$<?php echo Configure::read('Shipping.rates')['handling_fee']; ?></span>
                </h4>
            </div>
            <div class="col-sm-6 text-right">
                <h4>EST Shipping charge <span class="s5_valuePrice estimated_shipping_cost hs-text-light"></span>
                </h4>
            </div>
        </div>
        <div class="row">

            <div class="col-sm-12">
                <p><span class="hs-text-bold">To be charged now: <span
                            class="s5_totalPrice"><?php echo Configure::read('Shipping.rates')['handling_fee']; ?></span></span>
                    <br>
                    <em>After we receive your package it will be measured and weighed so we can get an accurate shipping
                        charge.</em></p>

                <br/>

                <p>Do you authorize us to charge you for shipping upon receipt of package or would you like to be
                    invoiced?</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                Charge me <input type="radio" name="payment_action" value="0">
                <br>
                Invoice me <input type="radio" name="payment_action" value="1">
            </div>
        </div>
        <div class="row">
            <div class="col col-md-6 hidden">
                <input type="radio" name="s5_paymentType2" class="s5_paymentType required" value="CreditCard"> Credit
                card (Visa, MasterCard, Amex)
            </div>
            <div class="col col-md-12">
                <input type="radio" name="s5_paymentType" class="s5_paymentType required" value="Paypal"
                       checked="checked"> Paypal
            </div>
            <span class="msg-error"></span>
        </div>
    </div>

    <div class="group s5_hidePaymentCreditCard">
        <div class="row text-center">
            <div class="col col-xs-12">
                <div class="paypal_paying">
                    <?php echo $this->Html->image('/img/loading.gif', array('alt' => '')); ?>
                    processing payment...
                </div>
            </div>
        </div>
    </div>

    <div class="group s5_hidePaymentPaypal">
        <div class="row">
            <div class="col col-md-3">
				<span>
					Enter your credit card details
				</span>
            </div>
            <div class="col col-md-9">
                <div class="row">
                    <div class="col col-md-6">
                        <input type="text" name="s5_nameOnCard" class="form-control required"
                               placeholder="Name on card">
                        <span class="msg-error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-4">
                        <input type="text" name="s5_cardNumber" class="form-control required" placeholder="Card number">
                        <span class="msg-error"></span>
                    </div>
                    <div class="col col-md-2">
                        <input type="text" name="s5_CVV" class="form-control required" placeholder="CVV">
                        <span class="msg-error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-3">
                        <input type="text" name="s5_cardMonth" class="form-control required" placeholder="MM">
                        <span class="msg-error"></span>
                    </div>
                    <div class="col col-md-3">
                        <input type="text" name="s5_cardYear" class="form-control required" placeholder="YYYY">
                        <span class="msg-error"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- row 1 -->
    </div>
    <div class="group">
        <div class="row hidden">
            <div class="col col-md-3">
				<span>
					Do you allow us to charge you the shipping cost before package has been received?
				</span>
                <br>
                <small>
                    If the shipping cost is different, you'll be refunded or charged at a later date
                </small>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col col-md-6">
                        <label class="radio-inline hide">
                            <input type="radio" checked="checked" name="s5_allowShipCost" id="s5_allowShipCostNo"
                                   value="no" class="toogle-no s5_allowShipCost required"> no
                            <input type="radio" name="s5_allowShipCost" id="s5_allowShipCostYes" value="yes"
                                   class="toogle-yes s5_allowShipCost required"> yes
                        </label>

                        <div class="toogle-parent">
                            <div class="toggles toggle-soft" data-toggle-on="false"></div>
                        </div>
                        <span class="msg-error"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- row 2 -->

        <div class="row s5_hideAllowShipCost hidden">
            <div class="col col-md-3">
				<span>
					Do you authorize us to make the charge for shipping upon receipt?
				</span>
                <br>
                <small>
                    Otherwise, will be notified of the shipping charge, and you will only be charged upon agreement
                </small>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col col-md-6">
                        <label class="radio-inline hide">
                            <input type="radio" checked="checked" name="s5_authCharge" value="no"
                                   class="toogle-no tmp-required"> no
                            <input type="radio" name="s5_authCharge" value="yes" class="toogle-yes tmp-required"> yes
                        </label>

                        <div class="toogle-parent">
                            <div class="toggles toggle-soft" data-toggle-on="false"></div>
                        </div>
                        <span class="msg-error"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- row 3 -->

        <div class="row hidden">
            <div class="col col-md-3">
				<span>
					Do you agree to be charged at a later date if necessary before payment is processed?
				</span>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col col-md-6">
                        <label class="radio-inline hide">
                            <input type="radio" checked="checked" name="s5_agreeCharge" value="no"
                                   class="toogle-no required"> no
                            <input type="radio" name="s5_agreeCharge" value="yes" class="toogle-yes required"> yes
                        </label>

                        <div class="toogle-parent">
                            <div class="toggles toggle-soft" data-toggle-on="false"></div>
                        </div>
                        <span class="msg-error"></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- row 4 -->

        <div class="row">
            <div class="col-sm-8">
                <?php if(Configure::read('debug') === 0) : ?>
                <script type="text/javascript"
                        src="https://sealserver.trustwave.com/seal.js?code=5940151352774f0887e2b5c8f189323b"></script>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col col-md-12 text-right">
                        <label class="radio-inline hide">
                            <!--<input type="radio" checked="checked" name="s5_agreeTerms" value="" class="toogle-no required pay-required"> no-->
                            <input type="radio" name="s5_agreeTerms" value="yes"
                                   class="toogle-yes required pay-required"> yes
                        </label>

                        Agree to <a href="#" title="Read our Terms and Conditions">Terms and Conditions</a>
                        <span class="msg-error"></span>&nbsp;&nbsp;

                        <div class="toogle-parent pull-right">
                            <div class="toggles toggle-soft" data-toggle-on="false"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- row 5 -->
    </div>

    <div class="group">
        <div class="row text-center">
            <div class="col col-md-12">
                <button type="button" class="btn btn-default go-to-slide-3 simpleSteps">Back 1</button>
                <button type="button" class="btn btn-default go-to-slide-4 hardSteps">Back 2</button>
                <!--button type="button" class="btn btn-default go-to-slide-2 hardSteps hideBackBtn">Back 2</button-->
                <button type="button" class="btn btn-success go-to-slide-6 s5_hidePaymentPaypal">Pay Now</button>
                <button type="button" id="paypal_pay" class="btn btn-success go-to-slide-6b s5_hidePaymentCreditCard">
                    Pay Now
                </button>
                <br><br>
            </div>
        </div>
        <!-- row 6 -->
    </div>

</div><!-- step-1 -->