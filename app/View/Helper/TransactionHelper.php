<?php

App::uses('AppHelper', 'Helper');

/**
 * Class TransactionHelper
 */
class TransactionHelper extends AppHelper
{

	/**
	 * @param $data
	 * @param string $type
	 * @param string $fallback
	 * @return bool
	 */
	public function extractAddress($data, $type = 'billing_shipping', $fallback = 'billing_shipping')
	{
		$internal_type = ($type == 'shipping' ? 'AddressShipping' : 'AddressBilling'); 
		$available_types = Hash::extract($data, sprintf('%s.{n}.AddressType.key', $internal_type));
		if (!in_array($type, $available_types) && $fallback === 'billing_shipping') $type = $fallback;
		if (!isset($data[$internal_type]) || count($data[$internal_type]) === 0) return false;
		foreach ($data[$internal_type] as $address) {
			if ($address['AddressType']['key'] == $type) {
				return $address;
			}
		}
		return false;
	}

	/**
	 * @param $shipment_data
	 * @return int
	 */
	public function getPackageValue($shipment_data)
	{
		$total = 0;
		foreach ($shipment_data['Package']['Item'] as $item) {
			$total += $item['value'];
		}
		return $total;
	}

	public function isInsured($data)
	{
		$default = false;
		$insured = Hash::extract($data, 'Shipment.{n}.Package.insured');
		if (in_array(true, $insured)) $default = true;
		return $default;
	}

	public function isExpedited($data)
	{
		$default = false;
		$expedited = Hash::extract($data, 'Shipment.{n}.Package.expedited');
		if (in_array(true, $expedited)) $default = true;
		return $default;
	}

	public function isRepackaged($data)
	{
		$default = false;
		$repackaged = Hash::extract($data, 'Shipment.{n}.Package.repackaged');
		if (in_array(true, $repackaged)) $default = true;
		return $default;
	}

	/**
	 * @param $data
	 * @return mixed|string
	 */
	public function getInstructions($data)
	{
		$default = __('None');
		$instructions = Hash::extract($data, 'Shipment.{n}.Package.instructions');
		if (isset($instructions[0]) && !empty($instructions[0])) $default = $instructions[0];
		return $default;
	}
}