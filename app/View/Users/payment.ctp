<script>

      function agreeClicked() {
          $("#btnPayNow").prop("enabled") = this.checked;
      }

      function payClicked() {

          if (Number($("#txtAmountToPay").val()) > Number($("#spnTotalDue").text())) {
              alert("Amount to Pay cannot be greater than Total Due.");
              return;
          }

          if (!$("#chkAgree").prop("checked")) {
              alert("You must first check Agree.");
              return;
          }

          $("#btnPayNow").prop("disabled", true);
          $('#divSpinner').show();

          var params = {
					    description: "Shipping fees",
					    price: $("#txtAmountToPay").val(),
					    selectedIds: $('#selectedIds').val()
				    };

          $.ajax({
              url: "/Users/process_payment.json",
              method: "POST",
              data: params,
              cache: false,
              dataType: "json"
          }).done(function (data) {
              	if (data.ok) {
              		window.location = data.approval_link;
              	} else {
              	    alert("Error, please try again");
              	    $("#btnPayNow").prop("disabled", false);
              	    $('#divSpinner').hide();
              	}
          });

          

      }
</script>

<div style="float:left;width:1140px;height:60px;margin-left:10px;">
  <div style="font-family:Arial-Bold;color:#eb6136;font-size:23px;float:left;">
    Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
  </div>
  <div style="float:right;padding-top:5px;font-family:Arial-Bold;color:#eb6136;font-size:17px;">
    My US Address
  </div>

  <div style="clear:both;height:0px;">&nbsp;</div>
  <div style="float:right;">

    <span style="font-family:Arial-Bold;color:#eb6136;font-size:17px;float:left;">
      23600 Mercantile Rd. Suite C-119, <?php echo $co_code ?>
      Beachwood, Ohio 44122
    </span>
  </div>
  <div style="clear:both;height:5px;">&nbsp;</div>
</div>

<div id="divUserPaymentContent" style="float:left;padding-top:20px;padding-left:30px;">

    <div style="font-size:18px;font-weight:bold;">Payment</div>
    <div style="clear:both;height:20px;">&nbsp;</div>

    <?php
    $total_due = 0;
    foreach ($filtered_shipments as $shipment): ?>
        <div  style="float:left;width:115px;">
            <span style="font-weight:bold;color:black;">&nbsp;NS#<?php echo $shipment['Transaction']['id']; ?></span>
        </div>
        <div style="float:left;">Handling Fee: $7.25 PAID, Shipping Fees: $
            <?php  if (empty($shipment['Shipment']['balance_due'])) echo("0.00"); else echo($shipment['Shipment']['balance_due']);
                   $total_due = $total_due + $shipment['Shipment']['balance_due']; ?>
        </div>
        <div style="clear:both;height:10px;">&nbsp;</div>
    <?php endforeach; ?>
    <?php unset($shipment); ?>

    <div style="clear:both;height:20px;">&nbsp;</div>
    <div style="font-size:16px;font-weight:bold;float:left;margin-right:200px;">
        Total Due: $<span id="spnTotalDue"><?php echo(number_format((float)$total_due, 2, '.', '')); ?></span>
    </div>
    <div style="font-size:16px;font-weight:bold;float:left;">
        Amount to Pay: $<input type="number" id="txtAmountToPay" value="<?php echo(number_format((float)$total_due, 2, '.', '')); ?>" style="width:100px;"/>
    </div>
    <div style="clear:both;height:30px;">&nbsp;</div>

    <div style="padding-top:10px;height:10px;">
        <input type="checkbox" style="float:left;" onclick="" id="chkAgree"/>
        <input type="hidden" style="float:left;" onclick="" id="selectedIds" value="<?php echo $selected_ids; ?>"/>
        <div style="float:left;"> Agree to Terms and Conditions</div>
        <button id="btnPayNow" onclick="payClicked()">Pay Now</button>
    </div>


    <div id="divSpinner" class="text-center" style="padding: 10px;display:none;margin-left:150px;">
        <img src="/img/loading.gif" alt="loading" style="width:30px;" />
        Redirecting to PayPal...
    </div>












</div>