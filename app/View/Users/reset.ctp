<div class="show-after-load">
    <section class="talk-to-us">
	<div class="container">
            <div class="mission-content">
                <div class="top-box-circle">
                        <div class="circle"></div>
                        <div class="icon">
                                <i class="icon-createaccount"></i>
                        </div>
                </div>
                <div class="wm-content">




                    <div class="text top-box-text">
                                <h2>Reset Your Password</h2>
                    </div>



                    <div class="info-contact">

                        <center><?php echo $this->Session->flash(); ?><br/></center>
                       <form action="/Users/reset" id="UserResetForm" method="post" accept-charset="utf-8">
                            <fieldset class="center">
                                <?php if (isset($userid) && $userid > 0) {
                                    $button_text = "Reset Password";
                                ?>
                                <input type="hidden" name="id" value="<?php echo $userid?>"/>
                                <input type="hidden" name="password_hashed" value="<?php echo $password_hashed ?>"/>
                                <p class="text-center">Almost done. Enter your new password, and you’re good to go.</p>
                                <div class="col-md-6">
                                <div class="row">
                                    <center><div style="padding-left: 200px;">
                                        <input required name="password" id="pwd" placeholder="Password" class="form-control" style="width:60%;" maxlength="50" type="password" id="password">
                                    </div></center>
                                </div>

                                <div class="row">

                                    <center><div style="padding-left: 200px;">
                                    <input required name="confirm_password" placeholder="Confirm Password" class="form-control" style="width:60%;" maxlength="50" type="password" id="confirm_password">
                                  </div></center>
                                </div>
                              </div>
                              <div id="insert" class="col-md-6" >

                              </div>

                                <?php } else {
                                    $button_text = "Reset Password";
                                ?>

                                <div class="row text-center">
                                    <p>Fear not. We'll email you instructions to reset your password.</p>
                                    <center><div>
                                    <input required name="email" placeholder="Email" class="form-control" style="width:300px;" maxlength="50" type="text" id="email">
                                  </div></center>
                                </div>
                                <?php } ?>
                            </fieldset>
                        <div class="row top-box-text">

                            <input style="background-color: #ef924b; border: none;" class="btn-send" type="submit" value="<?php echo $button_text ?>">&nbsp&nbsp&nbsp
                            <a  style="color: #ef924b;" href="/sign-in">Return to login</a>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">
  $("#pwd").keyup('click', function(){
    $("#insert").html(`<ul>
      <span>Password Rules:</span>
      <li>- Length must be 8 characters or greater</li>
      <li>- Must contain at least one letter</li>
      <li>- Must contain at least one number</li>
    </ul>`);
  })
</script>
