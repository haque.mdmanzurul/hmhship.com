<div style="float:left;width:1200px;margin-left:10px;">
    <div class="row">
      <div class="col-md-6">
        <div style="color:#eb6136;font-size:23px;float:left;">
          Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
        </div>
      </div>
      <div class="col-md-6">
        <div style="text-align: right; margin-right: 110px; padding-top:5px;color:#eb6136;font-size:17px; width=100%;">
              <span style="color: black;">My US Address:</span>
              <br>
              <span style="color: black;">Address 1:</span> 23600 Mercantile Rd. <span style="color: black;">Address 2:</span> Suite C-119
              <br>
              <span style="color: black;">City:</span> Beachwood <span style="color: black;">State/Province:</span> Ohio <span style="color: black;">Postal Code:</span> 44122,

              <br>
              <span style="color: black;">Country:</span> USA
         </div>
      </div>
    </div>
</div>
<div style="float:left;position:relative;height:600px;width:1000px;margin-top:60px;">
    <div id="divMessages" class="scroll" style="padding-top:50px;padding-left:30px; position: absolute;bottom: 0;overflow-y:auto;max-height:600px;">
        <?php
    foreach ($messages as $msg):
        if ($msg['Message']['from_hmh']) {
        ?>

            <div style="height:150px;position:relative;width:950px;">
                <div style="position:absolute;left:0px;margin-top:15px;width:76px;height:90px;background-color:lightsalmon;border-top-left-radius:140px;border-bottom-left-radius:140px;border: solid 1px #dddedf;border-right:none;padding-left:10px;padding-top:10px;">
                    <div class="msg-circle" style=""><?php echo (date('h:i A', strtotime($msg['Message']['created']))); ?></div>
                </div>
                <div style="width:820px;height:120px;background-color:lightsalmon;padding-top:20px;padding-left:10px;border: solid 1px #dddedf;margin-left:75px;">
                    <?php echo ($msg['Message']['message']); ?>
                </div>
            </div>       

        <?php
        } else { // from User
        ?>

            <div style="height:150px;position:relative;width:910px;">
                <div style="position:absolute;right:0px;margin-top:15px;width:71px;height:90px;background-color:#f5f6f7;border-top-right-radius:140px;border-bottom-right-radius:140px;border: solid 1px #dddedf;border-left:none;padding-right:20px;padding-top:10px;">
                    <div class="msg-circle" style=""><?php echo (date('h:i A', strtotime($msg['Message']['created']))); ?></div>
                </div>   
                <div style="width:840px;height:120px;background-color:#f5f6f7;padding-top:20px;padding-right:10px;padding-left:10px;border: solid 1px #dddedf;">
                    <?php echo ($msg['Message']['message']); ?>
                </div>
                             
            </div>       

        <?php
        }
        ?>
        <div style="clear:both;height:10px;">&nbsp;</div>
            
        <?php endforeach;
        ?>
        <?php unset($messages); ?>

        <div>
            <?php echo $this->Form->create(); ?>
            <textarea name="message" rows="3" style="resize: none;float:left;width:870px;margin-right:10px;padding-left:0px;"></textarea>
            <button style="margin-left:0px;height:50px;">Send</button>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>

<script>
    // scroll to bottom (most recent messages)
    var objDiv = document.getElementById("divMessages");
    objDiv.scrollTop = objDiv.scrollHeight;
</script>