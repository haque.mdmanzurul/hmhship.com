<div class="show-after-load">
    <section class="talk-to-us">
	<div class="container">
            <div class="mission-content">
                <div class="top-box-circle">
                        <div class="circle"></div>
                        <div class="icon">
                                <i class="icon-createaccount"></i>
                        </div>
                </div>
                <div class="wm-content">




                    <div class="text top-box-text">
                                <h1>Login</h1>
                    </div>

                    <div class="info-contact">
                        <?php

                        echo $this->Flash->render('auth'); ?><br/>
                       <form action="/sign-in" id="UserLoginForm" method="post" accept-charset="utf-8">
                            <fieldset>
                                <div class="row">
                                <div class="col-sm-3">
                                    <label for="UserUsername">Username</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input name="data[User][username]" class="form-control" maxlength="50" type="text" id="UserUsername">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                    <label for="UserPassword">Password</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input name="data[User][password]" class="form-control" type="password" id="UserPassword">
                                    </div>
                                </div>


                                  
                                    <a href="/Users/reset" class="primary pull-right">Forgot Password?</a>

                            </fieldset>
                        <div class="row text-center">

                            <input style="background-color: #ff9e59; color:#fff; border: none;" class="btn-send" type="submit" value="Login">
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
</div>
<script type="text/javascript">
    localStorage.clear();
</script>
