<div style="float:left;width:1140px;height:60px;margin-left:10px;">
  <div style="font-family:Arial-Bold;color:#eb6136;font-size:23px;float:left;">
    Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
  </div>
  <div style="float:right;padding-top:5px;font-family:Arial-Bold;color:#eb6136;font-size:17px;">
    My US Address
  </div>

  <div style="clear:both;height:0px;">&nbsp;</div>
  <div style="float:right;">

    <span style="font-family:Arial-Bold;color:#eb6136;font-size:17px;float:left;">
      23600 Mercantile Rd. Suite C-119, QS#<?php echo $co_code ?>
      Beachwood, Ohio 44122
    </span>
  </div>
  <div style="clear:both;height:5px;">&nbsp;</div>
</div>

<div id="divMyAccountContent" style="float:left;padding-top:20px;padding-left:50px;">
    <div style="border-bottom:solid 1px black;width:950px;height:25px;">
        <div style="font-family:Arial-Bold;color:#eb6136;font-size:22px;font-weight:bold;float:left;">
            <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?> - Account Settings
        </div>
    </div>

    <div style="height:50px;">&nbsp;</div>
    <div style="font-family:Arial;color:#eb6136;font-size:16px;">
        GENERAL ACCOUNT SETTINGS
    </div>
    <div style="height:50px;">&nbsp;</div>
    <div style="color:red;"><?php echo $this->Flash->render() ?></div>
    <div style="height:10px;">&nbsp;</div>
    <?php echo $this->Form->create(); ?>
        <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:bold;">E-mail:</div>
        <div style="float:left;width:100px;font-family:Arial-Bold;"><input type=text name="email" value="<?php echo AuthComponent::user('email'); ?>" /></div>
        <div style="clear:both;height:30px;">&nbsp;</div>
        <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:bold;">Password:</div>
        <div style="float:left;width:100px;font-family:Arial-Bold;"><input type="password" name="old_password" placeholder="enter old password" /></div>
        <div style="clear:both;height:20px;">&nbsp;</div>
        <div style="margin-left:100px;width:100px;font-family:Arial-Bold;"><input type="password" name="new_password" placeholder="enter new password" /></div>
        <div style="clear:both;height:20px;">&nbsp;</div>
        <div style="margin-left:100px;width:100px;font-family:Arial-Bold;"><input type="password" name="confirm_new_password" placeholder="reenter new password" /></div>
        <div style="clear:both;height:50px;">&nbsp;</div>

        <div ><button style="float:left;">SAVE</button> <button type="button" onclick="javascript: document.location = '/Users/account_settings';" style="float:left;margin-left:10px;">CANCEL</button></div>
        <div style="clear:both;height:30px;">&nbsp;</div>
    <?php echo $this->Form->end(); ?>
    




    </div>