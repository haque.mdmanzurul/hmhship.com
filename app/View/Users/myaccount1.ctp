<style>
    .actionbox, .clientsmsg, .mymsg {
        background-color: #f3f3f3;
        border: 1px solid #ddd;
        padding: 15px 25px 25px;
        margin-left: 45px;
        min-height: 114px;
        padding-left: 40px;
        position:relative;
        margin-bottom: 15px;
    }

    .actionbox h5{
        margin:0;
    }


    .actionbox .icon-wrap,.clientsmsg .icon-wrap,.mymsg .icon-wrap {
        background-color: #f3f3f3;
        left: -40px;
        width: 40px;
        height: 80px;
        -webkit-border-radius: 40px 0 0 40px;
        -moz-border-radius: 40px 0 0 40px;
        -o-border-radius: 40px 0 0 40px;
        -ms-border-radius: 40px 0 0 40px;
        border-radius: 40px 0 0 40px;
        border: 1px solid #ddd;
        border-right: 0;
        padding: 28px 15px;
        position: absolute;
    }

    .actionbox .icon-wrap > *,.clientsmsg .icon-wrap > *,.mymsg .icon-wrap > * {
        background-color: #fff;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        -o-border-radius: 50%;
        -ms-border-radius: 50%;
        border-radius: 50%;
        padding: 9px;
        color: #EC8C3A;
        border: 1px solid #EC8C3A;
    }
    .actionbox .icon-wrap > * {
        font-size: 24px;
    }


</style>

<div style="float:left;width:1200px;margin-left:10px;">

  <div style="float:left;width:1140px;height:60px;">
    <div style="font-family:Arial-Bold;color:#eb6136;font-size:23px;float:left;">
      Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
    </div>
    <div style="text-align: right; margin-right: 110px; float:right;padding-top:5px;font-family:Arial-Bold;color:#eb6136;font-size:17px; width=100%;">
      <span style="color: black;">My US Address:</span>
      <br>
      <?php echo AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name') ?> <span style="color: black;">℅</span> NS#<?php echo strtoupper(substr(AuthComponent::user('first_name'), 0, 1)) . strtoupper(substr(AuthComponent::user('last_name'), 0, 1)) . AuthComponent::user('id') ?>
      <br>
      <span style="color: black;">Address 1:</span> 23600 Mercantile Rd. <span style="color: black;">Address 2:</span> Suite C-119
      <br>
      <span style="color: black;">City:</span> Beachwood <span style="color: black;">State/Province:</span> Ohio <span style="color: black;">Postal Code:</span> 44122 USA
    </div>

    <div style="clear:both;height:0px;">&nbsp;</div>
    <!-- <div style="float:right;">
      <span style="font-family:Arial-Bold;color:#eb6136;font-size:17px;float:left;">
        23600 Mercantile Rd. Suite C-119, c/o <?php echo $co_code ?>
        Beachwood, Ohio 44122
      </span>
    </div> -->
    <div style="clear:both;height:5px;">&nbsp;</div>
  </div>

  <div id="divMyAccountContent" style="float:left;padding-top:0px;padding-left:30px;width:990px;">

          <div style="clear:both;height:50px;">&nbsp;</div>

          <div style="height:170px;position:relative;">

              <div style="position:absolute;left:0px;margin-top:30px;width:51px;height:72px;background-color:#f5f6f7;border-top-left-radius:140px;border-bottom-left-radius:140px;border: solid 1px #dddedf;border-right:none;">
                  <img src="/img/airplane.png" style="position:relative;top:10px;left:10px;"/>
              </div>
              <div style="font-family:Arial;margin-left:50px;width:320px;height:120px;background-color:#f5f6f7;padding-top:10px;padding-left:10px;border: solid 1px #dddedf;">
                  <table border="0" cellpadding="5" cellspacing="0" height="90" width="310" >
                    <tr height="10"><td>
                      <div style="font-family:Arial-Bold;color:#eb6136;font-size:17px;font-weight:bold;">Shipments</div>
                      </td><td >&nbsp;</td></tr>
                  <tr height="50">
                    <td width="140">
                          <table border="0" cellpadding="1" cellspacing="0" width="140">
                              <tr>
                                  <td width="80" style="font-weight:400px;font-size:19px;">
                                      Shipments<br/> pending
                                  </td>
                                  <td width="70" align="middle">
                                      <?php
   if ($count_waiting_package > 0) echo ("<a href='/Users/myaccount/pending' style='text-decoration:none;'>"); ?>

                                      <span style="color:#eb6136;font-size:29px;"><?php echo $count_waiting_package; ?></span>
                                      <?php
   if ($count_waiting_package > 0) echo ("</a>"); ?>
                                  </td></tr></table>
                      </td>
                      <td width="140">
                          <table border="0" cellpadding="5" cellspacing="0"  width="140" >
                              <tr><td style="font-weight:400px;font-size:19px;">
                              Awaiting<br/> payment to ship</td>
                              <td>
                              <?php if ($count_awaiting_payment > 0) echo ("<a href='/Users/payment' style='text-decoration:none;'>"); ?>
                              <span style="color:#eb6136;font-size:29px;"><?php echo $count_awaiting_payment; ?></span>
                              <?php if ($count_awaiting_payment > 0) echo ("</a>"); ?>
                              </td></tr></table>
                      </td></tr></table>
                  <div style="clear:both;height:30px;">&nbsp;</div>

              </div>
          </div>


  </div>

  <div style="clear:both;height:0px;">&nbsp;</div>

  <div style="margin-left:10px;padding-left:30px;padding-top:10px;width:600px;">


          <div style="background-color:white;color:#eb6136;float:left;width:140px;height:25px;padding-top:5px;padding-left:5px;padding-right:5px;">&nbsp;Shipment History</div>

          <div style="float:right;margin-right:64px;">
              <?php switch($param1) {
                  case "pending":
                      echo("<div style='float:left;color:#eb6136;margin-top:4px;margin-right:5px;'>Pending</div>");
                      break;
              }
              ?>
              <div class="icon-filter" style="background-color:white;color:#eb6136;float:left;height:25px;padding-top:5px;padding-left:5px;padding-right:5px;">&nbsp;</div>
              <a href="/Users/myaccount<?php switch($param2) {
                  case "": case "asc":
                      echo("/" . $param1 . "/desc");
                      break;
              }
              ?>"><div class="icon-sort" style="background-color:white;color:#eb6136;float:left;height:25px;padding-top:5px;padding-left:5px;padding-right:5px;">&nbsp;</div></a>
          </div>
          <div style="clear:both;height:10px;">&nbsp;</div>

          <div class="scroll" style="width:500px;height:500px;overflow-y:auto;padding-left:10px;font-size:15px;">
              <?php foreach ($shipments as $shipment): ?>
                  <div class="icon-user" style="float:left;width:115px;color:#eb6136;">
                      <?php if ($shipment['Transaction']['status'] == -1)
                              echo("<a style='color:red;' href='/new-shipment/" . $shipment['Transaction']['id'] . "'>
                              <span style='font-weight:bold;'>");
                            else
                              echo("<span style='font-weight:bold;color:black;'>");
                      ?>
                      &nbsp;NS#<?php echo $shipment['Transaction']['id']; ?></span>
                      <?php if ($shipment['Transaction']['status'] == -1)
                              echo("</a>");
                      ?>
                  </div>
                  <div style="float:left;margin-right:20px;">Last update: <?php echo date('m/d/Y', strtotime($shipment['Transaction']['modified'])); ?></div>
                  <div style="float:left;"> <?php
                      switch($shipment['Transaction']['status']) {
                           /**
                           * Default for newly created transaction
                           */
                          // STATUS_AWAITING_FEE = 0;
                          case 0:
                              echo("<div class='icon-invoice' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Awaiting Fee</div>");
                              break;
                          /**
                           * Waiting for Package (after handling fee has been paid)
                           */
                          // STATUS_WAITING_PACKAGE = 1;
                          case 1:
                              echo("<div class='icon-waitpack' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Awaiting Package</div>");
                              break;
                          /**
                           * Package has been received by HMHShip
                           */
                          // STATUS_PACKAGE_RECEIVED = 2;
                          case 2:
                              echo("<div class='icon-packreceived' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Package Received</div>");
                              break;

                          /**
                           * Awaiting shipping payment from customer
                           */
                          // STATUS_AWAITING_PAYMENT = 3;
                          case 3:
                              echo("<div class='icon-awaitingpaym' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Awaiting Payment</div>");
                              break;

                          /**
                           * Shipping payment received from customer
                           */
                          // STATUS_PAYMENT_RECEIVED = 4;
                          case 4:
                              echo("<div class='icon-paymreceived' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Payment Received</div>");
                              break;

                          /**
                           * Package sent by HMHShip
                           */
                          // STATUS_PACKAGE_SENT = 5;
                          case 5:
                              echo("<div class='icon-packsent' style='float:left;color:#eb6136;width:20px;margin-right:5px;'>&nbsp;</div><div style='float:left;'>Package Sent</div>");
                              break;
                      } ?></div>
                  <div style="clear:both;height:10px;">&nbsp;</div>
              <?php endforeach; ?>
              <?php unset($shipment); ?>
          </div>

  </div>

<app-root></app-root>
<script type="text/javascript" src="/dashboardapp/runtime.js"></script>
<script type="text/javascript" src="/dashboardapp/polyfills.js"></script>
<script type="text/javascript" src="/dashboardapp/styles.js"></script>
<script type="text/javascript" src="/dashboardapp/vendor.js"></script>
<script type="text/javascript" src="/dashboardapp/main.js"></script>

</div>

<?php
echo $this->Html->script('/dashboardapp/dist/runtime.js');
echo $this->Html->script('/dashboardapp/dist/polyfills.js');
echo $this->Html->script('/dashboardapp/dist/styles.js');
echo $this->Html->script('/dashboardapp/dist/vendor.js');
echo $this->Html->script('/dashboardapp/dist/main.js');
?>