<div style="float:left;width:1200px;margin-left:10px;">
    <div class="row">
      <div class="col-md-6">
        <div style="color:#eb6136;font-size:23px;float:left;">
          Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
        </div>
      </div>
      <div class="col-md-6">
        <div style="text-align: right; margin-right: 110px; padding-top:5px;color:#eb6136;font-size:17px; width=100%;">
              <span style="color: black;">My US Address:</span>
              <br>
              <span style="color: black;">Address 1:</span> 23600 Mercantile Rd. <span style="color: black;">Address 2:</span> Suite C-119
              <br>
              <span style="color: black;">City:</span> Beachwood <span style="color: black;">State/Province:</span> Ohio <span style="color: black;">Postal Code:</span> 44122,

              <br>
              <span style="color: black;">Country:</span> USA
         </div>
      </div>
    </div>
</div>

<div id="divMyAccountContent" style="float:left;padding-top:20px;padding-left:50px;">
    <div style="width:950px;height:25px;">
        <div style="font-family:Arial-Bold;color:#eb6136;font-size:22px;font-weight:bold;float:left;">
            <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?> - Account Settings
        </div>
    </div>

    <div style="height:50px;">&nbsp;</div>
    <div style="font-family:Arial;color:#eb6136;font-size:16px;">
        GENERAL ACCOUNT SETTINGS
    </div>
    <div style="height:50px;">&nbsp;</div>
    <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:bold;">E-mail:</div>
    <div style="float:left;width:100px;font-family:Arial-Bold;"><?php echo AuthComponent::user('email'); ?></div>
    <div style="height:50px;">&nbsp;</div>
  <br/>
    <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:bold;">Password:</div>
    <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:100;">**********</div>
  <div style="height:50px;">&nbsp;</div>
  <br/>

    <div style="float:left;"><button onclick="javascript: location.href = '/Users/account_settings_edit';">EDIT</button></div>
  <div style="height:30px;">&nbsp;</div>
  <br/>
    <hr />
  <div style="height:30px;">&nbsp;</div>
  <br/>
    <div style="float:left;">
        <button id="btnDeactivateAccount" type="button" onclick="deactivate()">DEACTIVATE ACCOUNT</button>
    </div>
  <div style="height:50px;">&nbsp;</div>
  <br/>

    <div id="divDeactivateAccount" style="display:none;">
        <?php echo $this->Form->create(); ?>
        <div style="float:left;width:100px;font-family:Arial-Bold;font-weight:bold;">Password:</div>
        <div style="float:left;width:100px;font-family:Arial-Bold;">
            <input type="password" name="password" placeholder="password" />
        </div>
      <div style="height:50px;">&nbsp;</div>
      <br/>

        <div>
            <button style="float:left;">DEACTIVATE</button>
            <button type="button" onclick="cancelDeactivate()" style="float:left;margin-left:10px;">CANCEL</button>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>


</div>

<script>
    function deactivate() {
        $("#btnDeactivateAccount").css("background-color", "#E86136");
        $("#btnDeactivateAccount").css("color", "white");
        $("#divDeactivateAccount").show();
    }

    function cancelDeactivate() {
        $("#btnDeactivateAccount").css("background-color", "white");
        $("#btnDeactivateAccount").css("color", "#E86136");
        $("#divDeactivateAccount").hide();
    }

</script>
