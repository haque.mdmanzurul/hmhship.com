<div style="float:left;width:1200px;margin-left:10px;">
<div class="row">
  <div class="col-md-6">
    <div style="color:#eb6136;font-size:23px;float:left;">
      Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
    </div>
  </div>
  <div class="col-md-6">
    <div style="text-align: right; margin-right: 110px; padding-top:5px;color:#eb6136;font-size:17px; width=100%;">
          <span style="color: black;">My US Address:</span><br>
          <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>,  <span style="color: black;">c/o</span> <?php echo $co_code ?>
          <br>
          <span style="color: black;">Address 1:</span> 23600 Mercantile Rd. <span style="color: black;">Address 2:</span> Suite C-119
          <br>
          <span style="color: black;">City:</span> Beachwood <span style="color: black;">State/Province:</span> Ohio <span style="color: black;">Postal Code:</span> 44122,

          <br>
          <span style="color: black;">Country:</span> USA
     </div>
  </div>
</div>
<div class="cleafix"></div>
<app-root></app-root>
</div>

<?php
echo $this->Html->script('/dashboardapp/dist/runtime.js');
echo $this->Html->script('/dashboardapp/dist/polyfills.js');
echo $this->Html->script('/dashboardapp/dist/styles.js');
echo $this->Html->script('/dashboardapp/dist/vendor.js');
echo $this->Html->script('/dashboardapp/dist/main.js');
?>