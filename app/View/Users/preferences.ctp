﻿<style>
    .row {margin-bottom:5px;clear:both;}
</style>

<div style="float:left;width:1200px;margin-left:10px;">
    <div class="row">
      <div class="col-md-6">
        <div style="color:#eb6136;font-size:23px;float:left;">
          Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
        </div>
      </div>
      <div class="col-md-6">
        <div style="text-align: right; margin-right: 110px; padding-top:5px;color:#eb6136;font-size:17px; width=100%;">
              <span style="color: black;">My US Address:</span>
              <br>
              <span style="color: black;">Address 1:</span> 23600 Mercantile Rd. <span style="color: black;">Address 2:</span> Suite C-119
              <br>
              <span style="color: black;">City:</span> Beachwood <span style="color: black;">State/Province:</span> Ohio <span style="color: black;">Postal Code:</span> 44122,

              <br>
              <span style="color: black;">Country:</span> USA
         </div>
      </div>
    </div>
</div>

<div id="divPreferencesContent" style="float:left;padding-top:20px;padding-left:30px;">    

    <div style="margin-top:0px;margin-bottom:20px;">
        Set your preferences to speed through the shipping process!
    </div>

    <div style="font-family:Arial;color:#eb6136;font-size:16px;margin-bottom:20px;">
        ADDRESSES
    </div>

    <?php for ($i = 0; $i < count($user['Address']); $i++) { ?>
        <div style="float:left;margin-right:5px;">

            <img src="/img/edit.gif" style="width:20px;height:20px;" onclick="showEdit(<?php echo($user['Address'][$i]['id']);?>)" />

        </div>
        <div style="float:left;width:700px;overflow:hidden">
            <?php echo($user['Address'][$i]['firstname'] . " " . $user['Address'][$i]['lastname'] . " " .
            $user['Address'][$i]['address1'] . " " . $user['Address'][$i]['address2'] . " " .
            $user['Address'][$i]['city'] . ", " . $user['Address'][$i]['adm_division'] . " " .
            $user['Address'][$i]['postal_code'] . " " .  $user['Address'][$i]['Country']['name'] . "<br/>" .
            "Phone: " . $user['Address'][$i]['phone'] . "<br/>" .
            "Email: " . $user['Address'][$i]['email']);
            ?>
        </div>
        <div style="float:left;margin-right:10px;color:#eb6136;">
            <?php
            if($user['Address'][$i]['default_billing'] && $user['Address'][$i]['default_shipping'])
                echo("DEFAULT BOTH");
            else if ($user['Address'][$i]['default_billing'])
                echo("DEFAULT BILLING");
            else if ($user['Address'][$i]['default_shipping'])
                echo("DEFAULT SHIPPING");
            ?>
        </div>
        


        <div style="clear:both;height:10px;">&nbsp;</div>

        <div id="divEdit<?php echo($user['Address'][$i]['id']);?>" style="display:none;" class="edit-form">

            <?php echo $this->Form->create(); ?>
            <input type="hidden" name="Address[id]" value="<?php echo($user['Address'][$i]['id']);?>"/>
            <div>
                <div class="row">

                    <div style="float:left;">
                        <input type="text" name="Address[firstname]" style="margin-right:10px;" placeholder="First Name *" required value="<?php echo ($user['Address'][$i]['firstname']) ?>">
                        <span class="msg-error"></span>
                    </div>
                    <div class="col col-md-3">
                        <input type="text" name="Address[lastname]" class="form-control" placeholder="Last Name *" required value="<?php echo ($user['Address'][$i]['lastname']) ?>">
                        <span class="msg-error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div style="float:left;">
                        <input type="text" name="Address[address1]" style="margin-right:10px;"  placeholder="Address 1 *" required value="<?php echo ($user['Address'][$i]['address1']) ?>">
                        <span class="msg-error"></span>
                    </div>
                    <div class="col col-md-3">
                        <input type="text" name="Address[address2]" class="form-control" placeholder="Address 2" value="<?php echo ($user['Address'][$i]['address2']) ?>">
                        <span class="msg-error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">

                    </div>
                    <div style="float:left;">
                        <input type="text" name="Address[city]" style="margin-right:10px;"  placeholder="City *" required value="<?php echo ($user['Address'][$i]['city']) ?>">
                        <span class="msg-error"></span>
                    </div>


                    <div style="float:left;">
                        <input type="text" name="Address[adm_division]" style="margin-right:10px;"  placeholder="State/Province *" required value="<?php echo ($user['Address'][$i]['adm_division'])?>">
                        <span class="msg-error"></span>
                    </div>
                    <div class="col col-md-3 no-margin">
                        <input type="text" name="Address[postal_code]" id="s1_personalPostalcode" class="form-control" placeholder="Postal Code *" required value="<?php echo ($user['Address'][$i]['postal_code']) ?>">
                        <span class="msg-error"></span>
                    </div>


                </div>
                <div class="row">
                    
                    <div style="float:left;">
                        <select name="Address[country_id]" style="margin-right:10px;height:20px;" required>
                            <option selected="selected">-- Country * --</option>

                            <?php foreach($countries as $id => $name) : ?>
                            <option value="<?php echo $id; ?>" <?php echo ($user['Address'][$i]['country_id'] == $id) ? 'selected' : '' ?>>
                                <?php echo $name; ?>
                            </option>
                            <?php endforeach; ?>
                        </select>
                        <span class="msg-error"></span>
                    </div>

                    <div style="float:left;">
                        <input type="text" name="Address[phone]" placeholder="Phone *" style="margin-right:10px;" value="<?php echo($user['Address'][$i]['phone'])?>"/>
                    </div>

                    <div style="float:left;">
                        <input type="text" name="Address[email]" placeholder="Email" value="<?php echo($user['Address'][$i]['email'])?>" />
                    </div>
                </div>
                <div class="row" style="padding-top:10px;">                    
                    

                    <select name="default" style="float:left;margin-right:450px;">
                        <option <?php if($user['Address'][$i]['default_billing'] && $user['Address'][$i]['default_shipping']) echo ("SELECTED")?> value="3">Default Both</option>
                        <option <?php if($user['Address'][$i]['default_billing'] && !$user['Address'][$i]['default_shipping']) echo ("SELECTED")?> value="1">Default Billing</option>
                        <option <?php if(!$user['Address'][$i]['default_billing'] && $user['Address'][$i]['default_shipping']) echo ("SELECTED")?> value="2">Default Shipping</option>
                        <option <?php if(!$user['Address'][$i]['default_billing'] && !$user['Address'][$i]['default_shipping']) echo ("SELECTED")?> value="0">(not a default)</option>
                    </select>

                    <button style="margin-left:0px;">Save</button>
                    </div>
                </div>
            <?php echo $this->Form->end(); ?>
        </div>

    <?php } // end loop on Addresses ?>

    <div style="clear:both;height:10px;">&nbsp;</div>
    <button type="button" onclick="addAddress()" style="margin-left:0px;">Add Address</button>
    <div style="clear:both;height:15px;">&nbsp;</div>

    <div id="divAdd" style="display:none;" class="edit-form">

        <?php echo $this->Form->create(); ?>
        <input type="hidden" name="Address[user_id]" value="<?php echo(AuthComponent::user('id'));?>" />
        <input type="hidden" name="Address[address_type_id]" value="2" />
        <div>
            <div class="row">

                <div style="float:left;">
                    <input type="text" name="Address[firstname]" style="margin-right:10px;" placeholder="First Name *" required >
                    <span class="msg-error"></span>
                </div>
                <div class="col col-md-3">
                    <input type="text" name="Address[lastname]" class="form-control" placeholder="Last Name *" required >
                    <span class="msg-error"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">

                </div>
                <div style="float:left;">
                    <input type="text" name="Address[address1]" style="margin-right:10px;" placeholder="Address 1 *" required >
                    <span class="msg-error"></span>
                </div>
                <div class="col col-md-3">
                    <input type="text" name="Address[address2]" class="form-control" placeholder="Address 2" >
                    <span class="msg-error"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">

                </div>
                <div style="float:left;">
                    <input type="text" name="Address[city]" style="margin-right:10px;" placeholder="City *" required >
                    <span class="msg-error"></span>
                </div>


                <div style="float:left;">
                    <input type="text" name="Address[adm_division]" style="margin-right:10px;" placeholder="State/Province *" required >
                    <span class="msg-error"></span>
                </div>
                <div class="col col-md-3 no-margin">
                    <input type="text" name="Address[postal_code]" id="s1_personalPostalcode" class="form-control" placeholder="Postal Code *" required >
                    <span class="msg-error"></span>
                </div>


            </div>
            <div class="row">

                <div style="float:left;">
                    <select name="Address[country_id]" style="margin-right:10px;height:20px;" required>
                        <option selected="selected">-- Country * --</option>

                        <?php foreach($countries as $id => $name) : ?>
                        <option value="<?php echo $id; ?>">
                            <?php echo $name; ?>
                        </option>
                        <?php endforeach; ?>
                    </select>
                    <span class="msg-error"></span>
                </div>

                <div style="float:left;">
                    <input type="text" name="Address[phone]" placeholder="Phone *" style="margin-right:10px;"  />
                </div>

                <div style="float:left;">
                    <input type="text" name="Address[email]" placeholder="Email"  />
                </div>
            </div>
            <div class="row" style="padding-top:10px;">


                <select name="default" style="float:left;margin-right:450px;">
                    <option value="3">Default Both</option>
                    <option value="1">Default Billing</option>
                    <option value="2">Default Shipping</option>
                    <option value="0" selected>(not a default)</option>
                </select>

                <button style="margin-left:0px;">Add</button>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
 </div>

<script>
    function showEdit(id) {
        $(".edit-form").hide();
        $("#divEdit" + id).show();
    }

    function addAddress() {
        $(".edit-form").hide();
        $("#divAdd").show();
    }
</script>