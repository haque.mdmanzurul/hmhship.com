<div class="show-after-load">
    <section class="talk-to-us">
	<div class="container">
            <div class="mission-content">
                <div class="top-box-circle">
                        <div class="circle"></div>
                        <div class="icon">
                                <i class="icon-createaccount"></i>
                        </div>
                </div>
                <div class="wm-content">




                    <div class="text top-box-text">
                                <h2>Congratulations</h2>
                    </div>

                    <div class="" style="width:100%;text-align:center;">
                        <p>Congratulations <?php echo AuthComponent::user('first_name') ?>! Your Account has been set up.</p><br/><br/>

                        <p>
Your US address is:<br/><br/>

<?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?> c/o <?php echo $co_code ?><br/>
23600 Mercantile Rd. Suite C-11<br/>

Beachwood, OH 44122, USA<br/>

                        </p><br/><br/>

                        <span style="width:200px">&nbsp;</span>
                        <span style="text-align:right;"><input class="btn-send" type="button" value="Access My Account" id="btnMyAccount"></span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    window.onload = function(){
      setTimeout(function(){
        document.location = '/Users/myaccount';
      }, 4000);
    }
    $("#btnMyAccount").click( function()
           {
                document.location = "/Users/myaccount";
           });
</script>
