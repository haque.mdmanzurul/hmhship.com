<html>
<head>
    <title>My Account</title>            
	<!-- Google PageSpeed tool doesn't like external CSS files.  So all CSS for Home Page here. -->
        <style>
          
        </style>
    <link rel="stylesheet" type="text/css" href="/css/user/style.css"/>    
    <style>
        button, .applyBtn {
            text-transform: uppercase;
            background-color: #fff;
            color: #E86136;
            border: 1px solid #E86136;
            padding: 8px 10px;
            font-family: "Open Sans Condensed";            
            width: auto!important;
            margin-left: auto;
            margin-right: 0;
            display: block;
        }
         .scroll::-webkit-scrollbar {
           width: 12px;
        }

        .scroll::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px #eb6136; 
        }

        .msg-circle {
          display: inline-flex;
          align-items: center;
          justify-content: center;
          background-color: white;          
          min-width: 1em;
          border-radius: 50%;
          vertical-align: middle;
          padding:10px;
          border: 1px solid;
          border-color:lightcoral;
		  height:65px;
		  width:65px;
        }
        #topLogo {display:block;width:310px;max-width:100%;height:80px;position:absolute;left:15px;top:40px;z-index:3}
        
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<!-- end Favicon -->

</head>
<body style="background:none;">



<div id="divUserTopBar" style="position:absolute;left:0px;top:0px;width:1300px;height:130px;font-weight:400;font-size:12px;background:url('/img/map-bg.png');">

    <a href="/" class="logo" style="" id="topLogo">
        <img src="/img/logo.png" alt="HMH Ship Parcel Forwarding Simplified" height="71" width="294">
    </a>

    <div style="float:right;padding-top:0px;margin-right:50px;">
       
        <div style="padding-top:7px;height:23px;float:left;margin-right:25px;">
            <a href="/Users/account_settings" style="">
                <div style="float:left;color:black;">ACCOUNT SETTINGS</div>                
            </a>
        </div>
		<div style="padding-top:5px;margin-right:25px;float:left;">|</div>
        <div style="padding-top:7px;height:23px;float:left;">
            <a href="/Users/logout" class="logouttrigger" style="padding-top:7px;">
				<div style="float:left;color:black;margin-right:5px;">LOG OUT</div>			
			</a>
        </div>
        <div style="padding-top:5px;margin-right:25px;float:left;">|</div>
         <?php
                 if ($count_waiting_package > 0 || $count_awaiting_payment > 0) {
                     echo("<a href='/Users/myaccount' style='text-decoration:none;'>
                             <div style='width:247px;height:50px;color:white;padding-top:10px;padding-left:20px;padding-right:20px;margin-top:10px;'>
                                 <div style='width:150px;color:white;'>
                                     <div class='icon-shipment' >&nbsp; test <?php echo $count_waiting_package+$count_received_package ?></div>
                                 </div>
                                 <img src='/img/flag.png' style=''/>
                             </div></a>");
                 }
         ?>
    </div>
    <div style="clear:both;">&nbsp;</div>
    <div style="margin-top:60px;color:black;float:right;height:40px;margin-left:260px;margin-right:50px;font-size:14px;">
        <div style="float:left;color:black"><a href="/" style="color:black;margin-right:80px;">HOME</a></div>
        <div style="float:left;color:black"><a href="/services-rates" style="color:black;margin-right:80px;">WHAT WE DO & RATES</a></div>
        <div style="float:left;color:black"><a href="/business" style="color:black;margin-right:80px;">BUSINESS</a></div>
        <div style="float:left;color:black"><a href="/hmhship-bio" style="color:black;margin-right:80px;">HMHSHIP BIO</a></div>
        <div style="float:left;color:black"><a href="/talk-to-us" style="color:black;">TALK TO US</a></div>
    </div>
</div>

    <div style="white-space: nowrap;width:100%;overflow:auto;">
        <div id="divUserNavigation" style="float:left;font-size:16px;width:247px;background-color:#4a4949;height:1000px;margin-top:130px;display: inline-block">
            <a href="/Users/myaccount" style="text-decoration:none;">
                <div style="<?php if ($this->request->params['action']=="myaccount") echo("background-color:#eb6136;"); ?>width:247px;height:50px;color:white;padding-top:15px;padding-left:30px;padding-right:20px;">Dashboard</div>
            </a>
            <a href="/Users/messages" style="text-decoration:none;">
                <div style="<?php if ($this->request->params['action']=="messages") echo("background-color:#eb6136;"); ?>width:247px;height:50px;color:white;padding-top:10px;padding-left:30px;padding-right:20px;">Messages</div>
            </a>
            <a href="/Users/preferences" style="text-decoration:none;">
                <div style="<?php if ($this->request->params['action']=="preferences") echo("background-color:#eb6136;"); ?>width:247px;height:50px;color:white;padding-top:10px;padding-left:30px;padding-right:20px;">User Preferences</div>
            </a>
            <a href="/shipping-calculator" style="text-decoration:none;">
                <div style="width:247px;height:50px;color:white;padding-top:10px;padding-left:30px;padding-right:20px;">Shipping Calculator</div>
            </a>
            <a href="/quickship" style="text-decoration:none;">
                <div style="width:247px;height:50px;color:white;padding-top:5px;padding-left:30px;padding-right:20px;">
                    <img src="/img/shipment.png" style="float:left;" />
                    <div style="float:left;padding-top:10px;">&nbsp;New Shipment</div>
                </div>
            </a>

        </div>

        <div id="divUserContent" style="padding-top:130px;padding-left:10px;height:1000px;display: inline-block">





            <div style="clear:both;height:0px;">&nbsp;</div>

            <div style="border-bottom:solid 1px black;width:990px;height:60px;border-top:solid 1px black;">
                <div style="font-family:Arial-Bold;color:#eb6136;font-size:22px;float:left;">
                    Hello <?php echo AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') ?>!
                </div>
                <div style="float:right;padding-top:5px;font-family:Arial-Bold;color:#eb6136;font-size:16px;">
                    My US Address
                </div>

                <div style="clear:both;height:0px;">&nbsp;</div>
                <div style="float:right;">

                    <span style="font-family:Arial-Bold;color:#eb6136;font-size:16px;float:left;">
                        23600 Mercantile Rd. Suite C-119, c/o<?php echo $co_code ?>
                        Beachwood, Ohio 44122
                    </span>
                </div>
                <div style="clear:both;height:5px;">&nbsp;</div>
            </div>

            <?php echo("<div style='color:red'>" . $this->Session->flash() . "</div>"); ?>
            <?php echo $this->fetch('content'); ?>

        </div>
    </div>

        <div style="clear:both;height:0px;">&nbsp;</div>

        <div>
            <?php echo $this->element('footer'); ?>
        </div>
<script type="text/javascript">
	// A $( document ).ready() block.
    jQuery( document ).ready(function() {
        function makeLogout(){
        	alert('works');
        	return false;
        }
    });
</script>
</body>
</html>

