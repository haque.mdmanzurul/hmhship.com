<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="<?php echo (isset($meta_description_for_layout)) ? $meta_description_for_layout : ""; ?>">
        <meta name="keywords" content="parcel forwarding service, us package forwarding, parcel forwarding international, parcel forwarding company">
        <?php if (Configure::read('debug') > 0): ?>
            <meta name="robots" content="noindex, nofollow">
        <?php endif; ?>
        <!-- Google PageSpeed tool doesn't like external CSS files.  So all CSS for Home Page here. -->


        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <meta name="google-site-verification" content="j63iUmPcvJmTl7WiDwA-euiQgT2CeaM5T5DwXHD2hdk" />
        <title><?php echo $title_for_layout; ?></title>


        <?php


         if (Configure::read('debug') > 0) {
             echo $this->Html->css('/css/app.css');
			 echo $this->Html->css('/FRONTEND/css/dev/angular-toggle-switch.css');
             echo $this->Html->css('/FRONTEND/_init/bower_components/angular-ui-select/dist/select.css');
             echo $this->Html->css('/FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.css');
             echo $this->Html->css('/FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.bootstrap.css');
             echo $this->Html->css('/FRONTEND/_init/bower_components/ng-dialog/css/ngDialog.css');
             echo $this->Html->css('/FRONTEND/_init/bower_components/ng-dialog/css/ngDialog-theme-default.css');

         } else {
            echo $this->Html->css('/css/app.min.css');
            echo $this->Html->css('/css/angular-toggle-switch.min.css');
            echo $this->Html->css('/css/select.min.css');
            echo $this->Html->css('/css/ng-tags-input.min.css');
            echo $this->Html->css('/css/ng-tags-input.bootstrap.min.css');
            echo $this->Html->css('/css/ngDialog.min.css');
            echo $this->Html->css('/css/ngDialog-theme-default.min.css');
         }

     ?>

        <!--
            IE8 support, see AngularJS Internet Explorer Compatibility http://docs.angularjs.org/guide/ie
            For Firefox 3.6, you will also need to include jQuery and ECMAScript 5 shim
        -->
        <!--[if lt IE 9]>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/2.2.0/es5-shim.js"></script>
        <script>
            document.createElement('ui-select');
            document.createElement('ui-select-match');
            document.createElement('ui-select-choices');
        </script>
        <![endif]-->

        <!--[if lt IE 9]>
        <script type="text/javascript" src="../js/html5shiv.js"></script>
        <script type="text/javascript" src="../js/respond.min.js"></script>
        <![endif]-->
        <!--[if lte IE 8]>
        <script type="text/javascript" src="../js/json3.min.js"></script>
        <![endif]-->


        <script type="text/javascript">
            window.Configure = <?php echo stripslashes($config_json); ?>;
        </script>
        <!-- <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          //  ga('create', 'UA-77813068-1', 'auto');
         //   ga('send', 'pageview');
        </script> -->

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-77813068-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-77813068-1');
        </script>



        <script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "LocalBusiness",
            "url": "https://www.hmhship.com/",
            "logo": "https://www.hmhship.com/img/logo.png",
            "image": "https://www.hmhship.com/img/logo.png",
            "priceRange": "$7.25",
            "address": {
                "@type": "PostalAddress",
                "addressLocality": "Beachwood",
                "addressRegion": "OH",
                "postalCode":"44122",
                "streetAddress": "23600 Mercantile Rd Suite C-119"
            },
            "description": "Have your U.S. parcels forwarded to you in any country worldwide.  QUICK, EASY, AND HASSLE-FREE",
            "name": "HMHShip",
            "telephone": "+1 216-359-3396",
            "openingHours": [
                "Mo-Su 00:00-24:00"
            ],
            "geo": {
                "@type": "GeoCoordinates",
                "latitude": "41.457792",
                "longitude": "-81.508605"
            },
            "sameAs" : [
            "https://www.facebook.com/hmhship/",
            "https://twitter.com/HMHShip",
            "https://plus.google.com/110836815863701052137",
            "https://www.linkedin.com/company/hmhship"]
            }
        </script>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

		<!-- Favicon -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
		<link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
		<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
		<link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />
		<meta name="application-name" content="&nbsp;"/>
		<meta name="msapplication-TileColor" content="#FFFFFF" />
		<meta name="msapplication-TileImage" content="/mstile-144x144.png" />
		<meta name="msapplication-square70x70logo" content="/mstile-70x70.png" />
		<meta name="msapplication-square150x150logo" content="/mstile-150x150.png" />
		<meta name="msapplication-wide310x150logo" content="/mstile-310x150.png" />
		<meta name="msapplication-square310x310logo" content="/mstile-310x310.png" />
		<!-- end Favicon -->

		<?php
		if (($this->request->params['controller'] == "Users" &&  $this->request->params['action'] == "reset") ||
			($this->request->params['controller'] == "Users" &&  $this->request->params['action'] == "login")) {
			echo('<meta name="robots" content="noindex">');
		}
		?>

    </head>
    <body>

        <nav class="navbar navbar-fixed-top">
            <div class="container">

			<?php
				if ($this->request->params['controller'] == "Users"
					&& $this->request->params['action'] != "login"
					&& $this->request->params['action'] != "newuser"
					&& $this->request->params['action'] != "reset") {
			?>


			 <ul class="top-menu" style="font-size:12px;">
                <li style="">
					<a href="/Users/account_settings" style="">
						<div style="float:left;color:black;">ACCOUNT SETTINGS</div>
					</a>
				</li>
				<li style="position:relative;top:-5px;">|</li>
				<li style="">
					<a href="/Users/logout" style="padding-top:7px;">
						<div style="float:left;color:black;margin-right:5px;">LOG OUT</div>
					</a>
				</li>


                         <?php
                                 if (isset($notification_count)) {
                         ?>
                         <li style="position:relative;top:-5px;">|</li>
                         <li style="">
                         					<a href='/Users/myaccount' style="padding-top:4px;">

                                                <div style="font-size: 14px;color: #EC8C3A;font-weight: bold;"> <img src='/img/flag2.png' style='width: 25px; margin-top: -5px'/>
                                                <span style="font-size: 18px;color: #EC8C3A;font-weight: bold;"><?php echo $notification_count ?></div>
                         					</a>
                         				</li>

                          <?php
                                 }
                         ?>
			</ul>


			<?php } else {?>

                <ul class="top-menu">
                    <li>
                        <a href="/shipping-calculator" title="Shipping calculator">
                            <i class="icon-calculator"></i>
                            Shipping calculator
                        </a>
                    </li>
                    <li>
                      <?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>
                               <a href="/new-shipment" title="New Shipment"><i class="icon-quickship"></i>New Shipment</a>
                              	<?php } else { ?>
                                  <a href="/quickship" title="Quickship"><i class="icon-quickship"></i>Quickship</a>
                                  	<?php } ?>

                    </li>
                    <?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>
                        <li>Welcome <a href="/sign-in">
					<?php echo(AuthComponent::user('username')) ?></a>
                            <a href="/Users/myaccount">My Account</a></li>
                        <li><a href="/Users/logout" title="Logout">
                                Logout
                            </a>
                        </li>
					<?php } else { ?>
                        <li class="account-section hidden-mobile important">
                            <a href="/sign-in" title="Sign in">
                                Sign In
                            </a>
                            <span> or </span>
                            <a href="/register" title="Create an account">
                                Register
                            </a>
                        </li>
					<?php } ?>
					<li>
						<div id="google_translate_element"></div>
						<script type="text/javascript">
							function googleTranslateElementInit() {
							  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE, gaTrack: true, gaId: 'UA-77813068-1'}, 'google_translate_element');
							}
							</script>
						<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					</li>
                </ul>
			<?php } ?>

                <a href="/" class="logo">
<?php echo $this->Html->image('/img/logo.png', array('alt' => 'HMH Ship Parcel Forwarding Simplified', 'height' => '71', 'width' => '294')); ?>
                </a>
                <div class="menu-btn visible-mobile"></div>

                <ul class="main-menu hidden-mobile">
                    <li class="account-section visible-mobile important">
                        <a href="/sign-in" title="Sign in">
                            Sign In
                        </a>
                    </li>
                    <li class="visible-mobile important">
                        <a href="/register" title="Create an account">
                            Register
                        </a>
                    </li>
                    <li>

                        <a href="/" title="Home"
						<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "home")
								echo("style='color:#fff;'");
						?>
						>
                            <span class="txt">Home</span>
                            <span class="bg"
							<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "home")
								echo("style='display:block;'");
							?>
							></span>
                        </a>
                    </li>
                    <li>
                        <a href="/services-rates" title="Services &amp; Rates"
						<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "what_we_do")
								echo("style='color:#fff;'");
						?>
						>
                            <span class="txt">Services &amp; rates</span>
                            <span class="bg"
							<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "what_we_do")
								echo("style='display:block;'");
							?>
							></span>
                        </a>
                    </li>
                    <li>
                        <a href="/business" title="Business"
						<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "business")
								echo("style='color:#fff;'");
						?>
						>
                            <span class="txt">Business</span>
                            <span class="bg"
							<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "business")
								echo("style='display:block;'");
							?>
							></span>
                        </a>
                    </li>
                    <li>
                        <a href="/hmhship-bio" title="hmhship bio"
						<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "bio")
								echo("style='color:#fff;'");
						?>
						>
                            <span class="txt">hmhship bio</span>
                            <span class="bg"
							<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "bio")
								echo("style='display:block;'");
							?>
							></span>
                        </a>
                    </li>
                    <li>
                        <a href="/talk-to-us" title="Talk to us"
						<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "contact")
								echo("style='color:#fff;'");
						?>
						>
                            <span class="txt">Talk to us</span>
                            <span class="bg"
							<?php if ($this->request->params['controller'] == "pages" &&  $this->request->params['action'] == "contact")
								echo("style='display:block;'");
							?>
							></span>
                        </a>
                    </li>
                </ul>

            </div>
            <div class="opac-bar hidden-mobile"></div>
        </nav>


			<?php
				if ($this->request->params['controller'] == "Users"
				  && $this->request->params['action'] != "login"
				  && $this->request->params['action'] != "newuser"
				  && $this->request->params['action'] != "reset") {
					echo("<div class='wm-main' style='background-color:white;'>");
				    echo("<link rel='stylesheet' type='text/css' href='/css/user/style.css'/>");
					echo("<div style='white-space:nowrap;overflow:auto;width:1600px;max-width:1900px;background-color:white;'>");
					echo $this->element('user_menu');
				 }
				 else
					echo("<div class='wm-main'>");
			?>

			<?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
			<?php
				if ($this->request->params['controller'] == "Users"
				  && $this->request->params['action'] != "login"
				  && $this->request->params['action'] != "newuser"
				  && $this->request->params['action'] != "reset") {
					echo("</div>");
				 }
			?>
        </div>
		<div style="height:0px;clear:both;">&nbsp;</div>

    <?php

	echo $this->element('footer');

    // Angular not required on Home Page
    if ( !($this->request->params['controller'] == "pages" && $this->request->params['action'] == "home")) {

        if(Configure::read('debug') > 0) {
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular/angular.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-route/angular-route.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-resource/angular-resource.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-sanitize/angular-sanitize.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-animate/angular-animate.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap-tpls.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/ngstorage/ngStorage.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.js');
            echo $this->Html->script('/FRONTEND/_init/bower_components/ng-dialog/js/ngDialog.js');
            echo $this->Html->script('/FRONTEND/js/dev/plugins/bootstrap.min.js');
            echo $this->Html->script('/FRONTEND/js/modules/app.js');
            echo $this->Html->script('/FRONTEND/js/modules/QuickshipSvc.js');
        } else {
            // PRODUCTION minified/compressed javascript files
            echo $this->Html->script('/js/angular-bundle.min.js');
            echo $this->Html->script('/js/app.min.js');
        }
    }
?>

    </body>
</html>