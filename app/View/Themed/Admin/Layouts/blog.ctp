﻿<!DOCTYPE html>
<html>
<head>
    <title></title>
	<meta charset="utf-8" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>
<body>
<main id="section">
	
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('auth', array('params' => array('class' => 'alert alert-danger'))); ?>
    <?php echo $this->fetch('content'); ?>
</main>
</body>
</html>
