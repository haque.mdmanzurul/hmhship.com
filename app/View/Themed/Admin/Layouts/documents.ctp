﻿<!DOCTYPE html>
<html>
<head>
    <title></title>
	<meta charset="utf-8" />
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	 <?php echo $this->fetch('css');
	echo $this->fetch('script'); ?>
</head>
<body>
<main id="section">
	
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('auth', array('params' => array('class' => 'alert alert-danger'))); ?>
    <?php echo $this->fetch('content'); ?>
</main>
</body>
</html>
