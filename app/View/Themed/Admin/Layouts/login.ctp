<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <title><?php echo $title_for_layout; ?></title>
	<?php if(Configure::read('debug') > 0): ?>
		<meta name="robots" content="noindex, nofollow">
	<?php endif; ?>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <?php echo $this->Html->css('admin/reset'); ?>
    <?php echo $this->Html->css('admin/iconfont'); ?>
    <?php echo $this->Html->css('admin/plugins'); ?>
    <?php echo $this->Html->css('admin/style'); ?>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <?php echo $this->Html->css('/js/admin/libs/autosuggest/autosuggest_inquisitor.css', array('media' => 'screen', 'charset' => 'utf-8')); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../js/html5shiv.js"></script>
    <script type="text/javascript" src="../js/respond.min.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script type="text/javascript" src="../js/json3.min.js"></script>
    <![endif]-->
    <?php echo $this->fetch('styles'); ?>
</head>
<body>

<header>

</header>
<main id="section">
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('auth'); ?>
    <?php echo $this->fetch('content'); ?>
</main>


<?php echo $this->Html->scriptBlock("var BASE_URL = '".Router::url('/')."';"); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<?php echo $this->fetch('script'); ?>
</body>