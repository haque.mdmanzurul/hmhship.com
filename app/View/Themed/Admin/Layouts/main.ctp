<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="Description" content="">
	<meta name="Keywords" content="">
	<title><?php echo $title_for_layout; ?></title>
	<?php if(Configure::read('debug') > 0): ?>
		<meta name="robots" content="noindex, nofollow">
	<?php endif; ?>
	<!--[if lte IE 8]>
	<script>
		document.createElement('ng-include');
		document.createElement('ng-pluralize');
		document.createElement('ng-view');

		// Optionally these for CSS
		document.createElement('ng:include');
		document.createElement('ng:pluralize');
		document.createElement('ng:view');
	</script>
	<![endif]-->
	<link rel="stylesheet" href="/FRONTEND/_init/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<?php echo $this->Html->css('admin/reset'); ?>
	<?php echo $this->Html->css('admin/iconfont'); ?>
	<?php echo $this->Html->css('admin/plugins'); ?>
	<?php echo $this->Html->css('admin/style'); ?>
	<?php echo $this->Html->css('/FRONTEND/_init/bower_components/angular-xeditable/dist/css/xeditable.css'); ?>
	<link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
	<?php echo $this->Html->css('/js/admin/libs/autosuggest/autosuggest_inquisitor.css', array('media' => 'screen')); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<!--[if lt IE 9]>
	<script type="text/javascript" src="../js/html5shiv.js"></script>
	<script type="text/javascript" src="../js/respond.min.js"></script>
	<![endif]-->
	<!--[if lte IE 8]>
	<script type="text/javascript" src="../js/json3.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
		window.Configure = <?php echo stripslashes($config_json); ?>;
	</script>
	<?php echo $this->fetch('styles'); ?>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<script type="text/javascript">
	var count = 1;
	function toggleMenu(){
	            console.log('working');
               // jQuery(this).addClass('menuexpanded');
                if (count%2 == 1)
                    jQuery('.nav-wrap').css('left', '90px');
                else
                    jQuery('.nav-wrap').css('left', '-270px');

                count++;
	}
    </script>
</head>
<body data-ng-app="adminApp" ng-class="{'panelloading': loadingData}" data-ng-controller="MainCtrl">

<header>
	<ul>
		
		<li>New Notification &nbsp;
			<span class="counter" style="background-color: #E86136;">
				<?php echo $count_registered + $messages_unread_count + $messages_registered_unread_count +
						$business_unread_count + $quickship_unread_count + $quickship_registered_unread_count; ?>
			</span>
		</li>
		<li><a href="<?php echo $this->Html->url(array('controller' => 'admins', 'action' => 'logout', 'admin' => true)); ?>">Log Out<span class="icon-logout righticon"></span></a></li>
	</ul>
</header>
<aside>
	<div class="logo"></div>
	<div class="nav-wrap">
		<nav>
			<span class="icon-menu visible-large-tablet" onclick="toggleMenu()"></span>

			<ul class="menu-wrap">
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'index')); ?>?section=dashboard">
					<li <?php if(isset($this->viewVars['section']) && ($this->viewVars['section'] == "" || $this->viewVars['section'] == 'dashboard')) { echo 'class="active"';}  ?>><span class="icon-dashboard"></span><span class="menuitem">Dashboard</span></li>
				</a>
				<a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'registered')); ?>#/registered">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'registered'){ echo 'class="active"';} ?> ><span class="icon-bizcust"></span><span class="menuitem">Registered Accounts</span> <span class="counter"><?php echo $count_registered; ?></span></li>
				</a>
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'messages')); ?>#/messages">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'messages'){ echo 'class="active"';} ?> ><span class="icon-messages"></span><span class="menuitem">Messages</span> <span class="counter"><?php echo $messages_unread_count; ?></span></li>
				</a>
				<a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'messages_registered')); ?>#/messages_registered">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'messages_registered'){ echo 'class="active"';} ?> ><span class="icon-messages"></span><span class="menuitem">Messages - Registered</span> <span class="counter"><?php echo $messages_registered_unread_count; ?></span></li>
				</a>
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'business')); ?>#/business">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'bizcust'){ echo 'class="active"';} ?> ><span class="icon-bizcust"></span><span class="menuitem">Business Customers</span> <span class="counter"><?php echo $business_unread_count; ?></span></li>
				</a>
				<a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'quickship', 'action' => 'index')); ?>">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'qscust'){ echo 'class="active"';} ?> ><span class="icon-qscust"></span><span class="menuitem">QuickShip Customers</span> <span class="counter"><?php echo $quickship_unread_count; ?></span></li>
				</a>
				<a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'quickship', 'action' => 'registered')); ?>#/quickship_registered">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'qscust_registered'){ echo 'class="active"';} ?> ><span class="icon-qscust"></span><span class="menuitem">New Shipments</span> <span class="counter"><?php echo $quickship_registered_unread_count; ?></span></li>
				</a>
				<a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'quickship', 'action' => 'ghost')); ?>#/ghost">
                					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'qscust_ghost'){ echo 'class="active"';} ?> ><span class="icon-qscust"></span><span class="menuitem">Ghost Shipments</span> <span class="counter"><?php echo $count_waiting_package_ghost; ?></span></li>
                				</a>
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'blogs')) ?>">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'blogs'){ echo 'class="active"';} ?> ><span class="icon-blogs"></span><span class="menuitem">Blogs</span></li>
				</a>
				 <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'documents')) ?>">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'documents'){ echo 'class="active"';} ?> ><span class="icon-blogs"></span><span class="menuitem">File Manager</span></li>
				</a>
			</ul>
		</nav>
        <div class="bottom-menu hidden">
			<div class="icon-arrowdown"></div>
			<fieldset>
				<select name="suite-selector" class="firstload">
					<option <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] == 458){ echo 'selected';} ?> value="458">458 - John Doe</option>
					<option <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] == 845){ echo 'selected';} ?> value="845">845 - Mike Dohnam</option>
					<option <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] == 254){ echo 'selected';} ?> value="254">254 - Alves Santos</option>
					<option <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] == 758){ echo 'selected';} ?> value="758">758 - Jose Garcia</option>
					<option <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] == 785){ echo 'selected';} ?> value="785">785 - Edward Suarez</option>
				</select>
			</fieldset>
			<a href="?section=regcust<?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] != ''){ echo "page=".$_GET['page']."&tab=newshipment"; } ?>">
				<p class="primary">
					<span class="icon-shipment lefticon"></span>Add New Shipment
				</p>
			</a>
			<hr />
			<ul>
				<a href="?section=alltransactions">
					<li><span class="icon-alltransactions lefticon"></span> All Transactions<span class="counter">182</span></li>
				</a>
				<a href="?section=allusers">
					<li><span class="icon-users lefticon"></span> All Users<span class="counter">56</span></li>
				</a>
			</ul>
		</div>
	</div>
</aside>
<main id="section">
	<?php echo $this->Session->flash(); ?>
	<?php echo $this->Session->flash('auth'); ?>
	<?php echo $this->fetch('content'); ?>
</main>


<?php echo $this->Html->scriptBlock("var BASE_URL = '".Router::url('/')."';"); ?>
<?php #echo $this->Html->script('admin/load.smartajax', array('inline' => false)); ?>
<?php #echo $this->Html->script('admin/plugins', array('inline' => false)); ?>
<?php #echo $this->Html->script('admin/libs/autosuggest/bsn.AutoSuggest_2.1.3', array('inline' => false)); ?>
<?php #echo $this->Html->script('admin/main', array('inline' => false)); ?>
<?php
echo $this->Html->script('/FRONTEND/_init/bower_components/jquery/dist/jquery.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/jqueryui/jquery-ui.min.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular/angular.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-route/angular-route.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-resource/angular-resource.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-sanitize/angular-sanitize.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-animate/angular-animate.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/ng-tags-input/ng-tags-input.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap/ui-bootstrap-tpls.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/ngstorage/ngStorage.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-xeditable/dist/js/xeditable.min.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/angular-bootstrap-toggle-switch/angular-toggle-switch.js');
echo $this->Html->script('/FRONTEND/_init/bower_components/ng-dialog/js/ngDialog.js');

echo $this->Html->script('src/admin/app.js');
echo $this->Html->script('src/admin/quickship/transactionSvc.js');
echo $this->Html->script('src/admin/QuickshipSvc.js');
?>
<script type="text/javascript">
jQuery(document).ready(function() {
    var toggled = false;
   jQuery('.expander').click(function(){
        if (!toggled) {
            jQuery('#innerpanel').addClass('innerpanel-mobile');
            toggled = true;
        }
        else {
            jQuery('#innerpanel').removeClass('innerpanel-mobile');
            toggled = false;
        }
   });
  
});
</script>
<?php echo $this->fetch('script'); ?>

</body>
</html>