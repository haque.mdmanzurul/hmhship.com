<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >
    <meta name="Description" content="">
    <meta name="Keywords" content="">
    <title><?php echo $title_for_layout; ?></title>
	<?php if(Configure::read('debug') > 0): ?>
		<meta name="robots" content="noindex, nofollow">
	<?php endif; ?>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <?php echo $this->Html->css('admin/reset'); ?>
    <?php echo $this->Html->css('admin/iconfont'); ?>
    <?php echo $this->Html->css('admin/plugins'); ?>
    <?php echo $this->Html->css('admin/style'); ?>
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
    <?php echo $this->Html->css('/js/admin/libs/autosuggest/autosuggest_inquisitor.css', array('media' => 'screen', 'charset' => 'utf-8')); ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <!--[if lt IE 9]>
    <script type="text/javascript" src="../js/html5shiv.js"></script>
    <script type="text/javascript" src="../js/respond.min.js"></script>
    <![endif]-->
    <!--[if lte IE 8]>
    <script type="text/javascript" src="../js/json3.min.js"></script>
    <![endif]-->
    <?php echo $this->fetch('styles'); ?>
</head>
<body data-ng-app="adminApp">

<header>
    <ul>
        <li>
            <div class="search_bar hidden">
                <form method="post" action="/search_engine/" class="home_searchEngine" id="form_search_all">
                    <input type="hidden" id="search_all_value" name="search_value">
                    <span class="search-text-wrap">
                        Search<span class="icon-search righticon"></span>
                    </span>
                    <div class="closebtn">x</div>
                    <input type="text" size="24" name="search_txt" id="input_search_all" class="search_txt" autocomplete="off" placeholder="Search for...">
                </form>
            </div>
        </li>
        <li class="hidden">New Notification<span class="icon-notifications righticon"><span class="path1"></span><span class="path2"></span></span></li>
        <li><a href="<?php echo $this->Html->url(array('controller' => 'admins', 'action' => 'logout', 'admin' => true)); ?>">Log Out<span class="icon-logout righticon"></span></a></li>
    </ul>
</header>
<aside>
    <div class="logo"></div>
    <div class="nav-wrap">
        <nav>
            <span class="icon-menu visible-large-tablet"></span>

            <ul class="menu-wrap">
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'index')); ?>?section=dashboard">
                    <li <?php if(isset($this->viewVars['section']) && ($this->viewVars['section'] == "" || $this->viewVars['section'] == 'dashboard')) { echo 'class="active"';}  ?>><span class="icon-dashboard"></span><span class="menuitem">Dashboard</span></li>
                </a>
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'messages')); ?>">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'messages'){ echo 'class="active"';} ?> ><span class="icon-messages"></span><span class="menuitem">Messages</span> <span class="counter"><?php echo $messages_unread_count; ?></span></li>
                </a>
                <a class="ajaxify" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'index')); ?>?section=bizcust">
					<li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'bizcust'){ echo 'class="active"';} ?> ><span class="icon-bizcust"></span><span class="menuitem">Business Customers</span> <span class="counter">0</span></li>
                </a>
                <a href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'quickship', 'action' => 'index')); ?>">
                    <li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'qscust'){ echo 'class="active"';} ?> ><span class="icon-qscust"></span><span class="menuitem">QuickShip Customers</span> <span class="counter"><?php echo $quickship_unread_count; ?></span></li>
                </a>
                <a class="ajaxify hidden" href="<?php echo $this->Html->url(array('admin' => true, 'controller' => 'admins', 'action' => 'index')); ?>?section=regcust">
                    <li <?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust'){ echo 'class="active"';} ?> ><span class="icon-regcust"></span><span class="menuitem">Registered Customers</span> <span class="counter"></span></li>
                </a>
				
            </ul>
        </nav>
        <div class="bottom-menu hidden">
            <div class="icon-arrowdown"></div>
            <fieldset>
                <select name="suite-selector" class="firstload">
                    
                </select>
            </fieldset>
            <a href="?section=regcust<?php if(isset($this->viewVars['section']) && $this->viewVars['section'] == 'regcust' && $_GET['page'] != ''){ echo "page=".$_GET['page']."&tab=newshipment"; } ?>">
                <p class="primary">
                    <span class="icon-shipment lefticon"></span>Add New Shipment
                </p>
            </a>
            <hr />
            <ul>
                <a href="?section=alltransactions">
                    <li><span class="icon-alltransactions lefticon"></span> All Transactions<span class="counter">182</span></li>
                </a>
                <a href="?section=allusers">
                    <li><span class="icon-users lefticon"></span> All Users<span class="counter">56</span></li>
                </a>
            </ul>
        </div>
    </div>
</aside>
<main id="section">
    <?php echo $this->Flash->render(); ?>
    <?php echo $this->Flash->render('auth', array('params' => array('class' => 'alert alert-danger'))); ?>
    <?php echo $this->fetch('content'); ?>
</main>

<?php echo $this->Html->scriptBlock("var BASE_URL = '".Router::url('/')."';"); ?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<?php echo $this->Html->script('admin/load.smartajax', array('inline' => false)); ?>
<?php echo $this->Html->script('admin/plugins', array('inline' => false)); ?>
<?php echo $this->Html->script('admin/libs/autosuggest/bsn.AutoSuggest_2.1.3', array('inline' => false)); ?>
<?php echo $this->Html->script('admin/main', array('inline' => false)); ?>
<?php echo $this->fetch('script'); ?>
</body>
</html>