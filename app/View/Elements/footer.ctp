<style>

</style>
<footer class="footer" >
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <a href="/" class="logo">
<?php echo $this->Html->image('/img/logo-white.png', array('alt' => 'HMH Ship Parcel Forwarding Simplified', 'height' => '74', 'width' => '305')); ?>
                        </a>
                        <ul class="contact">
                            
                            <li>
                                <div><i class="icon-location"></i>
								23600 Mercantile Rd. Suite C-119 Beachwood,</div>
								<div style="margin-left:27px;">OH 44122, USA</div>
                            </li>
                        </ul>
                        <ul class="social">
                            <li class="fb-wrap">
                                <a href="https://www.facebook.com/hmhship/" target="_blank" title="Facebook">                       
                                    <i class="icon-fb"></i>
                                </a>
                            </li>
                            <li class="tw-wrap">
                                <a href="https://twitter.com/HMHShip" target="_blank" title="Twitter" style="">                                
                                    <i class="icon-tw"></i>                                
                                </a>
                            </li>
                            <li >
                              <a href="https://www.instagram.com/hmhship/" target="_blank" title="Instagram" style="width:40px;">
                                <img src="/img/instagram.png" style="height:39px;margin-top:-3px;"></img>
                              </a>
                            </li>
                            <li class="gplus-wrap">
                                <a href="https://plus.google.com/110836815863701052137" target="_blank" title="Google+">                                
                                    <i class="icon-google-plus3"></i>                                
                                </a>
                            </li>
                            <li class="" >
                              <a href="https://www.youtube.com/channel/UCh5Uy0vsg0rr0m90PjGFsNg?view_as=subscriber" target="_blank" title="YouTube">
                                <img src="/img/youtube.png" style="width:40px;"></img>
                              </a>
                            </li>
                            <li class="in-wrap">
                                <a href="https://www.linkedin.com/company/hmhship" target="_blank" title="LinkedIn">                                
                                    <i class="icon-linkedin"></i>                                
                                </a>
                            </li>
                         
                        </ul>
                        <div class="visible-tablet">
                            <h4 class="footer-title">Legal</h4>
                            <ul class="menu menu-3">
                                <li>
                                    <a href="/terms" title="Terms & Conditions">Terms &amp; Conditions</a>
                                </li>
                                <li>
                                    <a href="/privacy" title="Privacy policy">Privacy policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-2 col-md-offset-2">
                        <h4 class="footer-title">HMHShip.com</h4>
                        <ul class="menu menu-1">
                            <li>
                                <a href="/" title="">Home</a>
                            </li>
                            <li>
                              <a href="/services-rates" title="Services &amp; Rates">Services &amp; Rates</a>
                            </li>
                            <li>
                                <a href="/hmhship-bio" title="HMHShip Bio">HMHShip Bio</a>
                            </li>
                            <li>
                                <a href="/talk-to-us" title="Talk to us">Talk to us</a>
                            </li>
                            <li>
                                <a href="/faq" title="FAQ">FAQ</a>
                            </li>
                            <li>
                                <a href="/blog" title="Blog">Blog</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-2">
                        <h4 class="footer-title">Our Services</h4>
                        <ul class="menu menu-2">
                          <li>
                            <a href="/quickship" title="QuickShip">QuickShip</a>
                          </li>
                          <?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>
                          <li>
                            <a href="/quickship" title="New Shipment">New Shipment</a>
                          </li>
                          <?php } else { ?>
                          <li>
                            <a href="/Users/myaccount" title="New Shipment">New Shipment</a>
                          </li>
                          <?php } ?>
                          <li>
                            <a href="/shipping-calculator" title="Shipping Calculator">Shipping Calculator</a>
                          </li>             
                            <li>
                                <a href="/business" title="Business">Business</a>
                            </li>    

                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-2 hidden-tablet">
                        <h4 class="footer-title">Legal</h4>
                        <ul class="menu menu-3">
                            <li>
                                <a href="/terms" title="Terms & Conditions">Terms & Conditions</a>
                            </li>
                            <li>
                                <a href="/privacy" title="Privacy Policy">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
              
                
				<h4>Featured Partners</h4>
                <div class="inline-all desktop-inline featured-partners">
                	<div class="partner-wrap">
                		<img src="/img/fedex.png" />
					</div>
                	<div class="partner-wrap">
                		<img src="/img/usps.png" />
					</div>
                	<div class="partner-wrap">
                		<img src="/img/ups.png" />
					</div>
                	<div class="partner-wrap">
                		<img src="/img/dhl.png" />
					</div>
                	<div class="partner-wrap">
                		<img src="/img/paypal.png" />
					</div>
				</div>
              
            </div>
            <div class="foot-bar foot-bar-1"></div>
            <div class="foot-bar foot-bar-2"></div>
        </footer>
<script type="text/javascript">
jQuery(document).ready(function() {

   jQuery('.logouttrigger').click(function(){
           alert('works'); return false;
      });
});
</script>
<script type='text/javascript' data-cfasync='false'>
  window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: 'dfa1c4c5-b87c-404f-9ed7-c65163b846cc', f: true }); done = true; } }; })();
</script>
