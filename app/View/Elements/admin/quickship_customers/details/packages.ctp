<div class="innersection">
	<h4 class="innersection-title">Package Details</h4>

	<!-- Start Packages -->
	<?php foreach ($transaction_detail['Shipment'] as $package_number => $shipment) : ?>
		<div class="block hasPadding">
			<h3>Package <?php echo $package_number + 1; ?></h3>
			<span class="edit-btn">Edit</span>

			<div class="row">
				<div class="col-md-4">
					<h4>Gross Value</h4>

					<div class="non-editable">
						<p>$<span
								name="pack1-value"><?php echo $this->Transaction->getPackageValue($shipment); ?></span>
						</p>
					</div>
					<div class="editable">
						<input type="text" name="pack1-value"
							   value="<?php echo $this->Transaction->getPackageValue($shipment); ?>">
					</div>
				</div>
				<div class="col-md-4">
					<h4>Estimate Gross Weight</h4>

					<div class="non-editable">
						<p><span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span></p>
					</div>
					<div class="editable">
						<fieldset class="twoinputs">
							<input name="pack1-weight" type="number" min="0" value="99"/>
						</fieldset>
						<fieldset class="twoinputs">
							<select name="pack1-weightmeas">
								<option value="lb">lb</option>
								<option value="kg">kg</option>
							</select>
						</fieldset>
					</div>
				</div>
				<div class="col-md-4">
					<h4>Gross Dimensions</h4>

					<div class="non-editable">
						<p><span name="pack1-width">99</span>x<span name="pack1-length">99</span>x<span
								name="pack1-height">99</span><span
								class="pack1-dimensionmeas">cm</span></p>
					</div>
					<div class="editable">
						<fieldset class="fourinputs">
							<input name="pack1-width" type="number" min="0" value="99"/>
						</fieldset>
						<fieldset class="fourinputs">
							<input name="pack1-length" type="number" min="0" value="99"/>
						</fieldset>
						<fieldset class="fourinputs">
							<input name="pack1-height" type="number" min="0" value="99"/>
						</fieldset>
						<fieldset class="fourinputs">
							<select name="pack1-dimensionmeas">
								<option value="in">in</option>
								<option value="cm">cm</option>
							</select>
						</fieldset>
						<button>Save</button>
					</div>
				</div>
			</div>
		</div>

		<h3>Item Details</h3>

		<!-- Start Items -->
		<?php foreach ($shipment['Package']['Item'] as $item) : ?>
			<div class="row">
				<div class="col-md-4">
					<div class="block hasPadding">
						<h4>Type of Content</h4>
						<span class="edit-btn">Edit</span>

						<div class="non-editable">
							<p><span name="typecontent"><?php echo $item['type_id']; ?></span></p>
						</div>
						<div class="editable">
							<fieldset>
								<input name="typecontent" type="text" value="<?php echo $item['type_id']; ?>">
							</fieldset>
							<button>Save</button>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block hasPadding">
						<h4>Weight</h4>
						<span class="edit-btn">Edit</span>

						<div class="non-editable">
							<p><span name="pack2-weight"><?php echo (is_numeric($item['weight_lb']) ? $item['weight_lb'] : $item['weight_kg']); ?></span><span name="pack2-weightmeas"><?php echo (is_numeric($item['weight_lb']) ? 'lb' : 'kg'); ?></span></p>
						</div>
						<div class="editable">
							<fieldset class="twoinputs">
								<input name="pack2-weight" type="number" min="0" value="<?php echo (is_numeric($item['weight_lb']) ? $item['weight_lb'] : $item['weight_kg']); ?>">
							</fieldset>
							<fieldset class="twoinputs">
								<select name="pack2-weightmeas">
									<option value="lb">lb</option>
									<option value="kg">kg</option>
								</select>
							</fieldset>
							<button>Save</button>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block hasPadding">
						<h4>Quantity</h4>
						<span class="edit-btn">Edit</span>

						<div class="non-editable">
							<p><span name="quantity"><?php echo $item['quantity']; ?></span></p>
						</div>
						<div class="editable">
							<fieldset>
								<input name="quantity" type="number" min="1" value="<?php echo $item['quantity']; ?>"/>
							</fieldset>
							<button>Save</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="block hasPadding">
						<h4>Country of origin</h4>
						<span class="edit-btn">Edit</span>

						<div class="non-editable">
							<p><span name="country"><?php echo $item['Country']['name']; ?></span></p>
						</div>
						<div class="editable">
							<fieldset>
								<input name="country" type="text" value="<?php echo $item['Country']['name']; ?>">
							</fieldset>
							<button>Save</button>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="block hasPadding">
						<h4>Price</h4>
						<span class="edit-btn">Edit</span>

						<div class="non-editable">
							<p>$<span name="value"><?php echo $item['value']; ?></span></p>
						</div>
						<div class="editable">
							<fieldset>
								<input name="value" type="number" min="1" value="<?php echo $item['value']; ?>"/>
							</fieldset>
							<button>Save</button>
						</div>
					</div>
				</div>
			</div>

		<?php endforeach; ?>
		<!-- End Items -->
		<hr>
	<?php endforeach; ?>
	<!-- End Packages -->
</div>