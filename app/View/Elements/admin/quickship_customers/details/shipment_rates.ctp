<div class="innersection">
	<h4 class="innersection-title">Shipment Rates</h4>

	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Package 1</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p>USPS Priority Mail International</p>

					<p>$99.99</p>
				</div>
				<div class="editable">
					<fieldset>
						<select name="service">
							<option>USPS Priority Mail International</option>
						</select>
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Package 2</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p>USPS Priority Mail International</p>

					<p>$99.99</p>
				</div>
				<div class="editable">
					<fieldset>
						<select name="service">
							<option>USPS Priority Mail International</option>
						</select>
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
</div>