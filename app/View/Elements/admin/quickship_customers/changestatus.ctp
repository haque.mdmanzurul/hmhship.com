<h3>Current Shipment Status</h3>
<hr class="gray" />
<div class="row">
    <div class="col-lg-9 col-lg-push-3">
        <div class="status-wrap">
            <div class="status checked">
                <div class="statusicon-wrap">
                    <div class="icon-waitpack"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Waiting for<br>Package</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status checked">
                <div class="statusicon-wrap">
                    <div class="icon-packreceived"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Package<br>Received</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status checked  active">
                <div class="statusicon-wrap">
                    <div class="icon-awaitingpaym"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Awaiting<br>Payment</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status">
                <div class="statusicon-wrap">
                    <div class="icon-paymreceived"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Payment<br>Received</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status">
                <div class="statusicon-wrap">
                    <div class="icon-packsent"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Package<br>Sent</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
        </div>
        <div class="visible-mobile">Payment Received</div>
        <h4 class="sidenote">Select a status to update</h4>
        <button id="editstatus">Edit status</button>
    </div>
    <div class="col-lg-3 col-lg-pull-9">
        <div class="block hasPadding">
            <h4>Dimensions & Weight</h4>
            <span class="edit-btn">Edit</span>
            <div class="non-editable">
                <p><span name="width">99</span>x<span name="height">99</span>x<span name="length">99</span><span name="dimensionmeas">in</span></p>
                <p><span name="weight">99</span><span name="weightmeas">lb</span></p>
            </div>
            <div class="editable">
                <div class="block">
                    <fieldset class="fourinputs">
                        <input name="width"  type="number" min="0" value="99" />
                    </fieldset>
                    <fieldset class="fourinputs">
                        <input name="length"  type="number" min="0" value="99" />
                    </fieldset>
                    <fieldset class="fourinputs">
                        <input name="height"  type="number" min="0" value="99" />
                    </fieldset>
                    <fieldset class="fourinputs">
                        <select name="dimensionmeas">
                            <option value="in">in</option>
                            <option value="cm">cm</option>
                        </select>
                    </fieldset>
                </div>
                <div class="block">
                    <fieldset class="twoinputs">
                        <input name="weight"  type="number" min="0" value="99" />
                    </fieldset>
                    <fieldset class="twoinputs">
                        <select name="weightmeas">
                            <option value="lb">lb</option>
                            <option value="kg">kg</option>
                        </select>
                    </fieldset>
                </div>
                <button>Save</button>
            </div>
        </div>
    </div>
</div>