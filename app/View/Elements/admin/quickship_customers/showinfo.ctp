<div class="innersection">
	<h4 class="innersection-title">Personal Information</h4>

	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Name</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="name"><?php echo $transaction_detail['Address'][0]['firstname'] . ' ' . $transaction_detail['Address'][0]['lastname']; ?></span>
					</p>
				</div>
				<div class="editable">
					<fieldset>
						<input name="name" type="text"
							   value="<?php echo $transaction_detail['Address'][0]['firstname'] . ' ' . $transaction_detail['Address'][0]['lastname']; ?>"/>
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Contact Information</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span name="email"><?php echo $transaction_detail['Address'][0]['email']; ?></span></p>

					<p><span name="phone"><?php echo $transaction_detail['Address'][0]['phone']; ?></span></p>
				</div>
				<div class="editable">
					<fieldset>
						<input name="email" type="email"
							   value="<?php echo $transaction_detail['Address'][0]['email']; ?>"/>
					</fieldset>
					<fieldset>
						<input name="phone" type="text"
							   value="<?php echo $transaction_detail['Address'][0]['phone']; ?>"/>
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Address 1</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="st-address1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'address1'); ?></span>
					</p>

					<p><span
							name="city-address-1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'city'); ?>
							- <?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'adm_division'); ?></span>,
						<span
							name="zip-address-1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'postal_code'); ?></span>,
						<span
							name="country-address1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'Country.name'); ?></span>
					</p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="st-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'address1'); ?>"/>
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="zip-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'postal_code'); ?>"/>
							</fieldset>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="city-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'city'); ?>
							- <?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'adm_division'); ?>"/>
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="country-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'Country.name'); ?>">
							</fieldset>
						</div>
					</div>
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Address 2</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="st-address2"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'address2'); ?></span>
					</p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="st-address2" type="text"
									   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'billing'), 'address2'); ?>"/>
							</fieldset>
						</div>
					</div>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
	<hr/>
	<h3>Shipping</h3>

	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Name</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="name"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'firstname') . ' ' . Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'lastname'); ?></span>
					</p>
				</div>
				<div class="editable">
					<fieldset>
						<input name="name" type="text"
							   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'firstname') . ' ' . Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'lastname'); ?>"/>
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Contact Information</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="email"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'email'); ?></span>
					</p>

					<p><span
							name="phone"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'phone'); ?></span>
					</p>
				</div>
				<div class="editable">
					<fieldset>
						<input name="email" type="email"
							   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'email'); ?>">
					</fieldset>
					<fieldset>
						<input name="phone" type="text"
							   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'phone'); ?>">
					</fieldset>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Address 1</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span
							name="st-address1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'address1'); ?></span>
					</p>

					<p><span name="city-address-1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'city'); ?>
							- <?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'adm_division'); ?></span>, <span
							name="zip-address-1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'postal_code'); ?></span>,
						<span name="country-address1"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'Country.name'); ?></span></p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="st-address1" type="text"
									   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'address1'); ?>">
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="zip-address1" type="text"
									   value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'postal_code'); ?>"/>
							</fieldset>
						</div>
					</div>
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="city-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'city'); ?>
							- <?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'adm_division'); ?>">
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="country-address1" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'Country.name'); ?>">
							</fieldset>
						</div>
					</div>
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Address 2</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><span name="st-address2"><?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'address2'); ?></span></p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset>
								<input name="st-address2" type="text" value="<?php echo Hash::get($this->Transaction->extractAddress($transaction_detail, 'shipping'), 'address2'); ?>">
							</fieldset>
						</div>
					</div>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="innersection">
	<h4 class="innersection-title">Additional Options</h4>

	<div class="row">
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Additional packages</h4>
				<span class="edit-btn">Edit</span>

				<p class="bold">Weight and Value</p>

				<div class="non-editable">
					<p><label>1</label> - <span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span> - $<span
							name="pack1-value">99</span></p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset class="twoinputs">
								<input name="pack1-weight" type="text" value="99"/>
							</fieldset>
							<fieldset class="twoinputs">
								<select name="pack1-weightmeas">
									<option value="lb">lb</option>
									<option value="kg">kg</option>
								</select>
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="pack1-value" type="text" value="99"/>
							</fieldset>
						</div>
					</div>
				</div>
				<div class="non-editable">
					<p><label>2</label> - <span name="pack2-weight">99</span>lb - $<span name="pack2-value">99</span>
					</p>
				</div>
				<div class="editable">
					<div class="row">
						<div class="col-md-8">
							<fieldset class="twoinputs">
								<input name="pack2-weight" type="text" value="99"/>
							</fieldset>
							<fieldset class="twoinputs">
								<select name="pack2-weightmeas">
									<option value="lb">lb</option>
									<option value="kg">kg</option>
								</select>
							</fieldset>
						</div>
						<div class="col-md-4">
							<fieldset>
								<input name="pack2-value" type="text" value="99"/>
							</fieldset>
						</div>
					</div>
					<button>Save</button>
				</div>
			</div>
			<div class="block hasPadding">
				<h4>Need these packages expedited</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><?php echo $this->Transaction->isExpedited($transaction_detail) ? 'Yes' : 'No'; ?></p>
				</div>
				<div class="editable">
					yes/no
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="block hasPadding">
				<h4>Insurance added</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><?php echo $this->Transaction->isInsured($transaction_detail) ? 'Yes' : 'No'; ?></p>
				</div>
				<div class="editable">
					<fieldset>
						yes/no
					</fieldset>
					<button>Save</button>
				</div>
			</div>
			<div class="block hasPadding">
				<h4>Repackage</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><?php echo $this->Transaction->isRepackaged($transaction_detail) ? 'Yes' : 'No'; ?></p>
				</div>
				<div class="editable">
					<fieldset>
						<input name="lower-weight" checked type="checkbox"/>Lower Weight
					</fieldset>
					<button>Save</button>
				</div>
			</div>
			<div class="block hasPadding">
				<h4>Special Instructions</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p><?php echo $this->Transaction->getInstructions($transaction_detail); ?></p>
				</div>
				<div class="editable">
					<textarea name="notes"></textarea>
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Packages -->
<?php echo $this->element('admin/quickship_customers/details/packages'); ?>

<!-- Shipment rates -->
<?php echo $this->element('admin/quickship_customers/details/shipment_rates'); ?>

<!-- Payment options -->
<div class="innersection">
	<h4 class="innersection-title">Payment Options</h4>

	<div class="row">
		<div class="col-md-4">
			<div class="block hasPadding">
				<h4>Charge the shipping cost before package has been received</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p>No</p>
				</div>
				<div class="editable">
					yes/no
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block hasPadding">
				<h4>Charge at a later date if necessary before payment is precessed</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p>Yes</p>
				</div>
				<div class="editable">
					yes/no
					<button>Save</button>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="block hasPadding">
				<h4>Charge for shipping upon receipt</h4>
				<span class="edit-btn">Edit</span>

				<div class="non-editable">
					<p>No</p>
				</div>
				<div class="editable">
					yes/no
					<button>Save</button>
				</div>
			</div>
		</div>
	</div>
</div>
<?php debug($transaction_detail); ?>