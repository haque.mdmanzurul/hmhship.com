
    <div class="container-fluid">
        <h3 class="toptitle">Edit Multiple Status</h3>
        <ul class="orderlist">
            <dl class="accordion noContent">
                <dt class="topaccordion">
                    <h4>QS#2152</h4>
                    <p class="date">Current Status: <span class="status-change">Package Received</span></p>
                </dt>
            </dl>
            <dl class="accordion noContent">
                <dt class="topaccordion">
                    <h4>QS#1142</h4>
                    <p class="date">Current Status: <span class="status-change">Payment Received</span></p>
                </dt>
            </dl>
            <dl class="accordion noContent">
                <dt class="topaccordion">
                    <h4>QS#8455</h4>
                    <p class="date">Current Status: <span class="status-change">Waiting for Package</span></p>
                </dt>
            </dl>
        </ul>
        <div class="text-right">
            <fieldset>
                <select name="multistatus">
                    <option disabled selected>Change status to</option>
                    <option value="waitpack">Waiting for Package</option>
                    <option value="packreceived">Package Received</option>
                    <option value="awaitingpaym">Awaiting Payment</option>
                    <option value="paymreceived">Payment Received</option>
                    <option value="packsent">Package Sent</option>
                </select>
            </fieldset>
            <a href="?section=qscust"  class="innerlink">
                <button id="sabemultstatus" class="ct-hidden">Save</button>
            </a>
        </div>
    </div>
