<div class="container-fluid">
        <h3 class="toptitle">Messages</h3>
        <section id="actionboxes">
            <div class="row">
                <div class="col-md-6">
                    <div class="actionbox" rel="allmsgsaction">
                        <div class="icon-wrap">
                            <span class="icon-message"></span>
                        </div>
                        <h4 class="title">All Messages</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="actionbox" rel="qsmsgsactions">
                        <div class="icon-wrap">
                            <span class="icon-quickship"></span>
                        </div>
                        <h4 class="title">QS customers</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="actionbox" rel="nsmsgsactions">
                        <div class="icon-wrap">
                            <span class="icon-shipment"></span>
                        </div>
                        <h4 class="title">NS customers</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="actionbox" rel="asmsgsactions">
                        <div class="icon-wrap">
                            <span class="icon-assistus"></span>
                        </div>
                        <h4 class="title">AS customers</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="actionbox" rel="ppmsgsactions">
                        <div class="icon-wrap">
                            <span class="icon-message"></span>
                        </div>
                        <h4 class="title">PP customers</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="actionbox" rel="regmsgsactions">
                        <div class="icon-wrap">
                            <span class="icon-users"></span>
                        </div>
                        <h4 class="title">Registered customers</h4>
                        <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                        <h5>Show me more</h5>
                    </div>
                </div>
            </div>
        </section>
    </div>