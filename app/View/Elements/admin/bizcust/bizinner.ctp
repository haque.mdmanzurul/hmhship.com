<?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
<?php 
        if(isset($_GET['tab']))
        {
            $tab = $_GET['tab'];
        }
        else
        {
            $tab = "";
        }
?>
<div class="container-fluid height100">
    <section id="topinner">
        <h3 class="toptitle">
            John Doe - 
            <ul class="tabs">
                <a href="<?php echo $actual_link;?>&tab=showmsgs">
                    <li <?php if($tab == 'showmsgs' || $tab == ''){ echo 'class="active"'; } ?>><span class="icon-chat lefticon"></span><span class="tabtext">Messages</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=showforms">
                    <li <?php if($tab == 'showforms'){ echo 'class="active"'; } ?>><span class="icon-form lefticon"></span><span class="tabtext">Submitted Forms</span></li>
                </a>
            </ul>
        </h3>
    </section>
    <div id="innertabs">
        <?php 

            if($tab == 'showmsgs' || $tab == "")
            {
                echo $this->element('admin/bizcust/showmsgs');
            }
            else
            {
                echo $this->element('admin/bizcust/'.$tab);
            }
        ?>
    </div>
</div>