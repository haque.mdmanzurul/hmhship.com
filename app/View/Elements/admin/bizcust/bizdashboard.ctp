<div class="container-fluid">
    <h3 class="toptitle">Business Customers</h3>
    <section id="actionboxes">
        <div class="row">
            <div class="col-md-6">
                <div class="actionbox" rel="allmsgsaction">
                    <div class="icon-wrap">
                        <span class="icon-message"></span>
                    </div>
                    <h4 class="title">Messages</h4>
                    <h4 class="notification"><span class="primary">16</span> unread messages in <span class="primary">10</span> conversations</h4>
                    <h5>Show me more</h5>
                </div>
            </div>
            <div class="col-md-6">
                <div class="actionbox" rel="qsmsgsactions">
                    <div class="icon-wrap">
                        <span class="icon-quickship"></span>
                    </div>
                    <h4 class="title">Submitted forms</h4>
                    <h4 class="notification"><span class="primary">16</span> new forms from <span class="primary">10</span> business customers</h4>
                    <h5>Show me more</h5>
                </div>
            </div>
        </div>
    </section>
</div>