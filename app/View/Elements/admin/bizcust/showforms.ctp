<ul class="orderlist">
    <dl class="accordion">
        <dt class="topaccordion">
            <h4>Order XXXXX</h4>
            <p class="date">01/04/2015</p>
        </dt>
        <dd>
            <div class="innersection">
                <h4 class="innersection-title">Shipping Options</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Shipping plan</h4>
                            <p>Lorem Ipsum</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Default shipping services</h4>                            
                            <p>Insured</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Specific shipping carrier</h4>
                            <div class="ups"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
                <h4 class="innersection-title">Existing Account</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block">
                            <p>Shipping carrier</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <p>Account Number</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <p>ZIP</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
                <h4 class="innersection-title">Company Information</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Basic Information</h4>
                            <p>Business name</p>
                            <p>John Doe</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Contact Information</h4>                            
                            <p>Phone: 9999999</p>
                            <p>Cellphone: 9999999</p>
                            <p>Fax: 999999</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h4>*Address</h4>
                            <p>1234, Apple St</p>
                            <p>New York, NY 10521</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Website</h4>                            
                            <p>www.example.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </dd>
        
    </dl>
    <dl class="accordion">
        <dt class="topaccordion">
            <h4>Order XXXXX</h4>
            <p class="date">01/04/2015</p>
        </dt>
        <dd>
            <div class="innersection">
                <h4 class="innersection-title">Shipping Options</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Shipping plan</h4>
                            <p>Lorem Ipsum</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Default shipping services</h4>                            
                            <p>Insured</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <h4>Specific shipping carrier</h4>
                            <div class="ups"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
                <h4 class="innersection-title">Existing Account</h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block">
                            <p>Shipping carrier</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <p>Account Number</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block">
                            <p>ZIP</p>
                        </div>
                    </div>
               </div>
            </div>
            <div class="innersection">
                <h4 class="innersection-title">Company Information</h4>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Basic Information</h4>
                            <p>Business name</p>
                            <p>John Doe</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Contact Information</h4>                            
                            <p>Phone: 9999999</p>
                            <p>Cellphone: 9999999</p>
                            <p>Fax: 999999</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block">
                            <h4>*Address</h4>
                            <p>1234, Apple St</p>
                            <p>New York, NY 10521</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block">
                            <h4>Website</h4>                            
                            <p>www.example.com</p>
                        </div>
                    </div>
                </div>
            </div>
        </dd>
    </dl>
</ul>