<div class="container-fluid">
    <h3 class="toptitle">All Transactions</h3>
    <section id="actionboxes">
        <div class="row">
            <div class="col-md-6">
                <div class="actionbox" rel="waitpackaction">
                    <div class="icon-wrap">
                        <span class="icon-waitpack"></span>
                    </div>
                    <h4 class="title">Waiting for Package</h4>
                    <h4 class="notification"><span class="primary">6</span> updated package status</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="actionbox" rel="packreceived">
                    <div class="icon-wrap">
                        <span class="icon-packreceived"></span>
                    </div>
                    <h4 class="title">Package Received</h4>
                    <h4 class="notification"><span class="primary">6</span> updated package status</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="actionbox" rel="awaitingpaymaction">
                    <div class="icon-wrap">
                        <span class="icon-awaitingpaym"></span>
                    </div>
                    <h4 class="title">Awaiting Payment</h4>
                    <h4 class="notification"><span class="primary">6</span> updated package status</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="actionbox" rel="paymreceived">
                    <div class="icon-wrap">
                        <span class="icon-paymreceived"></span>
                    </div>
                    <h4 class="title">Payment Received</h4>
                    <h4 class="notification"><span class="primary">6</span> updated package status</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="actionbox" rel="packsentaction">
                    <div class="icon-wrap">
                        <span class="icon-packsent"></span>
                    </div>
                    <h4 class="title">Package Sent</h4>
                    <h4 class="notification"><span class="primary">6</span> updated package status</h4>
                </div>
            </div>
        </div>
    </section>
</div>