<div class="container-fluid">
        <h3 class="toptitle">
            <?php echo $_GET['type']; ?>#<?php echo $_GET['page'];?>
        </h3>
        <div class="innersection">
           <h4 class="innersection-title">Personal Information</h4> 
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Name</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="name">Lorem Ipsum</span></p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <input name="name" type="text" value="Lorem Ipsum" />
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Contact Information</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="email">email@email.com</span></p>
                            <p><span name="phone">(201)5555555</span></p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <input name="email" type="email" value="email@email.com" />
                            </fieldset>
                            <fieldset>
                                <input name="phone" type="text" value="(201)555555" />
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Address 1</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="st-address1">123, Apple St</span></p>
                            <p><span name="city-address-1">New York - NY</span>, <span name="zip-address-1">10215</span>, <span name="country-address1">USA</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="st-address1" type="text" value="123, Apple St" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="zip-address1" type="text" value="10215" />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="city-address1" type="text" value="City" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="country-address1" type="text" value="country" />
                                    </fieldset>
                                </div>
                            </div>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Address 2</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="st-address2">123, Apple St</span></p>
                            <p><span name="city-address-2">New York - NY</span>, <span name="zip-address-2">10215</span>, <span name="country-address2">USA</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="st-address2" type="text" value="123, Apple St" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="zip-address2" type="text" value="10215" />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="city-address2" type="text" value="City" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="country-address2" type="text" value="country" />
                                    </fieldset>
                                </div>
                            </div>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <h3>Shipping</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Name</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="name">Lorem Ipsum</span></p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <input name="name" type="text" value="Lorem Ipsum" />
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Contact Information</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="email">email@email.com</span></p>
                            <p><span name="phone">(201)5555555</span></p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <input name="email" type="email" value="email@email.com" />
                            </fieldset>
                            <fieldset>
                                <input name="phone" type="text" value="(201)555555" />
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Address 1</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="st-address1">123, Apple St</span></p>
                            <p><span name="city-address-1">New York - NY</span>, <span name="zip-address-1">10215</span>, <span name="country-address1">USA</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="st-address1" type="text" value="123, Apple St" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="zip-address1" type="text" value="10215" />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="city-address1" type="text" value="City" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="country-address1" type="text" value="country" />
                                    </fieldset>
                                </div>
                            </div>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Address 2</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p><span name="st-address2">123, Apple St</span></p>
                            <p><span name="city-address-2">New York - NY</span>, <span name="zip-address-2">10215</span>, <span name="country-address2">USA</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="st-address2" type="text" value="123, Apple St" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="zip-address2" type="text" value="10215" />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset>
                                        <input name="city-address2" type="text" value="City" />
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="country-address2" type="text" value="country" />
                                    </fieldset>
                                </div>
                            </div>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="innersection">
           <h4 class="innersection-title">Additional Options</h4> 
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Additional packages</h4>
                        <span class="edit-btn">Edit</span>
                        <p class="bold">Weight and Value</p>
                        <div class="non-editable">
                            <p><label>1</label> - <span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span> - $<span name="pack1-value">99</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset class="twoinputs">
                                        <input name="pack1-weight" type="text" value="99" />
                                    </fieldset>
                                    <fieldset class="twoinputs">
                                        <select name="pack1-weightmeas">
                                            <option value="lb">lb</option>
                                            <option value="kg">kg</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="pack1-value" type="text" value="99" />
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="non-editable">
                            <p><label>2</label> - <span name="pack2-weight">99</span>lb - $<span name="pack2-value">99</span></p>
                        </div>
                        <div class="editable">
                            <div class="row">
                                <div class="col-md-8">
                                    <fieldset class="twoinputs">
                                        <input name="pack2-weight" type="text" value="99" />
                                    </fieldset>
                                    <fieldset class="twoinputs">
                                        <select name="pack2-weightmeas">
                                            <option value="lb">lb</option>
                                            <option value="kg">kg</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <input name="pack2-value" type="text" value="99" />
                                    </fieldset>
                                </div>
                            </div>
                            <button>Save</button>
                        </div>
                    </div>
                    <div class="block hasPadding">
                        <h4>Need these packages expedited</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>Yes</p>
                        </div>
                        <div class="editable">
                            yes/no
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Insurance added</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>Yes</p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                yes/no
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                    <div class="block hasPadding">
                        <h4>Repackage</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>Yes - Lower Weight</p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <input name="lower-weight" checked type="checkbox" />Lower Weight
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                    <div class="block hasPadding">
                        <h4>Special Instructions</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>None</p>
                        </div>
                        <div class="editable">
                            <textarea name="notes"></textarea>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="innersection">
           <h4 class="innersection-title">Package Details</h4> 

                        <div class="block hasPadding">
                            <h3>Package 1</h3>
                            <span class="edit-btn">Edit</span>
                            <div class="row">
                                <div class="col-md-4">
                                    <h4>Gross Value</h4>
                                    <div class="non-editable">
                                        <p>$<span name="pack1-value">99</span></p>
                                    </div>
                                    <div class="editable">
                                        <input type="text" name="pack1-value" value="99" />
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h4>Estimate Gross Weight</h4>
                                    <div class="non-editable">
                                        <p><span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span></p>
                                    </div>
                                    <div class="editable">
                                        <fieldset class="twoinputs">
                                            <input name="pack1-weight"  type="number" min="0" value="99" />
                                        </fieldset>
                                        <fieldset class="twoinputs">
                                            <select name="pack1-weightmeas">
                                                <option value="lb">lb</option>
                                                <option value="kg">kg</option>
                                            </select>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h4>Gross Dimensions</h4>
                                    <div class="non-editable">
                                        <p><span name="pack1-width">99</span>x<span name="pack1-length">99</span>x<span name="pack1-height">99</span><span class="pack1-dimensionmeas">cm</span></p>
                                    </div>
                                    <div class="editable">
                                        <fieldset class="fourinputs">
                                            <input name="pack1-width"  type="number" min="0" value="99" />
                                        </fieldset>
                                        <fieldset class="fourinputs">
                                            <input name="pack1-length"  type="number" min="0" value="99" />
                                        </fieldset>
                                        <fieldset class="fourinputs">
                                            <input name="pack1-height"  type="number" min="0" value="99" />
                                        </fieldset>
                                        <fieldset class="fourinputs">
                                            <select name="pack1-dimensionmeas">
                                                <option value="in">in</option>
                                                <option value="cm">cm</option>
                                            </select>
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>            
                        </div>
                        <div class="block hasPadding">
                            <h3>Package 2</h3>
                            <span class="edit-btn">Edit</span>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="block hasPadding">
                                        <h4>Gross Value</h4>
                                        <div class="non-editable">
                                            <p>$<span name="pack2-value">99</span></p>
                                        </div>
                                        <div class="editable">
                                            <fieldset>
                                                <input type="text" name="pack2-value" value="99" />
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="block hasPadding">
                                        <h4>Estimate Gross Weight</h4>
                                        <div class="non-editable">
                                            <p><span name="pack2-weight">99</span><span name="pack2-weightmeas">lb</span></p>
                                        </div>
                                        <div class="editable">
                                            <fieldset class="twoinputs">
                                                <input name="pack2-weight"  type="number" min="0" value="99" />
                                            </fieldset>
                                            <fieldset class="twoinputs">
                                                <select name="pack2-weightmeas">
                                                    <option value="lb">lb</option>
                                                    <option value="kg">kg</option>
                                                </select>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="block hasPadding">
                                        <h4>Gross Dimensions</h4>
                                        <div class="non-editable">
                                            <p><span name="pack2-width">99</span>x<span name="pack2-length">99</span>x<span name="pack2-height">99</span><span class="pack2-dimensionmeas">cm</span></p>
                                        </div>
                                        <div class="editable">
                                            <fieldset class="fourinputs">
                                                <input name="pack2-width"  type="number" min="0" value="99" />
                                            </fieldset>
                                            <fieldset class="fourinputs">
                                                <input name="pack2-length"  type="number" min="0" value="99" />
                                            </fieldset>
                                            <fieldset class="fourinputs">
                                                <input name="pack2-height"  type="number" min="0" value="99" />
                                            </fieldset>
                                            <fieldset class="fourinputs">
                                                <select name="pack2-dimensionmeas">
                                                    <option value="in">in</option>
                                                    <option value="cm">cm</option>
                                                </select>
                                            </fieldset>
                                            <button>Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>
                        <hr />
                        <h3>Item Details</h3>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="block hasPadding">
                                    <h4>Type of Content</h4>
                                    <span class="edit-btn">Edit</span>
                                    <div class="non-editable">
                                        <p><span name="typecontent">Commercial sample</span></p>                
                                    </div>
                                    <div class="editable">
                                        <fieldset>
                                            <input name="typecontent" type="text" value="Commercial sample" />
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block hasPadding">
                                    <h4>Weight</h4>
                                    <span class="edit-btn">Edit</span>
                                    <div class="non-editable">
                                        <p><span name="pack2-weight">99</span><span name="pack2-weightmeas">lb</span></p>
                                    </div>
                                    <div class="editable">
                                        <fieldset class="twoinputs">
                                            <input name="pack2-weight"  type="number" min="0" value="99" />
                                        </fieldset>
                                        <fieldset class="twoinputs">
                                            <select name="pack2-weightmeas">
                                                <option value="lb">lb</option>
                                                <option value="kg">kg</option>
                                            </select>
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block hasPadding">
                                    <h4>Quantity</h4>
                                    <span class="edit-btn">Edit</span>
                                    <div class="non-editable">
                                        <p><span name="quantity">2</span></p>
                                    </div>
                                    <div class="editable">
                                        <fieldset>
                                            <input name="quantity" type="number" min="1" value="2" />
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="block hasPadding">
                                    <h4>Country of origin</h4>
                                    <span class="edit-btn">Edit</span>
                                    <div class="non-editable">
                                        <p><span name="country">United States</span></p>                
                                    </div>
                                    <div class="editable">
                                        <fieldset>
                                            <input name="country" type="text" value="United States" />
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block hasPadding">
                                    <h4>Price</h4>
                                    <span class="edit-btn">Edit</span>
                                    <div class="non-editable">
                                        <p>$<span name="value">99.99</span></p>
                                    </div>
                                    <div class="editable">
                                        <fieldset>
                                            <input name="value" type="number" min="1" value="99.99" />
                                        </fieldset>
                                        <button>Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
        </div>
        <div class="innersection">
           <h4 class="innersection-title">Shipment Rates</h4> 
            <div class="row">
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Package 1</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>USPS Priority Mail International</p>
                            <p>$99.99</p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <select name="service">
                                    <option>USPS Priority Mail International</option>
                                </select>
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="block hasPadding">
                        <h4>Package 2</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>USPS Priority Mail International</p>
                            <p>$99.99</p>
                        </div>
                        <div class="editable">
                            <fieldset>
                                <select name="service">
                                    <option>USPS Priority Mail International</option>
                                </select>
                            </fieldset>
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="innersection">
           <h4 class="innersection-title">Payment Options</h4> 
            <div class="row">
                <div class="col-md-4">
                    <div class="block hasPadding">
                        <h4>Charge the shipping cost before package has been received</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>No</p>
                        </div>
                        <div class="editable">
                            yes/no
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block hasPadding">
                        <h4>Charge at a later date if necessary before payment is precessed</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>Yes</p>
                        </div>
                        <div class="editable">
                            yes/no
                            <button>Save</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="block hasPadding">
                        <h4>Charge for shipping upon receipt</h4>
                        <span class="edit-btn">Edit</span>
                        <div class="non-editable">
                            <p>No</p>
                        </div>
                        <div class="editable">
                            yes/no
                            <button>Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>