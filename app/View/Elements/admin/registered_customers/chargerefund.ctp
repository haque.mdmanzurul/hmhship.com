<?php if($_GET['type'] == 'transaction')
{
    ?>
    <div class="block">
        <h4><span class="icon-checked lefticon primary"></span>Authorized charge before package has been received</h4>
    </div>
    <div class="row">
        <div class="col-md-6">
            <h3>Charge</h3>
            <fieldset>
                <input type="text" placeholder="Value $x.xx" />
            </fieldset>
            <fieldset>
                <button>Charge</button>
            </fieldset>
        </div>
        <div class="col-md-6">
            <h3>Refund</h3>
            <fieldset>
                <input type="text" placeholder="Value $x.xx" />
            </fieldset>
            <div class="refund-info block">
                <p>$30.00 will be refunded</p>
                <p>$38.68 in available balance</p>
                <p class="primary">$8.68 remaining in your account</p>
            </div>
            <fieldset>
                <button>Refund</button>
            </fieldset>
        </div>
    </div>
<?php } else { ?>
    <ul class="orderlist">
        <dl class="accordion">
            <dt class="topaccordion">
            <h4>Order NS#11245</h4>
            <p class="date"><span class="icon-checked lefticon primary"></span>Authorized charge before package has been received</p>
            </dt>
            <dd>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Charge</h3>
                        <fieldset>
                            <input type="text" placeholder="Value $x.xx" />
                        </fieldset>
                        <fieldset>
                            <button>Charge</button>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <h3>Refund</h3>
                        <fieldset>
                            <input type="text" name="refund-value" placeholder="Value $x.xx" />
                        </fieldset>
                        <div class="refund-info block">
                            <p>$<span class="requested">0.00</span> will be refunded</p>
                            <p>$<span class="available">38.68</span> in available balance</p>
                            <p class="primary">$<span class="remaining">38.68</span> will remain in this suite account</p>
                        </div>
                        <fieldset>
                            <button>Refund</button>
                        </fieldset>
                    </div>
                </div>
            </dd>
        </dl>
        <dl class="accordion">
            <dt class="topaccordion">
            <h4>Order NS#11245</h4>
            <p class="date"><span class="icon-checked lefticon primary"></span>Authorized charge before package has been received</p>
            </dt>
            <dd>
                <div class="row">
                    <div class="col-md-6">
                        <h3>Charge</h3>
                        <fieldset>
                            <input type="text" placeholder="Value $x.xx" />
                        </fieldset>
                        <fieldset>
                            <button>Charge</button>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <h3>Refund</h3>
                        <fieldset>
                            <input type="text" name="refund-value" placeholder="Value $x.xx" />
                        </fieldset>
                        <div class="refund-info block">
                            <p>$<span class="requested">0.00</span> will be refunded</p>
                            <p>$<span class="available">38.68</span> in available balance</p>
                            <p class="primary">$<span class="remaining">38.68</span> will remain in this suite account</p>
                        </div>
                        <fieldset>
                            <button>Refund</button>
                        </fieldset>
                    </div>
                </div>
            </dd>
        </dl>
    </ul>
<?php } ?>