<?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
<?php
if(isset($_GET['tab']))
{
    $tab = $_GET['tab'];
}
else
{
    $tab = "";
}
?>
<div class="container-fluid height100">
    <section id="topinner">
        <h3 class="toptitle">
            Suite #<?php echo $_GET['page'];?>
            <ul class="tabs">
                <a href="<?php echo $actual_link;?>&tab=showinfo">
                    <li <?php if($tab == 'showinfo' || $tab == ''){ echo 'class="active"'; } ?>><span class="icon-show lefticon"></span><span class="tabtext">Overall Info</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=chargerefund">
                    <li <?php if($tab == 'chargerefund'){ echo 'class="active"'; } ?>><span class="icon-paymreceived lefticon"></span><span class="tabtext">Charge/Refund</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=printlabel">
                    <li <?php if($tab == 'printlabel'){ echo 'class="active"'; } ?>><span class="icon-print lefticon"></span><span class="tabtext">Print Label</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=printworkorder">
                    <li <?php if($tab == 'printworkorder'){ echo 'class="active"'; } ?>><span class="icon-printwork lefticon"></span><span class="tabtext">Print Work Order</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=assistushopper">
                    <li <?php if($tab == 'assistushopper'){ echo 'class="active"'; } ?>><span class="icon-assistus lefticon"></span><span class="tabtext">AssistUs Shopper</span></li>
                </a>
                <?php if(isset($_GET['user']) && $_GET['user'] == 1)
                {?>
                    <a href="<?php echo $actual_link;?>&tab=changestatus">
                        <li <?php if($tab == 'changestatus'){ echo 'class="active"'; } ?>><span class="icon-status lefticon"></span><span class="tabtext">Change Status</span></li>
                    </a>
                <?php } else { ?>

                    <li class="sub-tab-wrap<?php echo $tab; if($tab == 'allshipment'  ||  $tab == 'newshipment' ){ echo ' active'; } ?>"><span class="icon-shipment lefticon"></span>
                        <a href="<?php echo $actual_link;?>&tab=allshipments">
                            <span class="tabtext">Shipments</span>
                        </a>
                        <div class="sub-tab">
                            <a href="<?php echo $actual_link;?>&tab=newshipment">
                                <span>New Shipment</span>
                            </a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        </h3>
    </section>
    <div id="innertabs">
        <?php if ($tab == 'showinfo' || $tab == "") : ?>
            <?php echo $this->element('admin/registered_customers/showinfo'); ?>
        <?php else: ?>
            <?php echo $this->element('admin/registered_customers/' . $tab); ?>
        <?php endif; ?>
    </div>
</div>