<ul class="orderlist">
    <dl class="accordion">
        <dt class="topaccordion">
            <h4>Order XXXXX</h4>
            <p class="date">$318.95</p>
        </dt>
        <dd>
            <div class="innersection">
               <h4 class="innersection-title">End-Shipping Information</h4> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Name</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="name">Lorem Ipsum</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="name" type="text" value="Lorem Ipsum" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Contact Information</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="email">email@email.com</span></p>
                                <p><span name="phone">(201)5555555</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="email" type="email" value="email@email.com" />
                                </fieldset>
                                <fieldset>
                                    <input name="phone" type="text" value="(201)555555" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Address 1</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="st-address1">123, Apple St</span></p>
                                <p><span name="city-address-1">New York - NY</span>, <span name="zip-address-1">10215</span>, <span name="country-address1">USA</span></p>
                            </div>
                            <div class="editable">
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset>
                                            <input name="st-address1" type="text" value="123, Apple St" />
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset>
                                            <input name="zip-address1" type="text" value="10215" />
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset>
                                            <input name="city-address1" type="text" value="City" />
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset>
                                            <input name="country-address1" type="text" value="country" />
                                        </fieldset>
                                    </div>
                                </div>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Address 2</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="st-address2">123, Apple St</span></p>
                                <p><span name="city-address-2">New York - NY</span>, <span name="zip-address-2">10215</span>, <span name="country-address2">USA</span></p>
                            </div>
                            <div class="editable">
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset>
                                            <input name="st-address2" type="text" value="123, Apple St" />
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset>
                                            <input name="zip-address2" type="text" value="10215" />
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8">
                                        <fieldset>
                                            <input name="city-address2" type="text" value="City" />
                                        </fieldset>
                                    </div>
                                    <div class="col-md-4">
                                        <fieldset>
                                            <input name="country-address2" type="text" value="country" />
                                        </fieldset>
                                    </div>
                                </div>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Ship Right Away?</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="shipnow">Yes</span></p>
                            </div>
                            <div class="editable">
                                yes/no
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Package Dimensions</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="width">99</span>x<span name="length">99</span>x<span name="height">99</span><span class="dimensionmeas">cm</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="fourinputs">
                                    <input name="width"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="length"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="height"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <select name="dimensionmeas">
                                        <option value="in">in</option>
                                        <option value="cm">cm</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
               <h4 class="innersection-title">Additional Options</h4> 
                <div class="row">
                    <div class="col-md-4"> 
                        <div class="block hasPadding">
                            <h4>Gross Value</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>$<span name="pack1-value">99</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input type="text" name="pack1-value" value="99" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Estimate Gross Weight</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="twoinputs">
                                    <input name="pack1-weight"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="twoinputs">
                                    <select name="pack1-weightmeas">
                                        <option value="lb">lb</option>
                                        <option value="kg">kg</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Gross Dimensions</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="pack1-width">99</span>x<span name="pack1-length">99</span>x<span name="pack1-height">99</span><span class="pack1-dimensionmeas">cm</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="fourinputs">
                                    <input name="pack1-width"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="pack1-length"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="pack1-height"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <select name="pack1-dimensionmeas">
                                        <option value="in">in</option>
                                        <option value="cm">cm</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Customer Option</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>
                                    <span name="custopt">Consolidate</span>
                                </p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input type="checkbox" value="Consolidate" name="custopt" checked />Consolidate
                                    <input type="checkbox" value="Consolidate" name="custopt" />Opt 2
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Quantity</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="qty">2</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input  name="qty" value="2" type="number" min="0" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="block hasPadding">
                    <h4>Special Instructions</h4>
                    <span class="edit-btn">Edit</span>
                    <div class="non-editable">
                        <p><span name="specialinst">Lorem ipsum dolor sit amet, cibo affert habemus vix at, eius ullum lucilius nam ea. At nec alii saepe. Ei pri putent labitur labores, in sed eripuit copiosae consequuntur.</span></p>
                    </div>
                    <div class="editable">
                        <fieldset>
                            <textarea name="specialinst">Lorem ipsum dolor sit amet, cibo affert habemus vix at, eius ullum lucilius nam ea. At nec alii saepe. Ei pri putent labitur labores, in sed eripuit copiosae consequuntur.</textarea>
                        </fieldset>
                        <button>Save</button>
                    </div>
                </div>
            </div>
            <div class="innersection">
               <h4 class="innersection-title">Package Details</h4> 

                <div class="block hasPadding">
                    <h3>Package 1</h3>
                    <span class="edit-btn">Edit</span>
                    <div class="row">
                        <div class="col-md-4">
                            <h4>Gross Value</h4>
                            <div class="non-editable">
                                <p>$<span name="pack1-value">99</span></p>
                            </div>
                            <div class="editable">
                                <input type="text" name="pack1-value" value="99" />
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h4>Estimate Gross Weight</h4>
                            <div class="non-editable">
                                <p><span name="pack1-weight">99</span><span name="pack1-weightmeas">lb</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="twoinputs">
                                    <input name="pack1-weight"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="twoinputs">
                                    <select name="pack1-weightmeas">
                                        <option value="lb">lb</option>
                                        <option value="kg">kg</option>
                                    </select>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h4>Gross Dimensions</h4>
                            <div class="non-editable">
                                <p><span name="pack1-width">99</span>x<span name="pack1-length">99</span>x<span name="pack1-height">99</span><span class="pack1-dimensionmeas">cm</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="fourinputs">
                                    <input name="pack1-width"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="pack1-length"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <input name="pack1-height"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="fourinputs">
                                    <select name="pack1-dimensionmeas">
                                        <option value="in">in</option>
                                        <option value="cm">cm</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>            
                </div>
                <div class="block hasPadding">
                    <h3>Package 2</h3>
                    <span class="edit-btn">Edit</span>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="block hasPadding">
                                <h4>Gross Value</h4>
                                <div class="non-editable">
                                    <p>$<span name="pack2-value">99</span></p>
                                </div>
                                <div class="editable">
                                    <fieldset>
                                        <input type="text" name="pack2-value" value="99" />
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block hasPadding">
                                <h4>Estimate Gross Weight</h4>
                                <div class="non-editable">
                                    <p><span name="pack2-weight">99</span><span name="pack2-weightmeas">lb</span></p>
                                </div>
                                <div class="editable">
                                    <fieldset class="twoinputs">
                                        <input name="pack2-weight"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="twoinputs">
                                        <select name="pack2-weightmeas">
                                            <option value="lb">lb</option>
                                            <option value="kg">kg</option>
                                        </select>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="block hasPadding">
                                <h4>Gross Dimensions</h4>
                                <div class="non-editable">
                                    <p><span name="pack2-width">99</span>x<span name="pack2-length">99</span>x<span name="pack2-height">99</span><span class="pack2-dimensionmeas">cm</span></p>
                                </div>
                                <div class="editable">
                                    <fieldset class="fourinputs">
                                        <input name="pack2-width"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <input name="pack2-length"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <input name="pack2-height"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <select name="pack2-dimensionmeas">
                                            <option value="in">in</option>
                                            <option value="cm">cm</option>
                                        </select>
                                    </fieldset>
                                    <button>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <hr />
                <h3>Item Details</h3>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Type of Content</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="typecontent">Commercial sample</span></p>                
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="typecontent" type="text" value="Commercial sample" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Weight</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="pack2-weight">99</span><span name="pack2-weightmeas">lb</span></p>
                            </div>
                            <div class="editable">
                                <fieldset class="twoinputs">
                                    <input name="pack2-weight"  type="number" min="0" value="99" />
                                </fieldset>
                                <fieldset class="twoinputs">
                                    <select name="pack2-weightmeas">
                                        <option value="lb">lb</option>
                                        <option value="kg">kg</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Quantity</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="quantity">2</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="quantity" type="number" min="1" value="2" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Country of origin</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="country">United States</span></p>                
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="country" type="text" value="United States" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Price</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>$<span name="value">99.99</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input name="value" type="number" min="1" value="99.99" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
               <h4 class="innersection-title">Payment Options</h4> 
                <div class="row">
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Charge the shipping cost before package has been received</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>No</p>
                            </div>
                            <div class="editable">
                                yes/no
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Charge at a later date if necessary before payment is precessed</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>Yes</p>
                            </div>
                            <div class="editable">
                                yes/no
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="block hasPadding">
                            <h4>Charge for shipping upon receipt</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>No</p>
                            </div>
                            <div class="editable">
                                yes/no
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
               <h4 class="innersection-title">Shipment Details</h4> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Package 1</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>USPS Priority Mail International</p>
                                <p>$99.99</p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <select name="service">
                                        <option>USPS Priority Mail International</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Package 2</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p>USPS Priority Mail International</p>
                                <p>$99.99</p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <select name="service">
                                        <option>USPS Priority Mail International</option>
                                    </select>
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
               <h4 class="innersection-title">US Address</h4> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="block hasPadding">
                            <h4>Suite Number</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="suite">854</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input type="number" name="suite" min="1" value="854" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innersection">
                <h4 class="innersection-title">Edit Shipment Status</h4>
                <div class="row">
                    <div class="col-lg-9 col-lg-push-3">
                        <div class="status-wrap">
                            <div class="status checked">
                                <div class="statusicon-wrap">
                                    <div class="icon-waitpack"></div>
                                </div>
                                <div class="hidden-mobile">
                                    <span class="statusname">Waiting for<br>Package</span>
                                    <span class="statusdate">01/01/2015</span>
                                </div>
                            </div>
                            <div class="status checked">
                                <div class="statusicon-wrap">
                                    <div class="icon-packreceived"></div>
                                </div>
                                <div class="hidden-mobile">
                                    <span class="statusname">Package<br>Received</span>
                                    <span class="statusdate">01/01/2015</span>
                                </div>
                            </div>
                            <div class="status checked  active">
                                <div class="statusicon-wrap">
                                    <div class="icon-awaitingpaym"></div>
                                </div>
                                <div class="hidden-mobile">
                                    <span class="statusname">Awaiting<br>Payment</span>
                                    <span class="statusdate">01/01/2015</span>
                                </div>
                            </div>
                            <div class="status">
                                <div class="statusicon-wrap">
                                    <div class="icon-paymreceived"></div>
                                </div>
                                <div class="hidden-mobile">
                                    <span class="statusname">Payment<br>Received</span>
                                    <span class="statusdate">01/01/2015</span>
                                </div>
                            </div>
                            <div class="status">
                                <div class="statusicon-wrap">
                                    <div class="icon-packsent"></div>
                                </div>
                                <div class="hidden-mobile">
                                    <span class="statusname">Package<br>Sent</span>
                                    <span class="statusdate">01/01/2015</span>
                                </div>
                            </div>
                        </div>
                        <div class="visible-mobile">Payment Received</div>
                        <button id="editstatus">Edit status</button>
                    </div>
                    <div class="col-lg-3 col-lg-pull-9">
                        <div class="block hasPadding">
                            <h4>Tracking Code</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="tracking">845878484</span></p>
                            </div>
                            <div class="editable">
                                <fieldset>
                                    <input type="number" name="tracking" min="1" value="845878484" />
                                </fieldset>
                                <button>Save</button>
                            </div>
                        </div>
                        <div class="block hasPadding">
                            <h4>Dimensions & Weight</h4>
                            <span class="edit-btn">Edit</span>
                            <div class="non-editable">
                                <p><span name="width">99</span>x<span name="height">99</span>x<span name="length">99</span><span name="dimensionmeas">in</span></p>
                                <p><span name="weight">99</span><span name="weightmeas">lb</span></p>
                            </div>
                            <div class="editable">
                                <div class="block">
                                    <fieldset class="fourinputs">
                                        <input name="width"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <input name="length"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <input name="height"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="fourinputs">
                                        <select name="dimensionmeas">
                                            <option value="in">in</option>
                                            <option value="cm">cm</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <div class="block">
                                    <fieldset class="twoinputs">
                                        <input name="weight"  type="number" min="0" value="99" />
                                    </fieldset>
                                    <fieldset class="twoinputs">
                                        <select name="weightmeas">
                                            <option value="lb">lb</option>
                                            <option value="kg">kg</option>
                                        </select>
                                    </fieldset>
                                </div>
                                <button>Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </dd>
    </dl>
</ul>