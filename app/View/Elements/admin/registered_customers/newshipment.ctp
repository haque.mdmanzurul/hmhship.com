
<div class="row">

    <div class="col-md-3">
        <h4>Name *</h4>
    </div>
    <div class="col-md-9">
        <div class="fields-col">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <input name="firstname" type="text" placeholder="First Name" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <input name="lastname" type="text" placeholder="Last Name" />
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <h4>Address *</h4>
    </div>
    <div class="col-md-9">
        <div class="fields-col">
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <input name="address" type="text" placeholder="Address" />
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <input name="suite" type="number" min="1" placeholder="Suite" />
                    </fieldset>
                </div>
                <div class="col-md-3">
                    <fieldset>
                        <input name="zip" type="text" placeholder="Zip Code" />
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <fieldset>
                        <input name="city"  type="text" placeholder="City" />
                    </fieldset>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <input name="country" type="text" placeholder="Country" />
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-3">
        <h4>Packages Information</h4>
    </div>
    <div class="col-md-9">
        <div class="fields-col">
            <div class="pack-wrap">
                <p>Package <span class="packnum">1</span></p>
                <fieldset class="fourinputs">
                    <input name="width"  type="number" min="0" value="99" />
                </fieldset>
                <fieldset class="fourinputs">
                    <input name="length"  type="number" min="0" value="99" />
                </fieldset>
                <fieldset class="fourinputs">
                    <input name="height"  type="number" min="0" value="99" />
                </fieldset>
                <fieldset class="fourinputs">
                    <select name="dimensionmeas">
                        <option value="in">in</option>
                        <option value="cm">cm</option>
                    </select>
                </fieldset>
                <div class="row">
                    <div class="col-md-6">                
                        <fieldset class="twoinputs">
                            <input name="weight" type="text" placeholder="Weight" />
                        </fieldset>
                        <fieldset class="twoinputs">
                            <select name="weightmeas">
                                <option value="lb">lb</option>
                                <option value="kg">kg</option>
                            </select>
                        </fieldset>
                    </div>
                    <div class="col-md-6">
                        <fieldset>
                            <input name="tracking" type="text" placeholder="Tracking" />
                        </fieldset>
                    </div>
                </div>
            </div>
            <button id="addpack">Add Package</button>
        </div>
    </div>
</div>