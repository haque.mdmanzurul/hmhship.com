<div class="container-fluid">
    <h3 class="toptitle">Registered Customers</h3>
    <section id="actionboxes">
        <div class="row">
            <div class="col-md-6">
                <div class="actionbox" rel="nsaction">
                    <div class="icon-wrap">
                        <span class="icon-shipment"></span>
                    </div>
                    <h4 class="title">New Shipment</h4>
                    <h4 class="notification"><span class="primary">6</span> new notifications</h4>
                </div>
            </div>
            <div class="col-md-6">
                <div class="actionbox" rel="packreceived">
                    <div class="icon-wrap">
                        <span class="icon-packreceived"></span>
                    </div>
                    <h4 class="title">AssistUs Shopper</h4>
                    <h4 class="notification"><span class="primary">6</span> new notifications</h4>
                </div>
            </div>
        </div>
    </section>
</div>