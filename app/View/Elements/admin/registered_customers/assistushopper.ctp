<ul class="orderlist">
    <dl class="accordion">
        <dt class="topaccordion">
            <h4>Order XXXXX</h4>
            <p class="date">$318.95</p>
        </dt>
        <dd>
            <div class="innersection">
                <h3>Site: www.amazon.com</h3>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Item number</h4>
                        </div>
                    </div>
                            
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Description</h4>                            
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <h4>Qty</h4>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <h4>Price</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Tax</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Total</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <p>54215246</p>                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>Laptop</p>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <div>1</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <p>$999.99 *extended price $99.99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$9,99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$108.98</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <p>54345435</p>                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>Watch</p>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <div>2</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <p>$99.99 *extended price $99.99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$9,99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$209.97</p>
                        </div>
                    </div>
                </div>
            </div>
        </dd>
        
    </dl>
    <dl class="accordion">
        <dt class="topaccordion">
            <h4>Order XXXXX</h4>
            <p class="date">$318.95</p>
        </dt>
        <dd>
            <div class="innersection">
                <h3>Site: www.amazon.com</h3>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Item number</h4>
                        </div>
                    </div>
                            
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Description</h4>                            
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <h4>Qty</h4>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <h4>Price</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Tax</h4>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <h4>Total</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <p>54215246</p>                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>Laptop</p>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <div>1</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <p>$999.99 *extended price $99.99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$9,99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$108.98</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <div class="block">
                            <p>54345435</p>                            
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>Watch</p>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="block">
                            <div>2</div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="block">
                            <p>$99.99 *extended price $99.99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$9,99</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="block">
                            <p>$209.97</p>
                        </div>
                    </div>
                </div>
            </div>
        </dd>
    </dl>
</ul>