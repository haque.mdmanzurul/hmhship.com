
<div class="innersection">
    <h4 class="innersection-title">Personal Information</h4>
    <div class="row">
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Name</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="name">Lorem Ipsum</span></p>
                </div>
                <div class="editable">
                    <fieldset>
                        <input name="name" type="text" value="Lorem Ipsum" />
                    </fieldset>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Contact Information</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="email">email@email.com</span></p>
                    <p><span name="phone">(201)5555555</span></p>
                </div>
                <div class="editable">
                    <fieldset>
                        <input name="email" type="email" value="email@email.com" />
                    </fieldset>
                    <fieldset>
                        <input name="phone" type="text" value="(201)555555" />
                    </fieldset>
                    <button>Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Address 1</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="st-address1">123, Apple St</span></p>
                    <p><span name="city-address-1">New York - NY</span>, <span name="zip-address-1">10215</span>, <span name="country-address1">USA</span></p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="st-address1" type="text" value="123, Apple St" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="zip-address1" type="text" value="10215" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="city-address1" type="text" value="City" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="country-address1" type="text" value="country" />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Address 2</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="st-address2">123, Apple St</span></p>
                    <p><span name="city-address-2">New York - NY</span>, <span name="zip-address-2">10215</span>, <span name="country-address2">USA</span></p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="st-address2" type="text" value="123, Apple St" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="zip-address2" type="text" value="10215" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="city-address2" type="text" value="City" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="country-address2" type="text" value="country" />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
    </div>
    <hr />
    <h3>Shipping</h3>
    <div class="row">
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Name</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="name">Lorem Ipsum</span></p>
                </div>
                <div class="editable">
                    <fieldset>
                        <input name="name" type="text" value="Lorem Ipsum" />
                    </fieldset>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Contact Information</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="email">email@email.com</span></p>
                    <p><span name="phone">(201)5555555</span></p>
                </div>
                <div class="editable">
                    <fieldset>
                        <input name="email" type="email" value="email@email.com" />
                    </fieldset>
                    <fieldset>
                        <input name="phone" type="text" value="(201)555555" />
                    </fieldset>
                    <button>Save</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Address 1</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="st-address1">123, Apple St</span></p>
                    <p><span name="city-address-1">New York - NY</span>, <span name="zip-address-1">10215</span>, <span name="country-address1">USA</span></p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="st-address1" type="text" value="123, Apple St" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="zip-address1" type="text" value="10215" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="city-address1" type="text" value="City" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="country-address1" type="text" value="country" />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block hasPadding">
                <h4>Address 2</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span name="st-address2">123, Apple St</span></p>
                    <p><span name="city-address-2">New York - NY</span>, <span name="zip-address-2">10215</span>, <span name="country-address2">USA</span></p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="st-address2" type="text" value="123, Apple St" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="zip-address2" type="text" value="10215" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <fieldset>
                                <input name="city-address2" type="text" value="City" />
                            </fieldset>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="country-address2" type="text" value="country" />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="innersection">
    <h4 class="innersection-title">PreferredPacks</h4>
    <div class="row">
        <div class="col-md-4">
            <div class="block hasPadding">
                <h4>3 Packages</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span class="primary" name="3pack">2</span> available from <span class="primary" name="3pack-purch">3</span> purchased</p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-4">
                            <fieldset>
                                <input name="3pack" type="number" value="2" min='0' />
                            </fieldset>
                        </div>
                        <div class="col-md-4 text-center">
                            <span> from </span>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="3pack-purch" type="number" value="3" min='0' />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block hasPadding">
                <h4>5 Packages</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span class="primary" name="5pack">2</span> available from <span class="primary" name="5pack-purch">3</span> purchased</p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-4">
                            <fieldset>
                                <input name="5pack" type="number" value="2" min='0' />
                            </fieldset>
                        </div>
                        <div class="col-md-4 text-center">
                            <span> from </span>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="5pack-purch" type="number" value="3" min='0' />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block hasPadding">
                <h4>10 Packages</h4>
                <span class="edit-btn">Edit</span>
                <div class="non-editable">
                    <p><span class="primary" name="10pack">2</span> available from <span class="primary" name="10pack-purch">3</span> purchased</p>
                </div>
                <div class="editable">
                    <div class="row">
                        <div class="col-md-4">
                            <fieldset>
                                <input name="10pack" type="number" value="2" min='0' />
                            </fieldset>
                        </div>
                        <div class="col-md-4 text-center">
                            <span> from </span>
                        </div>
                        <div class="col-md-4">
                            <fieldset>
                                <input name="10pack-purch" type="number" value="3" min='0' />
                            </fieldset>
                        </div>
                    </div>
                    <button>Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
