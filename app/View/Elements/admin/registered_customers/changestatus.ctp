<h3>Current Shipment Status</h3>
<h4 class="sidenote">Select a status to update</h4>
<hr class="gray" />
<div class="row">
    <div class="col-lg-9 col-lg-push-3">
        <div class="status-wrap">
            <div class="status checked">
                <div class="statusicon-wrap">
                    <div class="icon-waitpack"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Waiting for<br>Package</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status checked">
                <div class="statusicon-wrap">
                    <div class="icon-packreceived"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Package<br>Received</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status checked  active">
                <div class="statusicon-wrap">
                    <div class="icon-awaitingpaym"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Awaiting<br>Payment</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status">
                <div class="statusicon-wrap">
                    <div class="icon-paymreceived"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Payment<br>Received</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
            <div class="status">
                <div class="statusicon-wrap">
                    <div class="icon-packsent"></div>
                </div>
                <div class="hidden-mobile">
                    <span class="statusname">Package<br>Sent</span>
                    <span class="statusdate">01/01/2015</span>
                </div>
            </div>
        </div>
        <div class="visible-mobile">Payment Received</div>
        <button id="editstatus">Edit status</button>
    </div>
    <div class="col-lg-3 col-lg-pull-9">
        
        <h4>Dimensions & Weight</h4>
        <p>99x99x99in</p>
        <p>99lb</p>
        <button>Edit info</button>
    </div>
</div>