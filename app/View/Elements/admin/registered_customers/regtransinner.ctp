<?php $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>
<?php
        if(isset($_GET['tab']))
        {
            $tab = $_GET['tab'];
        }
        else
        {
            $tab = "";
        }
?>
<div class="container-fluid height100">
    <section id="topinner">
        <h3 class="toptitle">
            NS#<?php echo $_GET['page'];?>
            <ul class="tabs">
                <a href="<?php echo $actual_link;?>&tab=showinfo">
                    <li <?php if($tab == 'showinfo' || $tab == ''){ echo 'class="active"'; } ?>><span class="icon-show lefticon"></span><span class="tabtext">Overall Info</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=chargerefund">
                    <li <?php if($tab == 'chargerefund'){ echo 'class="active"'; } ?>><span class="icon-paymreceived lefticon"></span><span class="tabtext">Charge/Refund</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=printlabel">
                    <li <?php if($tab == 'printlabel'){ echo 'class="active"'; } ?>><span class="icon-print lefticon"></span><span class="tabtext">Print Label</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=printworkorder">
                    <li <?php if($tab == 'printworkorder'){ echo 'class="active"'; } ?>><span class="icon-printworkorder lefticon"></span><span class="tabtext">Print Work Order</span></li>
                </a>
                <a href="<?php echo $actual_link;?>&tab=changestatus">
                    <li <?php if($tab == 'changestatus'){ echo 'class="active"'; } ?>><span class="icon-status lefticon"></span><span class="tabtext">Change Status</span></li>
                </a>
            </ul>
        </h3>
    </section>
    <div id="innertabs">
        <?php

            if($tab == 'showinfo' || $tab == "")
            {
                include 'showinfo.php';
            }
            else
            {
                include $tab.'.php';
            }
        ?>
    </div>
</div>
