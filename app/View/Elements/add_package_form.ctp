<div class="box-item">
    <div class="row">
        <h3 class="hs-package-title">Package 1</h3>
    </div>
    <div class="row hs-packages-dimensions">
		<div class="col col-md-2">
			<input type="number" min="1" name="packages[1][weight]" class="s3_copyWeight form-control required" placeholder="Weight *" value="">
			<span class="msg-error" style="display: none;"></span>
		</div>
		<div class="col col-md-2">
			<select name="packages[1][weight_unit]" class="s3_copyUnit form-control required">
				<option value="lb">lb</option>
				<option value="kg">kg</option>
			</select>
			<span class="msg-error" style="display: none;"></span>
		</div>
		<div class="col col-md-2">
			<input type="number" min="1" name="packages[1][width]" class="form-control required" placeholder="Width *" value="">
			<span class="msg-error" style="display: none;"></span>
		</div>
		<div class="col col-md-2">
			<input type="number" min="1" name="packages[1][height]" class="form-control required" placeholder="Height *" value="">
			<span class="msg-error" style="display: none;"></span>
		</div>
		<div class="col col-md-2">
			<input type="number" min="1" name="packages[1][length]" class="form-control required" placeholder="Length *" value="">
			<span class="msg-error" style="display: none;"></span>
		</div>
		<div class="col col-md-2">
			<select name="packages[1][size_unit]" class="form-control required">
				<option value="in">in</option>
				<option value="cm">cm</option>
			</select>
			<span class="msg-error" style="display: none;"></span>
		</div>
	</div>
	<div class="row">
		<div class="col col-md-6">
            <select name="packages[1][tracking_code]" data-placeholder="Tracking code" class="form-control hs-tracking-codes" multiple="multiple"></select>
		</div>
		<div class="col col-md-6">
			<input type="text" name="packages[1][carrier]" class="form-control" placeholder="Carrier">
		</div>
	</div>
	<div class="row action" style="display:none !important;">
		<div class="col col-md-12">
			<button type="button" class="btn btn-danger s3_delPkgDetail">Delete</button>
		</div>
	</div>
</div>
