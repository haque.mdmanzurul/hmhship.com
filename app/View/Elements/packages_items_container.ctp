<div id="pkg1" class="tab-pane active">
	<div class="s3_boxItemDetail">
		<div class="s3_itemDetail">
			<div class="row">
				<div class="col col-md-5">
					<input type="text" name="packages[1][items][1][description]" class="form-control required" placeholder="Item description *">
					<span class="msg-error"></span>
				</div>
				<div class="col col-md-2">
				    <input type="number" min="1" name="packages[1][items][1][quantity]" class="form-control required" placeholder="Qty" value="">
					<span class="msg-error"></span>
				</div>
				<div class="col col-md-3">
					<input name="packages[1][items][1][weight]" type="number"  min="1" class="form-control required" placeholder="Weight *">
					<span class="msg-error"></span>
				</div>
				<div class="col col-md-2">
					<select name="packages[1][items][1][weight_unit]" class="form-control required">
						<option value="lb">lb</option>
						<option value="kg">kg</option>
					</select>
					<span class="msg-error"></span>
				</div>
			</div>
			<div class="row">
				<div class="col col-md-5">
					<select name="packages[1][items][1][type_id]" class="form-control required">
						<option selected="selected" value="">-- Type of content * --</option>
						<?php foreach($item_types as $id => $description) { ?>
						<option value="<?php echo $id; ?>"><?php echo $description; ?></option>
						<?php } ?>
					</select>
					<span class="msg-error"></span>
				</div>
				<div class="col col-md-4">
                    <select name="packages[1][items][1][country_id]" id="s1_personalCountry" class="form-control required">
                        <option selected="selected" value="">-- Country of origin * --</option>
                        <?php foreach($countries as $id => $name) { ?>
                        <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                        <?php } ?>
                    </select>
					<span class="msg-error"></span>
				</div>
				<div class="col col-md-3">
					<input type="number"  min="1" name="packages[1][items][1][value]" class="form-control required" placeholder="U$xx.xx *">
					<span class="msg-error"></span>
				</div>
			</div>
			<div class="row action">
				<div class="col col-md-12">
                    <textarea name="packages[1][items][1][info]" class="form-control" rows="3" placeholder="More Info (Optional)"></textarea>
				</div>
			</div>
			<div class="row action">
				<div class="col col-md-12">
					<button type="button" class="btn btn-danger s3_delItemDetail">Delete</button>
				</div>
			</div>
		</div>
	</div>
</div>
