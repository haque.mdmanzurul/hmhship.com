<div class="show-after-load">
<section class="business">
   <div class="container">
   <div class="business-content">
      <div class="top-box-circle">
         <div class="circle"></div>
         <div class="icon">
            <i class="icon-business"></i>
         </div>
      </div>
      <div class="wm-content">
         <div class="text top-box-text">
            <h2>We're sorry to see you go!</h2>
         </div>
         <br/>
         <p class="text-center">You can reactivate your account at anytime by simply relogging into your account.</p>
         <center>
         <a href="/sign-in">
           <button type="button" style="background-color: orange; color: black;" class="btn" name="button">Sign-In</button>
         </a>
       </center>
         <br/>
      </div>
    </div>
    </div>
</section>

</div>
