<div class="show-after-load">
    <section class="talk-to-us">
	<div class="container">
            <div class="mission-content">
                <div class="top-box-circle">
                        <div class="circle"></div>
                        <div class="icon">
                                <i class="icon-createaccount"></i>
                        </div>
                </div>
                <div class="wm-content">




                    <div class="text top-box-text">
                                <h2>Done And Done!</h2>
                    </div>



                    <div class="info-contact">

                        <center>
                          <div style="background-color: #d3f1ed; width:30%; border-radius: 4px; text-align: left; padding-left: 8px; padding-top: 3px;">
                            Success!
                            <p>We've sent an email to the specified email with password reset instructions.</p>
                          </div>
                        </center>
                       <form action="/sign-in" id="UserResetForm" accept-charset="utf-8">
                            <fieldset class="center">
                                <?php if (isset($userid) && $userid > 0) {
                                    $button_text = "Reset Password";
                                ?>
                                <input type="hidden" name="id" value="<?php echo $userid?>"/>
                                <input type="hidden" name="password_hashed" value="<?php echo $password_hashed ?>"/>
                                <p class="text-center">Almost done. Enter your new password, and you’re good to go.</p>
                                <div class="col-md-6">
                                <div class="row">
                                    <center><div>
                                        <input required name="password" placeholder="Password" class="form-control" style="width:300px;" maxlength="50" type="password" id="password">
                                    </div></center>
                                </div>

                                <div class="row">

                                    <center><div >
                                    <input required name="confirm_password" placeholder="Confirm Password" class="form-control" style="width:300px;" maxlength="50" type="password" id="confirm_password">
                                  </div></center>
                                </div>
                              </div>
                                <ul class="col-md-6">
                                  <span>Password Rules:</span>
                                  <li>- Length must be 8 characters or greater</li>
                                  <li>- Must contain at least one letter</li>
                                  <li>- Must contain at least one number</li>
                                </ul>
                                <?php } else {
                                    $button_text = "Reset Password";
                                ?>

                                <div class="row text-center">
                                    <p>If the email does not arrive soon, check your spam folder. It was sent from confirm@hmhship.com</p>
                                    <center><div>

                                  </div></center>
                                </div>
                                <?php } ?>
                            </fieldset>
                        <div class="row top-box-text">

                            <input style="background-color: #ff9e59; border: none;" class="btn-send" type="submit" value="Return To Login">&nbsp&nbsp&nbsp
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
