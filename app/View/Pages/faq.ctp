
<div class="show-after-load">
    <section class="we-do">
    	<div class="container">
			<div class="wm-content">
				

				<div class="text top-box-text">
                    <h1>Frequently Asked Questions</h1>
                </div>
				<dl>
					<dt><h2>Sign Up Questions</h2></dt>
					<dd>
						<dl class="accordion">
							<dt><h4>How can I get a HMHShip Address?</h4></dt>
							<dd>
								<p>There are two way to get a HMHShip US address. If you choose not to sign up for an account you can get a US address by filling out the &lsquo;QuickShip' form. If you would like to sign up for an account with HMHShip, you will automatically get a US address after you sign up. </p>
							</dd>

							<dt><h4>Is my HMHShip address a PO BOX?</h4></dt>
							<dd>
								<p>Many websites do not ship to PO BOX addresses. The reason is that FedEx, UPS and DHL do not deliver parcels to PO BOXES. Our address is not a PO BOX address. We are a regular <a href="/services-rates">office building and warehouse</a> located at 23600 Mercantile Rd. Suite C-119 Beachwood, OH 44122, USA. We are staffed with full time employees from 9.00 AM till 6.00 PM Monday through Friday, therefore there is always someone available to receive incoming parcels. </p>
							</dd>

							<dt><h4>Does HMHShip ship to my country?</h4></dt>
							<dd>
								<p>Our service is available to customers all over the world except Iran, North Korea and Sudan. Please check shipping restrictions for your country (<a href="http://www.fedex.com/us/international/irc/profiles/" target="_new">FedEx</a>, <a target="_new" href="http://pe.usps.gov/text/imm/welcome.htm">USPS</a>, UPS, <a target="_new" href="http://www.dhl-usa.com/en/express/shipping/shipping_advice/terms_conditions.html">DHL</a>).</p>
							</dd>
						</dl>
					</dd>
					<dt><h2>Services & Rates</h2></dt>
					<dd>
						<dl class="accordion">
							<dt><h4>What is the cost to ship a parcel to my country?</h4></dt>
							<dd>

								<p>To receive a free shipping quote, please use our <a href="/pages/shipping-calculator">Shipping calculator</a>. Enter your country, the weight of the parcel, and ZIP codes. You can select how each parcel will be sent. Just log in to <a href="/Users/myaccount">your account </a>and change shipping method. Postage is automatically recalculated.</p>
							</dd>


							<dt><h4>What are the fees for the different services HMHShip offers?</h4></dt>
							<dd>
								<p>For our standard service of parcel-forwarding which is accessed through QuickShip on our homepage or New Package which is accessed through through the user account, the fee is $7.25 plus shipping charges.</p>	
								<p>The handling fee can be reduced by purchasing one of our PreferredPacks. The pricing for our PreferredPacks is as follows:</p>
								<p>PreferredPack - 3 Package $20.65 ($6.88 per parcel)</p>
								<p>PreferredPack - 5 Package $33.70 ($6.74 per parcel)</p>
								<p>PreferredPack - 10 Package $65.25 ($6.53 per parcel)</p>
								<p>For our AssistUS Shopper service, the fee is $4.25 per site. For all other services offered, there is no charge. These services are: Parcel Consolidation (up to three parcels), Storage (up to 90 days). There are no setup or monthly fees for HMHShip.</p>
							</dd>


							<dt><h4>Is it possible to consolidate my parcels into one box?</h4></dt>
							<dd>
								<p>Yes, we can consolidate up to three parcels into one larger box.</p>
							</dd>

							<dt><h4>How is the customs information entered for my parcel?</h4></dt>
							<dd>
								<p>When submitting a new QuickShip form from our home screen or a New Shipment from your account, you will be asked to enter all the information about the items that you are shipping. If you have an account with us and you wish to have your parcel sent to us using your US address without submitting the New Shipment form we will notify you by email when we receive new parcels. In the email please  enter all the information about the items that you are shipping.</p>

								<p>Please note, a correctly filled customs declaration is your duty and responsibility. If you are not sure what is inside the parcel (if the merchant divides one purchase into multiple parcels), we can open it and check the content. Use Special Request submitting a QuickShip form or the New Shipment form. </p>
							</dd>


							<dt><h4>Is the COD option available for my parcel?</h4></dt>
							<dd>
								<p>We can complete assisted purchases for you and forward any parcel to your home address, however, we do not provide COD forwarding.</p>
							</dd>


							<dt><h4>Is it possible to have non-parcels (example: letters, cataloges, etc.) sent to me?</h4></dt>
							<dd>
								<p>Yes, this is not a problem to forward to you.</p>
							</dd>


							<dt><h4>How will HMHShip identify the parcel as mine?</h4></dt>
							<dd>
								<p>As soon as you complete your registration, we will assign a unique reference number. You will use this unique reference number with your name and our address as the shipping address. Once any parcel arrives we will see your name and unique reference number on it. We will notify you by email and you will also see the parcel in your <a href="/Users/myaccount">account if you have one with us.</a></p>
							</dd>


							<dt><h4>Would it be possible to add other names or addresses to my account?</h4></dt>
							<dd>
								<p>Yes, you can. There is no additional cost. Just add them on your <a href="/Users/myaccount">account</a> under User Preferences.</p>
							</dd>

							<dt><h4>Which items cannot be shipped through HMHShip?</h4></dt>
							<dd>
								<p>There are many items that cannot be shipped by any carrier when being shipped. These items include:</p>
								<ul>
									<li><p>Protection, US Department of State and the US Department of Treasury.</p></li>
									<li><p>Flammable items</p></li>
									<li><p>Pornography</p></li>
									<li><p>Explosive material</p></li>
									<li><p>Corrosive materials</p></li>
									<li><p>Human remains</p></li>
									<li><p>Live or dead plants or animals</p></li>
									<li><p>Any Electronic equipment or any equipment that requires ECCN# (Export Control Classification Number). For carrier restrictions you can check the list on the <a href="/pages/shipping-calculator">calculator </a>directly.</p></li>
								</ul>
							</dd>


							<dt><h4>Which shipping methods are available for me to choose from? Would it be possible to use my own shipping account through USPS, Fedex, DHL or UPS shipment? </h4></dt>
							<dd>
								<p>We support only the shipping methods shown in our shipping <a href="/pages/shipping-calculator">calculator</a>. Please enter the weight and the dimensions of the parcel to see how it will be shipped. If you don't see a certain shipping method it is not supported by HMHShip. We currently do not allow billing through a third party shipping carrier.</p>
							</dd>

							<dt><h4>Would it be possible to use another shipping carrier not shown as an option on HMHShip's website?</h4></dt>
							<dd>
								<p>Our low pricing is based on automation and approved shipping methods we currently use. Please check what shipping methods we offer before shipping any parcels to us. We have wide range of shipping methods we offer. </p>
							</dd>
						</dl>

					</dd>
					<dt><h2>Payment options and billing</h2></dt>
					<dd>
						<dl class="accordion">
							<dt><h4>What are all the payment methods available when using HMHShip's services?</h4></dt>
							<dd>

								<p>All transactions are processed through PayPal, this is done for yours and our security. You have the option to either use your credit card or pay using your PayPal funds if you have an account. You can also add funds to your account and use these funds for any of our services. </p>
							</dd>


							<dt><h4>What other fees may there be with HMHShip's services?</h4></dt>
							<dd>
								<p>None, any and all fees that you will ever pay can be found above or in our <a href="/services-rates">What We Do & Rates</a> page.</p>
							</dd>
						</dl>

					</dd>
					<dt><h2>Online Shopping</h2></dt>
					<dd>
						<dl class="accordion">
							<dt><h4>Which telephone number is best to use when placing my order online?</h4></dt>
							<dd>
								<p>Some online stores will ask for a US phone number. Do not place HMHShip's phone number otherwise your order is likely to be cancelled. This can occur when the seller receives several orders with different names and credit cards, but the phone number is the same. Their system may detect this as a risky order, so you will need to place your own phone number. You can get a US phone number with many online services such as <a href="http://www.skype.com/en/features/online-number/">http://www.skype.com/en/features/online-number/</a>, <a href="http://www.spokn.com/">http://www.spokn.com/</a> or <a href="http://www.tollfreeforwarding.com/">http://www.tollfreeforwarding.com/</a></p>
								<p>Or you can install Whistle phone appplication on your smart phone which will allow you to have a US number created for free with the option to choose the area code as well <a href="https://www.whistlephone.com/">https://www.whistlephone.com/</a></p>
							</dd>
						</dl>
					</dd>
					<dt><h2>Security and fraud prevention</h2></dt>
					<dd>
						<dl class="accordion">
							<dt><h4>Is there anything I can do if the website I'm trying to buy from requires a US address with a US debit/credit?</h4></dt>
							<dd>

								<p>Some online stores will ask you to pay with a US debit card. If this is the case you can utilize our AssistUS Shopper service which will allow you to get what you're looking for.</p>
							</dd>

							<dt><h4>I am a merchant who got a chargeback from the credit card company after shipping an order to HMHShip.</h4></dt>
							<dd>
								<p>Store owners, please contact us immediately if you are a victim of credit card fraud. If we find that a stolen credit card was used, we immediately send parcels back to you. We have zero tolerance policy. Fraud accounts are immediately cancelled.</p>
							</dd>
						</dl>
					</dd>
				</dl>
			</div>
		</div>
	</section>
</div>