﻿<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>mike</o:Author>
  <o:LastAuthor>mike</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>1</o:TotalTime>
  <o:Created>2018-02-05T17:06:00Z</o:Created>
  <o:LastSaved>2018-02-05T17:06:00Z</o:LastSaved>
  <o:Pages>11</o:Pages>
  <o:Words>2411</o:Words>
  <o:Characters>13748</o:Characters>
  <o:Lines>114</o:Lines>
  <o:Paragraphs>32</o:Paragraphs>
  <o:CharactersWithSpaces>16127</o:CharactersWithSpaces>
  <o:Version>16.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:PunctuationKerning/>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:EnableOpenTypeKerning/>
   <w:DontFlipMirrorIndents/>
   <w:OverrideTableStyleHps/>
   <w:UseFELayout/>
  </w:Compatibility>
  <w:DoNotOptimizeForBrowser/>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
  DefSemiHidden="false" DefQFormat="false" DefPriority="99"
  LatentStyleCount="375">
  <w:LsdException Locked="false" Priority="0" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index 9"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" Name="toc 9"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="header"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footer"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="index heading"/>
  <w:LsdException Locked="false" Priority="35" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of figures"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="envelope return"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="footnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="line number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="page number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote reference"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="endnote text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="table of authorities"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="macro"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="toa heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Bullet 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Number 5"/>
  <w:LsdException Locked="false" Priority="10" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Closing"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Signature"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="true"
   UnhideWhenUsed="true" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="List Continue 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Message Header"/>
  <w:LsdException Locked="false" Priority="11" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Salutation"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Date"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text First Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Note Heading"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Body Text Indent 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Block Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="FollowedHyperlink"/>
  <w:LsdException Locked="false" Priority="22" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Document Map"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Plain Text"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="E-mail Signature"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Top of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Bottom of Form"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal (Web)"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Acronym"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Address"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Cite"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Code"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Definition"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Keyboard"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Preformatted"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Sample"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Typewriter"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="HTML Variable"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Normal Table"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="annotation subject"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="No List"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Outline List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Simple 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Classic 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Colorful 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Columns 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Grid 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 4"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 5"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 7"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table List 8"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table 3D effects 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Contemporary"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Elegant"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Professional"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Subtle 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 1"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 2"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Web 3"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Balloon Text"/>
  <w:LsdException Locked="false" Priority="39" Name="Table Grid"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Table Theme"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" SemiHidden="true" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" QFormat="true"
   Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" QFormat="true"
   Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" QFormat="true"
   Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" QFormat="true"
   Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" QFormat="true"
   Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" QFormat="true"
   Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" SemiHidden="true"
   UnhideWhenUsed="true" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" SemiHidden="true"
   UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
  <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
  <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
  <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
  <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
  <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
  <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
  <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="Grid Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="Grid Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="Grid Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
  <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
  <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 1"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 1"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 2"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 2"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 3"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 3"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 4"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 4"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 5"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 5"/>
  <w:LsdException Locked="false" Priority="46"
   Name="List Table 1 Light Accent 6"/>
  <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
  <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
  <w:LsdException Locked="false" Priority="51"
   Name="List Table 6 Colorful Accent 6"/>
  <w:LsdException Locked="false" Priority="52"
   Name="List Table 7 Colorful Accent 6"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Mention"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Smart Hyperlink"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Hashtag"/>
  <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
   Name="Unresolved Mention"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"MS Gothic";
	panose-1:2 11 6 9 7 2 5 8 2 4;
	mso-font-alt:"\FF2D\FF33 \30B4\30B7\30C3\30AF";
	mso-font-charset:128;
	mso-generic-font-family:modern;
	mso-font-pitch:fixed;
	mso-font-signature:-536870145 1791491579 134217746 0 131231 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:3 0 0 0 1 0;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:swiss;
	mso-font-pitch:variable;
	mso-font-signature:-536859905 -1073732485 9 0 511 0;}
@font-face
	{font-family:"\@MS Gothic";
	panose-1:2 11 6 9 7 2 5 8 2 4;
	mso-font-charset:128;
	mso-generic-font-family:modern;
	mso-font-pitch:fixed;
	mso-font-signature:-536870145 1791491579 134217746 0 131231 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:.15pt;
	margin-left:7.25pt;
	text-indent:-.5pt;
	line-height:104%;
	mso-pagination:widow-orphan;
	font-size:9.0pt;
	mso-bidi-font-size:11.0pt;
	font-family:"Arial",sans-serif;
	mso-fareast-font-family:Arial;
	color:#666666;}
h1
	{mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 1 Char";
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:7.25pt;
	margin-bottom:.0001pt;
	text-indent:-.5pt;
	line-height:107%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:1;
	font-size:15.0pt;
	mso-bidi-font-size:11.0pt;
	font-family:"Arial",sans-serif;
	mso-fareast-font-family:Arial;
	color:black;
	mso-font-kerning:0pt;
	font-weight:normal;}
h2
	{mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 2 Char";
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:.7pt;
	margin-left:7.25pt;
	text-indent:-.5pt;
	line-height:107%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:2;
	font-size:11.0pt;
	font-family:"Arial",sans-serif;
	mso-fareast-font-family:Arial;
	color:black;
	mso-bidi-font-weight:normal;}
h3
	{mso-style-priority:9;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 3 Char";
	mso-style-next:Normal;
	margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:6.75pt;
	margin-bottom:.0001pt;
	line-height:102%;
	mso-pagination:widow-orphan lines-together;
	page-break-after:avoid;
	mso-outline-level:3;
	font-size:12.0pt;
	mso-bidi-font-size:11.0pt;
	font-family:"Times New Roman",serif;
	mso-fareast-font-family:"Times New Roman";
	color:blue;
	font-weight:normal;
	text-underline:blue;
	text-decoration:underline;
	text-underline:single;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 2";
	mso-ansi-font-size:11.0pt;
	font-family:"Arial",sans-serif;
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:Arial;
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	color:black;
	font-weight:bold;
	mso-bidi-font-weight:normal;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 3";
	mso-ansi-font-size:12.0pt;
	font-family:"Times New Roman",serif;
	mso-ascii-font-family:"Times New Roman";
	mso-fareast-font-family:"Times New Roman";
	mso-hansi-font-family:"Times New Roman";
	mso-bidi-font-family:"Times New Roman";
	color:blue;
	text-underline:blue;
	text-decoration:underline;
	text-underline:single;}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-parent:"";
	mso-style-link:"Heading 1";
	mso-ansi-font-size:15.0pt;
	font-family:"Arial",sans-serif;
	mso-ascii-font-family:Arial;
	mso-fareast-font-family:Arial;
	mso-hansi-font-family:Arial;
	mso-bidi-font-family:Arial;
	color:black;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
.MsoPapDefault
	{mso-style-type:export-only;
	margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:72.65pt 74.25pt 73.85pt 65.25pt;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin-top:0in;
	mso-para-margin-right:0in;
	mso-para-margin-bottom:8.0pt;
	mso-para-margin-left:0in;
	line-height:107%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-bidi-font-family:"Times New Roman";
	mso-bidi-theme-font:minor-bidi;}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="1026"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->

<div class=WordSection1>

<p class=MsoNormal style='margin-left:6.5pt;line-height:107%'><b
style='mso-bidi-font-weight:normal'><span style='color:#CC0000'>Standard <span
class=SpellE>HMHShip</span> Prohibited Commodities</span></b><span
style='font-size:12.0pt;mso-bidi-font-size:11.0pt;line-height:107%;font-family:
"Times New Roman",serif;mso-fareast-font-family:"Times New Roman";color:black'>
</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.85pt;
margin-left:6.5pt'>The following commodities are <span style='font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span><i style='mso-bidi-font-style:
normal'>not </i><span style='font-size:9.5pt;mso-bidi-font-size:11.0pt;
line-height:104%;font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span>acceptable
for forwarding by <span class=SpellE>HMHShip</span> <span style='font-family:
"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>under any circumstances</i><span
style='font-size:9.5pt;mso-bidi-font-size:11.0pt;line-height:104%;font-family:
"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span>. </p>

<p class=MsoNormal style='margin-left:6.5pt'>This is a result of a policy
decision, following the full consideration of Operational, Legal and Risk
Management implications. </p>

<p class=MsoNormal style='margin-left:6.5pt'>Animals </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques (breakable and/or
fragile) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Bullion </p>

<p class=MsoNormal style='margin-left:6.5pt'>Currency </p>

<p class=MsoNormal style='margin-left:6.5pt'>Firearms, parts thereof and
ammunition </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Hazardous or combustible materials
(as defined in IATA Regulations) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Human remains, including ashes </p>

<p class=MsoNormal style='margin-left:6.5pt'>Jewelry, precious metals and
stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>Narcotics (illegal) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Isopropanol </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cosmetics </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods </p>

<p class=MsoNormal style='margin-left:6.5pt'>Oil products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Perishables </p>

<p class=MsoNormal style='margin-left:6.5pt'>Plants </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.05pt;
margin-left:6.5pt'>Property, the carriage of which is prohibited by any law,
regulation or statute of any federal, state or local government of any country
to or through which the shipment may be carried. <b style='mso-bidi-font-weight:
normal'><span style='color:#CC0000'>Counterfeit Goods </span></b></p>

<h1 style='margin-top:0in;margin-right:0in;margin-bottom:1.45pt;margin-left:
6.75pt;text-indent:0in'><span style='font-size:10.0pt;mso-bidi-font-size:11.0pt;
line-height:107%'><span style='mso-spacerun:yes'> </span></span><b
style='mso-bidi-font-weight:normal'><span style='font-size:10.5pt;mso-bidi-font-size:
11.0pt;line-height:107%;color:#666666'>Counterfeit Goods are Prohibited </span></b></h1>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>HMHShip's</span>
policy states that it is not allowed to forward counterfeit. <span
class=SpellE>HMHShip</span> does not want to be associated with counterfeit
goods and upon suspicion of such goods being imported, Customs will be
informed. It is prohibited in many countries to introduce counterfeit goods and
where proven the Customs authority will seize the goods and penalize the buyer.
</p>

<p class=MsoNormal style='margin-left:6.5pt'><span
style='mso-spacerun:yes'> </span>If you are offered very low prices for branded
goods that are sold on shopping websites or via other channels, you can
normally assume that the goods are counterfeit. High quality branded goods that
are often copied are: </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Shoes </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Clothing </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Bags </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Expensive Watches </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>CDs and DVDs </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Perfumes </p>

<p class=MsoNormal style='margin-left:.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"MS Gothic";mso-bidi-font-family:
"MS Gothic"'>&#9642; </span>Electronics </p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-indent:0in;
line-height:107%'><span style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Algeria </h1>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:1.95pt;
margin-left:6.5pt'>Shipping guidelines, standard prohibitions and restrictions
to consider when having shipments forwarded to Algeria. <span style='font-size:
10.0pt;mso-bidi-font-size:11.0pt;line-height:104%;color:black'><span
style='mso-spacerun:yes'> </span></span><b style='mso-bidi-font-weight:normal'><span
style='font-size:11.0pt;line-height:104%;color:black'>Prohibited Commodities
for Algeria </span></b></p>

<p class=MsoNormal style='margin-left:6.5pt'>All items offensive to Muslim
culture are prohibited. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Alcoholic beverages </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Carpets </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cocoa </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cosmetics </p>

<p class=MsoNormal style='margin-left:6.5pt'>Credit card blanks (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Passports </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.85pt;
margin-left:6.5pt'>Precious metals &amp; stones </p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Angola </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Angola. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:2.1pt;
margin-left:6.5pt;line-height:107%'><b style='mso-bidi-font-weight:normal'><span
style='font-size:11.0pt;line-height:107%;color:black'>Prohibited Commodities
for Angola </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:5.85pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='font-size:11.0pt;line-height:107%;color:black'><span
style='mso-spacerun:yes'> </span></span></b></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Benin </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending shipments to Benin. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Benin </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Botswana </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Botswana. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Botswana </h2>

<p class=MsoNormal style='margin-left:6.5pt;line-height:145%'>Standard DHL
prohibited commodities plus the following commodities are <span
style='font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>not </i><span style='font-size:9.5pt;
mso-bidi-font-size:11.0pt;line-height:145%;font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span>acceptable for transport by DHL
Botswana<span style='font-family:"Calibri",sans-serif;mso-fareast-font-family:
Calibri'>&#8203;</span><i style='mso-bidi-font-style:normal'>. </i></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Burkina Faso </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Burkina Faso. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Burkina Faso </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Liquids, non-hazardous </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Burundi </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Burundi. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Burundi </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Cameroon </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Cameroon. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Cameroon </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Animal products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Cape Verde </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Cape Verde. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Cape Verde </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Animal skins </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ceramic products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Liquids, non-hazardous </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Central African Republic </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Central African Republic. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Central African
Republic </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import
Guidelines: Chad </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when having shipments forwarded to
Chad. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Chad </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Animal skins </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cash letters (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ceramic products </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Cheques</span>,
blank (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Cheques</span>,
cancelled (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Cheques</span>,
cashier (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Circuits &amp; circuit boards </p>

<p class=MsoNormal style='margin-left:6.5pt'>Clocks/watches </p>

<p class=MsoNormal style='margin-left:6.5pt'>Coal &amp; firewood </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Fire extinguishers </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Fotolithos</span>
</p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Isopropanol </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Jewellery</span>,
costume </p>

<p class=MsoNormal style='margin-left:6.5pt'>Marble products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Money orders (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Oil products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Paper patterns for wearing apparel
</p>

<p class=MsoNormal style='margin-left:6.5pt'>Perishables </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>Radar
equip.-transmitters/receivers </p>

<p class=MsoNormal style='margin-left:6.5pt'>Soil samples </p>

<p class=MsoNormal style='margin-left:6.5pt'>Subs. <span class=SpellE>cont'g</span>
sodium/calc. cyclamate </p>

<p class=MsoNormal style='margin-left:6.5pt'>Swatches </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'><span class=SpellE>HMHShip</span> Import Guidelines:
Comoros </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Comoros. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Comoros </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Credit card blanks (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Invoices, blank </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Playing cards </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Congo </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Congo. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Congo </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Animal skins </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Isopropanol </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Money orders (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Perishables </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: The Democratic
Republic of Congo </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to the
Democratic Republic of Congo. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for the Democratic
Republic of Congo </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Military samples </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Côte D’Ivoire </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Côte D’Ivoire. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:.7pt;
margin-left:6.5pt;line-height:107%'><b style='mso-bidi-font-weight:normal'><span
style='font-size:11.0pt;line-height:107%;color:black'>Prohibited Commodities
for Côte D’Ivoire </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Djibouti </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Djibouti. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Djibouti </h2>

<p class=MsoNormal style='margin-left:6.5pt'>All items offensive to the Muslim
culture are prohibited. These include pork products, pornography, religious
publications/figures. </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Egypt </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Egypt. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:2.1pt;
margin-left:6.5pt;line-height:107%'><b style='mso-bidi-font-weight:normal'><span
style='font-size:11.0pt;line-height:107%;color:black'>Prohibited Commodities
for Egypt </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:5.85pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='font-size:11.0pt;line-height:107%;color:black'><span
style='mso-spacerun:yes'> </span></span></b></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Eritrea </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Eritrea. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Eritrea </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Ethiopia </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Ethiopia. </p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:2.1pt;margin-left:
6.5pt'>Prohibited Commodities for Ethiopia </h2>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:.6pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='font-size:11.0pt;line-height:107%;color:black'><span
style='mso-spacerun:yes'> </span></span></b></p>

<p class=MsoNormal style='margin-left:6.5pt;line-height:107%'><b
style='mso-bidi-font-weight:normal'><span style='color:#CC0000'>Prohibited
Commodities for DHL Ethiopia </span></b></p>

<p class=MsoNormal style='margin-left:6.5pt'><span style='font-size:10.0pt;
mso-bidi-font-size:11.0pt;line-height:104%;color:black'><span
style='mso-spacerun:yes'> </span></span>Standard DHL prohibitions plus the
following commodities are not acceptable for transport by DHL Ethiopia: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-top:0in;margin-right:392.2pt;margin-bottom:
.15pt;margin-left:6.5pt'>Ivory Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:5.85pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='font-size:11.0pt;line-height:107%;color:black'><span
style='mso-spacerun:yes'> </span></span></b></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Gabon </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Gabon. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Gabon </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Deeds </p>

<p class=MsoNormal style='margin-left:6.5pt'>Drugs: non-prescription </p>

<p class=MsoNormal style='margin-left:6.5pt'>Foodstuffs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Gambia </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Gambia. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Gambia </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Liquids, non-hazardous </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>All items offensive to Muslim
culture are prohibited. These include pornography, religious
publications/figures. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.8pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:6.5pt;
margin-bottom:.0001pt'><span style='font-size:14.0pt;mso-bidi-font-size:11.0pt;
line-height:107%;font-weight:normal'>DHL Express Import Guidelines: Ghana </span></h2>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Ghana. <b style='mso-bidi-font-weight:normal'><span style='color:black'>Prohibited
Commodities for DHL Ghana </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.9pt;
margin-left:6.5pt'>Standard DHL prohibitions plus the following commodities are
<span style='font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>not </i><span style='font-size:9.5pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span>acceptable for transport by DHL
Ghana: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Guinea-Bissau </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Guinea-Bissau. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Guinea-Bissau </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Animal skins </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Drugs: non-prescription </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Liquids, non-hazardous </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Equatorial Guinea </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Equatorial Guinea. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Equatorial Guinea </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Animal products </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Cheques</span>,
cashier (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cosmetics </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Foodstuffs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Personal effects </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Guinea Republic </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Guinea Republic. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Guinea Republic </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.8pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:6.5pt;
margin-bottom:.0001pt'><span style='font-size:14.0pt;mso-bidi-font-size:11.0pt;
line-height:107%;font-weight:normal'>DHL Express Import Guidelines Kenya </span></h2>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Kenya. <b style='mso-bidi-font-weight:normal'><span style='color:black'>Prohibited
Commodities for DHL Kenya </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.9pt;
margin-left:6.5pt'>Standard DHL prohibited commodities plus the following are <span
style='font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>not </i><span style='font-size:9.5pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span>acceptable for transport by DHL
Kenya: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Invoices, blank </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Lesotho </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Lesotho. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Lesotho </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Liberia </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Liberia. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Liberia </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Alcoholic beverages </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Cash letters (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'><span class=SpellE>Cheques</span>,
blank (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Coal &amp; firewood </p>

<p class=MsoNormal style='margin-left:6.5pt'>Credit card blanks (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Money orders (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Perishables </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Libya </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Libya. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Libya </h2>

<p class=MsoNormal style='margin-top:0in;margin-right:4.05pt;margin-bottom:
.15pt;margin-left:6.5pt'>All items offensive to the <span class=SpellE>muslim</span>
culture or sensitive to the Middle East security situation are prohibited.
These items include Pork products </p>

<p class=MsoNormal style='margin-top:0in;margin-right:290.15pt;margin-bottom:
.15pt;margin-left:6.5pt'>Religious publications/paraphernalia Military
uniforms. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Alcoholic beverages </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Deeds </p>

<p class=MsoNormal style='margin-left:6.5pt'>Drugs: non-prescription </p>

<p class=MsoNormal style='margin-left:6.5pt'>Drugs: prescription </p>

<p class=MsoNormal style='margin-left:6.5pt'>Films: 8mm, 16mm &amp; 35mm </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Liquids, non-hazardous </p>

<p class=MsoNormal style='margin-left:6.5pt'>Playing cards </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>Seeds </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Madagascar </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Madagascar. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Madagascar </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.8pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:6.5pt;
margin-bottom:.0001pt'><span style='font-size:14.0pt;mso-bidi-font-size:11.0pt;
line-height:107%;font-weight:normal'>DHL Express Import Guidelines: Malawi </span></h2>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Malawi. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.5pt;margin-bottom:.0001pt;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='color:black'>Prohibited Commodities for DHL Malawi </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.9pt;
margin-left:6.5pt'>Standard DHL prohibitions plus the following commodities are
<span style='font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>not </i><span style='font-size:9.5pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span>acceptable for transport by DHL
Malawi: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Credit cards (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Mali </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Mali. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Mali </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>Tobacco </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Mauritania </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Mauritania. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Mauritania </h2>

<p class=MsoNormal style='margin-left:6.5pt'>All items offensive to Muslim
culture are prohibited. </p>

<p class=MsoNormal style='margin-left:6.5pt'>Alcoholic beverages </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Microfiche &amp; microfilm </p>

<p class=MsoNormal style='margin-left:6.5pt'>Military equipment </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.8pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:6.5pt;
margin-bottom:.0001pt'><span style='font-size:14.0pt;mso-bidi-font-size:11.0pt;
line-height:107%;font-weight:normal'>DHL Express Import Guidelines: Mauritius </span></h2>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Mauritius. </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.5pt;margin-bottom:.0001pt;line-height:107%'><b style='mso-bidi-font-weight:
normal'><span style='color:black'>Prohibited Commodities for DHL Mauritius </span></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:3.9pt;
margin-left:6.5pt'>Standard DHL prohibitions plus the following commodities are
<span style='font-family:"Calibri",sans-serif;mso-fareast-font-family:Calibri'>&#8203;</span><i
style='mso-bidi-font-style:normal'>not</i><span style='font-size:9.5pt;
mso-bidi-font-size:11.0pt;line-height:104%;font-family:"Calibri",sans-serif;
mso-fareast-font-family:Calibri'>&#8203;</span> acceptable for transport by DHL
Mauritius: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Animal skins </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Fire extinguishers </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Mayotte </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Mayotte. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Mayotte </h2>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Fire extinguishers </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:4.55pt;
margin-left:6.75pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h1 style='margin-left:6.5pt'>DHL Express Import Guidelines: Morocco </h1>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Morocco. </p>

<h2 style='margin-left:6.5pt'>Prohibited Commodities for Morocco </h2>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><b
style='mso-bidi-font-weight:normal'>All items offensive to Muslim culture are
prohibited. Standard DHL prohibitions plus:</b> </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Films: 8mm, 16mm &amp; 35mm </p>

<p class=MsoNormal style='margin-left:6.5pt'>Films: promotional, training </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Gambling devices </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Passports </p>

<p class=MsoNormal style='margin-left:6.5pt'>Playing cards </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>

<p class=MsoNormal style='margin-left:6.5pt'>Publications for public resale </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<h2 style='margin-top:0in;margin-right:0in;margin-bottom:0in;margin-left:6.5pt;
margin-bottom:.0001pt'><span style='font-size:14.0pt;mso-bidi-font-size:11.0pt;
line-height:107%;font-weight:normal'>DHL Express Import Guidelines: Mozambique </span></h2>

<p class=MsoNormal style='margin-left:6.5pt'>Shipping guidelines, standard
prohibitions and restrictions to consider when sending express shipments to
Mozambique. </p>

<p class=MsoNormal style='margin-left:6.5pt;line-height:107%'><b
style='mso-bidi-font-weight:normal'><span style='color:#CC0000'>Prohibited
Commodities for DHL Mozambique </span></b></p>

<p class=MsoNormal style='margin-left:6.5pt'>Standard DHL prohibitions plus the
following commodities are not acceptable for transport by DHL Mozambique: </p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:6.75pt;margin-bottom:.0001pt;text-indent:0in;line-height:107%'><span
style='mso-spacerun:yes'> </span></p>

<p class=MsoNormal style='margin-left:6.5pt'>Airline tickets, blank stock (NI) </p>

<p class=MsoNormal style='margin-left:6.5pt'>Antiques </p>

<p class=MsoNormal style='margin-left:6.5pt'>Asbestos </p>

<p class=MsoNormal style='margin-left:6.5pt'>Dangerous goods, <span
class=SpellE>haz</span>. or comb. mats </p>

<p class=MsoNormal style='margin-left:6.5pt'>Furs </p>

<p class=MsoNormal style='margin-left:6.5pt'>Ivory </p>

<p class=MsoNormal style='margin-left:6.5pt'>Pornography </p>

<p class=MsoNormal style='margin-left:6.5pt'>Precious metals &amp; stones </p>
<br/><br/>
<h3>http://www.dhl.co.ao/en/country_profile/import_guidelines_express.html<u
style='text-underline:black'><span style='color:black;text-underline:black;
text-decoration:none;text-underline:none'> </span></u>
<br/><br/>
http://www.myus.com/en/ship-to-anguilla/<u
style='text-underline:black'><span style='color:black;text-underline:black;
text-decoration:none;text-underline:none'> </span></u></h3>

</div>


