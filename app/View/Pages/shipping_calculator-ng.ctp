<style>
  a:hover {
    text-decoration: none;
  }
</style>
<div class="show-after-load">
    <section class="form-quickship" data-ng-app="calculator" id="calculator" data-ng-controller="MainCtrl">
        <div class="container">


            <div class="top-box-circle">
                <div class="circle"></div>
                <div class="icon">
                    <i class="icon-quickship"></i>
                </div>
            </div>
            <div class="wrap" id="quickshipFrm"> 
                
                <div class="text top-box-text">
                    <h1>Shipping Calculator — Save Big on Parcel Forwarding</h1>
                    <br/>
                    <div style="text-align: left;">Use our handy shipping calculator to find out just you just how much you can save by choosing HMHShip for all your parcel-forwarding needs! Fill in the location, package, and carrier details for your order, and it will provide you with an accurate estimate of your shipping charges.</div>
                    <br/>
                    <div style="text-align: left;">When you’re ready, you’ll be able to quickly arrange your order <a href="/quickship#">here</a>. If you have any questions about our shipping services that you’d like to ask before you buy, <a href="/talk-to-us">please contact us here.</a></div>

                    <h2>Calculate Your Shipping Costs</h2> <br/> <br/>

                </div>

                <div data-ng-view></div>
                <br/>
                <div class="text top-box-text">
                <h2>How Do We Do It? With the Right Relationships</h2>
 
                <div style="text-align: left;">Our prices are some of the most competitive in shipping, but there’s no catch. Those prices come from our great relationships with all the major international shipping companies. By working closely with shippers from all over the world, we can negotiate better rates for our own parcels and then pass those savings onto you.</div>
                <br/>
                <div style="text-align: left;">Another advantage you’ll enjoy because of our relationships is the freedom to receive packages in almost any country in the world. Wherever it needs to go, we have a way to get it there.</div>
 
                <br/>
                <h2>Save Even More With Discounted Volume Rates!</h2>
 
                <div style="text-align: left;">If you enjoy the savings on single parcels, you’re going to love our discounted volume rates. When you ship a high volume of parcels through HMHShip, you can save as much as 10%-50% on international shipping rates. Provide your details on our order form to see rates for your specific order, or contact us directly for more information on volumes that are eligible for discounted rates.</div>
                </div>
                    
            </div>
        </div>

    </section>
</div>



