<div class="show-after-load">
	<section class="business">
		<div class="container">

			<div class="business-content">
				<div class="top-box-circle">
					<div class="circle"></div>
					<div class="icon">
						<i class="icon-business"></i>
					</div>
				</div>
				<div class="wm-content">
					<div class="text top-box-text">
						<h1>International Parcel Forwarding for Businesses</h1>
					</div>

                                    
                                    <h2>Sign Me Up</h2><br/>
                                    <p>Thank you for considering our business shipping plans. Forwarding packages from us at an amazing price begins with just a simple sign up. If you’re able to order at volume, you may also be able to take advantage of tremendous savings that you won’t be able to find elsewhere. We look forward to working with you to reach customers and partners all over the world!</p>
                                    <br/>
                                    <a href="/business-form" style="margin: 0 auto; display: block; max-width: 250px; padding: 5px"
                                    										   class="wm-btn wm-btn-fit" title="Sign me up">
                                    											<i class="icon-createaccount"></i> Sign me up now
                                    										</a>

                                    <h2>Already Have an Account?</h2><br/>
                                    <p>
We can ship for you and bill your international shipping costs directly to the carrier account of your choice. Please enter your account information for DHL, UPS, USPS, or FedEx to begin shipping with this carrier as your default choice.
 
Please remember that your international carrier options can be changed at any time to accommodate the needs of your customers. You can notify us whenever this is necessary.</p>
                                    <br/>
                                    
					<ul class="business-list">
						<li>
							<div class="row">
								<div class="col col-md-4 left">
									<div>
										<i class="icon-globally"></i>
										<span class="text">EXPAND YOUR<br>BUSINESS GLOBALLY</span>
									</div>
								</div>
								<div class="col col-md-8 right">
									<p>
										Allow us to ship your products from the U.S. directly to your customers
										anywhere, worldwide.
									</p>
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col col-md-4 left">
									<div>
										<i class="icon-sellworldwide"></i>
										<span class="text">SELL WORLDWIDE</span>
									</div>
								</div>
								<div class="col col-md-8 right">
									<p>
										We make it easy for you to ship your orders to over 200 countries worldwide.
										Let us streamline the shipping process to your customers overseas – simple,
										easy, productive. Shipping internationally through us is as simple as any
										domestic shipment.
									</p>
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col col-md-4 left">
									<div>
										<i class="icon-savebig"></i>
										<span class="text">SAVE BIG ON<br>SHIPPING</span>
									</div>
								</div>
								<div class="col col-md-8 right">
									<p>
										Take advantage of our discounted volume rates and save 10% to 50% on
										international shipping.
									</p>
								</div>
							</div>
						</li>
						<li>
							<div class="row">
								<div class="col col-md-4 left">
									<div>
										<i class="icon-invoice"></i>
										<span class="text">FREE COMMERCIAL<br>INVOICES</span>
									</div>
								</div>
								<div class="col col-md-8 right">
									<p>
										You provide the information; we prepare the documents for customs. All this will
										be handled for you at no extra cost.
									</p>
								</div>
							</div>
						</li>
						<li class="last">
							<div class="row">
								<div class="col col-md-4 left">
									<div>
										<i class="icon-discount"></i>
										<span class="text">DISCOUNTED RATES</span>
									</div>
								</div>
								<div class="col col-md-8 right">
									<p>
										Pay only the competitive shipping fees we pay, plus a discounted handling fee
										per package. Competitive rates are available for volume shippers.
									</p>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>

			<div class="business-boxes">

				<div class="row">
					<div class="col col-lg-5 col-sm-6  col-lg-offset-1 ">
						<div class="business-box first">
							<div class="top-box-circle">
								<div class="circle"></div>
								<div class="icon">
									<i class="icon-work"></i>
								</div>
							</div>
							<div class="wm-content">
								<div class="text top-box-text">
									<h2>How it works</h2>

									<p>
										See how we make it happen…
									</p>
									<ul class="packages-list">
										<li>
											<i class="icon-listitem"></i> Incorporate international shipping on your
											site.
										</li>
										<li>
											<i class="icon-listitem"></i> After an international customer has made a
											purchase, send the order to:
											<div>
												<p>
													HMHShip
												</p>

												<p>
													(customers name) c/o XXX
												</p>

												<p>
													23600 Mercantile Rd.
												</p>

												<p>
													Suite C-119
												</p>

												<p>
													Beachwood, OH 44122 USA
												</p>
											</div>
										</li>
										<li>
											<i class="icon-listitem"></i> With the order make sure to include, an
											invoice and customers
											<ul>
												<li>
													<i class="icon-listitem"></i> Full Name
												</li>
												<li>
													<i class="icon-listitem"></i> Address
												</li>
												<li>
													<i class="icon-listitem"></i> Phone
												</li>
												<li>
													<i class="icon-listitem"></i> Email
												</li>
												<li>
													<i class="icon-listitem"></i> Item Description
												</li>
												<li>
													<i class="icon-listitem"></i> Quantity
												</li>
												<li>
													<i class="icon-listitem"></i> Item Value
												</li>
											</ul>
										</li>
									</ul>
									<p class="text-left">
										We send the item oﬀ to the customer.... Simple as that!
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="col col-lg-5 col-sm-6">
						<div class="business-box last">
							<div class="top-box-circle">
								<div class="circle"></div>
								<div class="icon">
									<i class="icon-shippingplans"></i>
								</div>
							</div>
							<div class="wm-content">
								<div class="text top-box-text">
									<h2>Shippping plans</h2>

									<p>
										Choose your monthly plan
									</p>
									<table class="ship-plans">
										<tr>
											<td class="left">
												0-25<br>
												packages
											</td>
											<td>
												$ 0.00<br>
												<small>(+ $7.25 per package)</small>
											</td>
										</tr>
										<tr>
											<td class="left">
												25-100<br>
												packages
											</td>
											<td>
												$ 25.00<br>
												<small>(+ $6.00 per package)</small>
											</td>
										</tr>
										<tr>
											<td class="left">
												100-500<br>
												packages
											</td>
											<td>
												$ 100.00<br>
												<small>(+ $5.75 per package)</small>
											</td>
										</tr>
										<tr>
											<td class="left">
												+500<br>
												packages
											</td>
											<td>
												$ 500.00<br>
												<small>(+ $5.50 per package)</small>
											</td>
										</tr>
									</table>
									<p>
										You can upgrade your plan at any time if your shipping
										volume increases. We will notify you if your current
										plan would be more economical to upgrade.
									</p>

									<div class="action">
										<a href="/business-form"
										   class="wm-btn wm-btn-fit" title="Sign me up">
											<i class="icon-createaccount"></i> Sign me up
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>