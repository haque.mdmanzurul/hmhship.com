<div class="show-after-load">
    <section class="we-do">
        <div class="container">

            <div class="we-do-content">
                <div class="top-box-circle">
                    <div class="circle"></div>
                    <div class="icon">
                        <i class="icon-whatwedo"></i>
                    </div>
                </div>
                <div class="wm-content">
                    <div class="text top-box-text">
                        <h1>Our Package &amp; Mail Forwarding Services</h1>
                    </div>

                    <ul class="wm-all-packages">
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            Setup fee
                                        </h3>
                                        <p>
                                            That's right, no fee at all when you set up an account with us!
                                        </p>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">Free</div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            MONTHLY FEE
                                        </h3>
                                        <p>
                                            No pressure at all, use your account as much or as little as you need.
                                        </p>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">Free</div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            PARCEL-FORWARDED
                                        </h3>
                                        <p>
                                            Have your US shipment sent to us, and we take care of the rest, insuring it gets to you as quickly and safely as possible.
                                        </p>
                                        <a class="more show-packages-list" title="tell me more" role="button" data-toggle="collapse" href="#collapseParcelForwarded" aria-expanded="false" aria-controls="collapseParcelForwarded">
                                            <i class="icon-more"></i> tell me more
                                        </a>
                                        <ul class="packages-list" id="collapseParcelForwarded">
                                            <li>
                                                <i class="icon-listitem"></i> If possible, have the dimensions and weight of your parcel(s) ready. Also have the
                                                details about the items you wish to ship handy. If you don't have this information
                                                about your parcel(s) handy, not a problem, we will notify you when your parcel has
                                                been received and you can submit the information then.
                                            </li>
                                            <li>
                                                <i class="icon-listitem"></i> Hit &ldquo;New Shipment&rdquo; in your account or &ldquo;QuickShip&rdquo; if you do not wish to create an
                                                account with us. Enter;

                                                <ul>
                                                    <li>
                                                        <i class="icon-listitem"></i> Address
                                                    </li>
                                                    <li>
                                                        <i class="icon-listitem"></i> If necessary, Additional Options (Consolidate, Expedite, Insure, Repack)
                                                    </li>
                                                    <li>
                                                        <i class="icon-listitem"></i> Package Details and Item Details Package details are not required if you don't have that information.
                                                    </li>
                                                    <li>
                                                        <i class="icon-listitem"></i> Shipping Option
                                                    </li>
                                                    <li>
                                                        <i class="icon-listitem"></i> Payment Option
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <i class="icon-listitem"></i> Unless you have an account (which comes with your U.S. address), a U.S.
                                                address will be provided to you in order to have your parcel forwarded to us.
                                            </li>
                                            <li>
                                                <i class="icon-listitem"></i> We will notify you via email when we receive your parcel and when your parcel is
                                                shipped to you. If you have an account, shipping information can be found under
                                                &ldquo;My Shipments&rdquo;
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">
                                            $7.25 <br>
                                            <small>(PLUS SHIPPING CHARGE)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            PARCEL CONSOLIDATION
                                        </h3>
                                        <p>
                                            Upon request, we can consolidate many of your shipments into one box, which helps you save on postage.
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">Free</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            PARCEL REPACKAGING
                                        </h3>
                                        <p>
                                            Each package is forwarded to you exactly as it comes, unless speciﬁed in the instructions. Upon request, we can repackage your shipments to insure they get to you safely. When submitting a new package with us, make sure to check &ldquo;Repack&rdquo;.
                                        </p>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">Free</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            STORAGE <small>(UP TO 90 DAYS)</small>
                                        </h3>
                                        <p>
                                            We can store all your packages for free for up to 90 days. You'll receive a notiﬁcation two weeks before the 90 days are up. After the 90 days, you may request up to two weeks of additional storage time. After the additional two weeks is up, your package will be forfeited.
                                        </p>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">Free</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange">
                                            PREFERRED PACK <small>(ACCOUNT REQUIRED)</small>
                                        </h3>
                                        <p>
                                            Pay for one of three Smart Packs in advance and save big per parcel! Choose the Smart Pack that best suits your packaging needs and start shipping right away.
                                        </p>
                                        <a class="more show-packages-list" title="tell me more" role="button" data-toggle="collapse" href="#collapsePreferredPack" aria-expanded="false" aria-controls="collapsePreferredPack">
                                            <i class="icon-more"></i> tell me more
                                        </a>
                                        <ul id="collapsePreferredPack" class="packages-list">
                                            <li>
                                                <i class="icon-listitem"></i>
                                                <div class="indent pack-table">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <p><strong>PreferredPack - 3 Package</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p>$20.65 ($6.88 per parcel)</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <p><strong>PreferredPack - 5 Package</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p>$33.70 ($6.74 per parcel)</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <p><strong>PreferredPack  - 10 Package</strong></p>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <p>$65.25 ($6.53 per parcel)</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">
                                            $20.65 - $65.25
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="wm-packages">
                            <div class="row sm-row-eq-height">
                                <div class="col col-sm-9">
                                    <div class="left-box-circle">
                                        <div class="circle"></div>
                                        <div class="icon">
											<span class="path icon-package">
												<span class="path path1"></span>
												<span class="path path2"></span>
											</span>
                                        </div>
                                    </div>
                                    <div class="text">
                                        <h3 class="t-orange" style="text-transform:none;">
                                          AssistUS Shopper <small>(ACCOUNT REQUIRED)</small>
                                        </h3>
                                        <p>
                                            If you have trouble ordering online (for example, the online vendor doesn't accept international credit cards), we can shop on your behalf to get you your items. We're also able to purchase many hard-to-get products from various online sites.
                                        </p>
                                        
                                        
                                    </div>
                                </div>
                                <div class="col col-sm-3">
                                    <div class="price">
                                        <div class="middle">
                                            $4.25 <br>
                                            <small>(PER SITE)</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>