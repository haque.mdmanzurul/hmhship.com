<div class="show-after-load">
  <section class="ship-bio">
    <div class="container">

      <div class="mission-content">
        <div class="top-box-circle">
          <div class="circle"></div>
          <div class="icon">
            <i class="icon-mission"></i>
          </div>
        </div>
        <div class="wm-content">
          <div class="text top-box-text">
            <h1>Mission Statement</h1>
          </div>

          <div class="info-mission">
            <h3>Here at HMHShip our mission is simple:</h3>
            <div class="row info-cols">
              <div class="col col-md-4 first getmaxcol">
                <div class="content">
                  <h2>CUSTOMER SERVICE</h2>
                  <p>
                    Personal, professional &amp; efficient. We're here for you! Every question or comment, no matter how big or small is taken very seriously. We're always striving for 100% customer satisfaction. If you EVER have any issues with our service, big or small, never hesitate to contact us right away, we will get back to you and resolve any issue as soon as we possibly can.
                  </p>
                </div>
              </div>
              <div class="col col-md-4 center getmaxcol">
                <div class="content">
                  <h2>SIMPLE NAVIGATION </h2>
                  <p>
                    Easily maneuver through the shipping process. We've created the perfect system! Our entire site and its functions were designed with one goal in mind, to create a system which is as easy as possible for the customer to navigate.
                  </p>
                </div>
              </div>
              <div class="col col-md-4 last getmaxcol">
                <div class="content">
                  <h2>COMPLETE ASSISTANCE</h2>
                  <p>
                    AssistUS Shopper, negotiated returns &amp; thorough support. From us receiving your parcel, to you receiving your parcel, and beyond we’re here to help! Need to return something to us? No problem at all! International shipping should not be a hassle for you ever.
                    So, let’s start the shipping process and see for yourself!
                  </p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col col-md-4 col-md-offset-4 feat-bio">
                <div class="">
                  So, let's start the shipping process
                  <span>
                    and see for yourself!
                  </span>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="philosophy-content">
        <div class="top-box-circle">
          <div class="circle"></div>
          <div class="icon">
            <i class="icon-philosophy"></i>
          </div>
        </div>
        <div class="wm-content">
          <div class="text top-box-text">
            <h2>Philosophy</h2>
          </div>
          <div class="info-philosophy">
            <div class="row">
              <div class="col col-md-6 getmaxcol quotes">
                <blockquote class="content">
                  HMHShip started with the belief that there was room for somebody to do something new and better in parcel forwarding. We looked at the inefficiencies, inconsistencies and uncertainties that plagued the industry, and worked on solving all of them with a new set of practices founded on strong principles. These are the seven principles that still guide our services to this day:
                </blockquote>
              </div>
              <div class="col col-md-6 getmaxcol">
                <ul class="packages-list content">
                  <li>
                    <i class="icon-listitem"></i> We always strive for excellence and use every opportunity to learn & improve
                  </li>
                  <li>
                    <i class="icon-listitem"></i> Our integrity is important to us, and will not be compromised.
                  </li>
                  <li>
                    <i class="icon-listitem"></i> We always treat our customers with courtesy, and recognize that our business exists to provide them with the best service we possibly can.
                  </li>
                  <li>
                    <i class="icon-listitem"></i> The best solutions are win/win, and we strive to create those solutions for customers and partners alike.

                  </li>
                  <li>
                    <i class="icon-listitem"></i> Our customers and their experience is our chief focus.

                  </li>
                  <li>
                    <i class="icon-listitem"></i> Listening is one of the most effective ways to find common ground.

                  </li>
                  <li>
                    <i class="icon-listitem"></i> We trust that we can rely on our customers when they can rely on us

                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="philosophy-content">
        <div class="top-box-circle">
          <div class="circle"></div>
          <div class="icon">
            <i class="icon-philosophy"></i>
          </div>
        </div>
        <div class="wm-content">
          <div class="text top-box-text">
            <h2>The HMHShip Story</h2>
          </div>
          <div class="">
            <div class="row">
              <div class="col col-xs-12 getmaxcol quotes">
				<h4>
					The idea of HMHShip was conceived by Hirsh M. Henfield while working for a dental supply company in Cleveland, OH.&nbsp;&nbsp; At the company, Hirsh was primarily involved in online sales and would frequently be asked: “Do you ship overseas?”.&nbsp;&nbsp; After receiving countless inquiries, it became apparent that there was a high demand for US products sold online, products only available from U.S. sites, that did not ship internationally.</h4>
                <h4>
                	Despite there being quite a few parcel-forwarding companies, many online reviews suggested that customers were dissatisfied with the service they received.&nbsp;&nbsp; Hirsh spent much time crafting a company that would put the customer’s needs first, building a smart, “user-friendly” system, two points which the competition seemed to be lacking most.&nbsp;&nbsp; Soon thereafter, HMHShip was launched!
				</h4>

              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
  </section>
</div>