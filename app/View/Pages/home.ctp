
<div class="show-after-load">
	<section class="webdoor">
		<div class="box-graphic">
		</div>
		<div class="container">
			<h1 class="text-center">ASSURANCE. RELIABILITY. PEACE OF MIND</h1>
			<br/>


			<div id="myModal" class="modal">

			  <!-- Modal content -->
			  <div class="modal-content">
			    <span class="close">&times;</span>
					<video width="100%" controls>
					  <source src="/media/home-video.mp4" type="video/mp4">
					Your browser does not support the video tag.
					</video>
			  </div>

			</div>



			<div class="row">
				<div class="col-sm-6">
					<div class="graph hidden-mobile">
						<div class="graph-wrap graph1">
							<img id="step1" src="/img/cycle-1.png" alt="Have your parcel sent to us" height="138" width="138" style="border: 1px solid #ee964e;border-radius: 50%;" />
							<div class="caption">Have your parcel sent to us</div>
						</div>
						<div class="graph-wrap graph2">
							<img id="step4" src="/img/cycle-4.png" alt="You receive your Parcel" height="138" width="138" style="border: 1px solid #ee964e;border-radius: 50%;" />
							<div class="caption">You receive your parcel</div>
						</div>
						<div class="graph-wrap graph4">
							<img id="step2" src="/img/cycle-3.png" alt="We receive your parcel" height="138" width="138" style="border: 1px solid #ee964e;border-radius: 50%;" />
							<div class="caption">We receive your Parcel</div>
						</div>
						<div class="graph-wrap graph3">
							<img id="step3" src="/img/cycle-2.png" alt="We send your Parcel off to you internationally" height="138" width="138" style="border: 1px solid #ee964e;border-radius: 50%;" />
							<div class="caption">We send your parcel off to you</div>
						</div>
						<div class="graph-middle" style="padding-top:25px;">



							<span id="play-video" class="video-play-button" style="margin-top: 11px;">
								<span></span>
							</span>



							<img id="globe" src="/img/globe-gif.gif" alt="picture of globe" />
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box-feature">

						<?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>

							<div>
								<a href="/Users/myaccount">
									<div class="feature feat-2">

										<h3>My Account</h3>

									</div>
								</a>
							</div>

							<div>
								<a href="/new-shipment">
									<div class="feature feat-2">

										<h3>New Shipment</h3>

										<p>

										</p>

									</div>
								</a>
							</div>


							<?php } else { ?>
								<div>
									<a href="<?php echo Router::url(array('controller' => 'pages', 'action' => 'register')); ?>">
										<div class="feature feat-2">
											<h3>Create an Account</h3>
											<p>
												Better deals! More services
											</p>
										</div>
									</a>
								</div>
								<div>
									<a href="/quickship">
										<div class="feature feat-2">
											<h3>Quickship</h3>
											<p>
												Ship fast! Ship now! No account required
											</p>
										</div>
									</a>
								</div>
								<?php } ?>

					</div>
				</div>
			</div>


				<div class="top-ship-info">
					<div class="clean-box">
						<h2>US Parcel Forwarding Services</h2>
						<h3 class="text-center">Have your U.S. parcels <strong>forwarded to you in any country,</strong> worldwide.</h3>
						<h3 class="text-center"><strong>QUICK, EASY, AND HASSLE-FREE</strong></h3>


						<h3>US package forwarding is now safer, easier, and more affordable than ever before. <b>No monthly fee. No setup fee.</b> You won’t even need to create an account!</h3>
						<h4>Just follow the <a href="/quickship"><strong>QUICKSHIP</strong></a> button to fill out your package details and have your U.S. package delivered to our warehouse. From there, we’ll coordinate with local and international partners to make sure your package is delivered to you, even if you’re in a country without reliable parcel services.</h4>
					</div>

			<div class="row">

					<div class="col col-md-6">

						<ul>
							<li>
								<span class="cel-1">
									<i class="icon-customerservice"></i>
								</span>
								<span>
									<strong>Customer Service:</strong>
									Personal, professional &amp; efficient.  We’re here when you need us.
								</span>
							</li>
							<li>
								<span class="cel-1">
									<i class="icon-competitiverate"></i>
								</span>
								<span>
									<strong>Competitive Rates.</strong>
									Compare our rates to other parcel forwarding companies
								</span>
							</li>
							<li>
								<span class="cel-1">
									<i class="icon-packageconsolidation"></i>
								</span>
								<span>
									<strong>Package Consolidation.</strong>
									Lower costs on multiple shipments with this free service
								</span>
							</li>
						</ul>
					</div>
					<div class="col col-md-6">
						<ul>
							<li>
								<span class="cel-1">
									<i class="icon-shipfast"></i>
								</span>
								<span>
									<strong>Account, Optional.</strong> No account needed to start shipping. Fast and easy.
								</span>
							</li>
							<li>
								<span class="cel-1">
									<i class="icon-simplenavigation"></i>
								</span>
								<span>
									<strong>Simple Navigation.</strong>
									Easily maneuver the international parcel forwarding process
								</span>
							</li>
							<li>
								<span class="cel-1">
									<i class="icon-completeassis"></i>
								</span>
								<span>
									<strong>Complete Assistance.</strong>
									 AssistUS Shopper, negotiated returns, and thorough support.
								</span>
							</li>
						</ul>
					</div>
			</div>
		</div>
	</section>

	<section class="home-actions">
		<div class="container">
			<div class="circle"></div>
			<div class="row">
				<div class="col col-md-4 col-sm-6">
					<div class="home-action left">
						<div class="top-box-circle">
							<div class="circle"></div>
							<div class="icon">
								<i class="icon-whatwedo"></i>
							</div>
						</div>
						<div class="box-out">
							<div class="box top-box-text">
								<h2><a href="<?php echo Router::url(array('controller' => 'pages', 'action' => 'business')); ?>">Business</a></h2>
								<p>
									Expand your business globally with our international parcel forwarding services. We’ll ship your product quickly, securely and reliably from the U.S. directly to your customers around the world. Click here to learn how you can save big with discounts and special rates for volume shippers.
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col col-md-4 col-sm-6">
					<div class="home-action right">
						<div class="top-box-circle">
							<div class="circle"></div>
							<div class="icon">
								<i class="icon-work"></i>
							</div>
						</div>
						<div class="box-out">
							<div class="box top-box-text">
								<h2><a href="/shipping-calculator">Shipping Calculator</a></h2>
								<p>
									Use our shipping calculator to find out how much you could save by shipping your next package with us. Click here and enter details about your parcel and where you'd like it shipped. We’ll show you the best rates with all the carriers who operate in those countries.
								</p>
							</div>
						</div>
					</div>
				</div>

				<div class="col col-md-4 col-sm-6">
					<div class="home-action right">
						<div class="top-box-circle">
							<div class="circle"></div>
							<div class="icon">
								<i class="icon-completeassis"></i>
							</div>
						</div>
						<div class="box-out">
							<div class="box top-box-text">
								<h2><a href="/services-rates">Other Services</a></h2>
								<p>
									Enjoy a range of convenient package services when you choose to ship with us.
									<br/> The free services listed below are just some of the perks you’ll enjoy when you choose us:</p>
								<ul>
									<li>
										Free Parcel Consolidation
									</li>
									<li>
										Free Parcel Repackaging
									</li>
									<li>
										Free Storage (Up to 90 Days)
									</li>
								</ul>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
</div>


<style>
	.graph .graph-wrap img.active {
	box-shadow: 0 0 50px #ee964e;
}
.graph .graph-wrap img {

	-webkit-transition: .3s ease all;
	transition: .3s ease all;
}

</style>


<script type="text/javascript" src="js/lightbox.js"></script>
<script>
  $( document ).ready(function() {

		let open = false;

	function rotateOrangePulse(){

		setTimeout(function() {
			$("#step1").toggleClass('active');
		}, 1000);
		setTimeout(function() {
			$("#step1").toggleClass('active');
		}, 2000);
		setTimeout(function() {
		$("#step2").toggleClass('active');
		}, 3000);
		setTimeout(function() {
			$("#step2").toggleClass('active');
		}, 4000);
		setTimeout(function() {
		$("#step3").toggleClass('active');
		}, 5000);
		setTimeout(function() {
			$("#step3").toggleClass('active');
		}, 6000);
		setTimeout(function() {
		$("#step4").toggleClass('active');
		}, 7000);
		setTimeout(function() {
			$("#step4").toggleClass('active');
		}, 8000);

	}

	rotateOrangePulse();
	setInterval(rotateOrangePulse, 8000); //repeat

	function pulsePlayButton(){
		$("#play-btn").animate( { opacity: 1 }, 1000 );
		$("#play-btn").animate( { opacity: .54 }, 2000 );
	}

	pulsePlayButton();
	setInterval(pulsePlayButton, 2000);


	$(document).click(function(e) {
			if (e.target.closest(".modal-content")) {
					return;
			} else if(open == true && e.target.tagName.toLowerCase() != 'span') {
				$("#myModal").css("display", "none");
				$('video').trigger('pause');
				$("#play-video").css('display', "inline");
				 open = false;
			}
	});

	$("#play-video").on('click', function(){
    $("#myModal").css("display", "block");
		$("#play-video").css('display', "none");
		$('video').trigger('play');
		open = true;
	});

	$('.close').on("click", function(){
			 $("#myModal").css("display", "none");
			 $('video').trigger('pause');
			 $("#play-video").css('display', "inline");
			 open = false;
	});






  });
</script>
