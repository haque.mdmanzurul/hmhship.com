<div class="show-after-load">
    <section class="talk-to-us">
        <div class="container">
            <div class="mission-content">
                <div class="top-box-circle">
                    <div class="circle"></div>
                    <div class="icon">
                        <i class="icon-email"></i>
                    </div>
                </div>
                <div class="wm-content">
                    <div class="text top-box-text">
                        <h1>We Want to Hear from You!</h1>
                    </div>

                    <p>Contact us at HMHShip for the answers to any of your questions, or to let us know what you thought of our service from your last delivery. Questions are answered as quickly as possible, usually within one business day. We take comments and critiques seriously, and use them to continue to improve our services. Please use the form below to contact us immediately.
                    </p>
                    <br/>

                    <div class="info-contact">
                        <div class="row">

                            <div class="col col-md-6">

								<?php
                                                                    $msg = $this->Session->flash();
                                                                    if ($msg != "") {
                                                                        echo($msg);
                                                                        echo("<script>ga('send', 'event', 'Forms', 'Submit', 'Talk to Us'); </script>");
                                                                    }
                                                                ?>

								<?php echo $this->Form->create('Contact', array(
									'class' => 'form-valid',
									'url' => array('controller' => 'pages', 'action' => 'contact')
								)); ?>
                    * Required<br/><br/>
                    <fieldset>
										<span class="msg-error"><?php echo ($this->Form->isFieldError('contact_name') ? $this->Form->error('contact_name') : ''); ?></span>
                                        <input type="text" name="data[Contact][contact_name]" class="form-control required" placeholder="Name *"
                                               value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['Contact']['contact_name']; ?>">
                                    </fieldset>
                                    <fieldset>
										<span class="msg-error"><?php echo ($this->Form->isFieldError('contact_email') ? $this->Form->error('contact_email') : ''); ?></span>
                                        <input type="email" name="data[Contact][contact_email]" class="form-control required" placeholder="E-mail *"
                                               value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['Contact']['contact_email']; ?>">
                                      </fieldset>
                                    <fieldset>
										<span class="msg-error"><?php echo ($this->Form->isFieldError('contact_subject') ? $this->Form->error('contact_subject') : ''); ?></span>
                                        <input type="text" name="data[Contact][contact_subject]" class="form-control required email" placeholder="Subject *"
                                               value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['Contact']['contact_subject']; ?>">
                                      </fieldset>
                                    <fieldset>
										<span class="msg-error"><?php echo ($this->Form->isFieldError('contact_message') ? $this->Form->error('contact_message') : ''); ?></span>
                                        <textarea name="data[Contact][contact_message]" class="form-control required" placeholder="Your message *"><?php echo (empty($this->request->data)) ? '' : $this->request->data['Contact']['contact_message']; ?></textarea>
                                    </fieldset>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <fieldset>
                                                <input type="text" name="data[Contact][contact_transaction_number]" class="form-control" placeholder="Transaction Number"
                                                       value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['Contact']['contact_transaction_number']; ?>">
                                              </fieldset>
                                        </div>
                                        <div class="col-md-6">
                                            <fieldset>
                                                <select name="data[Contact][contact_transaction_type]" class="form-control">
                                                    <option value=""
                                                      <?php echo (empty($this->request->data)) ? 'selected': ""; ?>
                                                      >Transaction Type</option>
                                                    <option value="QuickShip"
                                                      <?php echo (!empty($this->request->data) && $this->request->data['Contact']['contact_transaction_type']=="QuickShip") ? 'selected': ""; ?>
                                                      >QuickShip</option>
                                                    <option value="New Shipment"
                                                      <?php echo (!empty($this->request->data) && $this->request->data['Contact']['contact_transaction_type']=="New Shipment") ? 'selected': ""; ?>
                                                      >New Shipment</option>
                                                    <option value="PreferredPack"
                                                      <?php echo (!empty($this->request->data) && $this->request->data['Contact']['contact_transaction_type']=="PreferredPack") ? 'selected': ""; ?>
                                                      >PreferredPack </option>
                                                    <option value="AssistUS Shopper"
                                                      <?php echo (!empty($this->request->data) && $this->request->data['Contact']['contact_transaction_type']=="AssistUS Shopper") ? 'selected': ""; ?>
                                                      >AssistUS Shopper </option>
                                                </select>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="action">
                                        <button class="btn-send" type="submit">Send <i class="icon-send"></i></button>
                                    </div>
								<?php echo $this->Form->end(); ?>
                            </div>

                            <div class="col col-md-6 details-contact">
                                <div class="row">


                                <div class="row">
									<div class="col col-md-12 contact-social text-center">
										<ul class="social">
                      <li class="fb-wrap">

                        <a href="https://www.facebook.com/hmhship/" target="_blank" title="Facebook">

													<i class="icon-fb"></i>

											</a>
                      </li>
                      <li class="tw-wrap">
											<a href="https://twitter.com/HMHShip" target="_blank" title="Twitter">

													<i class="icon-tw"></i>

											</a>
                      </li>
                      <li>
                      <a href="https://www.instagram.com/hmhship/" target="_blank" title="Instagram">
                        <img src="/img/instagram.png" style="height:39px;margin-top:-3px;margin-right:4px;"></img>
                      </a>
                      </li>
                      <li class="gplus-wrap">
											<a href="https://plus.google.com/110836815863701052137" target="_blank" title="Google+">

													<i class="icon-google-plus3"></i>

											</a>
                      </li>
                      <li>
                      <a href="https://www.youtube.com/channel/UCh5Uy0vsg0rr0m90PjGFsNg?view_as=subscriber" target="_blank" title="YouTube">

                          <img src="/img/youtube.png" style="width:40px;"></img>

                      </a>
                      </li>
                      <li class="in-wrap" style="position:relative;top:3px;">
											  <a href="https://www.linkedin.com/company/hmhship" target="_blank" title="LinkedIn" style="margin-top:4px;">

													  <i class="icon-linkedin"  ></i>

											  </a>
                      </li>



										</ul>
									</div>
                                    <div class="col col-md-12">

                                        <iframe src="https://www.google.com/maps/embed/v1/place?q=HMHShip+23600+Mercantile+Rd.+Suite+C-119+Beachwood,OH+44122&key=AIzaSyDx-BL0YsEbhi7bD9UYRJoB6HMj_J6rY98&maptype=roadmap&zoom=13" height="240" style="width:100%;" frameborder="0" style="border:0"></iframe>
                                    </div>
                                </div>
                                <div class="detail">
                                    <i class="icon-location"></i> <?php echo Configure::read('Credits.address'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <div class="text top-box-text"><h2>Find More Information on Our Site</h2></div>
                    <p>If you have questions, you may be able to find the answers you need on our website even more quickly than we could respond to you. Use the links below to find the information you need.</p>
                    <div class="row">
						<div class="col-md-4">
							<h3>About HMHShip</h3>
							<p>
								If you have questions about HMHShip—our history, standards, or principles—you’ll be able to find what you’re looking for <a href="/hmhship-bio">here on our company bio</a>. We've always believed in the same things: customer service, simplicity, and total support.
							</p>
						</div>
						<div class="col-md-4">
							<h3>About Our Services and rates</h3>
							<p>
								Learn more about the services we offer on <a href="/services-rates">our services and rates page</a>. You can also enter your <a href="/quickship">package details here</a> to see the final price without even needing to create an account. <a href="/business">Our business information page</a> will tell you more about rates and plans for our business clients.
							</p>
						</div>
						<div class="col-md-4">
							<h3>Legal Information</h3>
							<p>
								Our <a href="/terms">terms & conditions</a> and <a href="/privacy">privacy policy</a> are available through the provided links.
							</p>
						</div>
					</div>

                </div>
            </div>
        </div>
</div>
</section>
</div>
