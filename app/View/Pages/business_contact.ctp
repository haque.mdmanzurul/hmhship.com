<?php echo $this->Form->create('BusinessContact', array(
    'class' => 'form-horizontal',
    'role' => 'form',
    'inputDefaults' => array(
        'class' => 'form-control',
        'div' => 'form-group',
        'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
        'between' => '<div class="col-sm-8">',
        'after' => '</div>'
    )
)); ?>

    <div class="container">
        <div class="row">
            <h1 class="text-center"><img alt="" src="<?php echo Router::url('/img/logo.png'); ?>" title=""> Business</h1>

            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
                <tbody>
                <tr class="c39">
                    <td class="c57" colspan="1" rowspan="1"><p class="c10"><span
                                class="c11 c43">Shipping plans</span>
                        </p></td>
                    <td class="c21" colspan="1" rowspan="1"><p class="c10"><span class="c2 c43">How it works</span>
                        </p>
                    </td>
                    <td class="c21" colspan="1" rowspan="1"><p class="c10"><span
                                class="c43 c11 c63">Sign me up!</span>
                        </p></td>
                </tr>
                </tbody>
            </table>
            <p class="c47 c37 c23"><span class="c11 c58"></span></p>
            <ul class="c8 lst-kix_5yqfxpi9bpdi-0 start">
                <li class="c1"><span class="c11 c58">Expand Your Business Globally.</span></li>
            </ul>
            <p class="c3"><span>Allow us to ship your products from the U.S. directly to your customers anywhere, worldwide.</span>
            </p>
            <ul class="c8 lst-kix_5yqfxpi9bpdi-0">
                <li class="c1"><span class="c11 c58">Sell Worldwide</span></li>
            </ul>
            <p class="c3"><span>We make it easy for you to ship your orders to over 200 countries worldwide. Let us streamline the shipping process to your customers overseas &ndash;
                    easy, and efficient.. Shipping internationally through us is as simple as any domestic shipment.</span>
            </p>
            <ul class="c8 lst-kix_5yqfxpi9bpdi-0">
                <li class="c1"><span class="c11 c58">Save Big on Shipping</span></li>
            </ul>
            <p class="c3"><span>Take advantage of our discounted volume rates and save 10% to 30% on international shipping.</span>
            </p>
            <ul class="c8 lst-kix_5yqfxpi9bpdi-0">
                <li class="c1"><span class="c11 c58">Free Commercial Invoices</span></li>
            </ul>
            <p class="c3"><span>You provide the information; we prepare the documents for customs. All this will be handled for you at no extra cost.</span>
            </p>
            <ul class="c8 lst-kix_5yqfxpi9bpdi-0">
                <li class="c1"><span class="c11 c58">Discounted Rates</span></li>
            </ul>
            <p class="c3"><span>Pay only the competitive shipping fees we pay plus a discounted handling fee per parcel. Competitive rates are available for volume shippers.</span>
            </p>

            <h1 class="c7 c20 c60"><a name="h.g4hpi7jsu80c"></a><span class="c45 c11">How it works</span></h1>

            <p class="c15 c23"><span class="c12">See how we make it happen&hellip;</span></p>

            <p class="c9 c23"><span class="c12"></span></p>
            <ol class="c8 lst-kix_o75wszhox6kd-0 start" start="1">
                <li class="c1 c28"><span>Incorporate international shipping on your site.</span></li>
                <li class="c1 c28">
                    <span>After an international customer has made a purchase, send the order to:</span>
                </li>
            </ol>
            <p class="c15 c23"><span>HMHShip</span></p>

            <p class="c15 c23"><span>(customer&rsquo;s name) c/o XXX</span></p>

            <p class="c15 c23"><span>23600 Mercantile Rd.</span></p>

            <p class="c15 c23"><span>Suite C-119</span></p>

            <p class="c15 c23"><span>Beachwood, OH 44122</span></p>
            <ol class="c8 lst-kix_o75wszhox6kd-0" start="3">
                <li class="c1 c31"><span>With the order, please make sure you &nbsp;include an invoice and customer&rsquo;s:</span>
                </li>
            </ol>
            <ol class="c8 lst-kix_o75wszhox6kd-1 start" start="1">
                <li class="c27 c28"><span>Full Name</span></li>
                <li class="c27 c28"><span>Address</span></li>
                <li class="c27 c28"><span>Phone</span></li>
                <li class="c27 c28"><span>Email (if possible)</span></li>
                <li class="c27 c28"><span>Invoice </span></li>
            </ol>
            <ol class="c8 lst-kix_o75wszhox6kd-0" start="4">
                <li class="c1 c31">
                    <span>We send the item off to the customer....It&rsquo;s as simple as that!</span>
                </li>
            </ol>
            <h1 class="c7 c20 c29"><a name="h.j80d979yj2hc"></a></h1>

            <h1 class="c7 c20"><a name="h.l5x282odl7io"></a><span class="c45 c11">Shipping plans</span></h1>

            <p class="c15 c23"><span class="c12">Choose your monthly plan&hellip;</span></p>

            <p class="c7 c47"><span class="c12"></span></p><a href="#"
                                                              name="b2de20625d31a0775cbe317dc6654b34403933d0"></a><a
                href="#" name="3"></a>
            <table cellpadding="0" cellspacing="0" class="table table-condensed">
                <tbody>
                <tr class="c39">
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span class="c2">0 &ndash;
                                25 PKGS</span>
                        </p></td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span class="c2">25 &ndash;
                                100 PKGS</span></p></td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span class="c2">100 &ndash;
                                500 PKGS</span></p></td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span class="c2">500 + PKGS</span>
                        </p>
                    </td>
                </tr>
                <tr class="c39">
                    <td class="c0" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">$0.00 Per Month</span></p></td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">$25.00 Per Month</span></p>
                    </td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">$100.00 Per Month</span></p>
                    </td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">$500.00 Per Month</span></p>
                    </td>
                </tr>
                <tr class="c39">
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">+ $7.25 Per Parcel</span></p>
                    </td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">+ $6.00 Per Parcel</span></p>
                    </td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">+ $5.75 Per Parcel</span></p>
                    </td>
                    <td class="c36" colspan="1" rowspan="1"><p class="c10 c23"><span
                                class="c12 c17">+ $5.50 Per Parcel</span></p>
                    </td>
                </tr>
                </tbody>
            </table>
            <p class="c7 c47"><span class="c12"></span></p>

            <p class="c7"><span class="c12">Y</span><span>ou can upgrade your plan at anytime if your shipping volume increases. We will notify you if it would become more economical for you to upgrade. .</span>
            </p>

            <h1 class="c7 c29 c20 c61"><a name="h.2sjs4fwv2pge"></a></h1>

            <h1 class="c7 c20"><a name="h.aakt64su652w"></a><span class="c45 c11">Sign up!</span></h1>

            <p class="c7"><span
                    class="c12">Contact us to sign up or if you have any questions or comments&hellip;</span></p>

            <p class="c22 c3"><span>&nbsp;</span></p>

            <p class="c7"><span class="c11 c55">Sign me up!</span><span>&nbsp;(this should be a link to a form where the customer can a) fill out and submit online, when saved it should be sent to: business@hmhship.com or b) save as pdf)</span>
            </p>

            <p class="c22 c3"><span>&nbsp;</span></p>

            <p class="c7"><span class="c11">Email: </span><span class="c64">business</span><span class="c64">@hmhship.com</span>
            </p>

            <p class="c7"><span class="c11">Phone: </span><span>(888) 888-8888</span></p>

            <p class="c7"><span class="c11">Fax: </span><span>(216) 803-3457</span></p>

            <p class="c7"><span class="c11">Address: </span><span>HMHShip </span><span class="c12">Business</span>
            </p>

            <p class="c22 c3"><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;23600 Mercantile Rd. Suite C-119</span></p>

            <p class="c22 c3"><span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Beachwood, OH 44122</span></p>
            <hr>
            <p class="c47 c31 c37"><span></span></p>

            <p class="c22 c34 c49 c48"><span
                    style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 188.00px; height: 50.00px;"><img
                        alt="" src="images/image01.png"
                        style="width: 188.00px; height: 50.00px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
                        title=""></span><span class="c2 c65">Business</span></p>

            <p class="c7 c48"><span>Please fill out this form and hit &ldquo;submit&rdquo; at the bottom. We will contact you by the end of the next business day with more information.</span>
            </p>


            <h1 class="c7 c20"><a name="h.bl3ck0hgcjrv"></a><span class="c45 c11">Shipping plans</span></h1>

            <p class="c15 c23"><span class="c12">Choose your monthly plan&hellip;</span></p>

            <p class="c9 c23"><span class="c12"></span></p><a href="#"
                                                              name="b2de20625d31a0775cbe317dc6654b34403933d0"></a><a
                href="#" name="4"></a>
            <table cellpadding="0" cellspacing="0" class="c41">
                <tbody>
                <tr class="c39">
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span class="c50 c45 c2">0 &ndash;
                                25 PKGS</span></p></td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span class="c50 c45 c2">25 &ndash;
                                100 PKGS</span></p>
                    </td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span class="c50 c45 c2">100 &ndash;
                                500 PKGS</span></p>
                    </td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span class="c50 c45 c2">500 + PKGS</span>
                        </p>
                    </td>
                </tr>
                <tr class="c39">
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">$0.00 Per Month</span></p></td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">$25.00 Per Month</span></p></td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">$100.00 Per Month</span></p></td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">$500.00 Per Month</span></p></td>
                </tr>
                <tr class="c39">
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">+ $7.25 Per Parcel</span></p>
                    </td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">+ $6.00 Per Parcel</span></p>
                    </td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c12 c17 c35">+ $5.75 Per Parcel</span></p>
                    </td>
                    <td class="c0" colspan="1" rowspan="1"><p class="c5"><span
                                class="c35 c12 c17">+ $5.50 Per Parcel</span></p>
                    </td>
                </tr>
                </tbody>
            </table>
            <p class="c7 c47 c49"><span></span></p><a href="#"
                                                      name="473bbf1cbe012fef67a2f592890c37a0f3c7d233"></a><a
                href="#"
                name="5"></a>
            <table cellpadding="0" cellspacing="0" class="c41">
                <tbody>
                <tr class="c39">
                    <td class="c53" colspan="1" rowspan="1"><p class="c4"><span class="c24">&#9744;</span></p></td>
                    <td class="c53" colspan="1" rowspan="1"><p class="c4"><span class="c24">&#9744;</span></p></td>
                    <td class="c53" colspan="1" rowspan="1"><p class="c4"><span class="c24">&#9744;</span></p></td>
                    <td class="c53" colspan="1" rowspan="1"><p class="c4"><span class="c24">&#9744;</span></p></td>
                </tr>
                </tbody>
            </table>
            <p class="c22 c34 c49"><span>&nbsp;</span></p>

            <p class="c7"><span>You can upgrade your plan at anytime if your shipping volume increases. We will notify you if it would become more economical for you to upgrade.</span>
            </p>

            <p class="c22 c34"><span>&nbsp;</span></p>

            <p class="c7"><span class="c11">Shipping options</span></p>

            <p class="c15 c23"><span
                    class="c12">Choose the default shipping services that you require&hellip;</span>
            </p>

            <p class="c22 c34"><span>&nbsp;</span></p>

            <p class="c7"><span>Insurance
                    <?php echo $this->Form->input('shipping_service', array(
                        'value' => 1,
                        'options' => array(1 => null),
                        'div' => false,
                        'type' => 'radio',
                        'label' => false,
                        'legend' => false,
                        'class' => 'radio-input',
                        'after' => ''
                    )); ?></span></p>

            <p class="c7"><span>Economy &#9744;</span></p>

            <p class="c7"><span>Expedited &#9744;</span></p>

            <div class="clearfix">&nbsp;</div>

            <p class="c7"><span>We can also use a specific shipping carrier that you prefer. If you would like to use a specific shipping carrier please choose from:</span>

            <div class="clearfix">&nbsp;</div>

            <div class="row">
                <div class="col-sm-2 col-sm-offset-2 text-center">
                    <img alt="" src="<?php echo Router::url('/img/carriers/image00.png'); ?>" title="">
                    <br>
                    <?php echo $this->Form->input('carrier', array(
                        'value' => 1,
                        'options' => array(1 => null),
                        'div' => false,
                        'type' => 'radio',
                        'label' => false,
                        'legend' => false,
                        'class' => 'radio-input',
                        'after' => ''
                    )); ?>
                </div>
                <div class="col-sm-2 text-center">
                    <img alt="" src="<?php echo Router::url('/img/carriers/image04.png'); ?>" title="">
                    <br>
                    <?php echo $this->Form->input('carrier', array(
                        'value' => 2,
                        'options' => array(2 => null),
                        'div' => false,
                        'type' => 'radio',
                        'label' => false,
                        'legend' => false,
                        'class' => 'radio-input',
                        'after' => ''
                    )); ?>
                </div>
                <div class="col-sm-2 text-center">
                    <img alt="" src="<?php echo Router::url('/img/carriers/image02.png'); ?>" title="">
                    <br>
                    <?php echo $this->Form->input('carrier', array(
                        'value' => 3,
                        'options' => array(3 => null),
                        'div' => false,
                        'type' => 'radio',
                        'label' => false,
                        'legend' => false,
                        'class' => 'radio-input',
                        'after' => ''
                    )); ?>
                </div>
                <div class="col-sm-2 text-center">
                    <img alt="" src="<?php echo Router::url('/img/carriers/image03.png'); ?>" title="">
                    <br>
                    <?php echo $this->Form->input('carrier', array(
                        'value' => 4,
                        'options' => array(4 => null),
                        'div' => false,
                        'type' => 'radio',
                        'label' => false,
                        'legend' => false,
                        'class' => 'radio-input',
                        'after' => ''
                    )); ?>
                </div>
                <!-- /.col-sm-2 -->
            </div>
            <!-- /.row -->

            <div class="clearfix">&nbsp;</div>


            <p class="c7">If you have your own account with one of the shipping carriers above, we can ship your
                parcel(s) for you and bill your account.</p>

            <div class="clearfix">&nbsp;</div>


            <?php echo $this->Form->input('shipping_carrier', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Shipping carrier')
                )
            )); ?>

            <?php echo $this->Form->input('account_number', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Account number')
                )
            )); ?>

            <?php echo $this->Form->input('zip', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Zip')
                )
            )); ?>

            <div class="clearfix">&nbsp;</div>

            <p class="c7"><span>You can change any of these shipping options at anytime to meet your customer&rsquo;s needs. Just notify us! </span>
            </p>

            <div class="clearfix">&nbsp;</div>

            <h3 class="c7">Company Info</h3>

            <div class="clearfix">&nbsp;</div>

            <p class="c7"><span>Required *</span></p>

            <?php echo $this->Form->input('business', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('*Business')
                )
            )); ?>

            <?php echo $this->Form->input('primary_person', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('*Primary Person:')
                )
            )); ?>

            <?php echo $this->Form->input('phone', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Phone:')
                )
            )); ?>

            <?php echo $this->Form->input('main', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('*Main:')
                )
            )); ?>

            <?php echo $this->Form->input('cell', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Cell:')
                )
            )); ?>

            <?php echo $this->Form->input('fax', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Fax:')
                )
            )); ?>

            <?php echo $this->Form->input('address', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('*Address:')
                )
            )); ?>

            <?php echo $this->Form->input('street_address', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Street Address:')
                )
            )); ?>

            <?php echo $this->Form->input('street_address_2', array(
                'type' => 'text',
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Street Address #2:')
                )
            )); ?>

            <?php echo $this->Form->input('city', array(
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('City:')
                ),
                'class' => 'form-control'
            )); ?>

            <?php echo $this->Form->input('state_zip', array(
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('State/Zip:')
                ),
                'class' => 'form-control'
            )); ?>

            <?php echo $this->Form->input('website', array(
                'label' => array(
                    'class' => 'col-sm-3 control-label',
                    'text' => __('Website:')
                ),
                'class' => 'form-control'
            )); ?>

            <div class="col-sm-12 text-center">
                <?php echo $this->Form->submit(__('Submit'), array('class' => 'btn btn-primary')); ?>
            </div>

            <div class="clearfix">&nbsp;</div>


        </div>
    </div>

<?php echo $this->Form->end(); ?>