<div class="show-after-load">
	<section class="business-form">
		<div class="container">

			<div class="bf-content">
				<div class="top-box-circle">
					<div class="circle"></div>
					<div class="icon">
						<i class="icon-createaccount"></i>
					</div>
				</div>
				<div class="wm-content">
					<div class="text top-box-text">
						<h1>Sign Me Up</h1>
					</div>
					<?php echo $this->Session->flash(); ?>
          <form action="/business-form" class="form-valid" id="BusinessSignupBusinessFormForm" method="post" accept-charset="utf-8">

            <div class="group">
              <div class="row">
                <div class="col col-md-12">
                  <small>*required</small>
                  <h2>Shipping options</h2>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">

                  <span class="text">
                    Shipping plan *
                  </span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('shipping_plan') ? $this->Form->error('shipping_plan') : ''); ?>
                      </span>
                      <select name="shipping_plan" class="required form-control customSelect" required="required">
                        <option value="" selected="">Choose your monthly plan</option>
                        <option value="0">0 – 25 PKGS $0.00 Per Month</option>
                        <option value="25">25 – 100 PKGS $25.00 Per Month</option>
                        <option value="100">100 – 500 PKGS $100.00 Per Month</option>
                        <option value="500">500+ PKGS $500.00 Per Month</option>
                      </select>
                      <span class="msg-error"></span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">
                    Choose your default shipping services that you require
                  </span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-4">
                      <label>
                        <input type="checkbox" name="insured" class="customCheckbox" value="true"> Insured
											</label>
                    </div>
                    <div class="col col-md-4">
                      <label>
                        <input type="checkbox" name="economy" class="customCheckbox" value="true"> Economy
											</label>
                    </div>
                    <div class="col col-md-4">
                      <label>
                        <input type="checkbox" name="expedited" class="customCheckbox" value="true">
												Expedited
											</label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">
                    We can also use a speciﬁc shipping carrier
                    that you prefer. If you would like to use a
                    specific shipping carrier please choose from:
                  </span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <label>
                        <input type="checkbox" name="dhl" class="customCheckbox" value="true">
                          <img src="/img/carriers/dhlexpress.jpg" alt="dhl">
											</label>
                    </div>
                    <div class="col col-md-6">
                      <label>
                        <input type="checkbox" name="ups" class="customCheckbox" value="true">
                          <img src="/img/carriers/ups.jpg" alt="ups">
											</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col col-md-6">
                      <label>
                        <input type="checkbox" name="usps" class="customCheckbox" value="true">
                          <img src="/img/carriers/usps.jpg" alt="usps">
											</label>
                    </div>
                    <div class="col col-md-6">
                      <label>
                        <input type="checkbox" name="fedex" class="customCheckbox" value="true">
                          <img src="/img/carriers/fedex.jpg" alt="fedex">
											</label>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- end group -->

            <div class="group">

              <div class="row">
                <div class="col col-md-12">
                  <h2>ALREADY HAVE AN ACCOUNT</h2>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">
                    If you have your own account with one of
                    these shipping carriers, we can ship for you
                    and bill your account.
                  </span>
                  <small>
                    You can change any of these shipping
                    options at any time to meet your customer’s
                    needs. Just notify us when this needs to be done.
                  </small>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <input type="text" name="account_shipping_carrier" class="form-control"
												   placeholder="Shipping carrier">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col col-md-6">
                      <input type="text" name="account_number" class="form-control"
												   placeholder="Account number">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col col-md-6">
                      <input type="text" name="account_zip" class="form-control" placeholder="Zip code">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- end group -->

            <div class="group noborder">

              <div class="row">
                <div class="col col-md-12">
                  <h2>COMPANY INFORMATION</h2>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">Basic Information</span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('business') ? $this->Form->error('business') : ''); ?>
                      </span>
                      <input type="text" name="business" class="required form-control"
												   placeholder="Business *" required="required">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('primary_person') ? $this->Form->error('primary_person') : ''); ?>
                      </span>
                      <input type="text" name="primary_person" class="required form-control"
												   placeholder="Primary Person *" required="required">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">Contact Information</span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('main_phone') ? $this->Form->error('main_phone') : ''); ?>
                      </span>
                      <input type="text" name="main_phone" class="required form-control"
												   placeholder="Main Phone *" required="required">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-6">
                      <input type="text" name="cell_phone" class="form-control" placeholder="Cellphone">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col col-md-6">
                      <input type="text" name="fax" class="form-control" placeholder="Fax">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-6">
                      <input type="text" name="website" class="form-control" placeholder="Website">
                        <span class="msg-error"></span>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col col-md-6">
                      <input name="email" type="text" class="required form-control"
                                 placeholder="Email*" required="required" />

                    </div>
                  </div>

                </div>
              </div>

              <div class="row">
                <div class="col col-md-4">
                  <span class="text">Address</span>
                </div>
                <div class="col col-md-8">
                  <div class="row">
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('street_address') ? $this->Form->error('street_address') : ''); ?>
                      </span>
                      <input type="text" name="street_address" class="required form-control"
												   placeholder="Street Address *" required="required">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-6">
                      <input type="text" name="street_address2" class="form-control"
												   placeholder="Street Address 2">
                        <span class="msg-error"></span>
                      </div>
                  </div>
                  <div class="row">
                    <div class="col col-md-6">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('city') ? $this->Form->error('city') : ''); ?>
                      </span>
                      <input type="text" name="city" class="required form-control"
												   placeholder="City *" required="required">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-3">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('state') ? $this->Form->error('state') : ''); ?>
                      </span>
                      <input type="text" name="state" class="required form-control"
												   placeholder="State *" required="required">
                        <span class="msg-error"></span>
                      </div>
                    <div class="col col-md-3">
                      <span class="msg-error">
                        <?php echo ($this->Form->isFieldError('zip') ? $this->Form->error('zip') : ''); ?>
                      </span>
                      <input type="text" name="zip" class="required form-control"
												   placeholder="ZIP Code *" required="required">
                        <span class="msg-error"></span>
                      </div>
                  </div>

                  <div class="row">
                    <div class="col col-md-3">
                      <select name="country_id" class="form-control required" required="required" >
                        <option selected="selected">-- Country * --</option>

                        <?php foreach($countries as $id => $name) : ?>
                        <option value="<?php echo $id; ?>">
                          <?php echo $name; ?>
                        </option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- end group -->

            <div class="action text-center">
              <button class="btn-send" type="submit">
                Send <i class="icon-send"></i>
              </button>
            </div>
          </form>

				</div>
			</div>

		</div>
	</section>
</div>
