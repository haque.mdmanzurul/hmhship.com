<style>
   .switch {
   position: relative;
   display: inline-block;
   width: 60px;
   height: 34px;
   }
   .switch input {display:none;}
   .slider {
   position: absolute;
   cursor: pointer;
   top: 0;
   left: 0;
   right: 0;
   bottom: 0;
   background-color: #ccc;
   -webkit-transition: .4s;
   transition: .4s;
   }
   .slider:before {
   position: absolute;
   content: "";
   height: 26px;
   width: 26px;
   left: 4px;
   bottom: 4px;
   background-color: white;
   -webkit-transition: .4s;
   transition: .4s;
   }
   input:checked + .slider {
   background-color: #2196F3;
   }
   input:focus + .slider {
   box-shadow: 0 0 1px #2196F3;
   }
   input:checked + .slider:before {
   -webkit-transform: translateX(26px);
   -ms-transform: translateX(26px);
   transform: translateX(26px);
   }
   /* Rounded sliders */
   .slider.round {
   border-radius: 34px;
   }
   .slider.round:before {
   border-radius: 50%;
   }
   .form-control {
       -webkit-border-radius: 0;
       -moz-border-radius: 0;
       border-radius: 0;
       border: 1px solid #ed8c3b;
       color: #777;
       margin-bottom: 2px !important;
   }
   .btn-send {
       border: 2px solid #ed8c3b;
       color: #fff !important;
       padding: 5px 15px;
       font-size: 16px;
   }
</style>
<div class="show-after-load">
   <section class="talk-to-us">
      <div class="container">
         <div class="mission-content">
            <div class="top-box-circle">
               <div class="circle"></div>
               <div class="icon">
                  <i class="icon-createaccount"></i>
               </div>
            </div>
            <div class="wm-content">
               <div class="text top-box-text">
                  <h1>Register</h1>
               </div>
               <div class="info-contact">
                  <form class="form-valid" method="post">
                  <div class="row">
                     <div class="col-md-2">
                        <strong>Login Info</strong>
                     </div>
                     <div class="col-md-3">
                        <fieldset>

                           <input name="email" type="text" class="required form-control"
                              placeholder="Email*" value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['email'] ?>">
                           <span class="msg-error"><?php echo ($this->Form->isFieldError('User.email') ? $this->Form->error('User.email') : ''); ?></span>
                        </fieldset>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col-md-3">
                        <fieldset>

                           <input id="pwd" name="password" type="password" class="required form-control"
                              placeholder="Password*"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['password'] ?>">
                          <span class="msg-error"><?php echo ($this->Form->isFieldError('User.password') ? $this->Form->error('User.password') : ''); ?></span>
                        </fieldset>
                     </div>
                     <div class="col-md-3" id="pwdRules" style="display: none;">
                        <p>Password Rules</p>
                        <ul>
                           <li>Length must be 8 characters or greater</li>
                           <li>Must contain at least one letter</li>
                           <li>Must contain at least one number</li>
                        </ul>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col-md-3">
                        <fieldset>
                           <input name="confirm_password" type="password" class="required form-control"
                              placeholder="Confirm Password*"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['confirm_password'] ?>">
                           <span class="msg-error" style="display: none;"></span>
                        </fieldset>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <strong>Personal Info</strong>
                     </div>
                     <div class="col col-md-3">

                        <input type="text" name="billing[first_name]" class="form-control required" placeholder="First Name *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['first_name'] ?>">
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.firstname') ? $this->Form->error('Address.firstname') : ''); ?></span>

                     </div>
                     <div class="col col-md-3">

                        <input type="text" name="billing[last_name]"  class="form-control" placeholder="Last Name *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['last_name'] ?>">
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.lastname') ? $this->Form->error('Address.lastname') : ''); ?></span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col col-md-3">
                        <input type="text" name="billing[address1]" id="s1_personalAddress1" class="form-control required" placeholder="Address 1 *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['address1'] ?>">
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.address1') ? $this->Form->error('Address.address1') : ''); ?></span>
                     </div>
                     <div class="col col-md-3">
                        <input type="text" name="billing[address2]" id="s1_personalAddress2" class="form-control" placeholder="Address 2" value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['address2'] ?>">
                        <span class="msg-error"></span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col col-md-3">

                        <input type="text" name="billing[city]" id="s1_personalCity" class="form-control required" placeholder="City *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['city'] ?>">
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.city') ? $this->Form->error('Address.city') : ''); ?></span>
                     </div>
                     <div class="col col-md-3">

                        <input type="text" name="billing[state]" id="s1_personalState" class="form-control required" placeholder="State/Province *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['state'] ?>">
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.adm_division') ? $this->Form->error('Address.adm_division') : ''); ?></span>
                     </div>
                     <div class="col col-md-3 no-margin">

                        <input type="text" name="billing[postal_code]" id="s1_personalPostalcode" class="form-control" placeholder="Postal Code *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['postal_code'] ?>">
                       <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.postal_code') ? $this->Form->error('Address.postal_code') : ''); ?></span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col col-md-3">

                        <select name="billing[country]" id="s1_personalCountry" class="form-control required"  >
                           <option selected="selected">-- Country * --</option>
                           <?php foreach($countries as $id => $name) : ?>
                           <option value="<?php echo $id; ?>" <?php echo (!empty($this->request->data) && $this->request->data['billing']['country'] == $id) ? 'selected' : '' ?>>
                              <?php echo $name; ?>
                           </option>
                           <?php endforeach; ?>
                        </select>
                        <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.country') ? $this->Form->error('Address.country') : ''); ?></span>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                     </div>
                     <div class="col-md-3">
                        <fieldset>

                           <input name="billing[phone]" type="text" class="required form-control" id="phone"
                              placeholder="Phone *"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['billing']['phone'] ?>">
                           <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.postal_code') ? $this->Form->error('Address.postal_code') : ''); ?></span>
                        </fieldset>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col col-md-3">
                        <span>
                        Is this the same as the shipping address?
                        </span>
                     </div>
                     <div class="col-md-9">
                        <div class="row">
                           <div class="col col-md-6">
                              <label class="switch">
                                 <input type="checkbox" name="billing_shipping" id="billing_shipping" <?php echo (empty($_POST) || array_key_exists('billing_shipping',$this->request->data)) ? 'checked="checked"' : '' ?>>
                                 <div class="slider round"></div>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="group last shipInformation" style="<?php echo (empty($_POST) || array_key_exists('billing_shipping',$this->request->data)) ? 'display:none' : ''?>">
                     <div class="row">
                        <h2>Shipping information</h2>
                        <br/>
                        <div class="col col-md-3">
                           <span>
                           Who is going to receive the parcel?
                           </span>
                        </div>
                        <div class="col-md-9">
                           <div class="row">
                              <div class="col col-md-6">

                                 <input type="text" name="shipping[first_name]"
                                    class="form-control required" placeholder="First name *">
                                 <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.firstname') ? $this->Form->error('Address.firstname') : ''); ?></span>
                              </div>
                              <div class="col col-md-6">

                                 <input type="text" name="shipping[last_name]"
                                    class="form-control required" placeholder="Last name *">
                                 <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.lastname') ? $this->Form->error('Address.lastname') : ''); ?></span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row 1 -->
                     <div class="row">
                        <div class="col col-md-3">
                           <span>
                           What is the address you want your parcel to be sent to?
                           </span>
                        </div>
                        <div class="col col-md-9">
                           <div class="row">
                              <div class="col col-md-6">

                                 <input type="text" name="shipping[address1]"
                                    class="form-control required" placeholder="Address 1 *">
                                 <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.address1') ? $this->Form->error('Address.address1') : ''); ?></span>
                              </div>
                              <div class="col col-md-6">
                                 <input type="text" name="shipping[address2]"
                                    class="form-control" placeholder="Address 2">
                              </div>
                           </div>
                           <div class="row">
                              <div class="col col-md-6">

                                 <input type="text" name="shipping[city]"
                                    class="form-control required" placeholder="City *">
                                 <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.city') ? $this->Form->error('Address.city') : ''); ?></span>
                              </div>
                              <div class="col col-md-6">
                                 <div class="row row-inner">
                                    <div class="col col-md-6">

                                       <input type="text" name="shipping[state]"
                                          class="form-control required" placeholder="State/Province *">
                                       <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.adm_division') ? $this->Form->error('Address.adm_division') : ''); ?></span>
                                    </div>
                                    <div class="col col-md-6">

                                       <input type="text" name="shipping[postal_code]"
                                          class="form-control" placeholder="Postal Code *">
                                      <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.postal_code') ? $this->Form->error('Address.postal_code') : ''); ?></span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col col-md-6">

                                 <select name="shipping[country]" class="form-control required">
                                    <option selected="selected" value="">-- Country * --</option>
                                    <?php foreach($countries as $id => $name) : ?>
                                    <option value="<?php echo $id; ?>"><?php echo $name; ?></option>
                                    <?php endforeach; ?>
                                 </select>
                                 <span class="msg-error"><?php echo ($this->Form->isFieldError('Address.country_id') ? $this->Form->error('Address.country_id') : ''); ?></span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- row 5 -->
                  </div>
                  <!-- end shipping -->
                  <div class="row">
                     <div class="col col-md-3">
                        <span>
                        How did you hear about us?
                        </span>
                     </div>
                     <div class="col-md-9">
                        <div class="row">
                           <div class="col col-md-6">
                              <input type="text" name="how_did_you_hear"
                                 class="form-control" placeholder="">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col col-md-3">
                     </div>
                     <div class="col-md-9" style="text-align: right;">
                        <div class="row">
                           <div class="col col-md-6">
                              <input type="checkbox"/>
                              Agree to <a href="/terms" target="_new"
                                 title="Read our Terms and Conditions">Terms and Conditions</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="action">
                     <button class="btn-send" style="background-color: #e98d48; border: none;">Register</button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<script>
   $("#billing_shipping").bind( "change", function() {
       if( $(this).is(':checked') )
           $(".shipInformation").hide();
       else
           $(".shipInformation").show();
     });

   // only allow numbers in Phone field
   $('#phone').keyup(function () {
       this.value = this.value.replace(/[^0-9+\.]/g,'');
   });

   $('#phone').focus(function() {
       if (this.value.length == 0 || this.value.substr(0,1) != "+")
           this.value = "+" + this.value;
   });

   $("#pwd").keyup('click', function(){
     $("#pwdRules").css('display', 'inline')
   });
</script>
