<style>
  a:hover {
    text-decoration: none;
  }
</style>
<div class="show-after-load">
    <section class="form-quickship">
        <div class="container">


            <div class="top-box-circle">
                <div class="circle"></div>
                <div class="icon">
                    <i class="icon-quickship"></i>
                </div>
            </div>
            <div class="wrap" id="quickshipFrm">

                <div class="text top-box-text">
                    <h1>Shipping Calculator — Save Big on Parcel Forwarding</h1>
                    <br/>
                    <div style="text-align: left;">Use our handy shipping calculator to find out just you just how much you can save by choosing HMHShip for all your parcel-forwarding needs! Fill in the location, package, and carrier details for your order, and it will provide you with an accurate estimate of your shipping charges.</div>
                    <br/>
                    <div style="text-align: left;">When you’re ready, you’ll be able to quickly arrange your order <a href="/quickship#">here</a>. If you have any questions about our shipping services that you’d like to ask before you buy, <a href="/talk-to-us">please contact us here.</a></div>

                    <h2>Calculate Your Shipping Costs</h2>

                    <div class="wizard row wizard-box simpleSteps">
                        <ul class="wizard-steps" style="padding-left:300px;">
                            <li data-target="#step1" <?php if (!isset($package_rates)) echo "class='active'"; ?>>
                                <span class="step">1</span>
                                <span class="title">Package <br> Details</span>
                            </li>
                            <li data-target="#step2" <?php if (isset($package_rates)) echo "class='active'"; ?>>
                                <span class="step">2</span>
                                <span class="title">Shipment <br> Rates</span>
                            </li>

                        </ul>
                    </div>
                </div>

                <?php if (!isset($package_rates)) { ?>

                <form class="form-valid" method="post">

                    <div class="row"><strong>Location details</strong></div>
                    <div class="row">
                        <div class="col-md-2" style="margin-top:10px;">
                            Where would the parcel be going to?
                        </div>

                        <div class="col-md-3">
                            <fieldset>

                                <input name="city" type="text" class="required form-control"
                                       placeholder="City*" value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['city'] ?>">
                                <span class="msg-error"><?php echo ($this->Session->read('city') ? $this->Session->read('city') : ''); ?></span>
                            </fieldset>
                        </div>

                        <div class="col-md-3">
                            <fieldset>

                                <input name="state" type="text" class="required form-control"
                                       placeholder="State/Province*"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['state'] ?>">
                                       <span class="msg-error"><?php echo ($this->Session->read('state') ? $this->Session->read('state') : ''); ?></span>
                            </fieldset>
                        </div>

                        <div class="col-md-3">
                            <fieldset>

                                <input name="postal_code" type="text" class="required form-control"
                                       placeholder="Postal Code*" value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['postal_code'] ?>">
                                <span class="msg-error"><?php echo ($this->Session->read('postal_code') ? $this->Session->read('postal_code') : ''); ?></span>
                            </fieldset>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">

                        </div>
                        <div class="col col-md-3">
                            <select name="country" id="country" class="form-control required">
                                <option value="0">-- Country * --</option>

                                <?php foreach ($countries as $id => $name) : ?>
                                    <option value="<?php echo $id; ?>" <?php echo (!empty($this->request->data) && $this->request->data['country'] == $id) ? 'selected' : '' ?>>
                                        <?php echo $name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <span class="msg-error"><?php echo ($this->Session->read('country') ? $this->Session->read('country') : ''); ?></span>
                        </div>
                    </div>


                    <div class="row"><strong>Package details</strong></div>
                    <div class="row">
                        <div class="col-md-2">
                            Please enter your parcel details here.
                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">
                            <fieldset>

                                <input name="weight" type="number" step=".01" class="required form-control"
                                       placeholder="Weight*"  value="<?php echo (empty($this->request->data)) ? '' : $this->request->data['weight'] ?>">
                                <span class="msg-error"><?php echo ($this->Session->read('weight') ? $this->Session->read('weight') : ''); ?></span>
                            </fieldset>
                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">

                                <select name="weight_unit"

                                        class="s3_copyUnit form-control required">
                                    <option value="lb">lb</option>
                                    <option value="kg" <?php echo (!empty($this->request->data) && $this->request->data['weight_unit'] == 'kg') ? 'selected' : '' ?>>kg</option>
                                </select>

                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">
                          <input type="number" name="length" step=".01"
                                     class="form-control required" placeholder="Length *" value="<?php echo empty($this->request->data['length']) ? '' : $this->request->data['length'] ?>">
                                     <span class="msg-error"><?php echo ($this->Session->read('length') ? $this->Session->read('length') : ''); ?></span>
                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">
                                <input type="number" name="width" step=".01"
                                       class="form-control required" placeholder="Width *" value="<?php echo (empty($this->request->data['width'])) ? '' : $this->request->data['width'] ?>">
                                       <span class="msg-error"><?php echo ($this->Session->read('width') ? $this->Session->read('width') : ''); ?></span>

                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">
                                <input type="number" name="height"  step=".01"
                                       class="form-control required" placeholder="Height *" value="<?php echo empty($this->request->data['height']) ? '' : $this->request->data['height'] ?>">
                                       <span class="msg-error"><?php echo ($this->Session->read('height') ? $this->Session->read('height') : ''); ?></span>
                        </div>

                        <div class="col col-form-field col-md-2" style="width:13%;">
                                <select name="size_unit"
                                        class="form-control required"
                                        placeholder="Unit *">
                                    <option value="in">in</option>
                                    <option value="cm" <?php echo (!empty($this->request->data) && $this->request->data['size_unit'] == 'cm') ? 'selected' : '' ?>>cm</option>
                                </select>
                        </div>

                    </div>
                    <br/>
                    <div class="row"><input type="submit" class="btn btn-success" value="Enter" style="float:right;margin-right:65px;width:100px;"/></div>

                </form>

                <?php } else { // $package_rates is set

                    if (count($package_rates) <= 0) {
                        echo("<h3>No rates found.</h3>");
                    } else {
                        // show rates
                ?>

                    <table class="table table-hover qs-shipment-table">
                            <thead >
                            <tr>
                                    <th>Carrier</th>
                                    <th>Service</th>
                                    <th>Price</th>
                                    <th>Est. Delivery</th>
                            </tr>
                            </thead>
                            <tbody class="package_rate_list">
                            <?php foreach ($package_rates as $rate) : ?>
                            <tr>
                                <td class="text-center">
                                    <img
                                         src="/img/carriers/<?php echo strtolower($rate["carrier"]);?>.jpg">
                                </td>

                                <td class="title service"><?php echo $rate["service"];?></td>

                                <td class="price">EST <span><?php echo $rate["rate"];?> + $7.25 Handling</span></td>

                              <td>
                                <?php
                                echo $rate["delivery_date"];
                                   ?>
                              </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                    </table>
                     <?php } ?>

                    <br/>
                    <div class="row">
                      <?php if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) { ?>
                      <span style="text-align:left;margin-left:250px;">
                        <a href="/quickship">
                          <input class="btn-send" type="button" value="Submit a New Shipment" id="btnNewPackage">
                        </a>
                      </span>
                      <span style="width:200px">&nbsp;</span>
                      <span style="text-align:right;">
                        <a href="/Users/myaccount">
                          <input class="btn-send" type="button" value="Access My Account" id="btnMyAccount">
                        </a>
                      </span>
                      <span style="width:200px">&nbsp;</span>
                      <?php } ?>
                      <a href="/shipping-calculator">
                            <button class="btn btn-success" type="button" style="float:right;margin-right:65px;width:100px;">Reset</button>
                        </a>
                    </div>

                <?php } ?>

                <br/>
                <div class="text top-box-text">
                <h2>How Do We Do It? With the Right Relationships</h2>

                <div style="text-align: left;">Our prices are some of the most competitive in shipping, but there’s no catch. Those prices come from our great relationships with all the major international shipping companies. By working closely with shippers from all over the world, we can negotiate better rates for our own parcels and then pass those savings onto you.</div>
                <br/>
                <div style="text-align: left;">Another advantage you’ll enjoy because of our relationships is the freedom to receive packages in almost any country in the world. Wherever it needs to go, we have a way to get it there.</div>

                <br/>
                <h2>Save Even More With Discounted Volume Rates!</h2>

                <div style="text-align: left;">If you enjoy the savings on single parcels, you’re going to love our discounted volume rates. When you ship a high volume of parcels through HMHShip, you can save as much as 10%-50% on international shipping rates. Provide your details on our order form to see rates for your specific order, or contact us directly for more information on volumes that are eligible for discounted rates.</div>
                </div>

            </div>
        </div>

    </section>
</div>
<script type="text/javascript">
 jQuery(document).ready(function(){

 });
</script>
