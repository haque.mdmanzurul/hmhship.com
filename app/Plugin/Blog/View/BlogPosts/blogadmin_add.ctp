<div class="actions">
  <h3>
    <?php echo __('Actions'); ?>
  </h3>
  <ul>

    <li>
      <?php echo $this->Html->link(__('List Blog Posts'), array('action' => 'index'));?>
    </li>
    <li>
      <?php echo $this->Html->link(__('List Blog Post Categories'), array('controller' => 'blog_post_categories', 'action' => 'index')); ?>
    </li>
    <li>
      <?php echo $this->Html->link(__('New Blog Post Category'), array('controller' => 'blog_post_categories', 'action' => 'add')); ?>
    </li>
    <li>
      <?php echo $this->Html->link(__('List Blog Post Tags'), array('controller' => 'blog_post_tags', 'action' => 'index')); ?>
    </li>
    <li>
      <?php echo $this->Html->link(__('New Blog Post Tag'), array('controller' => 'blog_post_tags', 'action' => 'add')); ?>
    </li>
  </ul>
</div>

<div class="blogPosts form">
<?php echo $this->Form->create('BlogPost');?>
	<fieldset>
		<legend><?php __('Add Blog Post'); ?></legend>
	<?php
		echo $this->Form->input('title', array('style' => 'width:700px;'));
		echo $this->Form->input('slug', array('style' => 'width:700px;'));
		echo $this->Form->input('summary');
		echo $this->Form->input('body', array('width' => 1000, 'cols' => 100, 'rows' => 30));
		echo $this->Form->input('published');
		echo $this->Form->input('sticky');
		echo $this->Form->input('in_rss', array('default' => 1));
		echo $this->Form->input('meta_title', array('style' => 'width:700px;'));
		echo $this->Form->input('meta_description', array('style' => 'width:700px;'));
		echo $this->Form->input('meta_keywords', array('style' => 'width:700px;'));
		echo $this->Form->input('BlogPostCategory');
		echo $this->Form->input('BlogPostTag');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>


<script src='/js/tinymce/tinymce.min.js'></script>
<script>
  $(document).ready(function() {

  tinymce.init({
  selector: 'textarea',
  height: 360,
  menubar: false,
  plugins: [
  'advlist autolink lists link image charmap print preview anchor',
  'searchreplace visualblocks code fullscreen',
  'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
  content_css: '//www.tinymce.com/css/codepen.min.css',
  relative_urls : false,
  document_base_url: '/blog/',
  paste_data_images: true
  });


  });
</script>