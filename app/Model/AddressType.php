<?php

App::uses('AppModel', 'Model');

/**
 * Class AddressType
 */
class AddressType extends AppModel
{
    public static function get_all()
    {
        $o = ClassRegistry::init('AddressType');
        $data = $o->find('all');
        return Hash::combine($data, '{n}.AddressType.key', '{n}.AddressType.id');
    }
}
