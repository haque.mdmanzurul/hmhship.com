<?php

App::uses('AppModel', 'Model');

/**
 * Class ItemType
 */
class ItemType extends AppModel
{
	
	public $hasMany = array(
		'Item' => array(
			'className' => 'Item',
			'foreignKey' => 'type_id'
		)
	);

    /**
     * @return array
     */
    public static function get_all()
    {
        $o = new ItemType();
        return Hash::combine($o->find('all'), '{n}.ItemType.id', '{n}.ItemType.description');
    }
    
    /**
     * @param $id
     *
     * @return object
     */
    public static function get($id)
    {
        $o = new ItemType();
        $data = $o->findById((int)$id);
        
        return (object)$data['ItemType'];
    }

    /**
     * todo: clear cache
     * @return array
     */
    public function getTypes()
    {
        if(($types = Cache::read('item_types')) === false) {
            $types = Hash::combine($this->find('all'), '{n}.ItemType.id', '{n}.ItemType.description');
            Cache::write('item_types', $types);
        }
        return $types;
    }
}
