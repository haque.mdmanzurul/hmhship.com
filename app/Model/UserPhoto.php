<?php

App::uses('AppModel', 'Model');

/**
 * Class Item
 *
 * @property Package $Package
 * @property ItemType $ItemType
 */
class Photo extends AppModel
{
	/**
	 * @var array
	 */
	public $belongsTo = array(
		'User'
	);
}
