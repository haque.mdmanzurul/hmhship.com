<?php

App::uses('AppModel', 'Model');

/**
 * Class Country
 */
class Country extends AppModel
{

    public $actsAs = array('Containable');

    /**
     * @param $id
     * @return mixed
     */
    public static function get_code($id)
    {
        if (($code = Cache::read('country_' . $id)) === false) {
            $o = new Country();
            $data = $o->findById((int)$id);
            $code = $data['Country']['code'];
            Cache::write('country_' . $id, $code);
        }


        return $code;
    }

    /**
     * Returns a list of countries indexed by id
     * @return array
     */
    public function getList()
    {
        if (($countries = Cache::read('countries_list')) === false) {
            $countries = Hash::combine(
                $this->find('all', array('order' => array('Country.name'))),
                '{n}.Country.id', '{n}.Country.name'
            );
            Cache::write('countries_list', $countries);
        }
        return $countries;
    }
}
