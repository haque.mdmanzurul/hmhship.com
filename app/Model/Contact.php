<?php

App::uses('AppModel', 'Model');

/**
 * Class Contact
 */
class Contact extends AppModel
{

	public $useTable = false;

	public $validate = array(
		'contact_name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter your name',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contact_email' => array(
			'isemail' => array(
				'rule' => array('email'),
				'message' => 'Enter your email address',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contact_subject' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter a subject',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'contact_message' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter a message',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}