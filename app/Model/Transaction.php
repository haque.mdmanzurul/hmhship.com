<?php

App::uses('AppModel', 'Model');

/**
 * Class Transaction
 *
 * @property TransactionType $TransactionType
 * @property Address $Address
 */
class Transaction extends AppModel
{

    public $belongsTo = array(
        'TransactionType' => array(
            'className' => 'TransactionType',
            'foreignKey' => 'type_id'
        )
    );

    public $hasMany = array(
        'Shipment' => array(
            'className' => 'Shipment',
            'foreignKey' => 'transaction_id'
        ),
        'ReferenceTransaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'reference_tran'
        ),
        'RefundTransaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'refund_tran'
        ),
        'Photos' => array(
            'className' => 'Photo',
            'foreignKey' => 'transaction_id'
        ),
        'Notes' => array(
            'className' => 'Note',
            'foreignKey' => 'transaction_id'
        )
    );


    public $hasAndBelongsToMany = array(
        'AddressBilling' => array(
            'className' => 'Address',
            'joinTable' => 'addresses_transactions',
            'foreignKey' => 'transaction_id',
            'associationForeignKey' => 'address_id',
            'unique' => 'keepExisting',
            'conditions' => array(
                'AddressBilling.address_type_id' => array(2, 4)
            )
        ),
        'AddressShipping' => array(
            'className' => 'Address',
            'joinTable' => 'addresses_transactions',
            'foreignKey' => 'transaction_id',
            'associationForeignKey' => 'address_id',
            'unique' => 'keepExisting',
            'conditions' => array(
                'AddressShipping.address_type_id' => array(3)
            )
        )
    );

    /**
     * Ghost Shipment - customer needs to verify address info
     */
    const STATUS_GHOST = -1;

    /**
     * Default for newly created transaction
     */
    const STATUS_AWAITING_FEE = 0;

    /**
     * Waiting for Package (after handling fee has been paid)
     */
    const STATUS_WAITING_PACKAGE = 1;

    /**
     * Package has been received by HMHShip
     */
    const STATUS_PACKAGE_RECEIVED = 2;

    /**
     * Awaiting shipping payment from customer
     */
    const STATUS_AWAITING_PAYMENT = 3;

    /**
     * Shipping payment received from customer
     */
    const STATUS_PAYMENT_RECEIVED = 4;

    /**
     * Package sent by HMHShip
     */
    const STATUS_PACKAGE_SENT = 5;

    /**
     * transaction types
     */
    const TYPE_QUICKSHIP = 1;
    const TYPE_REFERENCE = 2;
    const TYPE_REFUND = 3;
    const TYPE_GHOST = 4;

    protected $_statuses = array(
        0 => 'Awaiting fee',
        1 => 'Waiting for Package',
        2 => 'Package Received',
        3 => 'Awaiting Payment',
        4 => 'Payment Received',
        5 => 'Package Sent'
    );

    public function __construct($id = false, $table = null, $ds = null)
    {
        parent::__construct($id, $table, $ds);
        $this->virtualFields['date_utc'] = 'DATE_FORMAT(' . sprintf("%s.date", $this->alias) . ", \"%Y%m%dT%H:%i:%s\")";
        $this->virtualFields['created_utc'] = 'DATE_FORMAT(' . sprintf("%s.created", $this->alias) . ", \"%Y%m%dT%H:%i:%s\")";
    }

    /**
     * @param $data
     * @param array $addresses
     *
     * @return mixed
     * @throws Exception
     */
    public function createTransaction($data, $addresses = array())
    {
        // TODO: save insurance and other options
        $data = array(
            'type_id' => $data['type_id'],
            'code' => $data['code'],
            'date' => $data['date'],
            'unread' => 1,
            'status' => 0,
            'expedited' => $data['expedited'],
            'insured' => $data['insured'],
            'repackaged' => $data['repackaged'],
            'special_instructions' => $data['special_instructions'],
            'ship_fast' => $data['ship_fast'],
            'ship_cheap' => $data['ship_cheap'],
            'reference_tran' => (array_key_exists('reference_tran', $data) ? $data['reference_tran'] : ''),
            'refund_tran' => (array_key_exists('refund_tran', $data) ? $data['refund_tran'] : ''),
            'amount' => (array_key_exists('amount', $data) ? $data['amount'] : ''),
            'charge_now' => $data['charge_now'],
            'request_photo' => isset($data['request_photo']) ? $data['request_photo'] : 0
        );

        if (array_key_exists('billing', $addresses)) {
            $data['Address'][] = $addresses['billing'];
        }
        if (array_key_exists('shipping', $addresses)) {
            $data['Address'][] = $addresses['shipping'];
        }

        $this->create();
        $this->bindModel(array('hasAndBelongsToMany' => array(
            'Address' => array(
                'className' => 'Address',
                'joinTable' => 'addresses_transactions',
                'foreignKey' => 'transaction_id',
                'associationForeignKey' => 'address_id',
                'unique' => 'keepExisting'
            )
        )));
        return $this->save($data);
    }

	public function update_status_by_id($id, $type)
	{

		$transaction = $this->find('first', array(
			'fields' => array('id'),
			'conditions' => array(
				$this->alias . '.id' => $id
			),
			'contain' => false
		));
		if (!$transaction) {
			return false;
		}
		$this->id = $transaction[$this->alias]['id'];
		return $this->saveField('type_id', $type);
	}

    /**
     * @param $code
     * @param $status
     *
     * @return mixed
     * @throws Exception
     */
    public function update_status($code, $status)
    {

        $transaction = $this->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                $this->alias . '.code' => $code
            ),
            'contain' => false
        ));
        if (!$transaction) {
            return false;
        }
        $this->id = $transaction[$this->alias]['id'];
        return $this->saveField('status', $status);
    }

    public function update_code($code, $newcode)
    {

        $transaction = $this->find('first', array(
            'fields' => array('id'),
            'conditions' => array(
                $this->alias . '.code' => $code
            ),
            'contain' => false
        ));
        if (!$transaction) {
            return false;
        }
        $this->id = $transaction[$this->alias]['id'];
        return $this->saveField('code', $newcode);
    }

	public function update_type($id, $status)
	{

		$transaction = $this->find('first', array(
			'fields' => array('id'),
			'conditions' => array(
				$this->alias . '.id' => $id
			),
			'contain' => false
		));
		if (!$transaction) {
			return false;
		}
		$this->id = $transaction[$this->alias]['id'];
		return $this->saveField('status', $status);
	}


    /**
     * @param $id
     *
     * @return array|null
     */
    public function getTransaction($id)
    {
        $transaction = $this->find('first', array(
            'conditions' => array(
                'Transaction.id' => $id
            ),
            'contain' => array(
                'AddressBilling' => array(
                    'AddressType',
                    'Country'
                ),
                'AddressShipping' => array(
                    'AddressType',
                    'Country'
                ),
                'TransactionType',
                'Shipment' => array(
                    'Address',
                    'Package' => array(
                        'Item' => array(
                            'Country',
                            'ItemType'
                        ),
                        'fields' => array(
                            'id', 'shipment_id', 'weight_lb', 'weight_kg', 'width_in', 'width_cm', 'height_in', 'height_cm',
                            'length_in', 'length_cm', 'carrier', 'in_tracking', 'easypost_rate_id', 'easypost_rate_price', 'easypost_rate_description',
                            'created', 'modified', 'out_tracking', 'out_carrier', 'consolidated_packages'
                        )
                    )
                ),
                'Photos',
                'Notes',
                'ReferenceTransaction',
                'RefundTransaction'
            )
        ));



        return $transaction;
    }

    /**
     * List all transactions by type (quickship by default)
     *
     * @param int $type
     *
     * @return array|null
     */
    public function listTransactions($type = self::TYPE_QUICKSHIP)
    {
        // only used by Admin?  List only Unregistered Trans


        $trans = $this->find('all', array(
            'conditions' => array(
                 $this->alias . '.type_id' => $type,
                 $this->alias . '.status >' => Transaction::STATUS_AWAITING_FEE,
                 $this->alias . '.user_id IS NULL'
            ),
            'order' => array(
                $this->alias . '.id' => 'desc'
            )
        ));

        $user_emails = array();
        if (count($trans) > 0) {
            for($i = count($trans) - 1; $i >= 0; $i--) {
                if (!isset($trans[$i]['AddressBilling'][0]['email'])) continue;
                $user_email = base64_encode($trans[$i]['AddressBilling'][0]['email']);
                CakeLog::write('debug user id', print_r($user_email, true));
                if (count($user_emails) == 0 || !isset($user_emails[$user_email]))
                    $user_emails[$user_email] = 1;
                else
                    $user_emails[$user_email] += 1;

                $trans[$i]['Transaction']['user_id_repeat'] = $user_emails[$user_email];
            }
        }


        //$log = $this->getDataSource()->getLog(false, false);
        //CakeLog::write('debug', print_r($log, true));
        //CakeLog::write('debug',print_r($trans, true));

        return $trans;


    }

    public function listRegisteredTransactions($type = self::TYPE_QUICKSHIP)
    {
        // only used by Admin
        // List only Registered Trans
        $trans = $this->find('all', array(
            'conditions' => array(
                 $this->alias . '.type_id' => $type,
                 $this->alias . '.status >' => Transaction::STATUS_AWAITING_FEE,
                 $this->alias . '.user_id IS NOT NULL'
            ),
            'order' => array(
                 $this->alias . '.id' => 'desc'
            )
        ));

        $user_ids = array();
        if (count($trans) > 0) {
            for($i = count($trans)-1; $i >= 0; $i--) {
                $user_id = $trans[$i]['Transaction']['user_id'];
                CakeLog::write('debug user id', print_r($user_id, true));
                if (count($user_ids) == 0 || !isset($user_ids[$user_id]))
                    $user_ids[$user_id] = 1;
                else
                    $user_ids[$user_id] += 1;

                $trans[$i]['Transaction']['user_id_repeat'] = $user_ids[$user_id];
            }
        }


        //$log = $this->getDataSource()->getLog(false, false);
        //CakeLog::write('debug', print_r($log, true));
        //CakeLog::write('debug',print_r($trans, true));

        return $trans;

    }

    public function listRegisteredGhostTransactions($type = self::TYPE_GHOST)
    {
        // only used by Admin
        // List only Registered Trans
        $trans = $this->find('all', array(
            'conditions' => array(
                $this->alias . '.type_id' => $type,
                $this->alias . '.user_id IS NOT NULL'
            ),
            'order' => array(
                $this->alias . '.id' => 'desc'
            )
        ));

        $user_ids = array();
        $ghost_trans = array();
        if (count($trans) > 0) {
            for($i = count($trans)-1; $i >= 0; $i--) {
                $user_id = $trans[$i]['Transaction']['user_id'];
                CakeLog::write('debug user id', print_r($user_id, true));
                if (count($user_ids) == 0 || !isset($user_ids[$user_id]))
                    $user_ids[$user_id] = 1;
                else
                    $user_ids[$user_id] += 1;

                $trans[$i]['Transaction']['user_id_repeat'] = $user_ids[$user_id];
            }

            for($i = count($trans)-1; $i >= 0; $i--) {
                array_push($ghost_trans, $trans[$i]);
            }
        }


        //$log = $this->getDataSource()->getLog(false, false);
        //CakeLog::write('debug', print_r($log, true));

        return $ghost_trans;

    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->_statuses;
    }

    /**
     * Returns a number of items with a specific status.
     *
     * @param $status
     *
     * @return array|null
     */
    public function countByStatus($status)
    {
        // displayed on Admin Quickship tab
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.type_id' => 1,
                $this->alias . '.status' => $status,
                $this->alias . '.user_id IS NULL'
            )
        ));
    }

     public function countRegisteredByStatus($status)
    {
        // displayed on Admin Shipments tab
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.type_id' => 1,
                $this->alias . '.status' => $status,
                $this->alias . '.user_id IS NOT NULL'
            )
        ));
    }


    public function countRegisteredUnreadByStatus($status)
    {
        // displayed on Admin Shipments tab
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.type_id' => 4,
                $this->alias . '.status' => 2,
                $this->alias . '.unread' => 1,
                $this->alias . '.user_id IS NOT NULL'
            )
        ));
    }

    /**
     * @param $type
     *
     * @return array|null
     */
    public function countUnreadByType($type)
    {
        // only Count unregistered Transactions
        return $this->query("SELECT COUNT(*)
        FROM transactions T
        WHERE unread = 1 AND type_id = $type AND status > 0 AND user_id IS NULL")[0][0]['COUNT(*)'];

    }


    /**
     * @param $type
     *
     * @return array|null
     */
    public function countUnreadByStatus($status)
    {
        // only Count unregistered Transactions
        return $this->query("SELECT COUNT(*)
        FROM transactions T
        WHERE unread = 1 AND status = '". $status . "' AND status > 0 AND user_id IS NOT NULL")[0][0]['COUNT(*)'];

    }

     public function countRegisteredUnreadByType($type)
    {
        return $this->query("SELECT COUNT(*)
        FROM transactions T
        WHERE unread = 1 AND type_id = $type AND status > 0  AND user_id IS NOT NULL")[0][0]['COUNT(*)'];

    }

    /**
     * @param $transaction_detail
     */
    public function processPackageUnits(&$transaction_detail)
    {
        for ($i = 0; $i < count($transaction_detail['Shipment']); $i++) {

            // Package weight
            $transaction_detail['Shipment'][$i]['Package']['weight'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['weight_kg']) ? $transaction_detail['Shipment'][$i]['Package']['weight_kg'] : $transaction_detail['Shipment'][$i]['Package']['weight_lb']);
            $transaction_detail['Shipment'][$i]['Package']['weight_unit'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['weight_kg']) ? 'kg' : 'lb');

            // Package Width
            $transaction_detail['Shipment'][$i]['Package']['width'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['width_cm']) ? $transaction_detail['Shipment'][$i]['Package']['width_cm'] : $transaction_detail['Shipment'][$i]['Package']['width_in']);
            $transaction_detail['Shipment'][$i]['Package']['width_unit'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['width_cm']) ? 'cm' : 'in');

            // Package Height
            $transaction_detail['Shipment'][$i]['Package']['height'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['height_cm']) ? $transaction_detail['Shipment'][$i]['Package']['height_cm'] : $transaction_detail['Shipment'][$i]['Package']['height_in']);
            $transaction_detail['Shipment'][$i]['Package']['height_unit'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['height_cm']) ? 'cm' : 'in');

            // Package Length
            $transaction_detail['Shipment'][$i]['Package']['length'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['length_cm']) ? $transaction_detail['Shipment'][$i]['Package']['length_cm'] : $transaction_detail['Shipment'][$i]['Package']['length_in']);
            $transaction_detail['Shipment'][$i]['Package']['length_unit'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['length_cm']) ? 'cm' : 'in');

            for ($item_index = 0; $item_index < count($transaction_detail['Shipment'][$i]['Package']['Item']); $item_index++) {
                // Item weight
                $transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight_kg']) ? $transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight_kg'] : $transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight_lb']);
                $transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight_unit'] = (is_numeric($transaction_detail['Shipment'][$i]['Package']['Item'][$item_index]['weight_kg']) ? 'kg' : 'lb');
            }
        }
    }
}
