<?php

App::uses('AppModel', 'Model');

/**
 * Class Transaction
 *
 * @property TransactionType $TransactionType
 * @property Address $Address
 */
class BusinessSignup extends AppModel
{
    public $belongsTo = array('Country');

    	public $validate = array(
            'shipping_plan' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Select a Shipping Plan',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'business' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Business Name',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'primary_person' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Primary Person',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
            'main_phone' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Main Phone Number',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            ),
             'street_address' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Street Address',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            ),
             'city' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter City',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            ),
             'state' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter State',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            ),
             'zip' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Zip Code',
				//'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
            )
	);

    public function listBusinessSignups()
    {
        return $this->find('all', array(          
            'order' => array(
                'BusinessSignup.created' => 'desc'
            )
        ));
    }
    
    public function createBusinessSignup($data)
    {        
        
        $this->create();
        
        return $this->save($data);
    }
    
     public function countUnread()
    {
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.read' => false                
            )
        ));
    }
    
     public function getBusinessSignup($id)
    {
        return $this->find('first', array(
            'conditions' => array(
                'BusinessSignup.id' => $id
            )
        ));
    }

}