<?php
App::uses('AppModel', 'Model');

/**
 * TransactionType Model
 *
 */
class TransactionType extends AppModel
{

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'description';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'description' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
