<?php

App::uses('AppModel', 'Model');

/**
 * Class Item
 *
 * @property Package $Package
 * @property ItemType $ItemType
 */
class Item extends AppModel
{


	/**
	 * @var array
	 */
	public $belongsTo = array(
		'Package',
		'ItemType' => array(
			'className' => 'ItemType',
			'foreignKey' => 'type_id'
		),
		'Country'
	);

	/**
	 * @param $package_id
	 * @param $item
	 * @throws Exception
	 */
	public function addItem($package_id, $item)
	{
		$this->create();
		$this->save(array(
			'package_id' => $package_id,
			'description' => isset($item['description']) ? $item['description'] : '',
			'quantity' => isset($item['quantity']) ? $item['quantity'] : 1,
			'weight_lb' => ($item['weight_unit'] == 'lb' && isset($item['weight'])) ? $item['weight'] : 0,
			'weight_kg' => ($item['weight_unit'] == 'kg' && isset($item['weight'])) ? $item['weight'] : 0,
			'value' => !empty($item['price_value']) ? (int)$item['price_value'] : 0,
			'type_id' => !empty($item['type_id']) ? $item['type_id'] : 1,
			'country_id' => !empty($item['country_id']) ? $item['country_id'] : 10,
			'info' => (isset($item['info']) ? $item['info'] : ''),
		));
	}
}
