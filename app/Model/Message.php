<?php

App::uses('AppModel', 'Model');

/**
 * Class Transaction
 *
 * @property TransactionType $TransactionType
 * @property Address $Address
 */
class Message extends AppModel
{
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        )
    );

    public function listMessages()
    {
        return $this->find('all', array( 
            'conditions' => array('Message.user_id' => null),         
            'order' => array(
                'Message.updated' => 'desc'
            )
        ));
    }

    public function listRegisteredMessages()
    {
        return $this->find('all', array(   
            'conditions' => array('not' => array('Message.user_id' => null)),
            'order' => array(
                'Message.updated' => 'desc'
            )
        ));
    }
    
    public function createMessage($data)
    {        
        
        $this->create();
        
        return $this->save($data);
    }
    
     public function countUnread()
    {
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.read' => false,
                'Message.user_id' => null                
            )
        ));
    }
    
     public function countRegisteredUnread()
    {
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.read' => false,
                array('not' => array('Message.user_id' => null))             
            )
        ));
    }

     public function getMessage($id)
    {
        return $this->find('first', array(
            'conditions' => array(
                'Message.id' => $id
            )
        ));
    }

}