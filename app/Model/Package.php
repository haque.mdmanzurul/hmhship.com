<?php

App::uses('AppModel', 'Model');

/**
 * Class Package
 */
class Package extends AppModel
{

	public $hasMany = array(
		'Item' => array(
			'className' => 'Item',
			'foreignKey' => 'package_id'
		)
	);

	public $hasOne = array(
		'Shipment' => array(
			'className' => 'Shipment',
			'foreignKey' => 'package_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	private $_item_data = array();
	// missing relationships

	/*

    data is in the form of

---------- Array ----------
array(1) {
  ["Package"]=>
  array(9) {
    ["weight"]=>
    string(1) "1"
    ["weight_unit"]=>
    string(2) "lb"
    ["width"]=>
    string(1) "1"
    ["height"]=>
    string(1) "1"
    ["length"]=>
    string(1) "1"
    ["size_unit"]=>
    string(2) "in"
    ["tracking_code"]=>
    string(3) "123"
    ["carrier"]=>
    string(5) "fedex"
    ["items"]=>
    array(1) {
      [0]=>
      array(7) {
        ["description"]=>
        string(11) "lorem ipsum"
        ["quantity"]=>
        string(1) "1"
        ["weight"]=>
        string(1) "1"
        ["weight_unit"]=>
        string(2) "lb"
        ["type_id"]=>
        string(1) "1"
        ["country_id"]=>
        string(2) "45"
        ["value"]=>
        string(2) "10"
      }
    }
    ["consolidated"]=>
    array(1) {
    [0]=>
      array(3) {
        ["weight"]=>
        string(1) "2"
        ["unit"]=>
        string(2) "lb"
        ["tracking_code"]=>
        string(4) "1234"
      }
    }
  }
}

Before saving, loop through items property
and create the correct saving $data structure

http://book.cakephp.org/2.0/en/models/saving-your-data.html
Model::saveAssociated

$data = array(
    'Package' => array(...),
    'Item' => array(
        array(...),
        array(...),
        ...
    ),
);


    */

	/**
	 * Returns array
	 * length, width, height in inches
	 * weight in ounces
	 */
	public function get_easypost_data()
	{
	}

	/*public function add_item_data($data) {
        $this->_item_data[] = $data;
    }*/

	public function getSumPackageItemsValues($items)
	{
		$total = null;
		foreach ($items as $item) {
			if (is_null($total)) $total = 0;
			$price = substr($item->price_value, 1, strlen($item->price_value));
			$total += floatval($price);
		}
		return $total;
	}

	/**
	 * @param $shipment_id
	 * @param $package_index
	 * @param $data
	 * @return array|bool|int|mixed|string
	 * @throws Exception
	 */
	public function addPackage($shipment_id, $package_index, $data)
	{
		CakeLog::write('debug', print_r($data, true));

        $consolidated_packages = "";
        if (isset($data['shipment']['quickship']['packages'][$package_index]['consolidated'])) {
            $consolidated_array = Hash::get($data['shipment']['quickship']['packages'][$package_index], 'consolidated') ;
            foreach($consolidated_array as $consol) {
                if (strlen($consolidated_packages) > 0)
                    $consolidated_packages = $consolidated_packages . ",";
                $consolidated_packages = $consolidated_packages . json_encode($consol);
            }
        }

		$this->create();
		$this->save(array(
			'shipment_id' => $shipment_id,
			'weight_lb' => (isset($data['shipment']['quickship']['packages'][$package_index]['weight_unit']) && $data['shipment']['quickship']['packages'][$package_index]['weight_unit'] == 'lb') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'weight') : null,
			'weight_kg' => (isset($data['shipment']['quickship']['packages'][$package_index]['weight_unit']) && $data['shipment']['quickship']['packages'][$package_index]['weight_unit'] == 'kg') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'weight') : null,
			'width_in' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'in') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'width') : null,
			'width_cm' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'cm') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'width') : null,
			'height_in' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'in') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'height') : null,
			'height_cm' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'cm') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'height') : null,
			'length_in' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'in') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'length') : null,
			'length_cm' => (isset($data['shipment']['quickship']['packages'][$package_index]['size_unit']) && $data['shipment']['quickship']['packages'][$package_index]['size_unit'] == 'cm') ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'length') : null,
			'value' => null,
			'expedited' => $data['shipment']['quickship']['packagesExpedited'],
			'insured' => $data['shipment']['quickship']['packagesInsurance'],
			'repackaged' => $data['shipment']['quickship']['repackaged'],
			'instructions' => (isset($data['shipment']['quickship']['special_instructions']) ? $data['shipment']['quickship']['special_instructions'] : ''),			
            'in_tracking' => (isset($data['shipment']['quickship']['packages'][$package_index]['tracking_code']) ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'tracking_code') : null),
            'carrier' => (isset($data['shipment']['quickship']['packages'][$package_index]['carrier']) ? Hash::get($data['shipment']['quickship']['packages'][$package_index], 'carrier') : null),
			'ship_fast' => (isset($data['shipment']['quickship']['shipping_options']['fastest']) ? $data['shipment']['quickship']['shipping_options']['fastest'] : 0),
			'ship_cheap' => (isset($data['shipment']['quickship']['shipping_options']['cheapest']) ? $data['shipment']['quickship']['shipping_options']['cheapest'] : 0),
			'easypost_rate_id' => (isset($data['shipment']['shippingRatesSelected'][$package_index]) ? Hash::get($data['shipment']['shippingRatesSelected'][$package_index], 'shipping_rate_id') : null),
			'easypost_rate_price' => (isset($data['shipment']['shippingRatesSelected'][$package_index]['rate']) ? Hash::get($data['shipment']['shippingRatesSelected'][$package_index]['rate'], 'rate_num') : null),
			'easypost_rate_description' => (isset($data['shipment']['shippingRatesSelected'][$package_index]['rate']) ? Hash::get($data['shipment']['shippingRatesSelected'][$package_index]['rate'], 'carrier') . ' ' . Hash::get($data['shipment']['shippingRatesSelected'][$package_index]['rate'], 'service') : null),
            'consolidated_packages' => $consolidated_packages
		));
		return $this->id;
	}
}
