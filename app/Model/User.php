<?php

App::uses('AppModel', 'Model');
App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

class User extends AppModel
{

    /**
     * @var string
     */
    public $hasMany = 'Address';

    /**
     * @var array
     */
    public $validate = array(
        'email' => array(
            'unique_email' => array(
                'rule' => array('isUnique'),
                'message' => 'An account already exists with the same email address'
            ),
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Enter Email',
                'required' => true
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Please enter a valid email address'
            )
        ),
        'password' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Enter Password',
                'required' => true
            ),
            'requireLetterNumber' => array(
                'rule' => '/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/',
                'message' => 'Password must contain at least 1 letter and 1 number'
            ),
            'length' => array(
                'rule' => '/^.{8,35}$/',
                'message' => 'Password must be 8 characters or greater'
            ),
        ),
        'first_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Enter First Name',
                'required' => true
            ),
        ),
        'last_name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
                'message' => 'Enter Last Name',
                'required' => true
            ),
        )

    );


    public function beforeSave($options = array())
    {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new BlowfishPasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }

    public function countUnread()
    {
        return $this->find('count', array(
            'conditions' => array(
                $this->alias . '.read' => false
            )
        ));
    }

    public function getUser($id)
    {
        return $this->find('first', array(
            'conditions' => array(
                'User.id' => $id
            ),
            'recursive' => 2
        ));
    }
}
