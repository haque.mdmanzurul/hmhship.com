<?php

App::uses('AppModel', 'Model');

/**
 * Class Address
 *
 * @property AddressType $AddressType
 * @property Country $Country
 * @property User $User
 * @property Transaction $Transaction
 */
class Address extends AppModel
{

	public $belongsTo = array('AddressType', 'Country', 'User');	

	public $hasMany = array(
		'Shipment' => array(
			'className' => 'Shipment',
			'foreignKey' => 'shipping_address_id'
		)
	);

	/**
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
		'Transaction' => array(
			'className' => 'Transaction',
			'joinTable' => 'addresses_transactions',
			'foreignKey' => 'address_id',
			'associationForeignKey' => 'transaction_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => ''
		)
	);

        public $validate = array(
            'firstname' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter First Name',				
				'required' => true
			),
            'lastname' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Last Name',				
				'required' => true
			),            
            'address1' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Address 1',				
				'required' => true
			),
            'city' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter City',				
				'required' => true
			),
            'adm_division' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter State/Province',				
				'required' => true
			),
            'postal_code' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Postal Code',				
				'required' => true
			),
            'country_id' => array(
				'rule' => array('notBlank'),
				'message' => 'Enter Country',				
				'required' => true
			)
        );
        
	const BILLING = 2;

	const SHIPPING = 3;

	const SHIPPING_AND_BILLING = 4;

	/**
	 * @return mixed
	 */
	public function get_type_id()
	{
		return $this->data['Address']['address_type_id'];
	}

	/**
	 * @param $data
	 * @param int $address_type
	 * @return array|bool|int|mixed|string
	 * @throws Exception
	 */
	public function addAddress($data, $address_type = self::SHIPPING_AND_BILLING)
	{
		$type = 'billing';
		
		if ($address_type === self::SHIPPING) {
			$type = 'shipping';			
		}
		$this->create();

        if (!isset($data['shipment'][$type]['email']))
            $data['shipment'][$type]['email'] = '';
        if (!isset($data['shipment'][$type]['phone']))
            $data['shipment'][$type]['phone'] = '';

		return $this->save(array(
			'firstname' => $data['shipment'][$type]['first_name'],
			'lastname' => $data['shipment'][$type]['last_name'],
			'address1' => $data['shipment'][$type]['address1'],
			'address2' => $data['shipment'][$type]['address2'],
			'city' => $data['shipment'][$type]['city'],
			'adm_division' => $data['shipment'][$type]['state'],
			'postal_code' => $data['shipment'][$type]['postal_code'],
			'country_id' => $data['shipment'][$type]['country'],
			'address_type_id' => $address_type,
			'email' => $data['shipment'][$type]['email'],
			'phone' => $data['shipment'][$type]['phone']            
		));
	}
	
    public function addAddress2($data, $address_type = self::SHIPPING_AND_BILLING)
	{
		$type = 'billing';
		
		if ($address_type === self::SHIPPING) {
			$type = 'shipping';			
		}
		$this->create();
		return $this->save(array(
			'firstname' => $data['firstname'],
			'lastname' => $data['lastname'],
			'address1' => $data['address1'],
			'address2' => $data['address2'],
			'city' => $data['city'],
			'adm_division' => $data['adm_division'],
			'postal_code' => $data['postal_code'],
			'country_id' => $data['country_id'],
			'address_type_id' => $address_type,
			'email' => $data['email'],
			'phone' => $data['phone']            
		));
	}

	/**
	 * @param $data
	 * @param int $type
	 *
	 * @return bool
	 */
	public function getAddressFromData($data, $type = self::SHIPPING_AND_BILLING)
	{
		if (!isset($data['Address']) || count($data['Address']) === 0) return false;
		if ($data['Address']['address_type_id'] == $type) {
			return $data['Address'];
		}

		return false;
	}
}
