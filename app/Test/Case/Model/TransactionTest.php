<?php
App::uses('Transaction', 'Model');

/**
 * Transaction Test Case
 *
 * @property Transaction $Transaction
 */
class TransactionTest extends CakeTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.transaction',
		'app.transaction_type',
		'app.shipment',
		'app.package',
		'app.item',
		'app.address',
		'app.addresses_transaction',
		'app.user',
		'app.address_type'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->Transaction = ClassRegistry::init('Transaction');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Transaction);

		parent::tearDown();
	}

	/**
	 * testCreateTransaction method
	 *
	 * @return void
	 */
	public function testCreateTransaction()
	{
		$return = $this->Transaction->createTransaction(array(
			'type_id' => 1, 
			'code' => 1, 
			'date' => date('Y-m-d H:i:s'),
			'expedited' => false,
			'insured' => false,
			'repackaged' => false,
			'special_instructions' => '',
			'ship_fast' => false,
			'ship_cheap' => false
		), array(
			'billing' => 1,
			'shipping' => 1
		));
		$this->assertInternalType('array', $return);
		$this->assertArrayHasKey('Transaction', $return);
	}

	/**
	 * testUpdateStatus method
	 *
	 * @return void
	 */
	public function testUpdateStatus()
	{
		$this->markTestIncomplete('testUpdateStatus not implemented.');
	}

}
