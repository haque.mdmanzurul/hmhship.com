<?php
App::uses('Address', 'Model');

/**
 * Address Test Case
 * 
 * @property Address $Address
 */
class AddressTest extends CakeTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.address',
		'app.address_type',
		'app.country',
		'app.user'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->Address = ClassRegistry::init('Address');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Address);

		parent::tearDown();
	}

	/**
	 * testGetTypeId method
	 *
	 * @return void
	 */
	public function testGetTypeId()
	{
		$this->markTestIncomplete('testGetTypeId not implemented.');
	}

	/**
	 * testAddAddress method
	 *
	 * @return void
	 */
	public function testAddAddress()
	{
		$mockData = array(
			'shipment' => array(
				'billing' => array(
					'address1' => '',
					'address2' => '',
					'city' => '',
					'state' => '',
					'postal_code' => '',
					'country' => '',
				),
				'user' => array(
					'first_name' => '',
					'last_name' => '',
					'email' => '',
					'phone' => '',
				)
			)
		);
		$result = $this->Address->addAddress($mockData);
		debug($result);
	}

}
