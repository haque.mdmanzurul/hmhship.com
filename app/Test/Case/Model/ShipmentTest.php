<?php
App::uses('Shipment', 'Model');

/**
 * Shipment Test Case
 */
class ShipmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.shipment',
		'app.transaction',
		'app.package'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Shipment = ClassRegistry::init('Shipment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Shipment);

		parent::tearDown();
	}

}
