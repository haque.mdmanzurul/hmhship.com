<?php
App::uses('TransactionsController', 'Controller');

/**
 * TransactionsController Test Case
 */
class TransactionsControllerTest extends ControllerTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.transaction',
		'app.transaction_type',
		'app.shipment',
		'app.address',
		'app.address_type',
		'app.country',
		'app.user',
		'app.package',
		'app.item',
		'app.item_type',
		'app.addresses_transaction'
	);

}
