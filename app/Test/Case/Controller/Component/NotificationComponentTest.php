<?php

App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('NotificationComponent', 'Controller/Component');
App::uses('Hmhship\Logger', 'Vendor');

// A fake controller to test against
class FakeController extends Controller
{
	public $paginate = null;
	
	public $name = 'Payments';
}

/**
 * NotificationComponent Test Case
 *
 * @property NotificationComponent $Notification
 * @property Transaction $Transaction
 */
class NotificationComponentTest extends CakeTestCase
{

	public $fixtures = array(
		'app.transaction',
		'app.transaction_type',
		'app.shipment',
		'app.addresses_transaction',
		'app.address',
		'app.package',
		'app.item',
		'app.item_type',
		'app.country',
		'app.address_type',
	);

	public $Notification;

	public $Transaction;
	
	public $Controller;

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$Collection = new ComponentCollection();
		$this->Controller = new FakeController();
		$this->Notification = new NotificationComponent($Collection);
		$this->Notification->initialize($this->Controller);

		$this->Transaction = ClassRegistry::init('Transaction');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Notification);

		parent::tearDown();
	}

	/**
	 * testSendTransactionCompleteEmail method
	 *
	 * @return void
	 */
	public function testSendTransactionCompleteEmail()
	{
		$data = $this->Transaction->getTransaction(1); // load from fixture
		debug($data);
		debug($this->Notification->controller);
		$result = $this->Notification->sendTransactionCompleteEmail($data);
		debug($result);
	}

	/**
	 * testSendTransactionCompleteAdminEmail method
	 *
	 * @return void
	 */
	public function testSendTransactionCompleteAdminEmail()
	{
		$this->markTestIncomplete('testSendTransactionCompleteAdminEmail not implemented.');
	}

	/**
	 * testSendShipmentReceivedEmail method
	 *
	 * @return void
	 */
	public function testSendShipmentReceivedEmail()
	{
		$data = array();
		//$result = $this->Notification->sendShipmentReceivedEmail($data);
		//debug($result);
	}

	/**
	 * testSendShipmentSentEmail method
	 *
	 * @return void
	 */
	public function testSendShipmentSentEmail()
	{
		$data = array();
		//$result = $this->Notification->sendShipmentSentEmail($data);
		//debug($result);
	}

	public function testGetEstimatedShippingCost()
	{
		$data = $this->Transaction->getTransaction(1); // load from fixture
		$result = $this->Notification->getEstimatedShippingCost($data);
		$this->assertEquals('111.92 USD', $result);
	}

}
