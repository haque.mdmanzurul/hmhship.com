<?php

App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('ShippingComponent', 'Controller/Component');
App::uses('Address', 'Model');
App::uses('Country', 'Model');
App::uses('ItemType', 'Model');
App::uses('Hmhship\Quickship\Package', 'Vendor');

// A fake controller to test against
class FakeControllerTest extends Controller
{
    public $paginate = null;
}

/**
 * ShippingComponent Test Case
 *
 * @property ShippingComponent $Shipping
 */
class ShippingComponentTest extends CakeTestCase
{

    public $Controller = null;

    /**
     * @var array
     */
    public $fixtures = array('app.country', 'app.item_type');

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $Collection = new ComponentCollection();
        $this->Shipping = new ShippingComponent($Collection);
        $CakeRequest = new CakeRequest();
        $CakeResponse = new CakeResponse();
        $this->Controller = new FakeControllerTest($CakeRequest, $CakeResponse);
        $this->Shipping->initialize($this->Controller);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Shipping);

        parent::tearDown();
    }

    /**
     * testInit method
     *
     * @return void
     */
    public function testInit()
    {
        $this->markTestIncomplete('testInit not implemented.');
    }

    /**
     * testGetRates method
     *
     * @return void
     */
    public function testGetRates()
    {
        $this->Shipping->init();
        $address_data = array(
            'Address' => array(
                'address1' => 'Jose Pascual Tamborini 5574',
                'address2' => '',
                'city' => 'Ciudad de Buenos Aires',
                'state' => 'Ciudad de Buenos Aires',
                'postal_code' => '1431',
                'country' => 12,
                'address_type_id' => 4,
                'phone' => '+5491164853376'
            )
        );
        $package = new \Hmhship\Quickship\Package();
        $package->height = 5;
        $package->width = 5;
        $package->length = 5;
        $package->weight = 5;
        $package->data['Package']['items'] = array(
            array(
                'country_id' => 16,
                'description' => "shirt",
                'price_value' => "$55",
                'quantity' => 5,
                'type_id' => 3,
                'weight' => 5,
                'weight_unit' => 'lb'
            )
        );
        $address = new Address();
        $address->set($address_data);
        //debug($address->data);

        $return = $this->Shipping->get_rates($address, $package);
        //debug($return);
    }

    /**
     * testCreateCustomsItem method
     *
     * @return void
     */
    public function testCreateCustomsItem()
    {
        $this->markTestIncomplete('testCreateCustomsItem not implemented.');
    }

    /**
     * testCreateCustomsInfo method
     *
     * @return void
     */
    public function testCreateCustomsInfo()
    {
        $this->markTestIncomplete('testCreateCustomsInfo not implemented.');
    }

    /**
     * testVerifyAddress method
     *
     * @return void
     */
    public function testVerifyAddress()
    {
        // Valid sample address in the US
        $address = array(
            'street1' => '179 N Harbor Dr',
            'city' => 'Redondo Beach',
            'state' => 'CA',
            'country' => 'US',
        );
        $return = $this->Shipping->verify_address($address);
        $this->assertInternalType('object', $return);
        $this->assertInstanceOf('EasyPost\Address', $return);


        // International address validation doesn't work as expected yet
        /*
        // Valid address in Argentina
        unset($address);
        $address = array(
            'street1' => 'Jose Pascual Tamborini 5574',
            'city' => 'Capital Federal',
            'state' => 'Capital Federal',
            'zip' => '1431',
            'country' => 'AR'
        );
        $return = $this->Shipping->verify_address($address);
        $this->assertInternalType('object', $return);
        debug($return);

        // Invalid address
        unset($address);
        $address = array(
            'street1' => 'Jose Pascual Tamborini 574',
            'city' => 'Capital Federal',
            'state' => 'Capital Federal',
            'zip' => 1431,
            'country' => 'AR'
        );
        $return = $this->Shipping->verify_address($address);
        $this->assertInternalType('boolean', $return);
        $this->assertFalse($return);
        debug($return);*/
    }

    /**
     * testStripMoneyPrefix method
     *
     * @return void
     */
    public function testStripMoneyPrefix()
    {
        $this->markTestIncomplete('testStripMoneyPrefix not implemented.');
    }

}
