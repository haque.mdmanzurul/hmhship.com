<?php
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('PaypalComponent', 'Controller/Component');

/**
 * PaypalComponent Test Case
 * 
 * @property PaypalComponent $Paypal
 */
class PaypalComponentTest extends CakeTestCase
{

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$Collection = new ComponentCollection();
		$this->Paypal = new PaypalComponent($Collection);
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Paypal);

		parent::tearDown();
	}

	/**
	 * testTmp method
	 *
	 * @return void
	 */
	public function testTmp()
	{
		$this->markTestIncomplete('testTmp not implemented.');
	}

	/**
	 * testSinglePayment method
	 *
	 * @return void
	 */
	public function testSinglePayment()
	{
		/*$description = 'Test payment';
		$price = 7.25;
		$return = $this->Paypal->single_payment($description, $price);
		$this->assertInternalType('object', $return);*/
	}

	/**
	 * testGetExperienceProfile method
	 *
	 * @return void
	 */
	public function testGetExperienceProfile()
	{
		$this->Paypal->getExperienceProfile();
	}

}
