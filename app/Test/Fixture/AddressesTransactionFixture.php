<?php

/**
 * AddressesTransaction Fixture
 */
class AddressesTransactionFixture extends CakeTestFixture
{

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'address_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'transaction_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => 1,
			'address_id' => 1,
			'transaction_id' => 1
		),
		array(
			'id' => 2,
			'address_id' => 2,
			'transaction_id' => 1
		),
	);

}
