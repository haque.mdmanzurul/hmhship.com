<?php
/**
 * Transaction Fixture
 */
class TransactionFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'type_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'date' => array('type' => 'timestamp', 'null' => false, 'default' => '0000-00-00 00:00:00'),
		'status' => array('type' => 'integer', 'null' => false, 'default' => '0', 'unsigned' => false),
		'unread' => array('type' => 'boolean', 'null' => true, 'default' => '1'),
		'consolidated' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'expedited' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'insured' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'repackaged' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'special_instructions' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'ship_fast' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'ship_cheap' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'charge_now' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'type_id' => 1,
			'code' => 'Lorem ipsum dolor sit amet',
			'date' => 1461072369,
			'status' => 1,
			'unread' => 1,
			'consolidated' => 1,
			'expedited' => 1,
			'insured' => 1,
			'repackaged' => 1,
			'special_instructions' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'ship_fast' => 1,
			'ship_cheap' => 1,
			'charge_now' => 1,
			'created' => '2016-04-19 09:26:09',
			'modified' => '2016-04-19 09:26:09'
		),
	);

}
