<?php

/**
 * Package Fixture
 */
class PackageFixture extends CakeTestFixture
{

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'shipment_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'weight_lb' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'weight_kg' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'width_in' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'width_cm' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'height_in' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'height_cm' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'length_in' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'length_cm' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'value' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'expedited' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'insured' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'repackaged' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'instructions' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'carrier' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'in_tracking' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'ship_fast' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'ship_cheap' => array('type' => 'boolean', 'null' => true, 'default' => '0'),
		'easypost_rate_id' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'easypost_rate_price' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '10,2', 'unsigned' => false),
		'easypost_rate_description' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 150, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => 1,
			'shipment_id' => 1,
			'weight_lb' => null,
			'weight_kg' => 2.39,
			'width_in' => 1,
			'width_cm' => 1,
			'height_in' => 1,
			'height_cm' => 1,
			'length_in' => 1,
			'length_cm' => 1,
			'value' => 1,
			'expedited' => 1,
			'insured' => 1,
			'repackaged' => 1,
			'instructions' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'carrier' => 'My custom carrier',
			'in_tracking' => '#123123',
			'ship_fast' => 1,
			'ship_cheap' => 1,
			'easypost_rate_id' => 'Lorem ipsum dolor sit amet',
			'easypost_rate_price' => 55.96,
			'easypost_rate_description' => 'USPS PriorityMailInternational',
			'created' => '2016-04-19 08:03:20',
			'modified' => '2016-04-19 08:03:20'
		),
		array(
			'id' => 2,
			'shipment_id' => 2,
			'weight_lb' => null,
			'weight_kg' => 2.39,
			'width_in' => 1,
			'width_cm' => 1,
			'height_in' => 1,
			'height_cm' => 1,
			'length_in' => 1,
			'length_cm' => 1,
			'value' => 1,
			'expedited' => 1,
			'insured' => 1,
			'repackaged' => 1,
			'instructions' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'carrier' => 'My custom carrier',
			'in_tracking' => '#123123',
			'ship_fast' => 1,
			'ship_cheap' => 1,
			'easypost_rate_id' => 'Lorem ipsum dolor sit amet',
			'easypost_rate_price' => 55.96,
			'easypost_rate_description' => 'USPS PriorityMailInternational',
			'created' => '2016-04-19 08:03:20',
			'modified' => '2016-04-19 08:03:20'
		),
	);

}
