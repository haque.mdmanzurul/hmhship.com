<?php

/**
 * ShipmentStatus Fixture
 */
class ShipmentStatusFixture extends CakeTestFixture
{

	/**
	 * Table name
	 *
	 * @var string
	 */
	public $table = 'shipment_status';

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => '1',
			'description' => 'Incomplete'
		),
		array(
			'id' => '2',
			'description' => 'Charged'
		),
		array(
			'id' => '3',
			'description' => 'Invoiced'
		),
		array(
			'id' => '4',
			'description' => 'Waiting'
		),
		array(
			'id' => '5',
			'description' => 'Sent'
		),
	);

}
