<?php

/**
 * ItemType Fixture
 */
class ItemTypeFixture extends CakeTestFixture
{

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => '1',
			'description' => 'Commercial Sample'
		),
		array(
			'id' => '2',
			'description' => 'Documents'
		),
		array(
			'id' => '3',
			'description' => 'Gifts'
		),
		array(
			'id' => '4',
			'description' => 'Humanitarian Donation'
		),
		array(
			'id' => '5',
			'description' => 'Merchandise'
		),
		array(
			'id' => '6',
			'description' => 'Returned Goods '
		),
		array(
			'id' => '7',
			'description' => 'Other'
		),
	);

}
