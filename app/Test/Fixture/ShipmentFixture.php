<?php

/**
 * Shipment Fixture
 */
class ShipmentFixture extends CakeTestFixture
{

	/**
	 * Fields
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'status_id' => array('type' => 'integer', 'null' => false, 'default' => '1', 'unsigned' => false, 'key' => 'index', 'comment' => '1 = Incomplete'),
		'transaction_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'easypost_shipment_id' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'shipping_address_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'easypost_label_url' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'status_id' => array('column' => 'status_id', 'unique' => 0),
			'shipping_address_id' => array('column' => 'shipping_address_id', 'unique' => 0),
			'idx_shipments_address_id' => array('column' => 'shipping_address_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => 1,
			'status_id' => 1,
			'transaction_id' => 1,
			'easypost_shipment_id' => 'Lorem ipsum dolor sit amet',
			'shipping_address_id' => 1,
			'easypost_label_url' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-04-19 08:29:38',
			'modified' => '2016-04-19 08:29:38'
		),
		array(
			'id' => 2,
			'status_id' => 1,
			'transaction_id' => 1,
			'easypost_shipment_id' => 'Lorem ipsum dolor sit amet',
			'shipping_address_id' => 1,
			'easypost_label_url' => 'Lorem ipsum dolor sit amet',
			'created' => '2016-04-19 08:29:38',
			'modified' => '2016-04-19 08:29:38'
		),
	);

}
