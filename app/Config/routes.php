<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
Router::parseExtensions('json', 'rss');

Router::connect('/', array('controller' => 'pages', 'action' => 'home'));

Router::connect('/business', array('controller' => 'pages', 'action' => 'business'));
Router::connect('/deactivated', array('controller' => 'pages', 'action' => 'deactivated'));
Router::connect('/reset2', array('controller' => 'pages', 'action' => 'reset2'));


Router::connect('/talk-to-us', array('controller' => 'pages', 'action' => 'contact'));

// PHP doesn't allow dashes in method names so match url's with dashes to appropriate action name
Router::connect('/pages/hmhship-bio', array('controller' => 'pages', 'action' => 'bio'));
Router::connect('/pages/shipping-calculator', array('controller' => 'pages', 'action' => 'shipping_calculator'));
Router::connect('/pages/business-form', array('controller' => 'pages', 'action' => 'business_form'));
Router::connect('/pages/what-we-do', array('controller' => 'pages', 'action' => 'what_we_do'));

Router::connect('/pages/shipinbatch', array('controller' => 'pages', 'action' => 'batchShipments'));

Router::connect('/quickship/your-us-address/*', array('controller' => 'payments', 'action' => 'shipping_address'));
Router::connect('/quickship/your-us-address', array('controller' => 'payments', 'action' => 'shipping_address'));

// Admin
Router::connect('/admin/login', array('controller' => 'admins', 'action' => 'login', 'admin' => true));
Router::connect('/admin/logout', array('controller' => 'admins', 'action' => 'logout', 'admin' => true));

Router::connect('/admin/quickship', array('controller' => 'quickship', 'admin' => true, 'action' => 'index'));
Router::connect('/admin/quickship/:action', array('controller' => 'quickship', 'admin' => true));
Router::connect('/admin/quickship/add-requested-photo', array('controller' => 'quickship', 'admin' => true, 'action' => 'add_requested_photo'));
Router::connect('/admin/quickship/add-shippingbox-photo', array('controller' => 'quickship', 'admin' => true, 'action' => 'add_shippingbox_photo'));
Router::connect('/admin/quickship/add-note', array('controller' => 'quickship', 'admin' => true, 'action' => 'admin_add_note'));
Router::connect('/admin/quickship/add-registered-requested-photo', array('controller' => 'quickship', 'admin' => true, 'action' => 'add_registered_requested_photo'));

Router::connect('/admin/transactions/:action', array('controller' => 'transactions', 'admin' => true));
Router::connect('/admin/:action', array('controller' => 'admins', 'admin' => true));
Router::connect('/admin', array('controller' => 'admins', 'action' => 'index', 'admin' => true));

Router::connect('/shipping-calculator', array('controller' => 'pages', 'action' => 'shipping_calculator'));
Router::connect('/services-rates', array('controller' => 'pages', 'action' => 'what_we_do'));
Router::connect('/business-form', array('controller' => 'pages', 'action' => 'business_form'));
Router::connect('/hmhship-bio', array('controller' => 'pages', 'action' => 'bio'));
Router::connect('/additional-options', array('controller' => 'quickship', 'action' => 'index'));
Router::connect('/package-details', array('controller' => 'quickship', 'action' => 'index'));
Router::connect('/shipment-rates', array('controller' => 'quickship', 'action' => 'index'));
Router::connect('/payment-options', array('controller' => 'quickship', 'action' => 'index'));
Router::connect('/sign-in', array('controller' => 'Users', 'action' => 'login'));
Router::connect('/register', array('controller' => 'pages', 'action' => 'register'));
Router::connect('/terms', array('controller' => 'pages', 'action' => 'terms'));
Router::connect('/privacy', array('controller' => 'pages', 'action' => 'privacy'));
Router::connect('/faq', array('controller' => 'pages', 'action' => 'faq'));
Router::connect('/new-shipment', array('controller' => 'quickship', 'action' => 'index'));
Router::connect('/new-shipment/:ghost_shipment_id', array('controller' => 'quickship', 'action' => 'index'), array('pass' => array('ghost_shipment_id')));



/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
