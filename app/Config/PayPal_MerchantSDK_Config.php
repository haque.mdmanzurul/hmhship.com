<?php 
class PaypalConfiguration
{
	// For a full list of configuration parameters refer in wiki page (https://github.com/paypal/sdk-core-php/wiki/Configuring-the-SDK)
	public static function getConfig()
	{
             if (Configure::read('debug') > 0) {
                $config = array(
                        // values: 'sandbox' for testing
                        //		   'live' for production
                        "mode" => "sandbox",
                        'log.LogEnabled' => false,
                        'log.FileName' => '../PayPal.log',
                        'log.LogLevel' => 'FINE'

                        // These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
                        // "http.ConnectionTimeOut" => "5000",
                        // "http.Retry" => "2",
                );
                     } else {
                         // LIVE
                         $config = array(
                        // values: 'sandbox' for testing
                        //		   'live' for production
                        "mode" => "live",
                        'log.LogEnabled' => false,
                        'log.FileName' => '../PayPal.log',
                        'log.LogLevel' => 'FINE'

                        // These values are defaulted in SDK. If you want to override default values, uncomment it and add your value.
                        // "http.ConnectionTimeOut" => "5000",
                        // "http.Retry" => "2",
                );
             }
		return $config;
	}
	
	// Creates a configuration array containing credentials and other required configuration parameters.
	public static function getAcctAndConfig()
	{
            if (Configure::read('debug') > 0) {
                
                // Sandbox credentials
                /*$config = array(
				// Signature Credential
				"acct1.UserName" => "hirsh-facilitator_api1.hmhship.com",
				"acct1.Password" => "1388917576",
				"acct1.Signature" => "AFcWxV21C7fd0v3bYYYRCpSSRl31AUL4vkMDuMro9gbAdiB61i9SbnPp"
				);*/

                $config = array(
                    // Signature Credential
                    "acct1.UserName" => "hmhship_merchant_api1.gmail.com",
                    "acct1.Password" => "RP8WUZ5CVBZDUWPX",
                    "acct1.Signature" => "AmGTFRuYhCyp.MmN.SNs2rMkvRRqAhqMz9-TlCG1AMVHNBCKltKbfs6y"
                );

            } else {
    
                $config = array(                    
				// Signature Credential
				"acct1.UserName" => "hirsh_api1.hmhship.com",
				"acct1.Password" => "NCKPR4SE6VQHP3U8",
				"acct1.Signature" => "ACUe-E7Hjxmeel8FjYAtjnx-yjHAAAeQ3mF0wfI6FSzwQGYJlqVnBiqM"
				);    
   
            }		
		
            return array_merge($config, self::getConfig());
	}

}
