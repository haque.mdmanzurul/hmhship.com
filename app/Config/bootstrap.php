<?php

// Load Composer autoload.
require ROOT . DS . APP_DIR . DS . 'Vendor' . DS . 'autoload.php';

// Remove and re-prepend CakePHP's autoloader as Composer thinks it is the
// most important.
// See: http://goo.gl/kKVJO7
spl_autoload_unregister(array('App', 'load'));
spl_autoload_register(array('App', 'load'), true, true);

/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File', 'mask' => 0777));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 */

/**
 * Custom Inflector rules can be set to correctly pluralize or singularize table, model, controller names or whatever
 * other string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. Make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
CakePlugin::loadAll(array('DocumentManager' => array('bootstrap' => true)));

/**
 * To prefer app translation over plugin translation, you can set
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * You can attach event listeners to the request lifecycle as Dispatcher Filter. By default CakePHP bundles two
 * filters:
 *
 * - AssetDispatcher filter will serve your asset files (css, images, js, etc) from your themes and plugins
 * - CacheDispatcher filter will read the Cache.check configure variable and try to serve cached content generated from
 * controllers
 *
 * Feel free to remove or add filters as you see fit for your application. A few examples:
 *
 * Configure::write('Dispatcher.filters', array(
 *        'MyCacheFilter', //  will use MyCacheFilter class from the Routing/Filter package in your app.
 *        'MyCacheFilter' => array('prefix' => 'my_cache_'), //  will use MyCacheFilter class from the Routing/Filter
 * package in your app with settings array.
 *        'MyPlugin.MyFilter', // will use MyFilter class from the Routing/Filter package in MyPlugin plugin.
 *        array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // A valid PHP callback type to be called
 * on beforeDispatch array('callable' => $anotherMethod, 'on' => 'after'), // A valid PHP callback type to be called on
 * afterDispatch
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

/**
 * Configures default file logging options
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
    'engine' => 'File',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
    'mask' => 0777
));
CakeLog::config('queries', array(
    'engine' => 'File',
    'types' => array('queries'),
    'file' => 'queries',
    'mask' => 0666
));
CakeLog::config('error', array(
    'engine' => 'File',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
    'mask' => 0777
));

// Fix issue on staging server because php-curl is not updated (staging has php 5.4 and cannot be upgraded atm)
if (!defined('CURL_SSLVERSION_TLSv1')) {
    define('CURL_SSLVERSION_TLSv1', 1);
}
// Fix issue with Paypal SDK not supporting SSLv3 anymore
PayPal\Core\PayPalHttpConfig::$defaultCurlOptions[CURLOPT_SSL_CIPHER_LIST] = 'TLSv1';

// the following line fixed the error "error:14094410:SSL routines:SSL3_READ_BYTES:sslv3 alert handshake failure" on 121getsitdone.com
PayPal\Core\PPHttpConfig::$DEFAULT_CURL_OPTS[CURLOPT_SSLVERSION] = 6;

/**
 * HMHShip App configuration
 */

Configure::write('Shipping.conversion', array(
    'lb_oz' => 16.000, // 1 lbs = 16 ounces
    'kg_oz' => 35.274, // 1 kg = 35.274 ounces
    'cm_in' => 0.393701, // 1 cm = 0.393701 inches
));

Configure::write('Shipping.from_address', array(
    'company' => 'HMHShip',
    'street1' => '23600 Mercantile Rd.',
    'street2' => 'Suite C-119',
    'city' => 'Beachwood',
    'state' => 'OH',
    'country' => 'US',
    'zip' => '44122',
    'phone' => '+1 (216) 359-3396',
    'email' => 'contact@hmhship.com'
));

Configure::write('Shipping.rates', array(
    'parcel_forwarded' => 7.25, /* per package */
    'handling_fee' => 7.25
));

if (env('SERVER_NAME')) {
    switch (env('SERVER_NAME')) {
        case 'localhost':
        case 'hmhship.test':
        case 'www.hmhship.test':
            Configure::write('Shipping.EasyPost.key', 'KFWN6DKXonj3n5iZGoaIIg');
            // send emails            
            // using the following key will cause emails to not actually be sent
            Configure::write('Mandrill.key', '0lmBzK1dhDw6m_wrZrTRZQ');
            Configure::write('Admin.email', 'michael@121ecommerce.com');
            Configure::write('Contact.email', 'michael@121ecommerce.com');
            break;

        case 'hmhship.mdmanzurul.pro':
        case 'hmhship.bitbytesoft.com':
            Configure::write('Shipping.EasyPost.key', 'KFWN6DKXonj3n5iZGoaIIg');
            // using the following key will cause emails to not actually be sent
            Configure::write('Mandrill.key', '0lmBzK1dhDw6m_wrZrTRZQ');
            Configure::write('Admin.email', 'contact@hmhship.com');
            Configure::write('Contact.email', 'contact@hmhship.com');
            break;

        default:
            // PRODUCTION
            Configure::write('Shipping.EasyPost.key', 'yuhOAzG1UTI121Vr2sbYwg');
            Configure::write('Mandrill.key', '_Bk-mxbBVHHYhSfFvXgcIQ');
            Configure::write('Admin.email', 'hirsh@hmhship.com');
            Configure::write('Contact.email', 'contact@hmhship.com');
    }
} else {
    // when would this happen?
    Configure::write('Shipping.EasyPost.key', 'KFWN6DKXonj3n5iZGoaIIg');
    Configure::write('Mandrill.key', '0lmBzK1dhDw6m_wrZrTRZQ');
    Configure::write('Admin.email', 'michael@121ecommerce.com');
    Configure::write('Contact.email', 'michael@121ecommerce.com');
}


if (Configure::read('debug') > 0) {

    /*Configure::write('Payment.Paypal', array(
        'client_id' => 'AQH0RR9wyl3Q5HVjt-gAp_VeKZ4wGZKUD_xSrLEedEwfwEyqyYtaa1stPF4Pq0zB80j8vyyOOd4hT5Z2',
        'client_secret' => 'EGQHe5_s97XpQ91DuCDZ0PlSGxwkvulRGjN4crh4bBqkNd4hXRLM4Q_nnb1UsFe4pBiyMji4ghBSvsve',
        'client_mode' => 'sandbox',
        'ExperienceProfileId' => 'XP-Q4MA-AU7E-42KU-T4TD'
    ));
    Configure::write('Paypal.Merchant.Email', 'hirsh-facilitator@hmhship.com');*/

    // Paypal details from Manzurul's paypal
    Configure::write('Payment.Paypal', array(
        'client_id' => 'AU6ZlazRkrVQ3Paorvil7hSbRI7hv9ODrwnWVWQ0douVyL1oKWGtwnb4O45e1o3OKQQrWogN7hTF0Af6',
        'client_secret' => 'ELf3T0gJnGyZG_TtN3qYRBwP948lqKiPDNueja_n6gUTadBaFanEarTazXzDj4jo3YkfsgESHRrJwTQV',
        'client_mode' => 'sandbox',
        'ExperienceProfileId' => 'XP-F8DU-5JA4-ZUZU-MP3F'
    ));

    Configure::write('Paypal.Merchant.Email', 'hmhship_merchant@gmail.com');

}
else {
    Configure::write('Payment.Paypal', array(
        'client_id' => 'AW7GFydaxpv30XyFQwxwu1jqTDQGppKdSti3bfpAyvyP7Q87pVwH4xAoVHgjgdtVRchUUBj0JcXk0yIs',
        'client_secret' => 'EJFTobCfsEitwA0xBPjvTHXRC3V4Qnjb8Gqz_lCVRYpeJoWS6U8ezM_r4LWkY2ZrLLaq9_zbaMOQqUNk',
        'client_mode' => 'live',
        'ExperienceProfileId' => 'XP-TLQX-QLYV-ZRJL-VTDM'
    ));
    Configure::write('Paypal.Merchant.Email', 'hirsh@hmhship.com');
}

Configure::write('CDN.Enabled', false);

Configure::write('Credits.phone', '(216) 359-3396');
Configure::write('Credits.address', '23600 Mercantile Rd. Suite C-119 Beachwood, OH 44122, USA');

// https://github.com/neilcrookes/CakePHP-Blog-Plugin
CakePlugin::load(array('Blog' => array('routes' => True)));