<?php
App::uses('AppController', 'Controller');

/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 * @property Country $Country
 * @property ItemType $ItemType
 * @property PaginatorComponent $Paginator
 * @property NotificationComponent $Notification
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class BusinessController extends AppController
{

	/**
	 * @var array
	 */
	public $components = array(
		'Notification'
	);
        
        public function isAuthorized($user) {       
        
            return true;
        }
        
        // called when Admin clicks on a Business Signup 
         public function admin_api_read($id = null)
	{
		$this->loadModel('BusinessSignup');
		
		if (!$this->BusinessSignup->exists($id)) {
			throw new NotFoundException(__('Item not found'));
		}
		
		$this->BusinessSignup->id = (int)$id;
		$this->BusinessSignup->saveField('read', 1);
                
		$this->response->compress();
		
		$detail = $this->BusinessSignup->getBusinessSignup((int)$id);
		

		$_serialize = array('detail');
		$_jsonOptions = JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT;
		$this->set(compact('detail', '_serialize', '_jsonOptions'));
	}
}