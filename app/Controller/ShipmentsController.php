<?php
App::uses('AppController', 'Controller');

/**
 * Shipments Controller
 *
 * @property Shipment $Shipment
 * @property PaginatorComponent $Paginator
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class ShipmentsController extends AppController
{

	public $components = array('Shipping','Notification','PayPalInvoice');
	
        public function isAuthorized($user) {       
        
            return true;
        }
        
	public function admin_generate_label($id)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException(__('Method not allowed'));
		}
		if (!$this->Shipment->exists($id)) {
			throw new NotFoundException(__('Shipment not found'));
		}
		$shipment = $this->Shipment->find('first', array(
			'conditions' => array(
				'Shipment.id' => $id
			)
		));
		$postage_label_url = null;
		if($shipment) {
			$easypost_shipment = \EasyPost\Shipment::retrieve($shipment['Shipment']['easypost_shipment_id']);

			if (isset($easypost_shipment->postage_label) && !empty($easypost_shipment->postage_label)) {
				$postage_label_url = $easypost_shipment->postage_label->label_url;
				$this->set('postage_label_url', $postage_label_url);
			} else {
				$rate = \EasyPost\Rate::retrieve($shipment['Package']['easypost_rate_id']);
				// TODO: handle insufficient funds
				// TODO: add insurance
				$response = $easypost_shipment->buy(array('rate' => $rate));
				$postage_label_url = $easypost_shipment->postage_label->label_url;
			}
			$this->Shipment->id = $shipment['Shipment']['id'];
			$this->Shipment->saveField('easypost_label_url', $postage_label_url);

			//Customs form
            $form = isset($easypost_shipment->forms) ? $easypost_shipment->forms[0] : null;
            if (isset($form) && isset($form->form_url))	{
                $this->Shipment->saveField('easypost_customs_form', $form->form_url);
                $this->set('easypost_customs_form', $form->form_url);
            }

            //Save note
            $this->loadModel('Note');
            $this->loadModel('Transaction');
            $transaction = $this->Transaction->getTransaction($shipment['Shipment']['transaction_id']);
            $this->Note->set('transaction_id', $transaction['Transaction']['id']);
            $user_id = ($transaction['Transaction']['user_id']) ? $transaction['Transaction']['user_id'] : 9999999;
            $this->Note->set('user_id', $user_id);
            $this->Note->set('note', 'Shipping Label generated for shipment ' . $shipment['Shipment']['id']);
            $this->Note->save();
		}
		$_serialize = array('shipment', 'postage_label_url');
		$this->set(compact('shipment', 'postage_label_url', '_serialize'));
	}

	// called from /js/src/admin/quickship/transactionSvc.js when Admin clicks to Save Balance Due
	public function saveBalanceDue($tran_id, $balance_due) {

        $this->loadModel('Transaction');     

        // get the Transaction record from the database
        $data = $this->Transaction->getTransaction($tran_id);

        //CakeLog::write('debug', print_r($data, true));
        //CakeLog::write('debug',"shipment id " . $data['Shipment'][0]['id']);

        // Bal Due for Transaction (Order) is stored in the first Shipment
        $this->Shipment->id = $data['Shipment'][0]['id'];
        $this->Shipment->saveField('balance_due', $balance_due);
        
        // get it again with the new Balance Due
        $data = $this->Transaction->getTransaction($tran_id);

        if ($balance_due > 0) {
          
            // set Transaction Status to Awaiting Payment
            $this->Transaction->id = $tran_id;
            $this->Transaction->saveField('status', Transaction::STATUS_AWAITING_PAYMENT);

            CakeLog::write('debug', 'ShipmentsController.saveBalanceDue() data: ' . print_r($data, true));

            if (!isset($data['Transaction']['user_id'])) {
                // QuickShip
                // Not associated with a User Account  
                // so they can't pay in User Portal 
                
                // Make sure there is a PayPal Invoice for this QuickShip
                if (!isset($data['Transaction']['paypal_invoice_id']) || $data['Transaction']['paypal_invoice_id'] == "") {
                    // create a PayPal Invoice for this transaction, send to cust, store invoice details
                    $invoice = $this->PayPalInvoice->createInvoice($data);
                    CakeLog::write('debug', 'ShipmentsController.saveBalanceDue() invoice data: ' . print_r($invoice, true));
                    $this->Transaction->saveField('paypal_invoice_id', $invoice->id);
                    $this->Transaction->saveField('paypal_invoice_payer_view_url', $invoice->metadata->payer_view_url);
                }
                else {
                    // update invoice with Balance Due
                    $invoice = $this->PayPalInvoice->getInvoice($data['Transaction']['paypal_invoice_id']);
                    $invoice->getItems()[0]->getUnitPrice()->setValue($balance_due);
                    $this->PayPalInvoice->updateInvoice($invoice);
                }

                $data['unsent_packages'] = 0;

                // send "We've Received Your Package! - Payment Required" email with link to PayPal invoice
                $this->Notification->sendCustomerPackageReceivedEmail($data, "we-ve-received-your-package-payment-required",
                  "We've Received Your Package! - Payment Required", $balance_due,  $invoice->metadata->payer_view_url);

                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $data['Transaction']['id']);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Balance due $' . $balance_due . ' saved and invoice sent via email');
                $this->Note->save();
                
            }
            else {
                // Shipment
                //get unsent package count
                $data['unsent_packages'] = $this->Transaction->find('count', array(
                    'conditions' => array('Transaction.status <' => '5', 'Transaction.user_id' => $data['Transaction']['user_id'])
                ));
                // send "We've Received Your Package! - Payment Required" email with link to User Payment page

                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $data['Transaction']['id']);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Balance due $' . $balance_due . ' saved and invoice sent via email');
                $this->Note->save();

                $this->Notification->sendCustomerPackageReceivedEmail($data, "we-ve-received-your-package-payment-required",
                  "We've Received Your Package! - Payment Required", $balance_due,  Router::url(array(
                                        "controller" => "Users",
                                        "action" => "payment"        
                                        ), true));
            }
           
        } // end if there is a Balance Due

        return null;

    }
}
