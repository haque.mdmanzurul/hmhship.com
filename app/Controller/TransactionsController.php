<?php
App::uses('AppController', 'Controller');

/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 * @property Country $Country
 * @property ItemType $ItemType
 * @property PaginatorComponent $Paginator
 * @property NotificationComponent $Notification
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class TransactionsController extends AppController
{

	/**
	 * @var array
	 */
	public $components = array(
        'Notification'
	);

        public function isAuthorized($user) {       
        
            return true;
        }
        
	/**
	 * load all data needed for the Admin section Angular app
	 */
	public function admin_api_list()
	{
		$this->helpers[] = 'Transaction';
		$this->loadModel('Transaction');
		$this->loadModel('Country');
		$this->loadModel('ItemType');
        $this->loadModel('Message');
        $this->loadModel('BusinessSignup');
        $this->loadModel('User');

		$transactions = $this->Transaction->listTransactions();
        CakeLog::write('debug', "transactions: " . print_r($transactions, true));
        $transactions_registered = $this->Transaction->listRegisteredTransactions();
        $transactions_ghost = $this->Transaction->listRegisteredGhostTransactions();

        CakeLog::write('debug', "transactions ghost count: " . print_r(($transactions_ghost), true));
		$messages = $this->Message->listMessages();
        $messages_registered = $this->Message->listRegisteredMessages();
        $businesssignups = $this->BusinessSignup->listBusinessSignups();
        $registereds = $this->User->find('all');

        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$count_waiting_package = $this->Transaction->countByStatus(Transaction::STATUS_WAITING_PACKAGE);
		$count_package_received = $this->Transaction->countByStatus(Transaction::STATUS_PACKAGE_RECEIVED);
		$count_awaiting_payment = $this->Transaction->countByStatus(Transaction::STATUS_AWAITING_PAYMENT);
		$count_payment_received = $this->Transaction->countByStatus(Transaction::STATUS_PAYMENT_RECEIVED);
		$count_package_sent = $this->Transaction->countByStatus(Transaction::STATUS_PACKAGE_SENT);
		
        $count_waiting_package_registered = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_WAITING_PACKAGE);
		$count_package_received_registered = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_PACKAGE_RECEIVED);
		$count_awaiting_payment_registered = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_AWAITING_PAYMENT);
		$count_payment_received_registered = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_PAYMENT_RECEIVED);
		$count_package_sent_registered = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_PACKAGE_SENT);
        $count_registered = $this->User->countUnread();

		$transaction_statuses = $this->Transaction->getStatuses();
		$countries = $this->Country->find('list', array(
			'order' => array('Country.name' => 'ASC')
		));
		$item_types = $this->ItemType->find('list', array(
			'fields' => array('id', 'description'),
			'order' => array('ItemType.description' => 'ASC')
		));
           
		$this->response->compress();
		$_jsonOptions = 0; // JSON_FORCE_OBJECT + JSON_NUMERIC_CHECK;
		$_serialize = array('transactions', 'transaction_statuses', 'countries', 'item_types', 'count_waiting_package', 'count_package_received', 'count_awaiting_payment', 'count_payment_received', 'count_package_sent',   'count_waiting_package_registered', 'count_package_received_registered', 'count_awaiting_payment_registered', 'count_payment_received_registered', 'count_package_sent_registered',  'messages','businesssignups','messages_registered','transactions_registered','registereds','count_registered', 'transactions_ghost', 'count_waiting_package_ghost');
		$this->set(compact('transactions', 'transaction_statuses', 'countries', 'item_types', 'count_waiting_package', 'count_package_received', 'count_awaiting_payment', 'count_payment_received', 'count_package_sent', 'count_waiting_package_registered', 'count_package_received_registered', 'count_awaiting_payment_registered', 'count_payment_received_registered', 'count_package_sent_registered', 'messages', 'businesssignups', 'messages_registered', 'transactions_registered','registereds','count_registered', 'transactions_ghost', 'count_waiting_package_ghost', '_jsonOptions', '_serialize'));
	}

	/**
	 * @param null $id
	 */
	public function admin_api_read($id = null)
	{
		$this->loadModel('Transaction');
		
		if (!$this->Transaction->exists($id)) {
			throw new NotFoundException(__('Item not found'));
		}
		
		$this->Transaction->id = (int)$id;
		$this->Transaction->saveField('unread', 0);
		$this->response->compress();
		
		$transaction_detail = $this->Transaction->getTransaction((int)$id);


		CakeLog::write('debug', "transaction_detail: " . print_r($transaction_detail, true));

		//processing photos
        $requested = array();
        $shipped = array();

        foreach ($transaction_detail['Photos'] as $photo) {
            if ($photo['type'] == 'requested') {
                array_push($requested, $photo);
            }
            else {
                array_push($shipped, $photo);
            }
        }

        $transaction_detail['requestedPhotos'] = $requested;
        $transaction_detail['shippingPhotos'] = $shipped;


		// Simplify weight and size units for output
		$this->Transaction->processPackageUnits($transaction_detail);
		
		$this->set(compact('transaction_detail'));
        $this->set('_serialize',  array('transaction_detail'));
	}

	/**
	 * @param $id
	 */
	public function admin_api_status($id)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException(__('Method not allowed'));
		}
		
        $this->loadModel('Transaction');
        $this->loadModel('Package');

		$this->Transaction->id = (int)$id;
		if (!$this->Transaction->exists()) {
			throw new NotFoundException(__('Item not found'));
		}
		$new_status = $this->request->data['Transaction']['status'];
		if (!$this->Transaction->saveField('status', $new_status)) {
			throw new InvalidArgumentException(__('Data cannot be saved'));
		}
		//$transaction = $this->Transaction->read(null, $id);
        $data = $this->Transaction->getTransaction($id);
        CakeLog::write('debug', "transaction: " . print_r($this->Transaction, true));

        if (isset($data['Transaction']['user_id'])) {                
            $order_suite = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1) . $data['Transaction']['user_id'];
        } else {                
            $order_suite = $data['Transaction']['id'];
        }

        $name = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1);
        if ($data['Transaction']['user_id'] != null) {
            $data['qsns'] = 'NS#' . $name . $data['Transaction']['user_id'];
            //get unsent package count
            $data['unsent_packages'] = $this->Transaction->find('count', array(
                'conditions' => array('Transaction.status <' => '5', 'Transaction.user_id' => $data['Transaction']['user_id'])
            ));
        }
        else {
            $data['qsns'] = 'QS#'. $name . $data['Transaction']['id'];
            $data['unsent_packages'] = 0;
        }

        $data['unsent_packages'] = $unsent_packages = 0;

        CakeLog::write('debug', 'Unsent Pakacges: ' . $unsent_packages, true);

		switch ($new_status) {
			case Transaction::STATUS_PACKAGE_RECEIVED:
				$this->Notification->sendCustomerPackageReceivedEmail($data);
                CakeLog::write('debug', "Packaged required email sent: ");
                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $id);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Package received email sent');
                $this->Note->save();
				break;

            case Transaction::STATUS_PAYMENT_RECEIVED:
                $this->Notification->sendCustomerPaymentReceivedEmail($data, $order_suite);
                CakeLog::write('debug', "Payment received email sent: ");
                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $id);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Payment received email sent');
                $this->Note->save();
                break;

            case Transaction::STATUS_AWAITING_PAYMENT:
                $paylink = $data['Transaction']['paypal_invoice_payer_view_url'];

                $this->Notification->sendCustomerPackageReceivedEmail($data, "we-ve-received-your-package-payment-required",
                    'We have received your package - Payment required', 0, $paylink);

                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $id);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Package received. Payment required email sent');
                $this->Note->save();
                CakeLog::write('debug', "Payment required email sent: ");
                break;

			case Transaction::STATUS_PACKAGE_SENT:
                $tracking_number = "";
                $out_carrier = "";
                foreach($data['Shipment'] as $shipment) {
                    CakeLog::write('debug', "shipment: " . print_r($shipment, true));
                    
                    $this->Package->id = $shipment['Package']['id'];
                    $track = $this->Package->field('out_tracking');
                    $carr = $this->Package->field('out_carrier');
                    CakeLog::write('debug', "track: " . $track);

                    if ($track != "") {
                        if ($tracking_number != "")
                            $tracking_number = $tracking_number . ", ";
                        $tracking_number = $tracking_number . $track;
                    }
                    if ($carr != "") {
                        if ($out_carrier != "")
                            $out_carrier = $out_carrier . ", ";
                        $out_carrier = $out_carrier . $carr;
                    }
                    
                }
                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $id);
                $user_id = ($data['Transaction']['user_id']) ? $data['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Shipment Status Updated: Package sent');
                $this->Note->save();
				$this->Notification->sendCustomerShipmentSentEmail($data, $order_suite, $tracking_number, $out_carrier);
				break;
		}

        $result = true;
        $_serialize = array('result');
		$_jsonOptions = JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT;
		$this->set(compact('result', '_serialize', '_jsonOptions'));
		//$this->sendJsonResponse(array('saved' => true));
	}

    /**
     * @param $id
     * @throws Exception
     */
	public function admin_api_update($id)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException(__('Method not allowed'));
		}

		$data = json_decode(file_get_contents('php://input'));
        CakeLog::write('debug', "admin_api_update: " . print_r($data, true));
		
        // Save Transaction
        $this->Transaction->id = $data->Transaction->id;
        CakeLog::write('debug', "about to call save");

        $tran = array(
            'Transaction' =>  json_decode(json_encode($data->Transaction), true)
        );

        try {
            $this->Transaction->save($tran);

        } catch (Exception $e) {
            CakeLog::write('error', 'error saving Transaction: ' . print_r($e, true));
            throw $e;
        }

        // CakeLog::write('debug', 'validation errors: ' . print_r($this->Transaction->validationErrors, true));
        // CakeLog::write('debug', 'last sql query: ' . print_r($this->Transaction->getDataSource()->getLog(false, false), true));

        // Save Shipments
        $this->loadModel('Shipment');
        foreach($data->Shipment as $shipment) {
            // put computed field values into db fields
            //Weight
            switch ($shipment->Package->weight_unit) {
                case "lb":
                    $shipment->Package->weight_lb = $shipment->Package->weight;
                    $shipment->Package->weight_kg = null;
                    break;
                case "kg":
                    $shipment->Package->weight_lb = null;
                    $shipment->Package->weight_kg = $shipment->Package->weight;
                    break;
            }

            //Width
            switch ($shipment->Package->width_unit) {
                case "in":
                    $shipment->Package->width_in = $shipment->Package->width;
                    $shipment->Package->width_cm = null;
                    break;
                case "cm":
                    $shipment->Package->width_in = null;
                    $shipment->Package->width_cm = $shipment->Package->width;
                    break;
            }

            //Length
            switch ($shipment->Package->length_unit) {
                case "in":
                    $shipment->Package->length_in = $shipment->Package->length;
                    $shipment->Package->length_cm = null;
                    break;
                case "cm":
                    $shipment->Package->length_in = null;
                    $shipment->Package->length_cm = $shipment->Package->length;
                    break;
            }

            //height
            switch ($shipment->Package->height_unit) {
                case "in":
                    $shipment->Package->height_in = $shipment->Package->height;
                    $shipment->Package->height_cm = null;
                    break;
                case "cm":
                    $shipment->Package->height_in = null;
                    $shipment->Package->height_cm = $shipment->Package->height;
                    break;
            }

            $this->Shipment->id = $shipment->id;
            $shipment = array(
                'Shipment' =>  json_decode(json_encode($shipment), true)
            );
            CakeLog::write('debug', "shipment: " . print_r($shipment, true));
            try {
                $this->Shipment->saveAssociated($shipment, array('deep' => true));              

            } catch (Exception $e) {
                CakeLog::write('error', 'error saving shipment: ' . print_r($e, true));
                throw $e;
            }
        }

        CakeLog::write('debug', "about to sendJsonResponse");       

        $result = true;
        $_serialize = array('result');
        $_jsonOptions = JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT;
		$this->set(compact('result', '_serialize', '_jsonOptions'));

        //$this->sendJsonResponse(array('saved' => true));
	}

}
