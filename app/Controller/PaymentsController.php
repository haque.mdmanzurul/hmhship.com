<?php

App::uses('AppController', 'Controller');
App::uses('Note', 'Model');
App::uses('User', 'Model');

/**
 * Class PaymentsController
 *
 * @property Transaction $Transaction
 * @property NotificationComponent $Notification
 * @property CryptComponent $Crypt
 */
class PaymentsController extends AppController
{

	/**
	 * Controller components
	 * @var array
	 */
	public $components = array(
		'SessionObject', 'Paypal', 'Notification', 'Crypt'
	);

        public function isAuthorized($user) {

            return true;
        }

	/**
	 * Updates Quickship transaction payment status
	 */
	public function complete()
	{
		$this->autoRender = false;
		$temp_transaction_ids = explode(',', $this->Session->read('bulk_transactions_ids'));
		if (count($this->request->query) > 0) {
			$params = $this->request->query;
            $tranid = null;
			CakeLog::write('debug', " bulk_transactions_ids found payment/complete: " .print_r($temp_transaction_ids, true));
            CakeLog::write('debug', "Paypal posted data" . print_r($params, true));
			if (isset($params['paymentId']) || isset($params['token'])) { // paypal transaction
                CakeLog::write('debug', " Paypal  do_express_checkout " . print_r($params, true));
			    $this->loadModel('Transaction');
				
                if (isset($params['paymentId'])) {
					$transaction = $this->Transaction->findByCode($params['paymentId']);
                    // Rest API payment was done
                    if ($this->Paypal->execute_payment($params['paymentId'], $_GET['PayerID']) != 1)
                            return null;
                    
                    $tranid = $params['paymentId'];
                    $payment_details = $this->Paypal->returnPayment($tranid);
                    CakeLog::write('debug', " Payment Resources " . print_r($payment_details, true));
                    $transactions = $payment_details->getTransactions();
                    $relatedResources = $transactions[0]->getRelatedResources();
                    $sale = $relatedResources[0]->getSale();
                    $saleId = $sale->getId();
                }
                else {

                    // SetExpressCheckout was done.  We have token and PayerId
					$transaction = $this->Transaction->findByCode($params['token']);
                    // do a DoExpressCheckout to complete the transaction and get a Transaction ID
                    $fee = (count($temp_transaction_ids) > 0) ? (count($temp_transaction_ids) * 7.25) : 7.25;
					$saleId = $tranid = $this->Paypal->do_express_checkout($params['token'], $params['PayerID'], $fee);
                    CakeLog::write('debug', "Paypal  do_express_checkout " . print_r($tranid, true));
					
                }


                //Save note
                $this->loadModel('Note');
                $this->loadModel('User');
                $user = $this->User->find('first', array(
                        'conditions' => array(
                            'User.id' => AuthComponent::user('id')
                        )
                    )
                );


                //Update notes
				$this->Note->set('transaction_id', $transaction['Transaction']['id']);
				$this->Note->set('user_id', AuthComponent::user('id'));

				if($user) {
					$name = strtoupper(substr($user['User']['first_name'], 0, 1) . substr($user['User']['last_name'], 0, 1));
					$nsqs = ' NS#' . $name . AuthComponent::user('id');
				}
				else {
					$name = substr($transaction['AddressBilling'][0]['firstname'],0,1) . substr($transaction['AddressBilling'][0]['lastname'],0,1);
					$nsqs = ' QS#' . $name . $transaction['Transaction']['id'];
				}
					
				
				if (count($temp_transaction_ids) > 0) {
					$this->Note->set('note', 'Handling fee paid for ' . $nsqs . '. Paypal  Transaction ID ' . $saleId . ' . Consolidated packages from Ghost parcels: ID: ' . $this->Session->read('bulk_transactions_ids'));
				}
				else {
					$this->Note->set('note', 'Handling fee paid for ' . $nsqs . '. Paypal  Transaction ID ' . $saleId);
				}
				$this->Note->save();

                //Update shipment status
                $hash_id = null;
                
                $this->Transaction->update_status( $tranid, Transaction::STATUS_WAITING_PACKAGE);


				$data = $this->Transaction->getTransaction($transaction['Transaction']['id']);
                // Is this a Shipment or a QuickShip?  Shipments are created by Signed-in Users
                if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
                    $data['isShipment'] = true;
                }
                else {
                    $data['isShipment'] = false;
                }

                if ($this->Session->read('ghost_shipment_id')) {
                    // Axo 9762 They are completing a Ghost Shipment
                    // package was already received
                    $this->Transaction->update_status( $tranid, Transaction::STATUS_PACKAGE_RECEIVED);

                    // delete the Ghost Shipment, this new Shipment replaces it
                    $this->Transaction->delete($this->Session->read('ghost_shipment_id'));

                    // send email to admin

                    CakeLog::write('debug', print_r($data, true));
                    $data['ghost_shipment'] = 'Ghost #' . $this->Session->read('ghost_shipment_id') . ' complete.  GP#' . $transaction['Transaction']['id'];

                }
                else {
                    // Normal QuickShip / Shipment
				    // send email to customer and generate Order HTML
					
					if (count($temp_transaction_ids) > 0) {
						$this->Transaction->update_status( $tranid, Transaction::STATUS_PACKAGE_RECEIVED);
						foreach ($temp_transaction_ids as $tid) {
							$this->Transaction->update_type( $tid, '9');
							$this->Transaction->update_status_by_id( $tid, '9');
						}
						$this->Notification->sendCustomerPackageReceivedActionRequiredEmail($data);
					}
					
					
					$this->Transaction->id = $transaction['Transaction']['id'];
					
                }
				$this->Notification->sendAdminQuickShipEmail($data);

				CakeLog::write('debug', 'Email data' . print_r($data, true));
				
                // send email to admin
				//$order_html = $this->Notification->sendTransactionCompleteEmail($data);

                $this->Session->delete('bulk_transactions_ids');

                if ($hash_id)
                    return $this->redirect(array('controller' => 'payments', 'action' => 'shipping_address', $this->Crypt->encrypt($hash_id)));
                else
				    return $this->redirect(array('controller' => 'payments', 'action' => 'shipping_address', $this->Crypt->encrypt($transaction['Transaction']['id'])));
			}
		}

		exit;
	}

	/**
	 * After a transaction has been completed, display US address for customer
	 * @param $transaction_id
	 */
	public function shipping_address($transaction_id)
	{
		$this->loadModel('Transaction');
		$this->helpers[] = 'Transaction';
		$decrypted_transaction_id = $this->Crypt->decrypt($transaction_id);
		$data = $this->Transaction->getTransaction($decrypted_transaction_id);
		$address = Configure::read('Shipping.from_address');
        $shipping_no = '1';

		$address1 = $address['street1'];
		$address2 = $address['street2'];
		$address3 = $address['city'] . ', ' . $address['state'] . ' ' . $address['zip'];

        if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
            $registered_users_transactions = $this->Transaction->listRegisteredTransactions();
            if (count($registered_users_transactions) > 0) {
                foreach($registered_users_transactions as $transaction) {
                    if ($transaction['Transaction']['id'] != $decrypted_transaction_id) continue;
                        $shipping_no = $transaction['Transaction']['user_id_repeat'];
                }
            }
            // User is Logged In
            $co_code = "NS#" . strtoupper(substr(AuthComponent::user('first_name'),0,1)) .
                strtoupper(substr(AuthComponent::user('last_name'),0,1)) .
                AuthComponent::user('id');

        } else {
            // no user logged in
            $unregistered_users_transactions = $this->Transaction->listTransactions();
            if (count($unregistered_users_transactions) > 0) {
                foreach($unregistered_users_transactions as $transaction) {
                    if ($transaction['Transaction']['id'] != $decrypted_transaction_id) continue;
                    $shipping_no = $transaction['Transaction']['user_id_repeat'];
                }
            }
            $co_code = "QS#"
                . strtoupper(substr($data['AddressBilling'][0]['firstname'], 0, 1))
                . strtoupper(substr($data['AddressBilling'][0]['lastname'], 0, 1))
                . $decrypted_transaction_id;

        }

        if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')){
            $msg = "We are done! Your package(s) will be forwarded from your US Address.";
        } else {

            $msg = "We are done! You can start sending your packages to your US Address.<br/>
                    This address was also sent to your email.";
        }

        if ($this->Session->read('ghost_shipment_id')) {
           // Axo 9762 They are completing a Ghost Shipment
           // package was already received
           $msg = "Ghost Shipment complete.  Your new Shipment Number is " . $decrypted_transaction_id .
             "  We will begin processing your package right away.";

           $this->Session->write('ghost_shipment_id', null);
        }

		$this->set(compact('data', 'address1', 'address2', 'address3', 'co_code', 'msg'));
	}

	/**
	 * Cancel payment action
	 */
	public function cancel()
	{
		// TODO: define what to do with a cancelled payment
        $transaction_id = $this->Session->read('last_transaction_id');
        $this->loadModel('User');
        $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => AuthComponent::user('id')
                )
            )
        );
        $this->loadModel('Note');
        $this->Note->set('transaction_id', null);
        $user_id = isset($user['User']['id']) ? $user['User']['id'] : 9999999;
        $this->Note->set('user_id', $user_id);
        $name = isset($user['User']['id']) ? strtoupper(substr($user['User']['first_name'], 0, 1) .
            substr($user['User']['last_name'], 0, 1)) : 'Anonymous User';
        $nsqs = ($user_id != 9999999) ? 'NS#' . $name . AuthComponent::user('id') : 'QS#' . $name;
        $this->Note->set('note', 'Payment cancelled by user ' . $nsqs . '. Cancellation ID ' . $this->request->data('token'));
        $this->Note->save();
	}

    public function reference_tran($ref_tranid, $amount) {

        CakeLog::write('debug', "reference tran");
        $msg = "";

        $this->loadModel('Transaction');
        $this->loadModel("Shipment");

         // get the Original Transaction record from the database
         $data = $this->Transaction->getTransaction($ref_tranid);

         // issue a Ref Transaction for the specified amount, using the same Name and Address as Original
        $resp = $this->Paypal->reference_transaction($data['Transaction']['code'], $amount, $data['AddressBilling'][0]['firstname'],
                $data['AddressBilling'][0]['lastname'], $data['AddressBilling'][0]['address1'], $data['AddressBilling'][0]['city'],
                $data['AddressBilling'][0]['adm_division'], $data['AddressBilling'][0]['Country']['code'], $data['AddressBilling'][0]['postal_code']);

        if (!isset($resp) || !isset($resp->DoReferenceTransactionResponseDetails) || !isset($resp->DoReferenceTransactionResponseDetails->PaymentInfo)) {

            if (!isset($resp))
                CakeLog::write('error', "Paypal->reference_transaction returned null");
            else {
                CakeLog::write('debug', "reference_tran: resp: " . print_r($resp, true));
                if (!isset($resp->DoReferenceTransactionResponseDetails) )
                    CakeLog::write('error', "DoReferenceTransactionResponseDetails is null");
                else if ( !isset($resp->DoReferenceTransactionResponseDetails->PaymentInfo)) {
                    CakeLog::write('error', "PaymentInfo is null");
                }
            }

            $msg = "Transaction failed.";
            $this->set(compact('msg'));
            return;
        }

        // get the new Paypal Tran ID
        $tranID = $resp->DoReferenceTransactionResponseDetails->PaymentInfo->TransactionID;

        if (!isset($tranID) || $tranID == "") {
            $msg = "Transaction failed.";
            $this->set(compact('msg'));
            return;
        }

        // create a new DB Transaction record for this new PayPal transaction
         $transaction = $this->Transaction->createTransaction(array(
            'type_id' => Transaction::TYPE_REFERENCE,
            'charge_now' => true,
            'code' => $tranID,
            'date' => date('Y-m-d H:i:s'),
            'reference_tran' => $ref_tranid,
            'expedited' => 0,
            'insured' => 0,
            'repackaged' => 0,
            'special_instructions' => '',
            'ship_fast' => 0,
            'ship_cheap' => 0,
            'amount' => $amount
        ), array());

        CakeLog::write('debug', "decrease Balance Due");
        // decrease Balance Due (if there is one)
        $balance_due = $data['Shipment'][0]['balance_due'];
        if ($balance_due > 0) {
            $balance_due = $balance_due - $amount;
            $this->Shipment->id = $data['Shipment'][0]['id'];
            $this->Shipment->saveField('balance_due', $balance_due);
        }

        // Send Payment Received email
        if ($balance_due > 0)
            $message = "Your new balance is $" . number_format($balance_due, 2);
        else
            $message = "We'll have your package shipped to you right away!";

        CakeLog::write('debug', "Reference_tran data: " . print_r($data, true));

        if (isset($data['Transaction']['user_id'])) {
            $order_suite = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1) . $data['Transaction']['user_id'];
        } else {
            $order_suite = $data['Transaction']['id'];
        }

        // CakeLog::write('debug', "sending Payment Received email");
        // $this->Notification->sendPaymentReceivedEmail($data, $message, $order_suite);

        $this->set(compact('msg'));
        return 1;
    }

     public function refund_tran($tranid, $amount) {

        $this->loadModel('Transaction');

        // get the Original Transaction record from the database
        $data = $this->Transaction->getTransaction($tranid);

        $refund_tranid = $this->Paypal->refund_payment($data['Transaction']['code'], $amount);

        if (!isset($refund_tranid))
            throw new Exception('Refund failed');

        // create a new DB Transaction record for this Refund transaction
         $transaction = $this->Transaction->createTransaction(array(
            'type_id' => Transaction::TYPE_REFUND,
            'charge_now' => true,
            'code' => $refund_tranid,
            'date' => date('Y-m-d H:i:s'),
            'refund_tran' => $tranid,
                            'expedited' => 0,
                            'insured' => 0,
                            'repackaged' => 0,
                            'special_instructions' => '',
                            'ship_fast' => 0,
                            'ship_cheap' => 0,
                            'amount' => $amount
        ), array());


         $this->loadModel('Note');
         $user = $this->User->find('first', array(
                 'conditions' => array(
                     'User.id' => $data['Transaction']['user_id']
                 )
             )
         );
         $this->loadModel('Note');
         $this->Note->set('transaction_id', $tranid);
         $user_id = ($user['User']['id']) ? $user['User']['id'] : 9999999;
         $this->Note->set('user_id', $user_id);
         $name = strtoupper(substr($user['User']['first_name'], 0, 1) .
             substr($user['User']['last_name'], 0, 1));
         $nsqs = ' NS#' . $name . AuthComponent::user('id');
         $this->Note->set('note', 'Transaction refunded for ' . $nsqs . '. Refund ID ' . $refund_tranid);
         $this->Note->save();

         return null;
    }
}
