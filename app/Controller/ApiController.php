<?php

App::uses('AppController', 'Controller');

/**
 * Class ApiController
 */
class ApiController extends AppController
{
	public $components = array('RequestHandler');

         public function isAuthorized($user) {       
        
            return true;
        }

	// Shipping Carriers //////////////////////////////////////////////////////////////////////////

	public function ups()
	{
		$data = array(
			'method' => 'ups',
			'test' => realpath(dirname(__FILE__) . '/../../vendors/easypost-php')
		);
		$this->output($data);
	}

	public function usps()
	{
		// USPS (Stamps.com)
		$this->output(array(
			'method' => 'usps'
		));
	}

	public function dhl()
	{
		$this->output(array(
			'method' => 'dhl'
		));
	}

	public function fedex()
	{
		$this->output(array(
			'method' => 'fedex'
		));
	}

	public function test()
	{
		
		
	}

	// Financials /////////////////////////////////////////////////////////////////////////////////

	public function paypal()
	{
		$this->output(array(
			'pay' => 'paypal'
		));
	}

	public function credit_card()
	{
		// FirstData
		$this->output(array(
			'pay' => 'credit_card'
		));
	}

	private function output($data)
	{
		$this->set($data);
		$this->set("_serialize", array_keys($data));
	}
}
