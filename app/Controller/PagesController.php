<?php

/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('Hmhship\Quickship\Package', 'Vendor');
App::uses('Hmhship\Quickship\Address', 'Vendor');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 *
 * @property FlashComponent $Flash
 * @property Contact $Contact
 */
class PagesController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public $components = array(
		'Shipping', 'Notification'
	);

    public function isAuthorized($user) {

        return true;
    }

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *    or MissingViewException in debug mode.
     */
    public function display() {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    // user submitted a Talk To Us message
    public function contact() {

        $this->set('title_for_layout', 'Talk to Us — Parcel Forwarding — HMHShip');
        $this->set('meta_description_for_layout', 'Send your questions and concerns to HMHShip using our contact form, here. We work to respond to all queries as quickly as possible!');

        if ($this->request->is('post')) {

            $this->loadModel('Contact');
            $this->Contact->set($this->request->data);
            if ($this->Contact->validates()) {
                // save to database
                $this->loadModel('Message');
                $message = $this->Message->createMessage(array(
                    'name' => $this->request->data['Contact']['contact_name'],
                    'email' => $this->request->data['Contact']['contact_email'],
                    'subject' => $this->request->data['Contact']['contact_subject'],
                    'message' => $this->request->data['Contact']['contact_message'],
                    'tran_number' => $this->request->data['Contact']['contact_transaction_number'],
                    'tran_type' => $this->request->data['Contact']['contact_transaction_type'],
                    'updated' => date("Y-m-d H:i:s")
                ));

                // Email to client
                try {
                    App::uses('CakeEmail', 'Network/Email');
                    $email = new CakeEmail($this->_getSmtpEnvironment());
                    $from = Configure::read('Contact.email');
                    $email->from($from);
                    $email->to($this->request->data['Contact']['contact_email']);
                    $email->subject('Thanks for Talking to Us!');
                    $viewVars = Hash::extract($this->request->data, 'Contact');
                    $email->addHeaders(array(
                        'X-MC-Template' => 'thanks-for-talking-to-us',
                        'X-MC-MergeVars' => json_encode($viewVars)
                    ));
                    $email->viewVars($viewVars);
                    $email->send();
                    \Hmhship\Logger::getInstance()->log(sprintf('Email sent with data: %s to %s', json_encode($viewVars), $this->request->data['Contact']['contact_email']), 'debug');
                    $this->Flash->set(__('Message sent'), array('element' => 'success'));
                } catch (SocketException $e) {
                    \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
                } catch (Exception $e) {
                    \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
                }

                // Email to admin
                App::uses('CakeEmail', 'Network/Email');
                $email = new CakeEmail($this->_getSmtpEnvironment());
                $to = "contact@hmhship.com";
                $email->to($to);
                $email->subject('New Talk To Us Received!');
                $viewVars = Hash::extract($this->request->data, 'Contact');
                $email->addHeaders(array(
                    'X-MC-Template' => 'new-talk-to-us-received',
                    'X-MC-MergeVars' => json_encode($viewVars)
                ));
                $email->viewVars($viewVars);
                try {
                    $email->send();
                    \Hmhship\Logger::getInstance()->log(sprintf('Email sent with data: %s to %s', json_encode($viewVars), $to), 'debug');
                    $this->Flash->set(__('Message sent'), array('element' => 'success'));
                    unset($this->request->data);
                } catch (SocketException $e) {
                    \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
                } catch (Exception $e) {
                    \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
                }

            } else {
                $this->Flash->set(__('Failed to send message. Check for errors'), array('element' => 'error'));
            }
        }
    }


    public function business() {
        $this->set('title_for_layout', 'International Parcel Forwarding Service — HMHShip');
        $this->set('meta_description_for_layout', 'By forwarding packages from us, you can easily serve customers all over the world. Find out how much you can save with our special volume discounts!');
    }

    public function deactivated() {
        $this->set('title_for_layout', 'Deactivated');
        $this->set('meta_description_for_layout', 'Deactivated');
    }

    public function reset2() {
        $this->set('title_for_layout', 'Users');
        $this->set('meta_description_for_layout', 'Reset');
    }

    public function business_form() {

        $this->set('title_for_layout', 'Business Form — Sign Up with HMHSHip');
        $this->set('meta_description_for_layout', 'Our business clients have access to great discounts on international parcel forwarding services. Sign up here to find your shipping options.');

        $this->loadModel('Country');
        $countries_list = $this->Country->getList();
        $countries = array();
        foreach ($countries_list as $country_id => $country_name) {
            $countries[$country_id] = $country_name;
        }

        if ($this->request->is('post')) {
            $this->loadModel('BusinessSignup');
            $this->BusinessSignup->set($this->request->data);
            if (!$this->BusinessSignup->validates()) {
                $this->Flash->set(__('Please enter all required fields'), array('element' => 'error'));
                return;
            }

            $this->BusinessSignup->createBusinessSignup($this->request->data);

            // send email to admin
            $data = $this->request->data;
            // get Country name
            $data["country"] = $countries[$data["country_id"]];
            // get Shipping Plan
            switch($data["shipping_plan"]) {
                case "0":
                    $data["shipping"] = "0 – 25 PKGS $0.00 Per Month";
                    break;
                case "25":
                    $data["shipping"] = "25 – 100 PKGS $25.00 Per Month";
                    break;
                case "100":
                    $data["shipping"] = "100 – 500 PKGS $100.00 Per Month";
                    break;
                case "500":
                    $data["shipping"] = "500+ PKGS $500.00 Per Month";
                    break;
                default:
                    $data["shipping"] = "";
                    break;
            }


            CakeLog::write('debug', "calling sendAdminBusinessEmail");
            CakeLog::write('debug', print_r($data, true));
			$this->Notification->sendAdminBusinessEmail($data);
            $this->Notification->sendCustomerBusinessEmail($data["email"], $data);

            $this->Flash->success('Your information was saved.');
        }

        $this->set(compact('countries'));
    }

    public function what_we_do() {
        $this->set('title_for_layout', 'US Parcel Forwarding Service — HMHShip');
        $this->set('meta_description_for_layout', 'Using our package and mail forwarding services, you can receive products or serve customers all over the world. Our package forwarding services are fast and secure!');
    }

    public function register() {

        $this->set('title_for_layout', 'Register — Pay Shipping Fees — HMHShip');
        $this->set('meta_description_for_layout', 'Pay here when you\'re ready to finalize your package forwarding order.');


        $this->loadModel('Country');
        $countries_list = $this->Country->getList();
        $countries = array();
        foreach ($countries_list as $country_id => $country_name) {
            $countries[$country_id] = $country_name;
        }
        $this->set(compact('countries'));

        if ($this->request->is('post')) {

            if ($this->request->data['password'] != $this->request->data['confirm_password']) {
                $this->Flash->set(__('Passwords do not match'), array('element' => 'error'));
                return;
            }

            $this->loadModel('User');
            $data = $this->request->data;
            CakeLog::write('debug', print_r($data, true));

            $data["first_name"] = $data["billing"]["first_name"];
            $data["last_name"] = $data["billing"]["last_name"];
            $this->User->set($data);
            if (!$this->User->validates()) {
                $this->Flash->set(__('Please enter all required fields'), array('element' => 'error'));
                //return;
            }

            // validation passed
            $this->loadModel('Address');

            // Create addresses
            $array_to_add = array();
            $data["billing"]["email"] = $data["email"];
            $data["shipping"]["email"] = $data["email"];
            $data["username"] = $data["email"];
            $data["billing"]["firstname"] = $data["billing"]["first_name"];
            $data["billing"]["lastname"] = $data["billing"]["last_name"];
            $data["shipping"]["firstname"] = $data["shipping"]["first_name"];
            $data["shipping"]["lastname"] = $data["shipping"]["last_name"];
            $data["billing"]["adm_division"] = $data["billing"]["state"];
            $data["shipping"]["adm_division"] = $data["shipping"]["state"];
            $data["billing"]["country_id"] = $data["billing"]["country"];
            $data["shipping"]["country_id"] = $data["shipping"]["country"];
            $data["shipping"]["phone"] = "";

            $array_to_add["shipment"] = $data;

            // validate Billing Address
            $this->Address->set($data['billing']);
            if (!$this->Address->validates()) {
                $this->Flash->set(__('Please enter all required fields'), array('element' => 'error'));
                return;
            }

            if (array_key_exists("billing_shipping", $data)) {
                // billing same as shipping

                $shipping_address = $this->Address->addAddress($array_to_add);
                $this->Address->saveField('default_billing', 1);
                $this->Address->saveField('default_shipping', 1);
                $userData = $this->Address->getAddressFromData($shipping_address);
            } else {
                // billing and shipping are different
                // validate Shipping address
                $this->Address->set($data['shipping']);
                if (!$this->Address->validates()) {
                    $this->Flash->set(__('Please enter all required fields'), array('element' => 'error'));
                    return;
                }

                $billing_address = $this->Address->addAddress($array_to_add, Address::BILLING);
                $this->Address->saveField('default_billing', 1);

                $shipping_address = $this->Address->addAddress($array_to_add, Address::SHIPPING);
                $this->Address->saveField('default_shipping', 1);

                $userData = $this->Address->getAddressFromData($billing_address, Address::BILLING);
            }

            $this->User->create();

            $user = $this->User->save(array(
                'first_name' => Hash::get($userData, 'firstname'),
                'last_name' => Hash::get($userData, 'lastname'),
                'email' => Hash::get($userData, 'email'),
                'phone' => Hash::get($userData, 'phone'),
                'username' => Hash::get($userData, 'email'),
                'password' => $data["password"],
                'how_did_you_hear' => $data["how_did_you_hear"],
                'active' => 1
            ));

            if (!$user) {
                debug($this->User->validationErrors, false);
            }

            if (isset($user['User']['id']) && is_numeric($user['User']['id'])) {
                if (isset($billing_address['Address']['id'])) {
                    $this->Address->id = $billing_address['Address']['id'];
                    $this->Address->saveField('user_id', $user['User']['id']);
                }
                if (isset($shipping_address['Address']['id'])) {
                    $this->Address->id = $shipping_address['Address']['id'];
                    $this->Address->saveField('user_id', $user['User']['id']);
                }
            }

            // get Country Names
            if (array_key_exists("billing_shipping", $data)) {
                // billing same as shipping
                $data["billing"]["country"] = $countries[$data["billing"]["country_id"]];
                $data["shipping"]["country"] = "";
            }
            else {
                $data["billing"]["country"] = $countries[$data["billing"]["country_id"]];
                $data["shipping"]["country"] = $countries[$data["shipping"]["country_id"]];
            }

            CakeLog::write('debug', "about to send email");
            CakeLog::write('debug', print_r($data, true));
            // Send "New Account Registered!" email
            $this->Notification->sendAdminNewAccountEmail($data);
            CakeLog::write('debug', "after send email");

            // Send "Welcome to the HMHShip Team!" email to Customer
            $this->Notification->sendCustomerNewAccountEmail(Hash::get($userData, 'email'), $data);

            $data["id"] = $user['User']['id'];
            unset($data['password']);
            $this->Auth->login($data);

            $this->redirect(array('controller' => 'Users', 'action' => 'newuser'));
        } // is a POST
    }

    public function terms() {
        $this->set('title_for_layout', 'Terms and Conditions — Parcel Forwarding — HMHShip');
        $this->set('meta_description_for_layout', 'Terms and conditions are located here for customers using our parcel forwarding services. Contact us with any questions.');
    }

    public function privacy() {
        $this->set('title_for_layout', 'Privacy Policy — Parcel Forwarding — HMHShip');
        $this->set('meta_description_for_layout', 'Our privacy policies are available here for you to view. Please contact us with any questions regarding our policies or services.');
    }

    public function faq() {
        $this->set('title_for_layout', 'Frequently Asked Questions — HMHShip');
        $this->set('meta_description_for_layout', 'Find answers to your most common questions about our parcel service when you visit our FAQs page. From services to rates, it’s all here.');
    }

    public function bio() {
        $this->set('title_for_layout', 'About Us — The HMHShip Bio');
        $this->set('meta_description_for_layout', 'Learn more about our US parcel forwarding services here at HMHShip, how we do it, and what keeps us striving to always do better for our customers.');
    }

    public function home() {
        $this->set('title_for_layout', 'Parcel Forwarding Service from US to Anywhere — HMHShip');
        $this->set('meta_description_for_layout', 'Our US package forwarding service will deliver U.S. packages to any country in the world for an amazingly low fee. Start shipping today.');
    }

    public function shipping_calculator() {
        $this->set('title_for_layout', 'Shipping Calculator — Save on Shipping with HMHShip');
        $this->set('meta_description_for_layout', 'Use our shipping calculator to find out just how much you can save when you have your package forwarded by HMHShip.');

        $this->loadModel('Country');

        if (!$this->request->is('post') || $this->request->data["country"] == "0") {
            // load list of Countries
            $countries_list = $this->Country->getList();
            $countries = array();
            foreach ($countries_list as $country_id => $country_name) {
                $countries[$country_id] = $country_name;
            }
            $this->set(compact('countries'));
        }


        if ($this->request->is('post')) {
            $data = $this->request->data;

            $error = false;
            $this->Session->delete('country');
            $this->Session->delete('city');
            $this->Session->delete('state');
            $this->Session->delete('postal_code');
            $this->Session->delete('weight');
            $this->Session->delete('height');
            $this->Session->delete('width');
            $this->Session->delete('length');


            if ($data["country"] == "0") {
                $this->Session->write('country', __('Please select a Country'));
                $error = true;
            }

            if ($data["city"] == "0" || $data["city"] == "") {
                $this->Session->write('city', __('Please enter city'));
                $error = true;
            }

            if ($data["state"] == "0" || $data["state"] == "") {
                $this->Session->write('state', __('Please enter state'));
                $error = true;
            }

            if ($data["postal_code"] == "0" || $data["postal_code"] == "") {
                $this->Session->write('postal_code', __('Please enter postal code'));
                $error = true;
            }

            if ($data["weight"] == "0" || $data["weight"] == "" || round($data["weight"]) < 1) {
                $this->Session->write('weight', __('Please enter'));
                $error = true;
            }

            if ($data["height"] == "0" || $data["height"] == "" || round($data["height"]) < 1) {
                $this->Session->write('height', __('Please enter'));
                $error = true;
            }

            if ($data["width"] == "0" || $data["width"] == "" || round($data["width"]) < 1) {
                $this->Session->write('width', __('Please enter'));
                $error = true;
            }

            if ($data["length"] == "0" || $data["length"] == "" || round($data["length"]) < 1) {
                $this->Session->write('length', __('Please enter'));
                $error = true;
            }

            if($error) return;
            // lookup rates for address and package input

            $objShipping = new \Hmhship\Quickship\Address();

            $data["first_name"] = "";
            $data["last_name"] = "";
            $data["address1"] = "";
            $data["address2"] = "";
            $data["phone"] = "";
            $objShipping->set((object)$data);

            CakeLog::write('debug', 'Shipping address: ' . json_encode($objShipping));
            \Hmhship\Logger::getInstance()->log('Shipping address: ' . json_encode($objShipping), 'debug');

            $package_obj = new \Hmhship\Quickship\Package();

            // $package_obj->items = $package->items;

            // Size
            $package_obj->size_unit = $this->request->data["size_unit"];
            $package_obj->height = round($this->request->data["height"]);
            $package_obj->width =  round($this->request->data["width"]);
            $package_obj->length = round($this->request->data["length"]);

            // Weight
            $package_obj->weight_unit = $this->request->data["weight_unit"];
            $package_obj->weight = round($this->request->data["weight"]);


            $package_rates = $this->Shipping->get_rates($objShipping, $package_obj);

            $this->set(compact('package_rates'));
        }

    }


    /**
     * is this used?
     */
    public function business_contact() {
        if ($this->request->is('post')) {
            debug($this->request->data);
            debug(Hash::extract($this->request->data, '{n}.Contact'));
        }
    }

    protected function _getSmtpEnvironment() {
        $default = 'smtp_test';
        if (env('SERVER_NAME')) {
            switch (env('SERVER_NAME')) {
                case 'hmhship.com':
                case 'www.hmhship.com':
                    $default = 'smtp';
                    break;
            }
        }
        return $default;
    }

    public function prohibited() {

        // $this->set('title_for_layout', 'Prohibited Items — HMHShip');
        // $this->set('meta_description_for_layout', 'Our US package forwarding service will deliver U.S. packages to any country in the world for an amazingly low fee. Start shipping today.');
        $this->layout = null;
    }


    public function batchShipments() {
        // create addresses
        $from_address = array(
            "company" => "EasyPost",
            "street1" => "388 Townsend St",
            "city"    => "San Francisco",
            "state"   => "CA",
            "zip"     => "94107-8273",
            "phone"   => "415-456-7890"
        );
        $parcel = array(
            "predefined_package" => 'Parcel',
            "weight"             => 22.9
        );
        $customs_item = array(
            "description"      => "T Shirt",
            "currency"         => "USD",
            "value"            => 35.0,
            "quantity"         => 7,
            "weight"           => 22.9,
            "hs_tariff_number" => "123456"
        );
        $customs_info = array(
            "contents_type" => "merchandise",
            "customs_items" => array(
                $customs_item
            )
        );

// in your application orders will likely
// come from an external data source
        $orders = array(
            /*array(
                "address"  => array(
                    "name"    => "Ronald",
                    "street1" => "6258 Amesbury St",
                    "city"    => "San Diego",
                    "state"   => "CA",
                    "zip"     => "92114"
                ),
                "customs_info" => $customs_info,
                "reference"   => "123456786",
                "carrier"     => "USPS",
                "service"     => "Priority"
            ),
            array(
                "address"  => array(
                    "name"    => "Hamburgler",
                    "street1" => "8308 Fenway Rd",
                    "city"    => "Bethesda",
                    "state"   => "MD",
                    "zip"     => "20817"
                ),
                "customs_info" => $customs_info,
                "reference"   => "123456787",
                "carrier"     => "USPS",
                "service"     => "Priority"
            ),
            array(
                "address"  => array(
                    "name"    => "Grimace",
                    "street1" => "10 Wall St",
                    "city"    => "Burlington",
                    "state"   => "MA",
                    "zip"     => "01803"
                ),
                "customs_info" => $customs_info,
                "reference"   => "123456788",
                "carrier"     => "USPS",
                "service"     => "Priority"
            ),*/
            array(
                "address"  => array(
                    "name"    => "Cosmc",
                    "street1" => "3315 W Greenway Rd",
                    "city"    => "Phoenix",
                    "state"   => "AZ",
                    "zip"     => "85053"
                ),
                "customs_info" => $customs_info,
                "reference"   => "123456789",
                "carrier"     => "USPS",
                "service"     => "Express"
            )
            /*array(
                "address"      => array(
                    "company" => "Tim Hortons",
                    "street1" => "65 Queen St W",
                    "city"    => "Toronto",
                    "state"   => "CA",
                    "zip"     => "M5H2M5",
                    "phone"   => "+1 416-360-6776",
                    "country" => "CA"
                ),
                "customs_info" => $customs_info,
                "reference"    => "timhortons",
                "carrier"      => "USPS",
                "service"      => "ExpressMailInternational"
            )*/
        );

// get a list of all my batches
// $all = \EasyPost\Batch::all();
// print_r($all);

// retrieve a batch
// $batch = \EasyPost\Batch::retrieve('batch_0SLoY64K');

// create shipment batch
        $shipments = array();
        for($i = 0, $j = count($orders); $i < $j; $i++) {
            $shipments[] = array(
                "to_address"   => $orders[$i]["address"],
                "from_address" => $from_address,
                "parcel"       => $parcel,
                "customs_info" => $orders[$i]["customs_info"],
                "reference"    => $orders[$i]["reference"],
                "carrier"      => $orders[$i]["carrier"],
                "service"      => $orders[$i]["service"]
            );
        }

        $batch = \EasyPost\Batch::create(array('shipments' => $shipments));

// asynchronous creation means you can send us up to
// 1000 shipments at once, but you'll have to wait
// for the shipments to be created before you can continue
        while($batch->status->created != count($orders)) {
            sleep(5);
            $batch->refresh();
            if($batch->status->creation_failed != 0) {
                throw new \EasyPost\Error('One of your batch shipments was unable to be created. Please manually retrieve and review your batch.');
            }
        }

// if creation_failed == 0 and the while loop above ends
// we know that all shipments have been created
        $batch->buy();

// asyncronous purchasing means we have to watch
// for when all labels have been purchased
        while($batch->status->postage_purchased != count($orders)) {
            sleep(5);
            $batch->refresh();
            if($batch->status->postage_purchase_failed != 0) {
                throw new \EasyPost\Error('One of your batch shipments was unable to be purchased. Please manually retrieve and review your batch.');
            }
        }

// generate a consolidated file containing all batch labels
        $batch->label(array("file_format" => "pdf"));

        while(empty($batch->label_url)) {
            sleep(5);
            $batch->refresh();
        }

        print_r($batch);


// request a scan form
        //$batch->create_scan_form();

// wait for scan form to complete
        /*while(empty($batch->scan_form)) {
            sleep(5);
            $batch->refresh();
        }*/

// retrieve scan form
        //$scan_form = \EasyPost\ScanForm::retrieve($batch->scan_form->id);

        //print_r($scan_form);
    }
}
