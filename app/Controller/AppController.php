<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Hmhship\Logger', 'Vendor');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package        app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

	public $helpers = array(
		'Form',
		'Paginator',
		'Session',
		'Flash'
	);

	public $components = array(
		 'Auth' => array(
                    'loginRedirect' => array(
                        'controller' => 'Users',
                        'action' => 'myaccount'
                    ),
                    'logoutRedirect' => array(
                        'controller' => 'pages',
                        'action' => 'display'
                    ), 
                    'authenticate' => array(
                        'Form' => array(
                            'passwordHasher' => 'Blowfish'
                        )
                    ),
                    'authorize' => array('Controller'),
                    'loginAction' => array(
                        'controller' => 'Users',
                        'action' => 'login'
                    )
                ),
		'Paginator',
		'Session',
		'Flash',
		'RequestHandler'
	);

	public function beforeFilter()
	{
        //CakeLog::write('debug', print_r($this->request, true));

		$this->Auth->authError = __('You are not authorized to access that location.');
		if ($this->request->prefix == 'users') {
			$this->theme = 'Users';
			// Specify which controller/action handles logging in:
			AuthComponent::$sessionKey = 'Auth.Users'; // solution from http://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
			$this->Auth->loginAction = array('controller' => 'login', 'action' => 'login', 'users' => true);
			
			$this->Auth->logoutRedirect = array('controller' => 'login', 'action' => 'login', 'users' => true);
			$this->Auth->authenticate = array(
				AuthComponent::ALL => array(
					'userModel' => 'User',
					'scope' => array(
						'User.deleted' => 0
					),
					'passwordHasher' => 'Blowfish'
				),
				'Basic',
				'Form'
			);
			$this->Auth->allow('users_login');

		} elseif ($this->request->prefix == 'admin') {
            
            $this->theme = 'Admin';            
			
			// Specify which controller/action handles logging in:
			AuthComponent::$sessionKey = 'Auth.Admin'; // solution from thtp://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
			$this->Auth->loginAction = array('controller' => 'admins', 'action' => 'login', 'admin' => true);
			$this->Auth->loginRedirect = array('controller' => 'admins', 'action' => 'index', 'admin' => true);
			$this->Auth->logoutRedirect = array('controller' => 'admins', 'action' => 'login', 'admin' => true);
			$this->Auth->authenticate = array(
				AuthComponent::ALL => array(
					'userModel' => 'Admin',
					'contain' => false,
					'scope' => array(
						'Admin.active' => 1,
						'Admin.deleted' => 0
					),
					'passwordHasher' => 'Blowfish'
				),
				'Basic',
				'Form'
			);

			if ($this->request->accepts('application/json') && !$this->Auth->user('id')) {
				$this->Auth->unauthorizedRedirect = false;
				$this->response->statusCode(401);
				$this->response->body('Unauthorized access');
				$this->response->send();
				$this->_stop();
			}

        } elseif ($this->request->prefix == 'blogadmin') {
            // these page url's are hidden and only accessed from /admin Blog section in an iframe
            $this->theme = 'Admin';            
            $this->layout = 'blog';
			
			// Specify which controller/action handles logging in:
			AuthComponent::$sessionKey = 'Auth.Admin'; // solution from thtp://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
			$this->Auth->loginAction = array('controller' => 'admins', 'action' => 'login', 'admin' => true, 'plugin' => '');
			$this->Auth->loginRedirect = array('plugin'=>'blog', 'controller' => 'blog_posts', 'action' => 'index', 'blogadmin' => true);
			$this->Auth->logoutRedirect = array('controller' => 'admins', 'action' => 'login', 'admin' => true, 'plugin' => '');
			$this->Auth->authenticate = array(
				AuthComponent::ALL => array(
					'userModel' => 'Admin',
					'contain' => false,
					'scope' => array(
						'Admin.active' => 1,
						'Admin.deleted' => 0
					),
					'passwordHasher' => 'Blowfish'
				),
				'Basic',
				'Form'
			);
        
		} 
        elseif ($this->request->plugin == 'document_manager') {
            // these page url's are hidden and only accessed from /admin Files section in an iframe
            $this->theme = 'Admin';            
            $this->layout = 'documents';
			
			// Specify which controller/action handles logging in:
			AuthComponent::$sessionKey = 'Auth.Admin'; // solution from thtp://stackoverflow.com/questions/10538159/cakephp-auth-component-with-two-models-session
			$this->Auth->loginAction = array('controller' => 'admins', 'action' => 'login', 'admin' => true, 'plugin' => '');
			$this->Auth->loginRedirect = array('plugin'=>'document_manager', 'controller' => 'documents', 'action' => 'index');
			$this->Auth->logoutRedirect = array('controller' => 'admins', 'action' => 'login', 'admin' => true, 'plugin' => '');
			$this->Auth->authenticate = array(
				AuthComponent::ALL => array(
					'userModel' => 'Admin',
					'contain' => false,
					'scope' => array(
						'Admin.active' => 1,
						'Admin.deleted' => 0
					),
					'passwordHasher' => 'Blowfish'
				),
				'Basic',
				'Form'
			);
        
		} 
        else {
                    // No Prefix
                    $this->theme = 'Global';
                    $this->layout = 'default';
                    // Open access to all pages without a prefix except Users controller
                    if ($this->params['controller'] !== 'Users')
                        $this->Auth->allow();
                    
                    $this->Auth->logoutRedirect = array('controller' => 'pages', 'action' => 'display');
		}

		parent::beforeFilter();

		$this->set('config_json', json_encode(array(
			'base_url' => Router::url('/'),
			'handling_fee' => Configure::read('Shipping.rates.parcel_forwarded')
		), JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT));
	}

	/**
	 * Send a JSON response to client
	 *
	 * @param $body
	 */
	public function sendJsonResponse($body)
	{
		$this->response->compress();
		$this->response->statusCode(200);
		$this->response->type('application/json');
		// JSON_NUMERIC_CHECK required by AngularJS to play nicely with numeric values
		$this->response->body(json_encode($body, JSON_NUMERIC_CHECK));
		$this->response->send();
		$this->_stop();
	}

	/**
	 * Send a invalid JSON response to client
	 *
	 * @param $body
	 */
	public function sendJsonInvalidResponse($body)
	{
		$this->response->compress();
		$this->response->statusCode(500);
		$this->response->type('application/json');
		// JSON_NUMERIC_CHECK required by AngularJS to play nicely with numeric values
		$this->response->body(json_encode($body, JSON_NUMERIC_CHECK));
		$this->response->send();
		$this->_stop();
	}
        
      
}
