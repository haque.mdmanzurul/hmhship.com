<?php

App::uses('Note', 'Model');
App::uses('Photo', 'Model');

App::uses('AppController', 'Controller');

class UsersController extends AppController
{

    public $components = array(
        'Paypal', 'Notification'
    );

    public function account_settings()
    {
        //     $this->layout = 'user';

        $this->loadModel('Shipment');
        $this->loadModel('Transaction');

        // used by user.ctp
        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $count_waiting_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));
        $count_awaiting_payment = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT,
                'Shipment.balance_due >' => 0
            ),
            'order' => array('Shipment.Created')
        ));

        $this->set(compact('co_code', 'count_waiting_package', 'count_awaiting_payment'));

        if ($this->request->is('post')) {
            // Customer would like to DEACTIVATE account
            $data = $this->request->data;
            // get the current hashed password
            $user = $this->User->find('first', array(
                    'conditions' => array(
                        'User.id' => AuthComponent::user('id')
                    )
                )
            );

            $passwordHasher = new BlowfishPasswordHasher();
            if ($passwordHasher->check($data['password'], $user['User']['password'])) {
                // they typed in the correct pw - DEACTIVATE acct
                $this->User->id = AuthComponent::user('id');
                $this->User->saveField('active', 0);
                $this->Auth->logout();

                $mandrill = new Mandrill(Configure::read('Mandrill.key'));
                $url = 'https://hmhship.com/sign-in';
                $message = array(
                    'html' => '<p>We are sorry to see you go. Log back in to reactivate: <br/><br/>  <a href="' . $url . '" >' . $url . '</a></p>',
                    'subject' => 'HMHShip Account Deactivated',
                    'from_email' => 'deactivate@hmhship.com',
                    'from_name' => 'HMHShip.com',
                    'to' => array(
                        array(
                            'email' => $user['User']['email'],
                            'name' => $user['User']['first_name'] . ' ' . $user['User']['last_name'],
                            'type' => 'to'
                        )
                    ),
                    'global_merge_vars' => array(
                        array(
                            'name' => 'LINK',
                            'content' => "<a href='" . $url . "'>Reset Pasword</a>"
                        )
                    )
                );

                $result = $mandrill->messages->sendTemplate('we-re-sorry-to-see-you-go', '', $message, false);


                return $this->redirect("/deactivated");


            } else {
                $this->Flash->error(__('Password not correct.'));
                return;
            }
        }

    }


    public function account_settings_edit()
    {
        //     $this->layout = 'user';

        $this->loadModel('Shipment');
        $this->loadModel('Transaction');

        // used by Nav bar
        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $count_waiting_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));
        $count_awaiting_payment = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT,
                'Shipment.balance_due >' => 0
            )
        ));
        $this->set(compact('co_code', 'count_waiting_package', 'count_awaiting_payment'));

        if ($this->request->is('post')) {

            $data = $this->request->data;

            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->Flash->set(__('Email address not valid'), array('element' => 'error'));
                return;
            }

            // save email address
            $user = $this->Auth->user();
            CakeLog::write('debug', print_r($user, true));

            $user['email'] = $data['email'];
            $this->Session->write('Auth.User', $user);
            $this->User->id = AuthComponent::user('id');
            $this->User->saveField('email', $data['email']);
            $this->User->saveField('username', $data['email']);

            if ($data['old_password'] != "" && $data['new_password'] != "" && $data['confirm_new_password'] != "") {

                // Attempt to save New Password
                if ($this->request->data['new_password'] != $this->request->data['confirm_new_password']) {
                    $this->Flash->set(__('Passwords do not match'), array('element' => 'error'));
                    return;
                }

                CakeLog::write('debug', "email: " . $data['email']);
                CakeLog::write('debug', "old_password: " . $data['old_password']);

                // get the current hashed password
                $user = $this->User->find('first', array(
                        'conditions' => array(
                            'User.id' => AuthComponent::user('id')
                        )
                    )
                );
                CakeLog::write('debug', print_r($user, true));

                $passwordHasher = new BlowfishPasswordHasher();
                if ($passwordHasher->check($data['old_password'], $user['User']['password'])) {

                    $this->User->read(null, AuthComponent::user('id'));
                    $this->User->set('password', $data["new_password"]);

                    if ($this->User->save()) {
                        return $this->redirect(
                            array('controller' => 'Users', 'action' => 'account_settings')
                        );
                    } else {
                        $this->Flash->error("Password must contain at least 1 letter and 1 number.");
                        return;
                    }

                } else {
                    $this->Flash->error(__('Old password not correct, try again'));
                    return;
                }
            } else {
                // they are not changing password
                return $this->redirect(
                    array('controller' => 'Users', 'action' => 'account_settings')
                );
            }


        }


    }

    // called when Admin clicks on a Registered User
    public function admin_api_read($id = null)
    {

        if (!$this->User->exists($id)) {
            throw new NotFoundException(__('Item not found'));
        }

        $this->loadModel('Note');
        $this->loadModel('Photo');

        $this->User->id = (int)$id;
        $this->User->saveField('read', 1);

        $this->response->compress();

        $detail = $this->User->getUser((int)$id);

        // NOtes start
        $notes = $this->Note->find('all', array(
            'conditions' => array('Note.user_id' => $detail['User']['id']),
            'order' => 'Note.created'
        ));



        $user_notes_all = array();
        if (count($notes) > 0) {
            foreach ($notes as $note) {
                array_push($user_notes_all, $note['Note']);
            }
        }
        CakeLog::write('debug', 'User Notes: ' . print_r($user_notes_all, true));

        $detail['Notes'] = $user_notes_all;
        // Notes end

        //Photos starts
        $photos = $this->Photo->find('all', array(
            'conditions' => array('Photo.user_id' => $detail['User']['id'], 'Photo.type' => 'registered'),
            'order' => 'Photo.id'
        ));



        $user_photos_all = array();
        if (count($photos) > 0) {
            foreach ($photos as $photo) {
                array_push($user_photos_all, $photo['Photo']);
            }
        }
        CakeLog::write('debug', 'User Photos: ' . print_r($user_photos_all, true));

        $detail['Photos'] = $user_photos_all;

        $_serialize = array('detail');
        $_jsonOptions = JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT;
        $this->set(compact('detail', '_serialize', '_jsonOptions'));
    }

    public function beforeFilter()
    {
        parent::beforeFilter();
        // Allow un-authenticated users
        $this->Auth->allow('login', 'reset');
    }

    public function isAuthorized($user)
    {

        if ($this->action === 'login' || $this->action === 'reset') {
            return true;
        }

        if (!isset($user))
            return false;

        return true;
    }

    public function login()
    {

        $this->set('title_for_layout', 'Login — Welcome Members of HMHShip');
        $this->set('meta_description_for_layout', 'Thank you for being a member of HMHShip.com. Login here to access your account and options.');

        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());

            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }

        if (array_key_exists('pwchanged', $this->request->query)) {
            $this->Flash->success('Your password has been changed.  You may now login with your new password.');
        }
    }

    // Authorized Users only...
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function messages()
    {

        $this->loadModel('Message');
        $this->loadModel('Shipment');
        $this->loadModel('Transaction');

        if ($this->request->is('post') && $this->request->data['message'] != "") {
            // add the message
            $data = $this->request->data;
            $message = $this->Message->createMessage(array(
                'message' => $this->request->data['message'],
                'user_id' => AuthComponent::user('id'),
                'name' => AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name'),
                'email' => AuthComponent::user('email'),
                'created' => date("Y-m-d H:i:s")
            ));

            // get all messages for the current user with Newest on Top
            $messages = $this->Message->find('all', array(
                'conditions' => array(
                    'Message.user_id' => AuthComponent::user('id')
                ),
                'order' => array('Message.created DESC')
            ));

            // Send Admin email
            $email_data = array(
                'from' => AuthComponent::user('first_name') . ' ' . AuthComponent::user('last_name'),
                'email' => AuthComponent::user('email'),
                'message' => $messages
            );
            $this->Notification->sendAdminMessageEmail($email_data);

            // Send Email to Customer
            $this->Notification->sendCustomerMessageEmail(AuthComponent::user('email'), $email_data);
        }

        // get all messages for the current user
        $messages = $this->Message->find('all', array(
            'conditions' => array(
                'Message.user_id' => AuthComponent::user('id')
            ),
            'order' => array('Message.created')
        ));

        // used by nav bar
        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $count_waiting_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));

        $count_awaiting_payment = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT,
                'Shipment.balance_due >' => 0
            ),
            'order' => array('Shipment.Created')
        ));

        $count_received_package = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PACKAGE_RECEIVED
            )
        ));

        $notification_count = $this->makeUnique($count_received_package) + $this->makeUnique($count_awaiting_payment);


        $this->set(compact('co_code', 'messages', 'count_waiting_package', 'count_awaiting_payment', 'notification_count'));

    }

    public function myaccount()
    {

        //      $this->layout = 'user';

        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');

        $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => AuthComponent::user('id')
                )
            )
        );

        $param1 = "all";
        $param2 = "";
        if ($this->request->params['pass']) {
            $param1 = $this->request->params['pass'][0];
            if (count($this->request->params['pass']) > 1)
                $param2 = $this->request->params['pass'][1];
        }


        $this->loadModel('Shipment');
        $this->loadModel('Transaction');
        $this->loadModel('Country');

        $conditions = array(
            'user_id' => AuthComponent::user('id')
        );

        if ($param2 == "desc")
            $order = array('modified');
        else
            $order = array('modified DESC');

        $shipments = $this->Transaction->find('all', array(
            'conditions' => $conditions,
            'order' => $order /*,
            'fields' => array(
                'Transaction.id', 'Transaction.modified', 'Transaction.status'
            )*/
        ));

        $count_awaiting_fee = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_FEE
            )
        ));

        $count_waiting_package = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));


        $count_received_package = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PACKAGE_RECEIVED
            )
        ));


        $count_awaiting_payment = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT
            )
        ));

        $count_received_payment = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PAYMENT_RECEIVED
            )
        ));

        $count_package_sent = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PACKAGE_SENT
            )
        ));

        $countries_list = $this->Country->getList();
        $countries = array();
        foreach ($countries_list as $country_id => $country_name) {
            $countries[$country_id] = $country_name;
        }


        $notification_count = $this->makeUnique($count_received_package) + $this->makeUnique($count_awaiting_payment);
        //Configure::write('debug', 2);
        //$log = $this->Shipment->getDataSource()->getLog(false, false);
        //debug($log);
        $ajax_only = $this->request->data('params')['ajax_only'];

        if ($ajax_only) {
            $filtered_shipments_data = array();
            if(count($shipments)) {
                foreach ($shipments as $shipment) {
                    $transaction = $this->Transaction->getTransaction($shipment['Transaction']['id']);
                    $in_tracking = $transaction['Shipment'][0]['Package']['in_tracking'];                    
                    $transaction['Shipment'][0]['Package']['in_tracking'] = "# " . $in_tracking;
                    array_push($filtered_shipments_data, $transaction);
                }
            }
            $this->sendJsonResponse(array(
                'ok' => 1,
                'co_code' => $co_code,
                'user' => $user,
                'countries' => $countries_list,
                'shipments' => $filtered_shipments_data,
                'count_waiting_package' => $this->makeUnique($count_waiting_package),
                'count_received_package' => $this->makeUnique($count_received_package),
                'count_awaiting_payment' => $this->makeUnique($count_awaiting_payment),
                'count_received_payment' => $this->makeUnique($count_received_payment),
                'count_awaiting_fee' => $this->makeUnique($count_awaiting_fee),
                'count_package_sent' => $this->makeUnique($count_package_sent),
                'notification_count' => $notification_count
            ));
        }
        else {
            $count_received_package = $this->makeUnique($count_received_package);
            $count_awaiting_payment = $this->makeUnique($count_awaiting_payment);

            $this->set(compact('co_code', 'user', 'shipments', 'count_waiting_package', 'count_awaiting_payment',  'count_received_package', 'notification_count', 'param1', 'param2'));
        }




    }

    private function  makeUnique($shipments) {
        if (!is_array($shipments) && count($shipments) == 0) {
            return 0;
        }
        else {
            $exist = array();
            foreach ($shipments as $shipment) {
                if(!in_array($shipment['Transaction']['id'], $exist)) {
                    array_push($exist, $shipment['Transaction']['id']);
                }
            }

            return count($exist);
        }

    }

    public function newuser()
    {
        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $this->set(compact('co_code'));
    }

    public function payment()
    {

        $this->loadModel('Shipment');
        $this->loadModel('Transaction');

        $selected_transactions_id = $this->request->params;

        // get all shipments awaiting payment

        $qs = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT
            )
        ));


        //print_r($selected_transactions_id['pass'][0]);
        $filtered_shipments = array();
        $ids = '';
        if (isset($selected_transactions_id['pass'][0])) {
            $ids = explode(',', $selected_transactions_id['pass'][0]);
            foreach ($qs as $key => $s) {
                if (in_array($s['Shipment']['transaction_id'], $ids)) {
                    array_push($filtered_shipments, $s);
                }
            }
        }
        else {
            $filtered_shipments = $qs;
        }



        $total_due = 0;
        foreach ($filtered_shipments as $shipment) {
            $total_due = $total_due + $shipment['Shipment']['balance_due'];
        }

        if ($total_due <= 0)
            return $this->redirect(array('controller' => 'Users', 'action' => 'myaccount'));

        // used by Nav bar
        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $count_waiting_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));

        $count_awaiting_payment = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT,
                'Shipment.balance_due >' => 0
            ),
            'order' => array('Shipment.Created')
        ));

        $count_received_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PACKAGE_RECEIVED
            )
        ));

        $notification_count = $count_received_package + $count_awaiting_payment;

        $selected_ids = ($ids != '' && count($ids)) ? implode(',', $ids): '';


        $this->set(compact('co_code', 'count_waiting_package', 'count_awaiting_payment', 'filtered_shipments', 'count_received_package', 'notification_count', 'selected_ids'));
    }

    public function payment_complete()
    {

        $this->loadModel('Shipment');
        $this->loadModel('Transaction');

        $params = $this->request->query;

        //CakeLog::write('debug', print_r($params,true));
        //CakeLog::write('debug', 'payerid: ' . $_GET['PayerID']);

        if ($this->Paypal->execute_payment($params['paymentId'], $_GET['PayerID']) != 1)
            die("Error finalizing payment.  Please try again.");

        $payment = $this->Paypal->returnPayment($params['paymentId']);
        CakeLog::write('debug', print_r($payment, true));

        // payment is complete - credit their Awaiting Payment Shipments until all funds are used
        // get all QS awaiting payment
        $qs = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT
            ),
            'order' => array('Shipment.Created')
        ));

        $remainingFunds = $payment->transactions[0]->amount->total;
        CakeLog::write('debug', 'remaining funds: ' . $remainingFunds);
        $NumQS = count($qs);
        $i = 0;
        while ($remainingFunds > 0 && $i < $NumQS) {

            $balance_due = $qs[$i]['Shipment']['balance_due'];
            CakeLog::write('debug', 'balance_due: ' . $balance_due);

            if ($balance_due > 0) {
                if ($balance_due <= $remainingFunds) {
                    $remainingFunds -= $balance_due;
                    $balance_due = 0;
                    $this->Transaction->id = $qs[$i]['Transaction']['id'];
                    $this->Transaction->saveField('status', Transaction::STATUS_PAYMENT_RECEIVED);
                    $message = "We'll have your package shipped to you right away!";
                } else {
                    $balance_due -= $remainingFunds;
                    $remainingFunds = 0;
                    $message = "Your new balance is $" . number_format($balance_due, 2);
                }

                $this->Shipment->id = $qs[$i]['Shipment']['id'];
                $this->Shipment->saveField('balance_due', $balance_due);

                // CakeLog::write('debug', "sending Payment Received email");
				$data = $this->Transaction->getTransaction($qs[$i]['Transaction']['id']);
				$name = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1);
				if ($data['Transaction']['user_id'] != null) {
					$data['qsns'] = 'NS#' . $name . $data['Transaction']['user_id'];
					//get unsent package count
					$data['unsent_packages'] = $this->Transaction->find('count', array(
						'conditions' => array('Transaction.status <' => '5', 'Transaction.user_id' => $data['Transaction']['user_id'])
					));
				}
				else {
					$data['qsns'] = 'QS#'. $name . $data['Transaction']['id'];
					$data['unsent_packages'] = 0;
				} 

				$this->Notification->sendCustomerPaymentReceivedEmail($data, null);
            }
            $i++;
        }

        return $this->redirect(array('controller' => 'Users', 'action' => 'myaccount'));
    }

    public function preferences()
    {
        //      $this->layout = 'user';

        $this->loadModel('Shipment');
        $this->loadModel('Transaction');
        $this->loadModel('Country');
        $this->loadModel('Address');

        if ($this->request->is('post')) {
            CakeLog::write('debug', print_r($this->request->data, true));
            // save data

            // calculate defaults
            switch ($this->request->data['default']) {
                case "0":
                    $this->Address->set('default_billing', 0);
                    $this->Address->set('default_shipping', 0);
                    break;
                case "1":
                    // remove any other default_billings
                    $this->Address->query('UPDATE addresses SET default_billing=0 WHERE user_id=' . AuthComponent::user('id'));
                    $this->Address->set('default_billing', 1);
                    $this->Address->set('default_shipping', 0);
                    break;
                case "2":
                    // remove any other default_shippings
                    $this->Address->query('UPDATE addresses SET default_shipping=0 WHERE user_id=' . AuthComponent::user('id'));
                    $this->Address->set('default_billing', 0);
                    $this->Address->set('default_shipping', 1);
                    break;
                case "3":
                    // remove any other defaults
                    $this->Address->query('UPDATE addresses SET default_billing=0 WHERE user_id=' . AuthComponent::user('id'));
                    $this->Address->query('UPDATE addresses SET default_shipping=0 WHERE user_id=' . AuthComponent::user('id'));
                    $this->Address->set('default_billing', 1);
                    $this->Address->set('default_shipping', 1);
                    break;
            }
            $this->Address->set($this->request->data);
            if (!$this->Address->save()) {
                $this->Flash->error("Please enter all required fields. " . $this->Address->validationErrors);
                return;
            }
        }

        $co_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        $countries_list = $this->Country->getList();
        $countries = array();
        foreach ($countries_list as $country_id => $country_name) {
            $countries[$country_id] = $country_name;
        }

        $user = $this->User->find('first', array(
                'conditions' => array(
                    'User.id' => AuthComponent::user('id')
                ),
                'recursive' => 2
            )
        );

        // used by user.ctp
        $count_waiting_package = $this->Shipment->find('count', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_WAITING_PACKAGE
            )
        ));


        $count_awaiting_payment = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_AWAITING_PAYMENT,
                'Shipment.balance_due >' => 0
            ),
            'order' => array('Shipment.Created')
        ));

        $count_received_package = $this->Shipment->find('all', array(
            'conditions' => array(
                'Transaction.user_id' => AuthComponent::user('id'),
                'Transaction.status' => Transaction::STATUS_PACKAGE_RECEIVED
            )
        ));

        $notification_count = $this->makeUnique($count_received_package) + $this->makeUnique($count_awaiting_payment);

        $this->set(compact('co_code', 'user', 'count_waiting_package', 'count_awaiting_payment', 'countries', 'notification_count'));

    }

    // this is POSTed to from /Users/payment
    public function process_payment()
    {
		$this->loadModel('User');
        $data = $this->request->data;
        //$transaction_ids = explode(',', $data['selectedIds']);
        CakeLog::write('debug', print_r($data, true));

        if (!isset($data['price']) || (float)$data['price'] < 0.1) {
            throw new BadRequestException(__('Invalid amount for payment'));
        }

		$user = $this->User->find('first', array(
				'conditions' => array(
					'User.id' => AuthComponent::user('id')
				)
			)
		);
        
		if($user) {
			$name = strtoupper(substr($user['User']['first_name'], 0, 1) . substr($user['User']['last_name'], 0, 1));
			$nsqs = ' NS#' . $name . AuthComponent::user('id');
			$data['description'] .= ' for ' . $nsqs;
		}
		

        // create and return PayPal\Api\Payment object
        $payment = $this->Paypal->single_payment($data['description'], (float)$data['price'], 0, "", $completeUrl = '/Users/payment_complete', $cancelUrl = '/Users/myaccount');

        if ($payment === null) {
            throw new FatalErrorException(__('Payment cannot be processed at this time'));
        }

        $approval_link = $payment->getApprovalLink();

        $this->sendJsonResponse(array(
            'ok' => !is_null($approval_link),
            'approval_link' => $approval_link
        ));
    }

    public function reset()
    {
        if ($this->request->is('post')) {
            if (isset($this->request->data["email"])) {

                // did they provide a valid email?
                $user = $this->User->findByEmail($this->request->data["email"]);
                if (isset($user['User']['email'])) {
                    // email the password hash
                    $mandrill = new Mandrill(Configure::read('Mandrill.key'));
                    $url = Router::url(array('controller' => 'Users', 'action' => 'reset'), true);
                    $url = $url . '?id=' . $user['User']['id'] . '&key=' . $user['User']['password'];
                    $message = array(
                        'html' => '<p>Use the following link to reset your password:<br/><br/>  <a href="' . $url . '" >' . $url . '</a></p>',
                        'subject' => 'HMHShip password reset',
                        'from_email' => 'confirm@hmhship.com',
                        'from_name' => 'HMHShip.com',
                        'to' => array(
                            array(
                                'email' => $user['User']['email'],
                                'name' => $user['User']['first_name'] . ' ' . $user['User']['last_name'],
                                'type' => 'to'
                            )
                        ),
                        'global_merge_vars' => array(
                            array(
                                'name' => 'LINK',
                                'content' => "<a style='color: white;' href='" . $url . "'>Reset Pasword</a>",
                            ),
                            array(
                                'name' => 'CONTACT_NAME',
                                'content' => $user['User']['first_name']
                            ),
                            array(
                                'name' => 'CUSTOMER_USERNAME',
                                'content' => $user['User']['email']
                            )
                        )
                    );

                    $result = $mandrill->messages->sendTemplate('reset-your-password', '', $message, false);

                    return $this->redirect("/reset2");

                } else {

                    $this->Flash->set("A user with this email does not exist.");

                }


            } else if (isset($this->request->data["password_hashed"]) && isset($this->request->data["id"])) {

                if ($this->request->data["password"] != $this->request->data["confirm_password"]) {
                    $this->Flash->error("Passwords don't match.");
                    $userid = $this->request->data["id"];
                    $password_hashed = $this->request->data["password_hashed"];
                    $this->set(compact('userid', 'password_hashed'));
                    return;
                }

                $user = $this->User->findByIdAndPassword($this->request->data["id"], $this->request->data["password_hashed"]);
                if (count($user) > 0) {
                    $this->User->read(null, $this->request->data["id"]);
                    $this->User->set('password', $this->request->data["password"]);
                    if ($this->User->save()) {
                        return $this->redirect(
                            array('controller' => 'Users', 'action' => 'login', '?' => 'pwchanged')
                        );
                    } else {
                        $this->Flash->error("Password must contain at least 1 letter and 1 number.");
                        $userid = $this->request->data["id"];
                        $password_hashed = $this->request->data["password_hashed"];
                        $this->set(compact('userid', 'password_hashed'));
                        return;
                    }
                }
            }
        } else {
            // not a post
            if (isset($this->request->query["key"]) && isset($this->request->query["id"])) {

                $user = $this->User->findByIdAndPassword($this->request->query["id"], $this->request->query["key"]);
                if (count($user) > 0) {
                    $userid = $this->request->query["id"];
                    $password_hashed = $this->request->query["key"];
                    $this->set(compact('userid', 'password_hashed'));
                }

            }
        }


    }

    public function RequestPhoto() {
        $this->loadModel('Transaction');
        $selected_transactions_id = $this->request->data('params')['ids'];
        $userData = AuthComponent::user();

        $name = AuthComponent::user('first_name') .' '. AuthComponent::user('last_name');

        $ids = explode(',', $selected_transactions_id);
        $shipments = array();
        $u_code = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1);
        foreach($ids as $tid) {
            $transaction = $this->Transaction->getTransaction($tid);
            if($transaction['Transaction']['status'] == '-1') {
                array_push($shipments, 'GS#'.$u_code.$tid);
            }
            else {
                array_push($shipments, 'NS#'.$u_code.$tid);
            }
        }

        $data = implode(',', $shipments);
        $Reqistered_Account_Number = substr(AuthComponent::user('first_name'), 0, 1) . substr(AuthComponent::user('last_name'), 0, 1) . AuthComponent::user('id');
        if($this->Notification->sendPhotoRequestEmail($data, $name, AuthComponent::user('email'), $Reqistered_Account_Number)) {
         $this->sendJsonResponse(array(
             'ok' => true,
             'message' => 'Email sent successfully',
             'result' => $selected_transactions_id
         ));
        }

        else {
         $this->sendJsonResponse(array(
             'ok' => false,
             'message' => 'Email could not be sent successfully. Please contact support.'
         ));
        }

    }

}
