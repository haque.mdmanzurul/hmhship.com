<?php

App::uses('User', 'Model');
App::uses('Address', 'Model');
App::uses('AddressType', 'Model');
App::uses('Package', 'Model');
App::uses('ItemType', 'Model');
App::uses('Country', 'Model');
App::uses('Transaction', 'Model');
App::uses('Photo', 'Model');
App::uses('Note', 'Model');

App::uses('AppController', 'Controller');
App::uses('Hmhship\Quickship\Package', 'Vendor');
App::uses('Hmhship\Quickship\Address', 'Vendor');

/**
 * Class QuickshipController
 *
 * @property ItemType $ItemType
 * @property AddressType $AddressType
 * @property Country $Country
 * @property Package $Package
 * @property Address $Address
 * @property Shipment $Shipment
 * @property Item $Item
 * @property User $User
 * @property SessionObjectComponent $SessionObject
 * @property ShippingComponent $Shipping
 * @property NotificationComponent $Notification
 * @property PaypalComponent $Paypal
 * @property Transaction $Transaction
 * @property CryptComponent $Crypt
 */
class QuickshipController extends AppController
{

	/**
	 * @var array
	 */
	public $uses = array('ItemType', 'Country', 'AddressType');

	/**
	 * @var array
	 */
	public $components = array(
		'SessionObject',
		'Shipping',
		'Paypal',
		'Crypt',
        'Notification'
	);

    public function isAuthorized($user) {

        return true;
    }

	/**
	 *
	 */
	public function index()
	{
        $this->set('title_for_layout', 'Quick Shipping Order Form — HMHShip');
        $this->set('meta_description_for_layout', 'Forward US parcels to anywhere in the world for a great price with HMHShip. You’ll be amazed by how much you can save over international shipping.');

        $session_key = 'Quickship.active';
        // Force redirect if session is lost
        if (($session = $this->Session->read($session_key)) !== true) {
            $this->Session->write($session_key, true);
            $errorMsg = __('Session has expired');
            \Hmhship\Logger::getInstance()->log($errorMsg, 'error');
            $this->Session->error($errorMsg);
            return $this->redirect('/quickship');
        }

        CakeLog::write('debug', "hello argument: " . print_r($this->request->params, true));
        if (isset($this->request->params['ghost_shipment_id'])) {
            // A Shipment ID was specified.  Is the user logged-in as the owner of this shipment?
            if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
                $this->loadModel('Transaction');

				$data = $this->Transaction->getTransaction($this->request->params['ghost_shipment_id']);
                if (!$data)
                {
                    $this->Flash->set(__('Invalid Ghost Shipment ID.'));
                    $this->Session->write('ghost_shipment_id', null);
                    return;
                }
                if ($data['Transaction']['user_id'] != AuthComponent::user('id')) {
                    $this->Flash->set(__('Invalid Ghost Shipment ID.'));
                    $this->Session->write('ghost_shipment_id', null);
                    return;
                }
                CakeLog::write('debug', "A Transaction: " . print_r($data, true));

            }
            else
                return $this->redirect('/sign-in');


            $this->Session->write('ghost_shipment_id', $this->request->params['ghost_shipment_id']);
        }
        else
            $this->Session->write('ghost_shipment_id', null);

	}



	/**
	 * called from Javascript in QuickShip process to get lookup data and pre-populate fields
	 */
	public function api_settings()
	{
        $this->loadModel('Transaction');
		// key/value pairs
		$item_types = $this->ItemType->getTypes();
		$countries_list = $this->Country->getList();
		$handling_fee = Configure::read('Shipping.rates')['handling_fee'];
		$countries = array();
		foreach ($countries_list as $country_id => $country_name) {
			array_push($countries, array('key' => $country_id, 'label' => $country_name));
		}

		$_jsonOptions = JSON_NUMERIC_CHECK;
		$this->response->compress();
		$base_url = Router::url('/');

        $user = null;
        $received_transactions = array();
        $received_packages = array();
        if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
            $this->LoadModel("User");

            $user = $this->User->findById(AuthComponent::user('id'));

            $conditions = array(
                'user_id' => AuthComponent::user('id'),
                'status' => Transaction::STATUS_PACKAGE_RECEIVED
            );

            $order = array('date DESC');

            $transactions = $this->Transaction->find('all', array(
                'conditions' => $conditions,
                'order' => $order
            ));



            if($transactions) {
                foreach ($transactions as &$tr) {
                    $transaction = $this->Transaction->getTransaction($tr['Transaction']['id']);
                    $transaction['Transaction']['order_html'] = true;
                    array_push($received_transactions, $transaction);

                    foreach($transaction['Shipment'] as &$s) {
                        $s['Package']['width'] = empty($s['Package']['width_in']) ? $s['Package']['width_cm']: $s['Package']['width_in'];
                        $s['Package']['length'] = empty($s['Package']['length_in']) ? $s['Package']['length_cm']: $s['Package']['length_in'];
                        $s['Package']['height'] = empty($s['Package']['height_in']) ? $s['Package']['height_cm']: $s['Package']['height_in'];
                        $s['Package']['weight'] = empty($s['Package']['weight_kg']) ? $s['Package']['weight_lb']: $s['Package']['weight_kg'];

                        $s['Package']['size_unit'] = empty($s['Package']['width_in']) ? 'cm' : 'in';
                        $s['Package']['weight_unit'] = empty($s['Package']['weight_kg']) ? 'lb': 'kg';
                        $s['Package']['tracking_code'] = $s['Package']['in_tracking'];
                        $s['Package']['in_tracking'] = "# " . $s['Package']['in_tracking'];
                        $s['Package']['transaction_id'] = $tr['Transaction']['id'];
                        foreach($s['Package']['Item'] as &$item) {
                            $item['price_value'] = $item['value'];
                            $item['weight'] = empty($item['weight_kg']) ? $item['weight_lb']: $item['weight_kg'];
                            $item['weight_unit'] = empty($item['weight_kg']) ? 'lb' : 'kg';
                        }

                        $s['Package']['items'] = $s['Package']['Item'];
                        array_push($received_packages, $s['Package']);
                    }
                }
            }

        }

        $transaction = null;
        if ($this->Session->read('ghost_shipment_id')) {
            $read_only_packages = true;
			$transaction = $this->Transaction->getTransaction($this->Session->read('ghost_shipment_id'));
        }
        else
            $read_only_packages = false;

		$_serialize = array('item_types', 'countries', 'handling_fee', 'base_url', 'user', 'read_only_packages', 'transaction', 'received_packages', 'received_transactions');
		$this->set(compact('item_types', 'countries', 'handling_fee', 'base_url', '_jsonOptions', '_serialize', 'user', 'read_only_packages', 'transaction', 'received_packages', 'received_transactions'));
	}

	/* Personal/Shipping information */
	/**
	 *
	 * @deprecated Unused functionality
	 */
	public function step1()
	{
		if ($this->request->is('post')) {
			$data = json_decode(file_get_contents('php://input'));

			$so = $this->SessionObject;

			// TODO: cache me
			$address_types = Hash::combine($this->AddressType->find('all'), '{n}.AddressType.key', '{n}.AddressType.id');

			if (!is_null($data)) {

                // 11/15/2017 MCW is this used?
                // $so->create('User', $data->user);

				CakeLog::write('debug', json_encode($data));

				$data->billing->address_type_id = $address_types['billing'];

				// If shipping address is not the same as billing
				if ($data->billing_shipping === false) {
					$data->shipping->address_type_id = $address_types['shipping'];
					$so->create('Address', $data->shipping, 'user_shipping');
				} else {
					$data->billing->address_type_id = $address_types['billing_shipping'];
				}

				$so->create('Address', $data->billing, 'user_billing');
			}

			$this->ok();
		}
	}

	/* Additional Options */
	/**
	 *
	 */
	public function step2()
	{
		$this->redirect('/quickship/api_rates');

		if ($this->request->is('post')) {
		} else { /* load view for step 2 */
		}
	}

	/**
	 * Shipping rates
	 */
	public function api_rates()
	{
		// set shipment id in session
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException(__('Invalid method'));
		}

        // Don't allow writing to Response because we need to return JSON
        ob_start();

		$data = json_decode(file_get_contents('php://input'));
		if($data === null) {
			throw new InvalidArgumentException(__('Invalid data'));
		}
		$shipping_address = ($data->billing_shipping === true) ? $data->billing : $data->shipping;

        // if Shipping Phone is empty, use Billing Phone
        if ($data->billing_shipping !== true) {
            if (!isset($shipping_address->phone))
                $shipping_address->phone = $data->billing->phone;
        }

		$objShipping = new \Hmhship\Quickship\Address();
		$objShipping->set($shipping_address);

		CakeLog::write('debug', 'Shipping address: ' . json_encode($objShipping));
		\Hmhship\Logger::getInstance()->log('Shipping address: ' . json_encode($objShipping), 'debug');

		$md5_data = md5(json_encode($objShipping) . json_encode($data));
		CakeLog::write('debug', sprintf('Using hash %s', $md5_data));

		$package_rates = array();

        CakeLog::write('debug', 'QuickshipController.api_rates()  packages = ' . print_r($data->quickship->packages, true));

		if (($cached_rates = Cache::read('rate_' . $md5_data)) === false) {
			CakeLog::write('debug', "getting rates from EasyPost...");
			$packages = array();

			foreach ($data->quickship->packages as $package) {

				$package_obj = new \Hmhship\Quickship\Package();
				$package_obj->items = $package->Item;

				foreach ($package_obj->items as $item) {
                    $item->quantity = round($item->quantity);
                }

				// If consolidated package, add defaults 24"x14"x12"
				if (isset($package->consolidated) && count($package->consolidated) > 0) {

					// Size
					$package_obj->size_unit = 'in';
					$package_obj->width = 24;
					$package_obj->height = 14;
					$package_obj->length = 12;

					// Weight
					$package_obj->weight = $package->weight;
					$package_obj->weight_unit = $package->weight_unit;
				} else {

					// Size
					$package_obj->size_unit = $package->size_unit;
					$package_obj->height = round($package->height);
					$package_obj->width = round($package->width);
					$package_obj->length = round($package->length);

					// Weight
					$package_obj->weight_unit = $package->weight_unit;
					$package_obj->weight = round($package->weight);
				}

				$package_rates[] = $this->Shipping->get_rates($objShipping, $package_obj);
				$packages[] = $package_obj;
			}

            CakeLog::write('debug', "rates: " . print_r($package_rates, true));

			$cached_rates = json_encode($package_rates);
			Cache::write('rate_' . $md5_data, $cached_rates);
			CakeLog::write('debug', "json encoded rates: " . print_r($cached_rates, true));

			$this->Session->write('user_packages', $packages);
		}
        else
        	CakeLog::write('debug', "using cached rates");

        // if any php notices were generated - write them to debug log
        CakeLog::write('debug', "buffer:  " . ob_get_contents());
        ob_end_clean();

		$this->sendJsonResponse(json_decode($cached_rates));
	}


	/**
	 * Payment with Paypal (or credit card using Paypal)
	 *
	 */
	public function payment()
	{

        // Don't allow writing to Response because we need to return JSON
        ob_start();

		$approval_link = null;
		$error = '';
		$transaction_id = null;

        CakeLog::write('debug', "top of payment()");

		try {

            // Charge Them the Handling Fee
			$data = $this->request->data;

			$combine_package = array();
			if(count($data['shipment']['received_transactions'])  > 0) {
				foreach($data['shipment']['received_transactions'] as $t) {
					if ($t['Transaction']['order_html']) {
						array_push($combine_package, $t['Transaction']['id']);
					}
				}
			}
			$this->Session->write('bulk_transactions_ids', implode(',', $combine_package));
			CakeLog::write('debug', " bulk_transactions_ids found: " .print_r($combine_package, true));
            CakeLog::write('debug',  'posted quickship data' . print_r($data, true));

			if (!isset($data['price']) || (float)$data['price'] < 0.1) {
				throw new BadRequestException(__('Invalid amount for payment'));
			}

            CakeLog::write('debug', "1 payment()");

            $this->loadModel('Transaction');
            $this->loadModel('Address');
            $this->loadModel('Item');
            $this->loadModel('Package');
            $this->loadModel('Shipment');
            $this->loadModel('User');

            CakeLog::write('debug', "3 payment()");
			// Create addresses
			if ($data['shipment']['billing_shipping'] === true) {
				// billing same as shipping

				$shipping_address_id = $this->Address->addAddress($data);
				$addresses = array(
					'shipping' => $shipping_address_id['Address']['id']
				);
				$conditions_address = array(
					'conditions' => array(
						'Address.id' => $shipping_address_id['Address']['id']
					),
					'contain' => false
				);
				$address = $this->Address->find('first', $conditions_address);

				$address_type = Address::SHIPPING_AND_BILLING;
				$email = Hash::get($this->Address->getAddressFromData($address), 'email');
				CakeLog::write('debug', "4 payment()");
			} else {
				// billing and shipping are different

				$billing_address_id = $this->Address->addAddress($data, Address::BILLING);
				$shipping_address_id = $this->Address->addAddress($data, Address::SHIPPING);
				$addresses = array(
					'billing' => $billing_address_id['Address']['id'],
					'shipping' => $shipping_address_id['Address']['id']
				);
				$address_type = Address::BILLING;
				$conditions_address = array(
					'conditions' => array(
						'Address.id' => $billing_address_id['Address']['id']
					),
					'contain' => false
				);
				$address = $this->Address->find('first', $conditions_address);
				$email = Hash::get($this->Address->getAddressFromData($address, Address::BILLING), 'email');
				CakeLog::write('debug', "5 payment()");
			}

			CakeLog::write('debug', "5.5 payment()");

			CakeLog::write('debug', "7 payment()");
			CakeLog::write('debug', "Charge Now: " . ($data['shipment']['quickship']['payment_action'] == 0));

			$transaction = $this->Transaction->createTransaction(array(
				'type_id' => Transaction::TYPE_QUICKSHIP,
				'charge_now' => ($data['shipment']['quickship']['payment_action'] == 0 ? 1 : 0),
				'code' => 'IN-PROCESS-NOW',
				'date' => date('Y-m-d H:i:s'),
				'consolidated' => $data['shipment']['quickship']['consolidatePackages'],
				'insured' => $data['shipment']['quickship']['packagesInsurance'],
				'expedited' => $data['shipment']['quickship']['packagesExpedited'],
				'special_instructions' => $data['shipment']['quickship']['special_instructions'],
				'repackaged' => $data['shipment']['quickship']['repackaged'],
				'ship_fast' => (isset($data['shipment']['quickship']['ship_fast']) && $data['shipment']['quickship']['ship_fast'] === true) ? true : false,
				'ship_cheap' => (isset($data['shipment']['quickship']['ship_cheap']) && $data['shipment']['quickship']['ship_cheap'] === true) ? true : false,
				'request_photo' => $data['shipment']['quickship']['requestPhoto']
			), $addresses);

			CakeLog::write('debug', "5.5 payment()");

			$nsqs =  '';

			if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
				CakeLog::write('debug', "5.6 payment() username is set: " . AuthComponent::user('id'));
				$user = $this->User->find('first', array(
						'conditions' => array(
							'User.id' => AuthComponent::user('id')
						)
					)
				);

				$name = strtoupper(substr($user['User']['first_name'], 0, 1) .
					substr($user['User']['last_name'], 0, 1));
				$nsqs = ' NS#' . $name . AuthComponent::user('id');
				
				$data['description'] = $data['description'] .  ' NS#' . $name . AuthComponent::user('id');

				CakeLog::write('debug',"user=" . print_r($user, true));
			}
			else {
				$name = strtoupper(substr($data['shipment']['billing']['first_name'], 0, 1) .
					substr($data['shipment']['billing']['last_name'], 0, 1));
				$nsqs = ' QS#' . $name . $this->Transaction->id;
				$data['description'] = $data['description'] .  ' QS#' . $name . $this->Transaction->id;

			}

			// Handling Fee should be multiplied by the Number of Packages (the packages may contain other packages (consolidated))
			$fee = (float)$data['price'] * count($data['shipment']['quickship']['packages']);

			// If they picked Charge Me Right Away, do Classic API Express Checkout with Billing Agreement.
			if ($data['shipment']['quickship']['payment_action'] == "0") {
				$payment = $this->Paypal->set_express_checkout($data['description'], $fee);
			}
			else {
				// Send me a bill
				// Use REST SDK to complete payment.  send address to PayPal to pre-populate fields
				$payment = $this->Paypal->single_payment($data['description'], $fee, 0, null,
					'/payments/complete', '/payments/cancel', $data);
			}

			CakeLog::write('debug', "2 payment()");
			if ($payment === null) {
				throw new FatalErrorException(__('Payment cannot be processed at this time'));
			}

			$utc_paypal = strtotime($payment->getCreateTime());
			$this->Transaction->saveField('date', date('Y-m-d H:i:s', $utc_paypal));
			$this->Transaction->saveField('code', $payment->getId());

			if ($transaction) {

				if(isset($user) && isset($user['User']['id']) && is_numeric($user['User']['id'])) {
					$this->Transaction->saveField('user_id', $user['User']['id']);
				}

				CakeLog::write('debug', "8 payment()");
				$transaction_id = $this->Transaction->id;
				$this->Session->write('last_transaction_id', $transaction_id);

				foreach ($data['shipment']['quickship']['packages'] as $package_index => $package) {
					$shipment_id = $this->Shipment->addShipment($transaction_id, $shipping_address_id['Address']['id'], $data['shipment']['shippingRatesSelected'][$package_index]['shipment_id']);
					$package_id = $this->Package->addPackage($shipment_id, $package_index, $data);
					foreach ($data['shipment']['quickship']['packages'][$package_index]['items'] as $item) {
						$this->Item->addItem($package_id, $item);
					}
				}

			}
			CakeLog::write('debug', "9 payment()");

			// Handling Fee must be paid now.  The option is giving HMH permission to charge Shipping Fees later or not.
            if ($data['shipment']['quickship']['payment_action'] == "0") {
			     // customer would like HMH to charge them Shipping Fees when we receive the package.
                 // use Classic Paypal API because new API doesn't support reference transactions as of late 2016.
                $approval_link = $payment->getRedirectUrls();
            }
            else {
			    // customer would like HMH to invoice them for Shipping Fees
                $approval_link = $payment->getApprovalLink();
            }

            //Save note
            $this->loadModel('Note');
            $this->Note->set('transaction_id', $transaction_id);
            $user_id = ($user['User']['id']) ? $user['User']['id'] : 9999999;
            $this->Note->set('user_id', $user_id);
            if (count($combine_package) > 0) {
				$this->Note->set('note', 'New Shipment created ' . $nsqs . 'from Ghost Shipments. GP IDs# ' . implode(', ', $combine_package) );
			}
            else{
				$this->Note->set('note', 'New Shipment created ' . $nsqs );
			} 
            	
            
            $this->Note->save();

			if (!empty($approval_link)) {
				$approval_link .= '&PAGESTYLE=HMHShip&LANDINGPAGE=Billing&SOLUTIONTYPE=Sole';
			}

            CakeLog::write('debug', "10 payment()");
			// hack: keep the active steps in session to show the correct ones when loading step 6 view
			$this->Session->write('active_steps', $data['steps']);

		} catch (Exception $ex) {
			$error = $ex->getMessage();
			debug($ex->getTraceAsString(), false);
			\Hmhship\Logger::getInstance()->log($error, 'error');
            CakeLog::write('error', $error);
		}

        // if any php notices were generated - write them to debug log
        //CakeLog::write('debug', "buffer:  " . html_entity_decode(ob_get_contents()));
        ob_end_clean();

		if (!empty($error)) {
			$this->sendJsonInvalidResponse(array(
				'ok' => !is_null($approval_link),
				'approval_link' => $approval_link,
				'transaction_id' => $this->Crypt->encrypt($transaction_id),
				'error' => $error
			));
		} else {
			$this->sendJsonResponse(array(
				'ok' => !is_null($approval_link),
				'approval_link' => $approval_link,
				'transaction_id' => $this->Crypt->encrypt($transaction_id),
				'error' => $error
			));
		}

	}

    // Create a New Shipment in Ghost Status (-1) using the package info and User_ID posted
    // This action gets posted to from /admin Registered Accounts page
    // shipment->quickship->packages
    // user_id
    public function new_shipment() {

    	$approval_link = null;
		$error = '';
		$transaction_id = null;

        CakeLog::write('debug', "top of new_shipment()");

		try {

			$data = $this->request->data;
            CakeLog::write('debug', print_r($data, true));

            $user_id = $data['user_id'];
            if (!isset($user_id) || !is_numeric($user_id))
                throw new Exception("Invalid user_id passed to /quickship/new_shipment");

            $this->loadModel('Transaction');
			$this->loadModel('Address');
			$this->loadModel('Item');
			$this->loadModel('Package');
			$this->loadModel('Shipment');
			$this->loadModel('User');

            // get the info for this user
            $user = $this->User->getUser($user_id);
            if (!$user)
			    throw new Exception("Could not retrieve User in /quickship/new_shipment");
			CakeLog::write('debug', print_r($user, true));

            // locate Billing and Shipping Addresses to use
            $billing_address = null;
            $shipping_address = null;

            foreach($user['Address'] as $address) {
                if ($address['default_billing'])
                    $billing_address = $address;
                if ($address['default_shipping'])
                    $shipping_address = $address;
                if (isset($billing_address, $shipping_address))
                    break;
            }

            // if no defaults found, just use the first address
            if (!isset($billing_address))
                $billing_address = $user['Address'][0];
            if (!isset($shipping_address))
                $shipping_address = $user['Address'][0];

			$billing_address_new = $this->Address->addAddress2($billing_address, Address::BILLING);
			$shipping_address_new = $this->Address->addAddress2($shipping_address, Address::SHIPPING);

			$addresses = array(
				'billing' => $billing_address_new['Address']['id'],
				'shipping' => $shipping_address_new['Address']['id']
			);
			$address_type = Address::BILLING;

			$data['AddressBilling'] = $billing_address_new;
            $data['AddressShipping'] = $shipping_address_new;

            CakeLog::write('debug', "5.5 new_shipment()");


			$transaction = $this->Transaction->createTransaction(array(
				'type_id' => isset($data['type_id']) ? $data['type_id'] : Transaction::TYPE_QUICKSHIP,
				'charge_now' => '',
				'code' => '',
				'date' => date('Y-m-d H:i:s'),
				'consolidated' => '',
				'insured' => '',
				'expedited' => '',
				'special_instructions' => '',
				'repackaged' => '',
				'ship_fast' => '',
				'ship_cheap' => ''
			), $addresses);

			if (!$transaction)
                throw new Exception("Couldn't create transaction.");

            $this->Transaction->saveField('user_id', $user_id);
            if (isset($data['type_id']) && $data['type_id'] == 4) {
                $this->Transaction->saveField('status', 2); // mark as Ghost Transaction
            } else {
                $this->Transaction->saveField('status', -1); // mark as Ghost Transaction
            }


            CakeLog::write('debug', "8 new_shipment()");
			$transaction_id = $this->Transaction->id;

            // create Shipments and Packages

            for ($package_index = 0; $package_index < count($data['shipment']['quickship']['packages']); $package_index++) {
                $shipment_id = $this->Shipment->addShipment($transaction_id, $shipping_address_new['Address']['id']);
				$package_id = $this->Package->addPackage($shipment_id, $package_index, $data);

                if (isset($data['type_id']) && $data['type_id'] == 4) {
                    $itm = array(
                        'package_id' => $package_id,
                        'description' => '',
                        'quantity' => 0,
                        'weight_unit' => 'kg',
                        'weight_lb' =>   0,
                        'weight_kg' => 0,
                        'value' =>  0,
                        'type_id' => 1,
                        'country_id' =>  1,
                        'info' =>  'NA',
                    );

                    $item_id = $this->Item->addItem($package_id, $itm);
                }
                else {
                    foreach ($data['shipment']['quickship']['packages'][$package_index]['items'] as $itm) {
                        $itm['type_id'] = 1;
                        $itm['country_id'] = null;
                        $item_id = $this->Item->addItem($package_id, $itm);
                    }
                }

			}

            $data['transaction_id'] = $transaction_id;
            $data = $this->Transaction->getTransaction($transaction_id);
            CakeLog::write('debug', 'before sendCustomerPackageReceivedActionRequiredEmail: ' . print_r($data, true));

            $this->Notification->sendCustomerPackageReceivedActionRequiredEmail($data);


            //Save note
            $this->loadModel('Note');
            $this->Note->set('transaction_id', $transaction_id);
            $user_id = ($user_id) ? $user_id : 9999999;
            $this->Note->set('user_id', $user_id);
            $this->Note->set('note', 'New Shipment created');
            $this->Note->save();

		} catch (Exception $ex) {
			$error = $ex->getMessage();
			debug($ex->getTraceAsString(), false);
			\Hmhship\Logger::getInstance()->log($error, 'error');
            CakeLog::write('error', "Error in new_shipment(): " . $error);
		}

		if (!empty($error)) {
            CakeLog::write('error', "Error in new_shipment(): " . $error);
			$this->sendJsonInvalidResponse(array(
				'ok' => false,
				'error' => $error
			));
		} else {
            CakeLog::write('debug', "new_shipment() success");
			$this->sendJsonResponse(array(
				'ok' => true,
				'shipment_id' => $transaction_id,
				'error' => $error
			));
		}


    }


	/**
	 *
	 */
	public function shipping_address()
	{
		$user = $this->Session->read('user')->data['User'];
		$address = Configure::read('Shipping.from_address');

		$email = $user['email'];
		$address1 = $address['street1'] . '<br> Suite X';
		$address2 = $address['city'] . ', ' . $address['state'] . ' ' . $address['zip'];

		$this->set('name', $user['first_name'] . ' ' . $user['last_name']);
		$this->set('address1', $address1);
		$this->set('address2', $address2);
		$this->set('active_steps', $this->Session->read('active_steps'));
	}

	/**
	 *
	 */
	public function config()
	{
		$this->output(array(
			'base_url' => Router::url('/'),
			'handling_fee' => Configure::read('Shipping.rates.parcel_forwarded')
		));
	}

	/**
	 *
	 * @deprecated
	 * @return mixed
	 */
	private function get_shipping_address()
	{
		$address_types = AddressType::get_all();
		$address = $this->Session->read('user_billing');
		if ($address === null) return false;

		if ($address->get_type_id() == $address_types['billing_shipping']) {
			return $address;
		} else {
			return $this->Session->read('user_shipping');
		}
	}

	/**
	 * @param $data
	 */
	private function output($data)
	{
		$this->set($data);
		$this->set('_serialize', array_keys($data));
		$this->set('_jsonOptions', JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT);
		//$this->response->compress();
		$this->response->send();
		$this->_stop();
	}

	public function admin_index()
	{
		$this->layout = 'main';
		$section = 'qscust';
		$this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('Message');
        $messages_unread_count = $this->Message->countRegisteredUnread();
        $messages_registered_unread_count = $this->Message->countUnread();
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('section', 'quickship_unread_count', 'messages_unread_count', 'business_unread_count','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));


	}

    public function admin_registered()
	{
		$this->layout = 'main';
		$section = 'qscust_registered';
		$this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('Message');
        $messages_unread_count = $this->Message->countRegisteredUnread();
        $messages_registered_unread_count = $this->Message->countUnread();
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('section', 'quickship_unread_count', 'messages_unread_count', 'business_unread_count','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
	}


    public function admin_ghost()
    {
        $this->layout = 'main';
        $section = 'qscust_ghost';
        $this->loadModel('Transaction');
        $quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
        $this->loadModel('Message');
        $messages_unread_count = $this->Message->countRegisteredUnread();
        $messages_registered_unread_count = $this->Message->countUnread();
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $this->set(compact('section', 'quickship_unread_count', 'messages_unread_count', 'business_unread_count','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
    }

	/**
	 *
	 */
	private function ok()
	{
		$this->sendJsonResponse(array('ok' => true));
	}

    /**
     * Admin upload requested photo
     * @throws Mandrill_Error
     */
    public function admin_add_requested_photo () {
        $this->loadModel('Transaction');
        $uploadPath = 'imgadmin'. DS. 'requested';

        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $fileName = $_FILES['file']['name'];
        $uploadFile = $uploadPath. DS . str_replace(' ', '_', $fileName);

        if( move_uploaded_file($_FILES["file"]["tmp_name"],  $uploadFile) ) {
            $this->Transaction->id = $this->request->data['transaction_id'];

            /* Send notification email to user*/
            $transaction = $this->Transaction->getTransaction($this->request->data['transaction_id']);
            $user_email =  $transaction['AddressBilling'][0]['email'];
            $user_name =  $transaction['AddressBilling'][0]['firstname'] . ' ' . $transaction['AddressBilling'][0]['lastname'];
            CakeLog::write('debug', 'Add Requested Transaction: ' . print_r($transaction, true));


            // Send Admin email
            $qsns = '';
            $name = strtoupper(substr($transaction['AddressBilling'][0]['firstname'], 0, 1) .
                    substr($transaction['AddressBilling'][0]['lastname'], 0, 1));
            $data['user_name'] = $name;
            $data['user_email'] = $user_email;
            $data['photo'] = 'https://'. $_SERVER['HTTP_HOST'] . DS . $uploadFile;

            if ($transaction['Transaction']['user_id'] != null) {
                $data['qsns'] = 'NS#' . $name . $transaction['Transaction']['user_id'];
            }
            else $data['qsns'] = 'QS#'. $name . $this->request->data['transaction_id'];

            $data['requestedPhotos'] = isset($transaction['requestedPhotos']) ? $transaction['requestedPhotos'] : array();
            $result = $this->Notification->sendRequestedPhotoEmail($data);
            CakeLog::write('debug', 'Add Requested Photo Email: ' . print_r($result, true));

            if (isset($result[0]['_id']) && $result[0]['_id'] != null) {
                $this->loadModel('Photo');
                $this->Photo->set('transaction_id',$this->request->data['transaction_id']);
                $this->Photo->set('requested_photo_url', $uploadFile);
                $this->Photo->set('type', 'requested');
                $this->Photo->save();


                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', $this->request->data['transaction_id']);
                $user_id = ($transaction['Transaction']['user_id']) ? $transaction['Transaction']['user_id'] : 9999999;
                $this->Note->set('user_id', $user_id);
                $this->Note->set('note', 'Added requested photo');
                $this->Note->save();

                $this->sendJsonResponse(array(
                    'ok' => true,
                    'transaction_id' => $this->request->data['transaction_id'],
                    'message' => 'Email sent successfully',
                    'email' => $user_email,
                    'status' => $result[0]['status']
                ));
            } else {
                $this->sendJsonResponse(array(
                    'ok' => false,
                    'transaction_id' => $this->request->data['transaction_id'],
                    'message' => 'Attachment uploaded but failed send the email to ' . $user_email
                ));
            }

        } else {
            $this->sendJsonResponse(array(
                'ok' => false,
                'message' => 'Something went wrong uploading file'
            ));
        }
        die();
    }

    /**
     * Admin upload shippingbox photo
     * @throws Mandrill_Error
     */
    public function admin_add_shippingbox_photo () {
        $this->loadModel('Transaction');
        $uploadPath = 'imgadmin'. DS. 'shippingbox';
        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $fileName = $_FILES['file']['name'];
        $uploadFile = $uploadPath. DS . str_replace(' ', '_', $fileName);

        if( move_uploaded_file($_FILES["file"]["tmp_name"],  $uploadFile) ) {
            $this->Transaction->id = $this->request->data['transaction_id'];

            /* Send notification email to user*/
            $transaction = $this->Transaction->getTransaction($this->request->data['transaction_id']);
            $user_email =  $transaction['AddressBilling'][0]['email'];
            $user_name =  $transaction['AddressBilling'][0]['firstname'] . ' ' . $transaction['AddressBilling'][0]['lastname'];
            $attachment = file_get_contents(WWW_ROOT . DS . $uploadFile);
            $attachment_encoded = base64_encode($attachment);
            CakeLog::write('debug', 'Add Requested Transaction: ' . print_r($transaction, true));


            $qsns = '';
            $name = strtoupper(substr($transaction['AddressBilling'][0]['firstname'], 0, 1) . substr($transaction['AddressBilling'][0]['lastname'], 0, 1));

            if ($transaction['Transaction']['user_id'] != null) {
                 $qsns = 'NS#' . $name . $transaction['Transaction']['user_id'];
            }
            else {
                $qsns = 'QS#'.$name . $this->request->data['transaction_id'];
            }


            //$result = $this->Notification->sendPackagedShippedEmail($transaction, $qsns, $uploadFile);
            //CakeLog::write('debug', 'ShipingBox Photo Add Mandrill response: ' . print_r($result, true));
            $this->Transaction->saveField('shippingbox_photo_url',  $uploadFile);
            $this->loadModel('Photo');
            $this->Photo->set('transaction_id',$this->request->data['transaction_id']);
            $this->Photo->set('requested_photo_url', $uploadFile);
            $this->Photo->set('type', 'shipping');
            $this->Photo->save();

            //Save note
            $this->loadModel('Note');
            $this->Note->set('transaction_id', $this->request->data['transaction_id']);
            $user_id = ($transaction['Transaction']['user_id']) ? $transaction['Transaction']['user_id'] : 9999999;
            $this->Note->set('user_id', $user_id);
            $this->Note->set('note', 'Added shipping box photo');
            $this->Note->save();

            $this->sendJsonResponse(array(
                'ok' => true,
                'transaction_id' => $this->request->data['transaction_id'],
                'message' => 'Email sent successfully',
                'email' => $user_email
            ));
        } else {
            $this->sendJsonResponse(array(
                'ok' => false,
                'message' => 'File upload failed. Something went wrong uploading file, please contact @developer'
            ));
        }
        die();
    }



    /**
     * Admin upload shippingbox photo
     * @throws Mandrill_Error
     */
    public function admin_add_note () {
        $this->loadModel('Note');
        $this->Note->set('transaction_id', $this->request->data['transaction_id']);
        $user_id = ($this->request->data['user_id']) ? $this->request->data['user_id'] : 9999999;
        $this->Note->set('user_id', $user_id);

        $this->Note->set('note', $this->request->data['note']);
        $this->Note->save();
        $data['id'] = $this->Note->id;
        $data['transaction_id'] = $this->request->data['transaction_id'];
        $data['user_id'] = $user_id;
        $data['note'] = $this->request->data['note'];
        $data['created'] = date('Y-m-d h:i:s');
        $this->sendJsonResponse(array(
            'ok' => true,
            'message' => 'Note added successfully',
            'note' => $data
        ));

        die();
    }

    /**
     * Get rates
     */
    public function shipping_calculator_get_rate() {

        CakeLog::write('error', print_r($this->request->data, true));
        $this->loadModel('Country');

        if ( $this->request->data["country"] == "0") {
            // load list of Countries
            $countries_list = $this->Country->getList();
            $countries = array();
            foreach ($countries_list as $country_id => $country_name) {
                $countries[$country_id] = $country_name;
            }
            $this->set(compact('countries'));
        }


        $data = $this->request->data;

        $objShipping = new \Hmhship\Quickship\Address();

        $data["first_name"] = "";
        $data["last_name"] = "";
        $data["address1"] = "";
        $data["address2"] = "";
        $data["phone"] = "";
        $objShipping->set((object)$data);

        CakeLog::write('debug', 'Shipping address: ' . json_encode($objShipping));
        \Hmhship\Logger::getInstance()->log('Shipping address: ' . json_encode($objShipping), 'debug');

        $package_obj = new \Hmhship\Quickship\Package();

        // $package_obj->items = $package->items;

        // Size
        $package_obj->size_unit = $this->request->data["size_unit"];
        $package_obj->height = round($this->request->data["height"]);
        $package_obj->width =  round($this->request->data["width"]);
        $package_obj->length = round($this->request->data["length"]);

        // Weight
        $package_obj->weight_unit = $this->request->data["weight_unit"];
        $package_obj->weight = round($this->request->data["weight"]);


        $package_rates = $this->Shipping->get_rates($objShipping, $package_obj);

        $this->sendJsonResponse($package_rates);

        //$this->set(compact('package_rates'));
    }



    /**
     * Admin upload requested photo for registered account
     * @throws Mandrill_Error
     */
    public function admin_add_registered_requested_photo () {
        $this->loadModel('User');
        $uploadPath = 'imgadmin'. DS. 'requested-registered';

        if (!is_dir($uploadPath)) {
            mkdir($uploadPath, 0777, true);
        }

        $fileName = $_FILES['file']['name'];
        $uploadFile = $uploadPath. DS . str_replace(' ', '_', $fileName);

        if( move_uploaded_file($_FILES["file"]["tmp_name"],  $uploadFile) ) {
            /* Send notification email to user*/
            $user = $this->User->getUser($this->request->data['user_id']);
            $user_email =  $user['User']['email'];
            $user_name =  $user['User']['first_name'] . ' ' . $user['User']['last_name'];
            CakeLog::write('debug', 'Add Requested Transaction: ' . print_r($user, true));


            // Send Admin email
            $qsns = '';
            $name = strtoupper(substr($user['User']['first_name'], 0, 1) .
                substr($user['User']['last_name'], 0, 1));
            $data['user_name'] = $name;
            $data['user_email'] = $user_email;
            $data['photo'] = 'https://'. $_SERVER['HTTP_HOST'] . DS . $uploadFile;

            $data['qsns'] = 'NS#'. $name . $this->request->data['user_id'];

            $result = $this->Notification->sendRequestedPhotoEmail($data);
            CakeLog::write('debug', 'Add Requested Photo Email: ' . print_r($result, true));

            if (isset($result[0]['_id']) && $result[0]['_id'] != null) {
                $this->loadModel('Photo');
                $this->Photo->set('transaction_id', $this->request->data['user_id']);
                $this->Photo->set('user_id', $this->request->data['user_id']);
                $this->Photo->set('requested_photo_url', $uploadFile);
                $this->Photo->set('type', 'registered');
                $this->Photo->save();


                //Save note
                $this->loadModel('Note');
                $this->Note->set('transaction_id', null);
                $this->Note->set('user_id', $this->request->data['user_id']);
                $this->Note->set('note', 'Added requested photo');
                $this->Note->save();

                $this->sendJsonResponse(array(
                    'ok' => true,
                    'user_id' => $this->request->data['user_id'],
                    'message' => 'Email sent successfully',
                    'email' => $user_email,
                    'status' => $result[0]['status']
                ));
            } else {
                $this->sendJsonResponse(array(
                    'ok' => false,
                    'user_id' => $this->request->data['user_id'],
                    'message' => 'Attachment uploaded but failed send the email to ' . $user_email
                ));
            }

        } else {
            $this->sendJsonResponse(array(
                'ok' => false,
                'message' => 'Something went wrong uploading file'
            ));
        }
        die();
    }

}
