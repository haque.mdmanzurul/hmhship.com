<?php

App::uses('Component', 'Controller');

/**
 * Class SessionObjectComponent
 *
 * @property SessionComponent $Session
 */
class SessionObjectComponent extends Component
{
	public $components = array('Session');

	/**
	 * @param $class
	 * @param $data
	 * @param null $session_key
	 *
	 * @return mixed
	 */
	public function create($class, $data, $session_key = null)
	{
		if (is_object($data)) {
			$data = (array)$data;
		}
		$session_key = is_null($session_key) ? strtolower($class) : $session_key;

		$o = ClassRegistry::init($class);
		$o->set($data);
		$this->Session->write($session_key, $o);

		return $o;
	}
}
