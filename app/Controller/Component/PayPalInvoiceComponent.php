﻿<?php

App::uses('Component', 'Controller');
use PayPal\Api\Address;
use PayPal\Api\BillingInfo;
use PayPal\Api\Cost;
use PayPal\Api\Currency;
use PayPal\Api\Invoice;
use PayPal\Api\InvoiceAddress;
use PayPal\Api\InvoiceItem;
use PayPal\Api\MerchantInfo;
use PayPal\Api\PaymentTerm;
use PayPal\Api\Phone;
use PayPal\Api\ShippingInfo;

    
/**
 * Class PaypalComponent
 */
class PaypalInvoiceComponent extends Component {

    /**
     * @return ApiContext
     */
    private function get_context() {
        $client_id = Configure::read('Payment.Paypal.client_id');
        $client_secret = Configure::read('Payment.Paypal.client_secret');
        $client_mode = Configure::read('Payment.Paypal.client_mode');
		
        
        $context = new PayPal\Rest\ApiContext(new PayPal\Auth\OAuthTokenCredential($client_id, $client_secret));
        $context->setConfig(array(
            'mode' => $client_mode,
            'log.LogEnabled' => (Configure::read('debug') > 0) ? true : true,
            'log.FileName' => '../tmp/logs/paypal.log',
            'log.LogLevel' => (Configure::read('debug') > 0) ? 'DEBUG' : 'FINE', // 'FINE' LEVEL FOR LOGGING LIVE
            'validation.level' => 'log',
            'cache.enabled' => false
        ));
        
        return $context;
    }	

    // $data should contain Transaction array
    // returns PayPal Invoice object
    public function createInvoice($data) {
    
        $invoice = new Invoice();

        // ### Invoice Info
        // Fill in all the information that is
        // required for invoice APIs
        $invoice
            ->setMerchantInfo(new MerchantInfo())
            ->setBillingInfo(array(new BillingInfo()))
            //->setNote("Medical Invoice 16 Jul, 2013 PST")
            ->setPaymentTerm(new PaymentTerm())
            ->setShippingInfo(new ShippingInfo());

        // ### Merchant Info
        // A resource representing merchant information that can be
        // used to identify merchant
        $invoice->getMerchantInfo()
            ->setEmail(Configure::read('Paypal.Merchant.Email'))
            //->setFirstName("Dennis")
            //->setLastName("Doctor")
            ->setbusinessName("HMHShip")
            ->setPhone(new Phone())
            ->setAddress(new Address());

        $invoice->getMerchantInfo()->getPhone()
            ->setCountryCode("001")
            ->setNationalNumber(Configure::read('Credits.phone'));

        // ### Address Information
        // The address used for creating the invoice
        $invoice->getMerchantInfo()->getAddress()
            ->setLine1("23600 Mercantile Rd.")
            ->setLine2("Suite C-119")
            ->setCity("Beachwood")
            ->setState("OH")
            ->setPostalCode("44122")
            ->setCountryCode("US");

        // ### Billing Information
        // Set the email address for each billing
        $billing = $invoice->getBillingInfo();
        $billing[0]
            ->setFirstName($data['AddressBilling'][0]['firstname'])
            ->setLastName($data['AddressBilling'][0]['lastname']) 
            ->setEmail($data['AddressBilling'][0]['email']);

        $billing[0]->setAddress(new InvoiceAddress());
        //  ->setBusinessName("Jay Inc")
        //    ->setAdditionalInfo("This is the billing Info")
        

        $billing[0]->getAddress()
            ->setLine1($data['AddressBilling'][0]['address1'])
            ->setLine2($data['AddressBilling'][0]['address2'])
            ->setCity($data['AddressBilling'][0]['city'])
            ->setState($data['AddressBilling'][0]['adm_division'])
            ->setPostalCode($data['AddressBilling'][0]['postal_code'])
            ->setCountryCode($data['AddressBilling'][0]['Country']['code']);

        // ### Items List
        // You could provide the list of all items for
        // detailed breakdown of invoice
        $items = array();


        $items[0] = new InvoiceItem();
        $items[0]
            ->setName("QuickShip #". $data['Transaction']['id'] . " Shipping Fees")
            ->setQuantity(1)
            ->setUnitPrice(new Currency());

        $items[0]->getUnitPrice()
            ->setCurrency("USD")
            ->setValue($data['Shipment'][0]['balance_due']);


        $invoice->setItems($items);
        

        $invoice->getPaymentTerm()
            ->setTermType("NET_15");

        // ### Shipping Information
        $ShippingEntity = "AddressBilling";
        if (isset($data['AddressShipping'][0]['id']))
            $ShippingEntity = "AddressShipping";

        $invoice->getShippingInfo()
            ->setFirstName($data[$ShippingEntity][0]['firstname'])
            ->setLastName($data[$ShippingEntity][0]['lastname'])            
            ->setPhone(new Phone())
            ->setAddress(new InvoiceAddress());

        $invoice->getShippingInfo()->getAddress()
            ->setLine1($data[$ShippingEntity][0]['address1'])
            ->setLine2($data[$ShippingEntity][0]['address2'])
            ->setCity($data[$ShippingEntity][0]['city'])
            ->setState($data[$ShippingEntity][0]['adm_division'])
            ->setPostalCode($data[$ShippingEntity][0]['postal_code'])
            ->setCountryCode($data[$ShippingEntity][0]['Country']['code']);

        $invoice->getShippingInfo()->getPhone()
            ->setCountryCode($data[$ShippingEntity][0]['Country']['code'])
            ->setNationalNumber($data[$ShippingEntity][0]['phone']);
      

        // ### Logo
        // You can set the logo in the invoice by providing the external URL pointing to a logo
        $invoice->setLogoUrl('https://www.HMHShip.com/img/logo.png');


        // ### Create Invoice
        // Create an invoice by calling the invoice->create() method
        // with a valid ApiContext (See bootstrap.php for more on `ApiContext`)
        $context = $this->get_context();  
        $invoice->create($context);

        // have to send it to make it Payable?
        $invoice->send($context);

        // retrieve it to get the uri
        $invoice = Invoice::get($invoice->getId(), $context);

        return $invoice;
    }

    public function getInvoice($id) {
        $invoice = Invoice::get($id, $this->get_context());

        return $invoice;
    }

    public function updateInvoice($invoice) {
        $invoice->update($this->get_context());
    }
 }