<?php

App::uses('Component', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('NotificationComponent', 'Controller/Component');

/**
 * Class MailerComponent
 */
class NotificationComponent extends Component
{

	public $controller;

	public function initialize(Controller $controller)
	{
		$this->controller = $controller;
		parent::initialize($controller);
	}

    /**
     * @param $data
     * @param string $template
     * @return array|string
     * @throws Mandrill_Error
     */
    // Called for both QuickShip and Shipments
    public function sendRequestedPhotoEmail($data, $template = 'you-re-requested-photo')
    {

        /*$photohtml = '';
        if (count($data['requestedPhotos'])) {
            foreach($data['requestedPhotos'] as $photo) {
                $photohtml .= '<img src="https://'. $_SERVER['HTTP_HOST']  . DS . $photo['requested_photo_url'] .'" style="width: 95%; height: auto"><br/>';
            }
        }*/

        $contactName = isset($data['AddressBilling']) ? $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname'] : '';

        $message = array(
            'html' => '<p>Dear '. $data['user_name'] . ',<br/> Your package has been shipped successfully. Please find attached image for more details. <br/><br/>Best Regards<br/> HMHShip Support</p>',
            'subject' => 'Your Requested Photo',
            'from_email' => 'contact@hmhship.com',
            'from_name' => 'HMHShip.com',
            'to' => array(
                array(
                    'email' => $data['user_email'],
                    'name' => $data['user_name'],
                    'type' => 'to'
                )
            ),
            'global_merge_vars' => array(
                array(
                    'name' => 'CONTACT_NAME',
                    'content' => $contactName,
                ),
                array(
                    'name' => 'NSQS',
                    'content' => $data['qsns'],
                ),
                array(
                    'name' => 'PHOTO',
                    'content' =>  $data['photo'],
                )
            )
        );

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        $template_name = $template;

        try {

            $result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            \Hmhship\Logger::getInstance()->log(sprintf('Email send to %s', $message['to'][0]['email']), 'debug');
            return $result;

        } catch (SocketException $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
            return $e->getMessage();
        } catch (Exception $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
            return $e->getMessage();
        }
    }

    public function sendPhotoRequestEmail($data, $contactName, $user_email, $Reqistered_Account_Number, $template = 'new-picture-request')
    {

        $message = array(
            'html' => '<p>Dear '. $contactName . ',<br/> Your package has been shipped successfully. Please find attached image for more details. <br/><br/>Best Regards<br/> HMHShip Support</p>',
            'subject' => 'We have receieved your photo request',
            'from_email' => 'contact@hmhship.com',
            'from_name' => 'HMHShip.com',
            'to' => array(
                array(
                    'email' => $user_email,
                    'name' => $contactName,
                    'type' => 'to'
                )
            ),
            'global_merge_vars' => array(
                array(
                    'name' => 'CONTACT_NAME',
                    'content' => $contactName,
                ),
                array(
                    'name' => 'Reqistered_Account_Number',
                    'content' => $Reqistered_Account_Number,
                ),
                array(
                    'name' => 'SHIPMENTS',
                    'content' => $data,
                )
            )
        );

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        $template_name = $template;

        try {

            $result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            \Hmhship\Logger::getInstance()->log(sprintf('Email send to %s', $message['to'][0]['email']), 'debug');
            return $result;

        } catch (SocketException $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
            return $e->getMessage();
        } catch (Exception $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
            return $e->getMessage();
        }
    }


    /**
     * @param $data
     * @param $qsns
     * @param $uploadFile
     * @param string $template
     * @return array|null
     * @throws Mandrill_Error
     */
    // Called for both QuickShip and Shipments
    public function sendPackagedShippedEmail($data, $qsns, $uploadFile, $template = 'we-ve-shipped-your-package')
    {
        // generate Packages HTML
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        $template_name = $template;

        $to = $data['AddressBilling'][0]['email'];

        $merge_vars_all = array(
            array(
                'name' => 'CONTACT_NAME',
                'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
            ),
            array(
                'name' => 'QSNS',
                'content' => $qsns
            ),
            array(
                'name' => 'PHOTO',
                'content' => '<img src="https://'. $_SERVER['HTTP_HOST']  . DS . $uploadFile .'" style="width: 24%; height: auto">'
            ),
            array(
                'name' => 'billing_firstname',
                'content' => $data['AddressBilling'][0]['firstname']
            ),
            array(
                'name' => 'billing_lastname',
                'content' => $data['AddressBilling'][0]['lastname']
            ),
            array(
                'name' => 'billing_address1',
                'content' => $data['AddressBilling'][0]['address1']
            ),
            array(
                'name' => 'billing_address2',
                'content' => $data['AddressBilling'][0]['address2']
            ),
            array(
                'name' => 'billing_postal_code',
                'content' => $data['AddressBilling'][0]['postal_code']
            ),
            array(
                'name' => 'billing_city',
                'content' => $data['AddressBilling'][0]['city']
            ),
            array(
                'name' => 'billing_state',
                'content' => $data['AddressBilling'][0]['adm_division']
            ),
            array(
                'name' => 'billing_country',
                'content' => $data['AddressBilling'][0]['Country']['name']
            ),
            array(
                'name' => 'billing_email',
                'content' => $data['AddressBilling'][0]['email']
            ),
            array(
                'name' => 'billing_phone',
                'content' => $data['AddressBilling'][0]['phone']
            ),
            array(
                'name' => 'shipping_firstname',
                'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
            ),
            array(
                'name' => 'shipping_lastname',
                'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
            ),
            array(
                'name' => 'shipping_address1',
                'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
            ),
            array(
                'name' => 'shipping_address2',
                'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
            ),
            array(
                'name' => 'shipping_postal_code',
                'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
            ),
            array(
                'name' => 'shipping_city',
                'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
            ),
            array(
                'name' => 'shipping_state',
                'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
            ),
            array(
                'name' => 'shipping_country',
                'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
            ),
            array(
                'name' => 'shipping_email',
                'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
            ),
            array(
                'name' => 'shipping_phone',
                'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
            ),
            array(
                'name' => 'order_number',
                'content' => sprintf('%05s', $data['Transaction']['id'])
            ),
            array(
                'name' => 'order_suite',
                'content' => $qsns
            ),
            array(
                'name' => 'order_date',
                'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['created']))
            ),
            array(
                'name' => 'handling_fee',
                'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
            ),
            array(
                'name' => 'payment_method',
                'content' => 'Paypal'
            ),
            array(
                'name' => 'packages',
                'content' => $packages_html
            ),
            array(
                'name' => 'charge_now',
                'content' => ($data['Transaction']['charge_now'] === true) ? __('Charge me right away') : __('Invoice me right away')
            ),
            array(
                'name' => 'shipment_consolidate',
                'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_expedited',
                'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_insured',
                'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_repackaged',
                'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_special_instructions',
                'content' =>  $data['Transaction']['special_instructions']
            ),
            array(
                'name' => 'est_shipping_cost',
                'content' => $this->getEstimatedShippingCost($data)
            )
        );

        $message = array(
            'subject' => 'We Have Shipped Your Packages!',
            'from_email' => 'Shipments@hmhship.com',
            'from_name' => 'HMHShip',
            'to' => array(
                array(
                    'email' => $to,
                    'name' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname'],
                    'type' => 'to'
                )
            ),
            'global_merge_vars' => $merge_vars_all

        );

        try {

            $result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            \Hmhship\Logger::getInstance()->log(sprintf('Email send to %s', $to), 'debug');
            return $result;

        } catch (SocketException $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
        } catch (Exception $e) {
            \Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
        }

        return null;

    }

    /**
     * @param $data
     * @param string $template
     * @return null|string
     * @throws Mandrill_Error
     */
    // Called for both QuickShip and Shipments
	public function sendTransactionCompleteEmail($data, $template = 'you-re-ready-to-ship')
	{
            // generate Packages HTML
            $view = new View($this->controller);
            $view->layout = null;
            $view->set(compact('data'));
            $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

            $mandrill = new Mandrill(Configure::read('Mandrill.key'));
            $template_name = $template;            
            
            $to = $data['AddressBilling'][0]['email'];
                
            if (null !== AuthComponent::user('username') && "" !== AuthComponent::user('username')) {
                // User is Logged In
                $name = strtoupper(substr(AuthComponent::user('first_name'),0,1) . substr(AuthComponent::user('last_name'),0,1));
                $co_code = 'NS#' . $name . AuthComponent::user('id');
            } else {
                // no user logged in
                $name = strtoupper(substr($data['AddressBilling'][0]['firstname'], 0, 1) .
                    substr($data['AddressBilling'][0]['lastname'], 0, 1));
                $co_code = 'QS#' . $name . $data['Transaction']['id'];
            }
                
            $message = array(                
                'subject' => 'You\'re Ready to Ship!', 
                'from_email' => 'Shipments@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $to,
                        'name' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname'],
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => array(
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => strtoupper('billing_firstname'),
                        'content' => $data['AddressBilling'][0]['firstname']
                    ),
                    array(
                        'name' => strtoupper('billing_lastname'),
                        'content' => $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => strtoupper('billing_address1'),
                        'content' => $data['AddressBilling'][0]['address1']
                    ),
                    array(
                        'name' => strtoupper('billing_address2'),
                        'content' => $data['AddressBilling'][0]['address2']
                    ),
                    array(
                        'name' => strtoupper('billing_postal_code'),
                        'content' => $data['AddressBilling'][0]['postal_code']
                    ),
                    array(
                        'name' => strtoupper('billing_city'),
                        'content' => $data['AddressBilling'][0]['city']
                    ),                   
                    array(
                        'name' => strtoupper('billing_state'),
                        'content' => $data['AddressBilling'][0]['adm_division']
                    ),
                    array(
                        'name' => strtoupper('billing_country'),
                        'content' => $data['AddressBilling'][0]['Country']['name']
                    ),
                    array(
                        'name' => strtoupper('billing_email'),
                        'content' => $data['AddressBilling'][0]['email']
                    ),
                    array(
                        'name' => strtoupper('billing_phone'),
                        'content' => $data['AddressBilling'][0]['phone']
                    ),
                    array(
                        'name' => strtoupper('shipping_firstname'),
                        'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
                    ),                    
                    array(
                        'name' => strtoupper('shipping_lastname'),
                        'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
                    ),
                    array(
                        'name' => strtoupper('shipping_address1'),
                        'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
                    ),
                    array(
                        'name' => strtoupper('shipping_address2'),
                        'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
                    ),
                    array(
                        'name' => strtoupper('shipping_postal_code'),
                        'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
                    ),
                    array(
                        'name' => strtoupper('shipping_city'),
                        'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
                    ),                    
                    array(
                        'name' => strtoupper('shipping_state'),
                        'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
                    ),
                    array(
                        'name' => strtoupper('shipping_country'),
                        'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
                    ),
                    array(
                        'name' => strtoupper('shipping_email'),
                        'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
                    ),
                    array(
                        'name' => strtoupper('shipping_phone'),
                        'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
                    ),
                    array(
                        'name' => strtoupper('order_number'),
                        'content' => sprintf('%05s', $data['Transaction']['id'])
                    ),
                    array(
                        'name' => strtoupper('order_suite'),
                        'content' => $co_code
                    ),                   
                    array(
                        'name' => strtoupper('order_date'),
                        'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['created']))
                    ),
                    array(
                        'name' => strtoupper('handling_fee'),
                        'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
                    ),
                    array(
                        'name' => strtoupper('payment_method'),
                        'content' => 'Paypal'
                    ),
                    array(
                        'name' => strtoupper('packages'),
                        'content' => $packages_html
                    ),
                    array(
                        'name' => strtoupper('charge_now'),
                        'content' => ($data['Transaction']['charge_now'] === true) ? __('Charge me right away') : __('Invoice me right away')
                    ),
                    array(
                        'name' => strtoupper('shipment_consolidate'),
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => strtoupper('shipment_expedited'),
                        'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => strtoupper('shipment_insured'),
                        'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => strtoupper('shipment_repackaged'),
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => strtoupper('shipment_special_instructions'),
                        'content' =>  $data['Transaction']['special_instructions']
                    ),
                    array(
                        'name' => strtoupper('est_shipping_cost'),
                        'content' => $this->getEstimatedShippingCost($data)
                    )                   
                )    
               
            );         
            
        try {
			
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            \Hmhship\Logger::getInstance()->log(sprintf('Email send to %s', $to), 'debug');
           

		} catch (SocketException $e) {
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}
            
        // generate and save Order Html
        $view = new View($this->controller);
        $view->layout = null;
        foreach($message['global_merge_vars'] as $arr) {
            CakeLog::write('debug', "name: " . $arr['name']);
            ${strtoupper($arr['name'])} = $arr['content'];
            $view->set(compact(strtoupper($arr['name'])));
        }
        $order_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'order_html');

		//CakeLog::write('debug', "order_html: " . $order_html);

		return $order_html;
	}

    /**
     * @todo fix mailchimp
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendAdminNewAccountEmail($data, $template = 'new-account-registered')
	{
        CakeLog::write('debug', "top of sendAdminNewAccountEmail...");
       
        $template_name = $template;            
            
        $to = "Registered-Accounts@hmhship.com";
                
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        
        $global_merge_vars = array(
                    array(
                        'name' => 'USER_EMAIL',
                        'content' => $data["email"]
                    ),
                    array(
                        'name' => 'USER_FIRST_NAME',
                        'content' => $data["billing"]["first_name"]
                    ),
                    array(
                        'name' => 'USER_LAST_NAME',
                        'content' => $data["billing"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADDRESS1',
                        'content' => $data["billing"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADDRESS2',
                        'content' => $data["billing"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS0_CITY',
                        'content' => $data["billing"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADM_DIVISION',
                        'content' => $data["billing"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS0_POSTAL_CODE',
                        'content' => $data["billing"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS0_COUNTRY_NAME',
                        'content' => $data["billing"]["country"]
                    ),
                    array(
                        'name' => 'USER_PHONE',
                        'content' => $data["billing"]["phone"]
                    ),
                    array(
                        'name' => 'USER_HOW_DID_YOU_HEAR',
                        'content' => $data["how_did_you_hear"]
                    )
                );

        if (array_key_exists("billing_shipping", $data)) {
            // shipping same as billing
            $global_merge_vars = array_merge($global_merge_vars, 
                array(
                    array(
                        'name' => 'ADDRESS1_FIRSTNAME',
                        'content' => $data["billing"]["first_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_LASTNAME',
                        'content' => $data["billing"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS1',
                        'content' => $data["billing"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS2',
                        'content' => $data["billing"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS1_CITY',
                        'content' => $data["billing"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADM_DIVISION',
                        'content' => $data["billing"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS1_POSTAL_CODE',
                        'content' => $data["billing"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS1_COUNTRY_NAME',
                        'content' => $data["billing"]["country"]
                    )
                )
            );
        }
        else {
            $global_merge_vars = array_merge($global_merge_vars, 
                array(
                    array(
                        'name' => 'ADDRESS1_FIRSTNAME',
                        'content' => $data["shipping"]["first_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_LASTNAME',
                        'content' => $data["shipping"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS1',
                        'content' => $data["shipping"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS2',
                        'content' => $data["shipping"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS1_CITY',
                        'content' => $data["shipping"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADM_DIVISION',
                        'content' => $data["shipping"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS1_POSTAL_CODE',
                        'content' => $data["shipping"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS1_COUNTRY_NAME',
                        'content' => $data["shipping"]["country"]
                    )
                )
            );
        }
        
        $message = array(                
                'subject' => 'New Account Registered!', 
                'from_email' => 'contact@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => 'Registered-Accounts@hmhship.com',                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

         try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('debug', "Socket Exception");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('debug', "Exception!!");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $to
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerNewAccountEmail($to, $data, $template = 'welcome-to-the-hmhship-team')
	{
        CakeLog::write('debug', "top of sendCustomerNewAccountEmail...");
       
        $template_name = $template;            
                
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        
        $global_merge_vars = array(
                    array(
                        'name' => 'USER_EMAIL',
                        'content' => $data["email"]
                    ),
                    array(
                        'name' => 'USER_FIRST_NAME',
                        'content' => $data["billing"]["first_name"]
                    ),
                    array(
                        'name' => 'USER_LAST_NAME',
                        'content' => $data["billing"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADDRESS1',
                        'content' => $data["billing"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADDRESS2',
                        'content' => $data["billing"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS0_CITY',
                        'content' => $data["billing"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS0_ADM_DIVISION',
                        'content' => $data["billing"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS0_POSTAL_CODE',
                        'content' => $data["billing"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS0_COUNTRY_NAME',
                        'content' => $data["billing"]["country"]
                    ),
                    array(
                        'name' => 'USER_PHONE',
                        'content' => $data["billing"]["phone"]
                    ),
                    array(
                        'name' => 'USER_HOW_DID_YOU_HEAR',
                        'content' => $data["how_did_you_hear"]
                    ),
                    array(
                        'name' => 'CURRENT_YEAR',
                        'content' => date("Y")
                    )     
                );

        if (array_key_exists("billing_shipping", $data)) {
            // shipping same as billing
            $global_merge_vars = array_merge($global_merge_vars, 
                array(
                    array(
                        'name' => 'ADDRESS1_FIRSTNAME',
                        'content' => $data["billing"]["first_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_LASTNAME',
                        'content' => $data["billing"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS1',
                        'content' => $data["billing"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS2',
                        'content' => $data["billing"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS1_CITY',
                        'content' => $data["billing"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADM_DIVISION',
                        'content' => $data["billing"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS1_POSTAL_CODE',
                        'content' => $data["billing"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS1_COUNTRY_NAME',
                        'content' => $data["billing"]["country"]
                    )
                )
            );
        }
        else {
            $global_merge_vars = array_merge($global_merge_vars, 
                array(
                    array(
                        'name' => 'ADDRESS1_FIRSTNAME',
                        'content' => $data["shipping"]["first_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_LASTNAME',
                        'content' => $data["shipping"]["last_name"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS1',
                        'content' => $data["shipping"]["address1"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADDRESS2',
                        'content' => $data["shipping"]["address2"]
                    ),
                    array(
                        'name' => 'ADDRESS1_CITY',
                        'content' => $data["shipping"]["city"]
                    ),
                    array(
                        'name' => 'ADDRESS1_ADM_DIVISION',
                        'content' => $data["shipping"]["adm_division"]
                    ),
                    array(
                        'name' => 'ADDRESS1_POSTAL_CODE',
                        'content' => $data["shipping"]["postal_code"]
                    ),
                    array(
                        'name' => 'ADDRESS1_COUNTRY_NAME',
                        'content' => $data["shipping"]["country"]
                    )
                )
            );
        }
        
        $message = array(                
                'subject' => 'Welcome to the HMHShip Team!', 
                'from_email' => 'Registered-Accounts@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $to,                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

         try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('debug', "Socket Exception");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('debug', "Exception!!");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendAdminMessageEmail($data, $template = 'new-message-from-customer-received')
	{
        CakeLog::write('debug', "top of sendAdminMessageEmail...");
      
        $template_name = $template;            
                     
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        
        // build message display
        $messages = "<ul style='list-style: none;padding-left:0px;'>";
        foreach ($data['message'] as $msg) {
            $messages = $messages . "<li>" . date('m/d/y h:i A', strtotime($msg['Message']['created'])) . " ";
            if ($msg['Message']['from_hmh']) {
                $messages = $messages . "HMH: ";
            }
            else {
                $messages = $messages . "User: ";
            }
            
            $messages = $messages . $msg['Message']['message'] . "</li>";
        }
        $messages = $messages . "</ul>";
       

        $global_merge_vars = array(
                    array(
                        'name' => 'FROM',
                        'content' => $data['from']
                    ),
                    array(
                        'name' => 'EMAIL',
                        'content' => $data['email']
                    ),
                    array(
                        'name' => 'MESSAGE',
                        'content' => $messages
                    )
                );

        $message = array(                
                'subject' => 'New Message from Customer Received!', 
                'from_email' => 'contact@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => 'contact@hmhship.com',                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('debug', "Socket Exception");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('debug', "Exception!!");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $to
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerMessageEmail($to, $data, $template = 'we-ve-received-your-message')
	{
        CakeLog::write('debug', "top of sendCustomerMessageEmail...");
      
        $template_name = $template;            
                     
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));
        
        // build message display
        $messages = "<ul style='list-style: none;padding-left:0px;'>";
        foreach ($data['message'] as $msg) {
            $messages = $messages . "<li>" . date('m/d/y h:i A', strtotime($msg['Message']['created'])) . " ";
            if ($msg['Message']['from_hmh']) {
                $messages = $messages . "HMH: ";
            }
            else {
                $messages = $messages . "User: ";
            }
            
            $messages = $messages . $msg['Message']['message'] . "</li>";
        }
        $messages = $messages . "</ul>";
       

        $global_merge_vars = array(
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['from']
                    ),
                    array(
                        'name' => 'EMAIL',
                        'content' => $data['email']
                    ),
                    array(
                        'name' => 'MESSAGE',
                        'content' => $messages
                    ),
                    array(
                        'name' => 'CURRENT_YEAR',
                        'content' => date("Y")
                    )     
                );

        $message = array(                
                'subject' => "We've Received Your Message!", 
                'from_email' => 'contact@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $to,                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('debug', "Socket Exception");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('debug', "Exception!!");
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendAdminBusinessEmail($data, $template = 'new-hmhship-business-received')
	{        
      
        $template_name = $template;            
                     
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        $global_merge_vars = array(
                    array(
                        'name' => 'Monthly_Plan',
                        'content' => $data["shipping"]
                    ),
                    array(
                        'name' => 'Insured',
                        'content' => (array_key_exists("insured", $data)) ? "Insured" : ""
                    ),
                    array(
                        'name' => 'Economy',
                        'content' => (array_key_exists("economy", $data)) ? "Economy" : ""
                    ),
                    array(
                        'name' => 'Expedited',
                        'content' => (array_key_exists("expedited", $data)) ? "Expedited" : ""
                    ),
                    array(
                        'name' => 'DHL',
                        'content' => (array_key_exists("dhl", $data)) ? "DHL" : ""
                    ),
                    array(
                        'name' => 'UPS',
                        'content' => (array_key_exists("ups", $data)) ? "UPS" : ""
                    ),
                    array(
                        'name' => 'USPS',
                        'content' => (array_key_exists("usps", $data)) ? "USPS" : ""
                    ),
                    array(
                        'name' => 'FedEx',
                        'content' => (array_key_exists("fedex", $data)) ? "FedEx" : ""
                    ),
                    array(
                        'name' => 'Shipping_Carrier',
                        'content' => $data["account_shipping_carrier"]
                    ),
                    array(
                        'name' => 'Account_Number',
                        'content' => $data["account_number"]
                    ),
                    array(
                        'name' => 'Account_Zip',
                        'content' => $data["account_zip"]
                    ),
                    array(
                        'name' => 'Business_NAME',
                        'content' => $data["business"]
                    ),
                    array(
                        'name' => 'Primary_Person',
                        'content' => $data["primary_person"]
                    ),
                    array(
                        'name' => 'Main_Phone',
                        'content' => $data["main_phone"]
                    ),
                    array(
                        'name' => 'Cellphone',
                        'content' => $data["cell_phone"]
                    ),
                    array(
                        'name' => 'CONTACT_EMAIL',
                        'content' => $data["email"]
                    ),
                    array(
                        'name' => 'Fax',
                        'content' => $data["fax"]
                    ),
                    array(
                        'name' => 'Website',
                        'content' => $data["website"]
                    ),
                    array(
                        'name' => 'Street_Address',
                        'content' => $data["street_address"]
                    ),
                    array(
                        'name' => 'Street_Address_2',
                        'content' => $data["street_address2"]
                    ),
                    array(
                        'name' => 'City',
                        'content' => $data["city"]
                    ),
                    array(
                        'name' => 'State',
                        'content' => $data["state"]
                    ),
                    array(
                        'name' => 'Zip_Code',
                        'content' => $data["zip"]
                    ),
                    array(
                        'name' => 'Country',
                        'content' => $data["country"]
                    ),
                    array(
                        'name' => 'CURRENT_YEAR',
                        'content' => date("Y")
                    )                    
                );

        $message = array(                
                'subject' => 'New HMHShip Business Received!', 
                'from_email' => 'contact@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => 'Business@hmhship.com',                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', "Socket Exception " . $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $to
     * @param $data
     * @param string $template
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerBusinessEmail($to, $data, $template = 'hmhship-business')
	{        
      
        $template_name = $template;            
                     
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        $global_merge_vars = array(
                    array(
                        'name' => 'Monthly_Plan',
                        'content' => $data["shipping"]
                    ),
                    array(
                        'name' => 'Insured',
                        'content' => (array_key_exists("insured", $data)) ? "Insured" : ""
                    ),
                    array(
                        'name' => 'Economy',
                        'content' => (array_key_exists("economy", $data)) ? "Economy" : ""
                    ),
                    array(
                        'name' => 'Expedited',
                        'content' => (array_key_exists("expedited", $data)) ? "Expedited" : ""
                    ),
                    array(
                        'name' => 'DHL',
                        'content' => (array_key_exists("dhl", $data)) ? "DHL" : ""
                    ),
                    array(
                        'name' => 'UPS',
                        'content' => (array_key_exists("ups", $data)) ? "UPS" : ""
                    ),
                    array(
                        'name' => 'USPS',
                        'content' => (array_key_exists("usps", $data)) ? "USPS" : ""
                    ),
                    array(
                        'name' => 'FedEx',
                        'content' => (array_key_exists("fedex", $data)) ? "FedEx" : ""
                    ),
                    array(
                        'name' => 'Shipping_Carrier',
                        'content' => $data["account_shipping_carrier"]
                    ),
                    array(
                        'name' => 'Account_Number',
                        'content' => $data["account_number"]
                    ),
                    array(
                        'name' => 'Account_Zip',
                        'content' => $data["account_zip"]
                    ),
                    array(
                        'name' => 'Business_NAME',
                        'content' => $data["business"]
                    ),
                    array(
                        'name' => 'Primary_Person',
                        'content' => $data["primary_person"]
                    ),
                    array(
                        'name' => 'Main_Phone',
                        'content' => $data["main_phone"]
                    ),
                    array(
                        'name' => 'Cellphone',
                        'content' => $data["cell_phone"]
                    ),
                    array(
                        'name' => 'CONTACT_EMAIL',
                        'content' => $data["email"]
                    ),
                    array(
                        'name' => 'Fax',
                        'content' => $data["fax"]
                    ),
                    array(
                        'name' => 'Website',
                        'content' => $data["website"]
                    ),
                    array(
                        'name' => 'Street_Address',
                        'content' => $data["street_address"]
                    ),
                    array(
                        'name' => 'Street_Address_2',
                        'content' => $data["street_address2"]
                    ),
                    array(
                        'name' => 'City',
                        'content' => $data["city"]
                    ),
                    array(
                        'name' => 'State',
                        'content' => $data["state"]
                    ),
                    array(
                        'name' => 'Zip_Code',
                        'content' => $data["zip"]
                    ),
                    array(
                        'name' => 'Country',
                        'content' => $data["country"]
                    ),
                    array(
                        'name' => 'CURRENT_YEAR',
                        'content' => date("Y")
                    )                    
                );

        $message = array(                
                'subject' => 'HMHShip Business', 
                'from_email' => 'Business@hmhship.com',
                'from_name' => 'HMHShip Business',
                'to' => array(
                    array(
                        'email' => $to,                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', "Socket Exception " . $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @todo Fix mailchimp template
     * @param $data
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendAdminQuickShipEmail($data)
	{ 
        
        if ($data['isShipment']) {
            $template_name = "new-shipment-received";
            $subject = "New Shipment Received!";
            $to = "New-Shipments@hmhship.com";

            if (isset($data['ghost_shipment'])) {
                $subject = $data['ghost_shipment'];  // special subject with additional info
            }
            
        }
        else {            
            $template_name = "new-quickship-received";
            $subject = "New QuickShip Received!";
            $to = "QuickShip@hmhship.com";
        }

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        // generate Packages HTML
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

        $global_merge_vars = array(
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_firstname',
                        'content' => $data['AddressBilling'][0]['firstname']
                    ),
                    array(
                        'name' => 'billing_lastname',
                        'content' => $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_address1',
                        'content' => $data['AddressBilling'][0]['address1']
                    ),
                    array(
                        'name' => 'billing_address2',
                        'content' => $data['AddressBilling'][0]['address2']
                    ),
                    array(
                        'name' => 'billing_postal_code',
                        'content' => $data['AddressBilling'][0]['postal_code']
                    ),
                    array(
                        'name' => 'billing_city',
                        'content' => $data['AddressBilling'][0]['city']
                    ),                   
                    array(
                        'name' => 'billing_state',
                        'content' => $data['AddressBilling'][0]['adm_division']
                    ),
                    array(
                        'name' => 'billing_country',
                        'content' => $data['AddressBilling'][0]['Country']['name']
                    ),
                    array(
                        'name' => 'billing_email',
                        'content' => $data['AddressBilling'][0]['email']
                    ),
                    array(
                        'name' => 'billing_phone',
                        'content' => $data['AddressBilling'][0]['phone']
                    ),
                    array(
                        'name' => 'shipping_firstname',
                        'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
                    ),                    
                    array(
                        'name' => 'shipping_lastname',
                        'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
                    ),
                    array(
                        'name' => 'shipping_address1',
                        'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
                    ),
                    array(
                        'name' => 'shipping_address2',
                        'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
                    ),
                    array(
                        'name' => 'shipping_postal_code',
                        'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
                    ),
                    array(
                        'name' => 'shipping_city',
                        'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
                    ),                    
                    array(
                        'name' => 'shipping_state',
                        'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
                    ),
                    array(
                        'name' => 'shipping_country',
                        'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
                    ),
                    array(
                        'name' => 'shipping_email',
                        'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
                    ),
                    array(
                        'name' => 'shipping_phone',
                        'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
                    ),
                    array(
                        'name' => 'order_number',
                        'content' => sprintf('%05s', $data['Transaction']['id'])
                    ),                                
                    array(
                        'name' => 'order_date',
                        'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['date']))
                    ),
                    array(
                        'name' => 'handling_fee',
                        'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
                    ),
                    array(
                        'name' => 'payment_method',
                        'content' => 'Paypal'
                    ),
                    array(
                        'name' => 'packages',
                        'content' => $packages_html
                    ),
                    array(
                        'name' => 'charge_now',
                        'content' => (strlen($data['Transaction']['code']) > 4 && substr($data['Transaction']['code'],0,4) == 'PAY-') ? __('Invoice me right away') : __('Charge me right away') 
                    ),
                    array(
                        'name' => 'shipment_consolidate',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_expedited',
                        'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_insured',
                        'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_repackaged',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_special_instructions',
                        'content' =>  $data['Transaction']['special_instructions']
                    ),
                    array(
                        'name' => 'est_shipping_cost',
                        'content' => $this->getEstimatedShippingCost($data)
                    )                   
                );

        $message = array(                
                'subject' => $subject, 
                'from_email' => 'contact@hmhship.com',
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $to,                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @todo Fix mailchimp template
     * @param $data
     * @param string $template_name
     * @param string $subject
     * @param int $balance_due
     * @param string $pay_link
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerPackageReceivedEmail($data, $template_name = "we-ve-received-your-package",
     $subject = "We've Received Your Package!", $balance_due = 0, $pay_link = "")
	{
        //$this->loadModel('Transaction');

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        if ($template_name == "we-ve-received-your-package-payment-required")
            $from = "Accounting@hmhship.com";
        else
            $from = "Shipments@hmhship.com";


        // generate Packages HTML
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

        $name = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1);
        if ($data['Transaction']['user_id'] != null) {
            $data['qsns'] = 'NS#' . $name . $data['Transaction']['user_id'];
        }
        else $data['qsns'] = 'QS#'. $name . $data['Transaction']['id'];

        $global_merge_vars = array(
                    array(
                        'name' => 'UNSENTPACKAGECOUNT',
                        'content' => $data['unsent_packages']
                    ),
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_firstname',
                        'content' => $data['AddressBilling'][0]['firstname']
                    ),
                    array(
                        'name' => 'billing_lastname',
                        'content' => $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_address1',
                        'content' => $data['AddressBilling'][0]['address1']
                    ),
                    array(
                        'name' => 'billing_address2',
                        'content' => $data['AddressBilling'][0]['address2']
                    ),
                    array(
                        'name' => 'billing_postal_code',
                        'content' => $data['AddressBilling'][0]['postal_code']
                    ),
                    array(
                        'name' => 'billing_city',
                        'content' => $data['AddressBilling'][0]['city']
                    ),                   
                    array(
                        'name' => 'billing_state',
                        'content' => $data['AddressBilling'][0]['adm_division']
                    ),
                    array(
                        'name' => 'billing_country',
                        'content' => $data['AddressBilling'][0]['Country']['name']
                    ),
                    array(
                        'name' => 'billing_email',
                        'content' => $data['AddressBilling'][0]['email']
                    ),
                    array(
                        'name' => 'billing_phone',
                        'content' => $data['AddressBilling'][0]['phone']
                    ),
                    array(
                        'name' => 'shipping_firstname',
                        'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
                    ),                    
                    array(
                        'name' => 'shipping_lastname',
                        'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
                    ),
                    array(
                        'name' => 'shipping_address1',
                        'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
                    ),
                    array(
                        'name' => 'shipping_address2',
                        'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
                    ),
                    array(
                        'name' => 'shipping_postal_code',
                        'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
                    ),
                    array(
                        'name' => 'shipping_city',
                        'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
                    ),                    
                    array(
                        'name' => 'shipping_state',
                        'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
                    ),
                    array(
                        'name' => 'shipping_country',
                        'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
                    ),
                    array(
                        'name' => 'shipping_email',
                        'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
                    ),
                    array(
                        'name' => 'shipping_phone',
                        'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
                    ),
                    array(
                        'name' => 'ORDER_NUMBER',
                        'content' => sprintf('%05s', $data['Transaction']['id'])
                    ),                                
                    array(
                        'name' => 'order_date',
                        'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['date']))
                    ),
                    array(
                        'name' => 'handling_fee',
                        'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
                    ),
                    array(
                        'name' => 'payment_method',
                        'content' => 'Paypal'
                    ),
                    array(
                        'name' => 'packages',
                        'content' => $packages_html
                    ),
                    array(
                        'name' => 'charge_now',
                        'content' => (strlen($data['Transaction']['code']) > 4 && substr($data['Transaction']['code'],0,4) == 'PAY-') ? __('Invoice me right away') : __('Charge me right away') 
                    ),
                    array(
                        'name' => 'shipment_consolidate',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_expedited',
                        'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_insured',
                        'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_repackaged',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_special_instructions',
                        'content' =>  $data['Transaction']['special_instructions']
                    ),
                    array(
                        'name' => 'est_shipping_cost',
                        'content' => $this->getEstimatedShippingCost($data)
                    ),
                    array(
                        'name' => 'ORDER_HTML',
                        'content' => (isset($data['Transaction']['order_html'])) ? $data['Transaction']['order_html'] : '' 
                    ),
                    array(
                        'name' => 'PAY_LINK',
                        'content' => $pay_link
                    ),
                    array(
                        'name' => 'BALANCE_DUE',
                        'content' => "$" . number_format($balance_due, 2)
                    ),
                    array(
                        'name' => 'QSNS',
                        'content' => $data['qsns']
                    )
                );

        $message = array(                
                'subject' => $subject, 
                'from_email' => $from,
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $data['AddressBilling'][0]['email'],                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @todo fix mailchimp template
     * @param $data
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerPackageReceivedActionRequiredEmail($data)
	{  

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        $from = "Shipments@hmhship.com";

        // generate Packages HTML -- uses $data['Shipment']['Package']        
        //CakeLog::write('debug', 'top of sendCustomerPackageReceivedActionRequiredEmail: ' . print_r($data, true));
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'packages_simple');
        $name = substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1);
        if ($data['Transaction']['user_id'] != null) {
            $data['qsns'] = 'NS#' . $name . $data['Transaction']['user_id'];
        }
        else $data['qsns'] = 'QS#'. $name . $data['Transaction']['id'];

        $global_merge_vars = array(
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_firstname',
                        'content' => $data['AddressBilling'][0]['firstname']
                    ),
                    array(
                        'name' => 'billing_lastname',
                        'content' => $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_address1',
                        'content' => $data['AddressBilling'][0]['address1']
                    ),
                    array(
                        'name' => 'billing_address2',
                        'content' => $data['AddressBilling'][0]['address2']
                    ),
                    array(
                        'name' => 'billing_postal_code',
                        'content' => $data['AddressBilling'][0]['postal_code']
                    ),
                    array(
                        'name' => 'billing_city',
                        'content' => $data['AddressBilling'][0]['city']
                    ),                   
                    array(
                        'name' => 'billing_state',
                        'content' => $data['AddressBilling'][0]['adm_division']
                    ),
                    array(
                        'name' => 'billing_country',
                        'content' => $data['AddressBilling'][0]['Country']['name']
                    ),
                    array(
                        'name' => 'billing_email',
                        'content' => $data['AddressBilling'][0]['email']
                    ),
                    array(
                        'name' => 'billing_phone',
                        'content' => $data['AddressBilling'][0]['phone']
                    ),
                    array(
                        'name' => 'shipping_firstname',
                        'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
                    ),                    
                    array(
                        'name' => 'shipping_lastname',
                        'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
                    ),
                    array(
                        'name' => 'shipping_address1',
                        'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
                    ),
                    array(
                        'name' => 'shipping_address2',
                        'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
                    ),
                    array(
                        'name' => 'shipping_postal_code',
                        'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
                    ),
                    array(
                        'name' => 'shipping_city',
                        'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
                    ),                    
                    array(
                        'name' => 'shipping_state',
                        'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
                    ),
                    array(
                        'name' => 'shipping_country',
                        'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
                    ),
                    array(
                        'name' => 'shipping_email',
                        'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
                    ),
                    array(
                        'name' => 'shipping_phone',
                        'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
                    ),
                    array(
                        'name' => 'packages',
                        'content' => $packages_html
                    ),
                    array(
                        'name' => 'QSNS',
                        'content' => $data['qsns']
                    ),
                    array(
                        'name' => 'order_suite',
                        'content' => substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1) . $data['Transaction']['user_id']
                    ),
                    array(
                        'name' => 'ORDER_NUMBER',
                        'content' => sprintf('%05s', $data['Transaction']['id'])
                    ),
                    array(
                        'name' => 'ORDER_DATE',
                        'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['date']))
                    )
                    
                );
               

        $message = array(                
                'subject' => "We've Received Your Package! - Action Required", 
                'from_email' => $from,
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $data['AddressBilling'][0]['email'],                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate...");
			$result = $mandrill->messages->sendTemplate("we-ve-received-your-package-action-required", '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @todo fix mailchimp template
     * @param $data
     * @param $order_suite
     * @return array|bool
     * @throws Mandrill_Error
     */
    public function sendCustomerPaymentReceivedEmail($data, $order_suite)
	{  

        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        $template_name = "payment-received";

        $message = "We'll have your package shipped to you right away!";       
        
        // generate Packages HTML
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

        $global_merge_vars = array(
            array(
                'name' => 'CONTACT_NAME',
                'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
            ),
            array(
                'name' => 'billing_firstname',
                'content' => $data['AddressBilling'][0]['firstname']
            ),
            array(
                'name' => 'billing_lastname',
                'content' => $data['AddressBilling'][0]['lastname']
            ),
            array(
                'name' => 'billing_address1',
                'content' => $data['AddressBilling'][0]['address1']
            ),
            array(
                'name' => 'billing_address2',
                'content' => $data['AddressBilling'][0]['address2']
            ),
            array(
                'name' => 'billing_postal_code',
                'content' => $data['AddressBilling'][0]['postal_code']
            ),
            array(
                'name' => 'billing_city',
                'content' => $data['AddressBilling'][0]['city']
            ),
            array(
                'name' => 'billing_state',
                'content' => $data['AddressBilling'][0]['adm_division']
            ),
            array(
                'name' => 'billing_country',
                'content' => $data['AddressBilling'][0]['Country']['name']
            ),
            array(
                'name' => 'billing_email',
                'content' => $data['AddressBilling'][0]['email']
            ),
            array(
                'name' => 'billing_phone',
                'content' => $data['AddressBilling'][0]['phone']
            ),
            array(
                'name' => 'shipping_firstname',
                'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
            ),
            array(
                'name' => 'shipping_lastname',
                'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
            ),
            array(
                'name' => 'shipping_address1',
                'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
            ),
            array(
                'name' => 'shipping_address2',
                'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
            ),
            array(
                'name' => 'shipping_postal_code',
                'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
            ),
            array(
                'name' => 'shipping_city',
                'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
            ),
            array(
                'name' => 'shipping_state',
                'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
            ),
            array(
                'name' => 'shipping_country',
                'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
            ),
            array(
                'name' => 'shipping_email',
                'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
            ),
            array(
                'name' => 'shipping_phone',
                'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
            ),
            array(
                'name' => 'packages',
                'content' => $packages_html
            ),
            array(
                'name' => 'QSNS',
                'content' => $data['qsns']
            ),
            array(
                'name' => 'order_suite',
                'content' => substr($data['AddressBilling'][0]['firstname'],0,1) . substr($data['AddressBilling'][0]['lastname'],0,1) . $data['Transaction']['user_id']
            ),
            array(
                'name' => 'ORDER_DATE',
                'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['date']))
            ),
            array(
                'name' => 'ORDER_NUMBER',
                'content' => sprintf('%05s', $data['Transaction']['id'])
            ),
            array(
                'name' => 'handling_fee',
                'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
            ),
            array(
                'name' => 'payment_method',
                'content' => 'Paypal'
            ),
            array(
                'name' => 'packages',
                'content' => $packages_html
            ),
            array(
                'name' => 'charge_now',
                'content' => (strlen($data['Transaction']['code']) > 4 && substr($data['Transaction']['code'],0,4) == 'PAY-') ? __('Invoice me right away') : __('Charge me right away')
            ),
            array(
                'name' => 'shipment_consolidate',
                'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_expedited',
                'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_insured',
                'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_repackaged',
                'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
            ),
            array(
                'name' => 'shipment_special_instructions',
                'content' =>  $data['Transaction']['special_instructions']
            ),
            array('name' => 'pending_shipment_count', 
				'content' => $data['unsent_packages']
			),
            array(
                'name' => 'est_shipping_cost',
                'content' => $this->getEstimatedShippingCost($data)
            )

        );

               

        $message = array(                
                'subject' => "Payment Received!", 
                'from_email' => "Accounting@HMHShip.com",
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $data['AddressBilling'][0]['email'],                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate Payment Received...");
			$result = $mandrill->messages->sendTemplate($template_name, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @todo fix mailchimp template
     * @param $data
     * @param string $template
     * @param string $subject
     * @return array|bool
     */
	public function sendTransactionCompleteAdminEmail($data, $template = 'transaction-complete-admin', $subject = 'New Quickship Transaction')
	{
        CakeLog::write('debug', "top of sendTransactionCompleteAdminEmail");
		$email = new CakeEmail($this->_getSmtpEnvironment());
		$to = "QuickShip@hmhship.com";
		$email->to($to);
		$email->subject($subject);
		$viewVars = array('transaction_details' => json_encode($data));
		$email->addHeaders(array(
			'X-MC-Template' => $template,
			'X-MC-MergeVars' => json_encode($viewVars)
		));
        CakeLog::write('debug', "sendTransactionCompleteAdminEmail 2");
		$email->viewVars($viewVars);
		try {			
			return $email->send();
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}
		return false;
	}


    /**
     * @todo fix mailchimp template
     * @param $data
     * @param $order_suite
     * @param $tracking_number
     * @param $out_carrier
     * @return array|bool
     * @throws Mandrill_Error
     */
	public function sendCustomerShipmentSentEmail($data, $order_suite, $tracking_number, $out_carrier)
	{
		
        $mandrill = new Mandrill(Configure::read('Mandrill.key'));

        $template = 'we-ve-shipped-your-package';
       
        CakeLog::write('debug', "data: " . print_r($data, true));        

        // generate Packages HTML
        $view = new View($this->controller);
        $view->layout = null;
        $view->set(compact('data'));
        $packages_html = $view->render((property_exists($this->controller, 'name') && $this->controller->name === 'Payments' ? '' : '../Payments/') . 'transaction_packages');

        $photohtml = '';
        if (count($data['shippingPhotos'])) {
            foreach($data['shippingPhotos'] as $photo) {
                $photohtml .= '<img src="https://'. $_SERVER['HTTP_HOST']  . DS . $photo['requested_photo_url'] .'" style="width: 95%; height: auto"><br/>';
            }
        }
        $global_merge_vars = array(
                    array(
                        'name' => 'CONTACT_NAME',
                        'content' => $data['AddressBilling'][0]['firstname'] . ' ' . $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'PHOTO',
                        'content' => $photohtml
                    ),
                    array(
                        'name' => 'CURRENT_YEAR',
                        'content' => date("Y")
                    ),
                    array(
                        'name' => 'QSNS',
                        'content' => $data['qsns']
                    ),
                    array(
                        'name' => 'ORDER_SUITE',
                        'content' => $order_suite
                    ),
                    array(
                        'name' => 'order_number',
                        'content' => sprintf('%05s', $data['Transaction']['id'])
                    ),                    
                    array(
                        'name' => 'order_date',
                        'content' => date('F jS, Y g:i a', strtotime($data['Transaction']['date']))
                    ),
                    array(
                        'name' => 'TRACKING_NUMBER',
                        'content' => $tracking_number
                    ),
                    array(
                        'name' => 'OUT_CARRIER',
                        'content' => $out_carrier
                    ),
                     array(
                        'name' => 'billing_firstname',
                        'content' => $data['AddressBilling'][0]['firstname']
                    ),
                    array(
                        'name' => 'billing_lastname',
                        'content' => $data['AddressBilling'][0]['lastname']
                    ),
                    array(
                        'name' => 'billing_address1',
                        'content' => $data['AddressBilling'][0]['address1']
                    ),
                    array(
                        'name' => 'billing_address2',
                        'content' => $data['AddressBilling'][0]['address2']
                    ),
                    array(
                        'name' => 'billing_postal_code',
                        'content' => $data['AddressBilling'][0]['postal_code']
                    ),
                    array(
                        'name' => 'billing_city',
                        'content' => $data['AddressBilling'][0]['city']
                    ),                   
                    array(
                        'name' => 'billing_state',
                        'content' => $data['AddressBilling'][0]['adm_division']
                    ),
                    array(
                        'name' => 'billing_country',
                        'content' => $data['AddressBilling'][0]['Country']['name']
                    ),
                    array(
                        'name' => 'billing_email',
                        'content' => $data['AddressBilling'][0]['email']
                    ),
                    array(
                        'name' => 'billing_phone',
                        'content' => $data['AddressBilling'][0]['phone']
                    ),
                    array(
                        'name' => 'shipping_firstname',
                        'content' => (isset($data['AddressShipping'][0]['firstname']) ? $data['AddressShipping'][0]['firstname'] : $data['AddressBilling'][0]['firstname'])
                    ),                    
                    array(
                        'name' => 'shipping_lastname',
                        'content' => (isset($data['AddressShipping'][0]['lastname']) ? $data['AddressShipping'][0]['lastname'] : $data['AddressBilling'][0]['lastname'])
                    ),
                    array(
                        'name' => 'shipping_address1',
                        'content' => (isset($data['AddressShipping'][0]['address1']) ? $data['AddressShipping'][0]['address1'] : $data['AddressBilling'][0]['address1'])
                    ),
                    array(
                        'name' => 'shipping_address2',
                        'content' => (isset($data['AddressShipping'][0]['address2']) ? $data['AddressShipping'][0]['address2'] : $data['AddressBilling'][0]['address2'])
                    ),
                    array(
                        'name' => 'shipping_postal_code',
                        'content' => (isset($data['AddressShipping'][0]['postal_code']) ? $data['AddressShipping'][0]['postal_code'] : $data['AddressBilling'][0]['postal_code'])
                    ),
                    array(
                        'name' => 'shipping_city',
                        'content' => (isset($data['AddressShipping'][0]['city']) ? $data['AddressShipping'][0]['city'] : $data['AddressBilling'][0]['city'])
                    ),                    
                    array(
                        'name' => 'shipping_state',
                        'content' => (isset($data['AddressShipping'][0]['adm_division']) ? $data['AddressShipping'][0]['adm_division'] : $data['AddressBilling'][0]['adm_division'])
                    ),
                    array(
                        'name' => 'shipping_country',
                        'content' => (isset($data['AddressShipping'][0]['Country']['name']) ? $data['AddressShipping'][0]['Country']['name'] : $data['AddressBilling'][0]['Country']['name'])
                    ),
                    array(
                        'name' => 'shipping_email',
                        'content' => (isset($data['AddressShipping'][0]['email']) ? $data['AddressShipping'][0]['email'] : $data['AddressBilling'][0]['email'])
                    ),
                    array(
                        'name' => 'shipping_phone',
                        'content' => (isset($data['AddressShipping'][0]['phone']) ? $data['AddressShipping'][0]['phone'] : $data['AddressBilling'][0]['phone'])
                    ),                    
                    array(
                        'name' => 'handling_fee',
                        'content' => Configure::read('Shipping.rates')['handling_fee'] . ' /per package'
                    ),
                    array(
                        'name' => 'payment_method',
                        'content' => 'Paypal'
                    ),
                    array(
                        'name' => 'packages',
                        'content' => $packages_html
                    ),
                    array(
                        'name' => 'charge_now',
                        'content' => (strlen($data['Transaction']['code']) > 4 && substr($data['Transaction']['code'],0,4) == 'PAY-') ? __('Invoice me right away') : __('Charge me right away') 
                    ),
                    array(
                        'name' => 'shipment_consolidate',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_expedited',
                        'content' => ($data['Transaction']['expedited'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_insured',
                        'content' => ($data['Transaction']['insured'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_repackaged',
                        'content' => ($data['Transaction']['repackaged'] === true) ? __('Yes') : __('No')
                    ),
                    array(
                        'name' => 'shipment_special_instructions',
                        'content' =>  $data['Transaction']['special_instructions']
                    ),
                    array(
                        'name' => 'est_shipping_cost',
                        'content' => $this->getEstimatedShippingCost($data)
                    )                   
                );               

        $message = array(                
                'subject' => "We've Shipped Your Package!", 
                'from_email' => "Shipments@HMHShip.com",
                'from_name' => 'HMHShip',
                'to' => array(
                    array(
                        'email' => $data['AddressBilling'][0]['email'],                        
                        'type' => 'to'
                    )
                ),                
                'global_merge_vars' => $global_merge_vars
              );

        try {
			CakeLog::write('debug', "about to call sendTemplate Shipment Sent...");
			$result = $mandrill->messages->sendTemplate($template, '', $message, false);
            CakeLog::write('debug', "after call sendTemplate.");                       
            return $result;
		} catch (SocketException $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		} catch (Exception $e) {
            CakeLog::write('error', $e->getMessage());
			\Hmhship\Logger::getInstance()->log(sprintf('Failed to send email, reason: %s', $e->getMessage()), 'error');
		}

		return false;
	}

    /**
     * @param $data
     * @return mixed|string
     */
	public function getEstimatedShippingCost($data)
	{
		$cost = 0;
		foreach ($data['Shipment'] as $shipment) {
			if (isset($shipment['Package']['easypost_rate_price']) && is_numeric($shipment['Package']['easypost_rate_price'])) {
				$cost += floatval($shipment['Package']['easypost_rate_price']);
			}
		}
		if ($cost === 0) return __('Not available');
		return sprintf(__('%s USD'), $cost);
	}

    /**
     * @return string
     */
	protected function _getSmtpEnvironment()
	{
		$default = 'smtp_test';
		if (env('SERVER_NAME')) {
			switch (env('SERVER_NAME')) {
				case 'hmhship.com':
				case 'www.hmhship.com':
					$default = 'smtp';
					break;
			}
		}
		return $default;
	}
}