<?php

App::uses('Component', 'Controller');

/**
 * Class PaypalComponent
 */
class PaypalComponent extends Component {

    /**
     * @return ApiContext
     */
    private function get_context() {
        $client_id = Configure::read('Payment.Paypal.client_id');
        $client_secret = Configure::read('Payment.Paypal.client_secret');
        $client_mode = Configure::read('Payment.Paypal.client_mode');

        CakeLog::write('error', "Paypal credentials: ". $client_id);
        $context = new PayPal\Rest\ApiContext(new PayPal\Auth\OAuthTokenCredential($client_id, $client_secret));
        $context->setConfig(array(
            'mode' => $client_mode,
            'log.LogEnabled' => (Configure::read('debug') > 0) ? true : true,
            'log.FileName' => '../tmp/logs/paypal.log',
            'log.LogLevel' => (Configure::read('debug') > 0) ? 'DEBUG' : 'FINE', // 'FINE' LEVEL FOR LOGGING LIVE
            'validation.level' => 'log',
            'cache.enabled' => false
        ));
        
        return $context;
    }	
     
    // retrieve Payment
    public function returnPayment($tranId) {
        $context = $this->get_context();
        return PayPal\Api\Payment::get($tranId, $context);
    }
    
    // Use PayPal Merchant SDK (CLASSIC API) to setup a Express Checkout with Billing Agreement
    // this function redirects to /payments/complete?token=XXX&PayerID=XXX if successful
    public function set_express_checkout($description, $price, $tax = 0, $invoice_number = null) {              
       
        require_once   APP . 'Config' . DS . 'PayPal_MerchantSDK_Config.php';
        $url = dirname('http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI']);
        $returnUrl = Router::url('/payments/complete', true);
        $cancelUrl = Router::url('/payments/cancel', true) ;

        $currencyCode = "USD";
        // total shipping amount
        $shippingTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, 0);
        //total handling amount if any
        $handlingTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, 0);
        //total insurance amount if any
        $insuranceTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, 0);
        
        // shipping address
        $address = new PayPal\EBLBaseComponents\AddressType();
        $address->CityName = '';
        $address->Name = '';
        $address->Street1 = '';
        $address->StateOrProvince = '';
        $address->PostalCode = '';
        $address->Country = '';
        $address->Phone = '';

        // details about payment
        $paymentDetails = new PayPal\EBLBaseComponents\PaymentDetailsType();
              
        $itemAmount = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, $price);	
        $itemTotalValue = $price; 
        $taxTotalValue = $tax;
        $itemDetails = new PayPal\EBLBaseComponents\PaymentDetailsItemType();
        $itemDetails->Name = $description;
        $itemDetails->Amount = $itemAmount;
        $itemDetails->Quantity = 1;

        $itemDetails->ItemCategory = 'Physical';
        $itemDetails->Tax = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, $tax);	

        $paymentDetails->PaymentDetailsItem[0] = $itemDetails;	


        /*
         * The total cost of the transaction to the buyer. If shipping cost and tax charges are known, include them in this value. If not, this value should be the current subtotal of the order. If the transaction includes one or more one-time purchases, this field must be equal to the sum of the purchases. If the transaction does not include a one-time purchase such as when you set up a billing agreement for a recurring payment, set this field to 0.
         */
        $orderTotalValue = $shippingTotal->value + $handlingTotal->value +
        $insuranceTotal->value +
        $itemTotalValue + $taxTotalValue;

        //Payment details
        $paymentDetails->ShipToAddress = $address;
        $paymentDetails->ItemTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, $itemTotalValue);
        $paymentDetails->TaxTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, $taxTotalValue);
        $paymentDetails->OrderTotal = new PayPal\CoreComponentTypes\BasicAmountType($currencyCode, $orderTotalValue);

        /*
         * How you want to obtain payment. When implementing parallel payments, this field is required and must be set to Order. When implementing digital goods, this field is required and must be set to Sale. If the transaction does not include a one-time purchase, this field is ignored. It is one of the following values:

            Sale � This is a final sale for which you are requesting payment (default).

            Authorization � This payment is a basic authorization subject to settlement with PayPal Authorization and Capture.

            Order � This payment is an order authorization subject to settlement with PayPal Authorization and Capture.

         */
        $paymentDetails->PaymentAction = 'Sale';

        $paymentDetails->HandlingTotal = $handlingTotal;
        $paymentDetails->InsuranceTotal = $insuranceTotal;
        $paymentDetails->ShippingTotal = $shippingTotal;       

        $setECReqDetails = new PayPal\EBLBaseComponents\SetExpressCheckoutRequestDetailsType();
        $setECReqDetails->PaymentDetails[0] = $paymentDetails;
        /*
         * (Required) URL to which the buyer is returned if the buyer does not approve the use of PayPal to pay you. For digital goods, you must add JavaScript to this page to close the in-context experience.
         */
        $setECReqDetails->CancelURL = $cancelUrl;
        /*
         * (Required) URL to which the buyer's browser is returned after choosing to pay with PayPal. For digital goods, you must add JavaScript to this page to close the in-context experience.
         */
        $setECReqDetails->ReturnURL = $returnUrl;

        /*
         * Determines where or not PayPal displays shipping address fields on the PayPal pages. For digital goods, this field is required, and you must set it to 1. It is one of the following values:

            0 � PayPal displays the shipping address on the PayPal pages.

            1 � PayPal does not display shipping address fields whatsoever.

            2 � If you do not pass the shipping address, PayPal obtains it from the buyer's account profile.

         */
        $setECReqDetails->NoShipping = 1;
        /*
         *  (Optional) Determines whether or not the PayPal pages should display the shipping address set by you in this SetExpressCheckout request, not the shipping address on file with PayPal for this buyer. Displaying the PayPal street address on file does not allow the buyer to edit that address. It is one of the following values:

            0 � The PayPal pages should not display the shipping address.

            1 � The PayPal pages should display the shipping address.

         */
        $setECReqDetails->AddressOverride = 0;

        /*
         * Indicates whether or not you require the buyer's shipping address on file with PayPal be a confirmed address. For digital goods, this field is required, and you must set it to 0. It is one of the following values:

            0 � You do not require the buyer's shipping address be a confirmed address.

            1 � You require the buyer's shipping address be a confirmed address.

         */
        $setECReqDetails->ReqConfirmShipping = 0;     

        // Billing agreement details
        $billingAgreementDetails = new PayPal\EBLBaseComponents\BillingAgreementDetailsType('MerchantInitiatedBillingSingleAgreement');
        $billingAgreementDetails->BillingAgreementDescription = 'HMH Ship will charge you the Shipping Cost after we receive the package.';
        $setECReqDetails->BillingAgreementDetails = array($billingAgreementDetails);
        
        // Advanced options
        $setECReqDetails->AllowNote = 0;
        
        $setECReqType = new PayPal\PayPalAPI\SetExpressCheckoutRequestType();
        $setECReqType->SetExpressCheckoutRequestDetails = $setECReqDetails;
        $setECReq = new PayPal\PayPalAPI\SetExpressCheckoutReq();
        $setECReq->SetExpressCheckoutRequest = $setECReqType;

        /*
         * 	 ## Creating service wrapper object
        Creating service wrapper object to make API call and loading
        PaypalConfiguration::getAcctAndConfig() returns array that contains credential and config parameters
        */
        $paypalService = new PayPal\Service\PayPalAPIInterfaceServiceService(PaypalConfiguration::getAcctAndConfig());
       
      
        try {
            /* wrap API method calls on the service object with a try catch */
            $setECResponse = $paypalService->SetExpressCheckout($setECReq);
        } catch (Exception $ex) {
            \Hmhship\Logger::getInstance()->log($ex->getMessage(), 'error');
            CakeLog::write('error', "Paypal EC error: ".$ex->getMessage());            
            return $ex->getMessage();
        }               
           
          
        if(isset($setECResponse) && $setECResponse->Ack =='Success') {
            $payment = new PayPal\Api\Payment();
            $payment->setCreateTime($setECResponse->Timestamp);

            $token = $setECResponse->Token;
            $payment->setId($setECResponse->Token);                
            $payPalURL = 'https://www.';
            if (Configure::read('debug') > 0)
                $payPalURL = $payPalURL . 'sandbox.';
            $payPalURL = $payPalURL . 'paypal.com/webscr?cmd=_express-checkout&token=' . $token;
            $payment->setRedirectUrls($payPalURL);                       
            return $payment;                
        }
        else {
            
            if (isset($setECResponse)) {
               CakeLog::write('error', "Paypal SetExpressCheckout error: " . print_r($setECResponse, true));
            }             
        }
        return null;
    }
    
    public function do_express_checkout($token, $payerId, $amount){
      
       require_once   APP . 'Config' . DS . 'PayPal_MerchantSDK_Config.php';
       // SetExpressCheckout should have already been called and it returned $token
       
        /*
        * The DoExpressCheckoutPayment API operation completes an Express Checkout transaction. If you set up a billing agreement in your SetExpressCheckout API call, the billing agreement is created when you call the DoExpressCheckoutPayment API operatio
        */

       /*
        * The total cost of the transaction to the buyer. If shipping cost (not applicable to digital goods) and tax charges are known, include them in this value. If not, this value should be the current sub-total of the order. If the transaction includes one or more one-time purchases, this field must be equal to the sum of the purchases. 
        * Set this field to 0 if the transaction does not include a one-time purchase such as when you set up a billing agreement for a recurring payment that is not immediately charged. When the field is set to 0, purchase-specific fields are ignored.
        * For digital goods, the following must be true:
        * total cost > 0
        * total cost <= total cost passed in the call to SetExpressCheckout
       */       
       $token =urlencode( $token);

       /*
        *  Unique PayPal buyer account identification number as returned in the GetExpressCheckoutDetails response
       */
       $payerId=urlencode($payerId);
       $paymentAction = urlencode("Sale");
       
       /*
        * The total cost of the transaction to the buyer. If shipping cost (not applicable to digital goods) and tax charges are known, include them in this value. If not, this value should be the current sub-total of the order. If the transaction includes one or more one-time purchases, this field must be equal to the sum of the purchases. Set this field to 0 if the transaction does not include a one-time purchase such as when you set up a billing agreement for a recurring payment that is not immediately charged. When the field is set to 0, purchase-specific fields are ignored.
       */
       $orderTotal = new PayPal\CoreComponentTypes\BasicAmountType();
       $orderTotal->currencyID = "USD";
       $orderTotal->value = $amount;

       $paymentDetails= new PayPal\EBLBaseComponents\PaymentDetailsType();
       $paymentDetails->OrderTotal = $orderTotal;   

       $DoECRequestDetails = new PayPal\EBLBaseComponents\DoExpressCheckoutPaymentRequestDetailsType();
       $DoECRequestDetails->PayerID = $payerId;
       $DoECRequestDetails->Token = $token;
       $DoECRequestDetails->PaymentAction = $paymentAction;
       $DoECRequestDetails->PaymentDetails[0] = $paymentDetails;

       $DoECRequest = new PayPal\PayPalAPI\DoExpressCheckoutPaymentRequestType();
       $DoECRequest->DoExpressCheckoutPaymentRequestDetails = $DoECRequestDetails;

       $DoECReq = new PayPal\PayPalAPI\DoExpressCheckoutPaymentReq();
       $DoECReq->DoExpressCheckoutPaymentRequest = $DoECRequest;

       $paypalService = new PayPal\Service\PayPalAPIInterfaceServiceService(PaypalConfiguration::getAcctAndConfig());
       
       try {
           /* wrap API method calls on the service object with a try catch */
           $DoECResponse = $paypalService->DoExpressCheckoutPayment($DoECReq);
           CakeLog::write('debug', "DoECResponse: " . print_r($DoECResponse, true));
           if(isset($DoECResponse)) {
               return $DoECResponse->DoExpressCheckoutPaymentResponseDetails->PaymentInfo[0]->TransactionID;
           } else {
               CakeLog::write('error', "Paypal DoExpressCheckout error: DoECResponse is not set.");
               return null;
           }
       } catch (Exception $ex) {
                CakeLog::write('error', "Paypal DoExpressCheckout error: ".$ex->getMessage());      
                return null;
       }

        
    }
    
    public function reference_transaction($referenceID, $amt, $firstName, $lastName, $address1, $city, $state, $country, $zip) {       
          require_once   APP . 'Config' . DS . 'PayPal_MerchantSDK_Config.php';         

        /*
         *  # DoReferenceTransaction API
        The DoReferenceTransaction API operation processes a payment from a
        buyer's account, which is identified by a previous transaction.
        This sample code uses Merchant PHP SDK to make API call
        */

        /*
         *  The total cost of the transaction to the buyer. If shipping cost and
        tax charges are known, include them in this value. If not, this value
        should be the current subtotal of the order.

        If the transaction includes one or more one-time purchases, this field must be equal to
        the sum of the purchases. Set this field to 0 if the transaction does
        not include a one-time purchase such as when you set up a billing
        agreement for a recurring payment that is not immediately charged.
        When the field is set to 0, purchase-specific fields are ignored

        * `Currency ID` - You must set the currencyID attribute to one of the
        3-character currency codes for any of the supported PayPal
        currencies.
        * `Amount`
        */
        $amount = new PayPal\CoreComponentTypes\BasicAmountType("USD", $amt);

        // Address the order is shipped to.
        $shippingAddress = new PayPal\EBLBaseComponents\AddressType();
        $shippingAddress->Name = $firstName.' '.$lastName;
        $shippingAddress->Street1 = $address1;
        $shippingAddress->CityName = $city;
        $shippingAddress->StateOrProvince = $state;
        $shippingAddress->Country = $country;
        $shippingAddress->PostalCode = $zip;   

        // Information about the payment.
        $paymentDetails = new PayPal\EBLBaseComponents\PaymentDetailsType();
        $paymentDetails->OrderTotal = $amount;
        $paymentDetails->ShipToAddress = $shippingAddress;
       

        /*
         * 	 `DoReferenceTransactionRequestDetails` takes mandatory params:

        * `Reference Id` - A transaction ID from a previous purchase, such as a
        credit card charge using the DoDirectPayment API, or a billing
        agreement ID.
        * `Payment Action Code` - How you want to obtain payment. It is one of
        the following values:
        * Authorization
        * Sale
        * Order
        * None
        * `Payment Details`
        */
        $RTRequestDetails = new PayPal\EBLBaseComponents\DoReferenceTransactionRequestDetailsType();
       

        $RTRequestDetails->PaymentDetails = $paymentDetails;
        $RTRequestDetails->ReferenceID = $referenceID;
        $RTRequestDetails->PaymentAction = "Sale";
        $RTRequestDetails->PaymentType = "Any";

        $RTRequest = new PayPal\PayPalAPI\DoReferenceTransactionRequestType();
        $RTRequest->DoReferenceTransactionRequestDetails  = $RTRequestDetails;

        $RTReq = new PayPal\PayPalAPI\DoReferenceTransactionReq();
        $RTReq->DoReferenceTransactionRequest = $RTRequest;

        /*
        ## Creating service wrapper object
        Creating service wrapper object to make API call and loading
        PaypalConfiguration::getAcctAndConfig() returns array that contains credential and config parameters
        */
        $paypalService = new PayPal\Service\PayPalAPIInterfaceServiceService(PaypalConfiguration::getAcctAndConfig());
        
        try {
                /* wrap API method calls on the service object with a try catch */
                $RTResponse = $paypalService->DoReferenceTransaction($RTReq);
        } catch (Exception $ex) {
                 CakeLog::write('error', "Paypal DoExpressCheckout error: ".$ex->getMessage());      
                return null;
        }
        
        if (!isset($RTResponse)) {
            CakeLog::write('error', "paypalService->DoReferenceTransaction() returned null");
            return null;
        }
        else
            return $RTResponse;
    }
    
    /**
     * @param $description
     * @param $price
     * @param int $tax
     * @param null $invoice_number
     *
     * @return null|\PayPal\Api\Payment
     */
    public function single_payment($description, $price, $tax = 0, $invoice_number = null, 
      $completeUrl = '/payments/complete', $cancelUrl = '/payments/cancel', $addressData = null) {

        $context = $this->get_context();
        if(is_null($invoice_number)) { $invoice_number = uniqid(); }
        
        $payer = new PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');

        if (isset($addressData)) {
            // send Billing Address to PayPal so it will pre-populate address fields
            $pi = new PayPal\Api\PayerInfo();

            $billing = new PayPal\Api\Address();
            $billing->line1 = $addressData['shipment']['billing']['address1'];
            $billing->line2 = $addressData['shipment']['billing']['address2'];
            $billing->city = $addressData['shipment']['billing']['city'];            
            $billing->country_code = Country::get_code($addressData['shipment']['billing']['country']);
            $billing->postal_code = $addressData['shipment']['billing']['postal_code'];
            $billing->state = strtoupper($addressData['shipment']['billing']['state']);
            $billing->phone = $addressData['shipment']['billing']['phone'];
            $pi->billing_address = $billing;

            $pi->first_name = $addressData['shipment']['billing']['first_name'];
            $pi->last_name = $addressData['shipment']['billing']['last_name'];            

            $payer->payer_info = $pi;
        }

        $item = new PayPal\Api\Item();
        $item->setName($description)->setCurrency('USD')->setQuantity(1)->setPrice($price);
        $items = new PayPal\Api\ItemList();
        $items->setItems(array($item));
        
        $details = new PayPal\Api\Details();
        $details->setShipping(0)->setTax($tax)->setSubtotal($price);
        
        $amount = new PayPal\Api\Amount();
        $amount->setCurrency("USD")->setTotal($price)->setDetails($details);
        
        $transaction = new PayPal\Api\Transaction();
        $transaction
            ->setAmount($amount)->setItemList($items)
            ->setDescription($description)->setInvoiceNumber($invoice_number);
        
        $redirectUrls = new PayPal\Api\RedirectUrls();
        $redirectUrls
            ->setReturnUrl(Router::url($completeUrl, true))
            ->setCancelUrl(Router::url($cancelUrl, true));
        
        $payment = new PayPal\Api\Payment();
        $payment
            ->setIntent('sale')->setPayer($payer)
            ->setRedirectUrls($redirectUrls)->setTransactions(array($transaction))->setExperienceProfileId(Configure::read('Payment.Paypal.ExperienceProfileId'));

        
        try {
            $payment->create($context);
        } catch (Exception $ex) {
            \Hmhship\Logger::getInstance()->log('Paypal error: '.$ex->getMessage(), 'error');
            return null;
        }
        
        return $payment;
    }
    
    public function execute_payment($paymentId, $payerid) {
        
        $context = $this->get_context();        
        
        $payment = PayPal\Api\Payment::get($paymentId, $context);

        // ### Payment Execute
        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PayPal\Api\PaymentExecution();
        $execution->setPayerId($payerid);

         try {
            // Execute the payment                                        
            $result =  $payment->execute($execution, $context);

        } catch (Exception $ex) {
            \Hmhship\Logger::getInstance()->log('Paypal payment execution error: '.$ex->getMessage(), 'error');
            return null;
        }

        return 1;
    }
    
    public function authorize_payment($description, $price, $tax = 0, $invoice_number = null) {
        $context = $this->get_context();
        if(is_null($invoice_number)) { $invoice_number = uniqid(); }
        
        $payer = new PayPal\Api\Payer();
        $payer->setPaymentMethod('paypal');
        
        $item = new PayPal\Api\Item();
        $item->setName($description)->setCurrency('USD')->setQuantity(1)->setPrice($price);
        $items = new PayPal\Api\ItemList();
        $items->setItems(array($item));
        
        $details = new PayPal\Api\Details();
        $details->setShipping(0)->setTax($tax)->setSubtotal($price);
        
        $amount = new PayPal\Api\Amount();
        $amount->setCurrency("USD")->setTotal($price)->setDetails($details);
        
        $transaction = new PayPal\Api\Transaction();
        $transaction
            ->setAmount($amount)->setItemList($items)
            ->setDescription($description)->setInvoiceNumber($invoice_number);
        
        $redirectUrls = new PayPal\Api\RedirectUrls();
        $redirectUrls
            ->setReturnUrl(Router::url('/payments/complete', true))
            ->setCancelUrl(Router::url('/payments/cancel', true));
        
        $payment = new PayPal\Api\Payment();
        $payment
            ->setIntent('authorize')->setPayer($payer)
            ->setRedirectUrls($redirectUrls)->setTransactions(array($transaction));
        
        try {
            $payment->create($context);
        } catch (Exception $ex) {
			\Hmhship\Logger::getInstance()->log('Paypal error: '.$ex->getMessage(), 'error');
            return null;
        }
        
        return $payment;
    }
	
    public function getExperienceProfile() {
		$context = $this->get_context();
		try {
			$list = \PayPal\Api\WebProfile::get_list($context);
		} catch (\PayPal\Exception\PayPalConnectionException $ex) {
			echo $ex->getMessage();
		}
		$result = '';
		var_dump($list);
		foreach ($list as $object) {
			debug($object);
		}
	}
	
    public function createExperienceProfile() {
		$context = $this->get_context();
		try {
			$flowConfig = new \PayPal\Api\FlowConfig();
			$flowConfig->setLandingPageType("Billing");
		} catch(\PayPal\Exception\PayPalConnectionException $ex) {
			
		}
	}

    public function createPlan() {
            
            // Create a new instance of Plan object
            $plan = new Plan();

            // # Basic Information
            // Fill up the basic information that is required for the plan
            $plan->setName('HMH Ship Billing Plan')
                ->setDescription('This plan allows HMH to charge you when we receive your package.')
                ->setType('INFINITE');

            // # Payment definitions for this billing plan.
            $paymentDefinition = new PaymentDefinition();

            // The possible values for such setters are mentioned in the setter method documentation.
            // Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
            // You should be able to see the acceptable values in the comments.
            $paymentDefinition->setName('HMH Payment Definition')
                ->setType('REGULAR')
                ->setFrequency('DAY')
                ->setFrequencyInterval("1")
                ->setCycles("0")
                ->setAmount(new Currency(array('value' => 100, 'currency' => 'USD')));

            // Charge Models
            $chargeModel = new ChargeModel();
            $chargeModel->setType('SHIPPING')
                ->setAmount(new Currency(array('value' => 10, 'currency' => 'USD')));

            $paymentDefinition->setChargeModels(array($chargeModel));

            $merchantPreferences = new MerchantPreferences();
            $baseUrl = getBaseUrl();
            // ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
            // However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
            // This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
            $merchantPreferences->setReturnUrl("$baseUrl/ExecuteAgreement.php?success=true")
                ->setCancelUrl("$baseUrl/ExecuteAgreement.php?success=false")
                ->setAutoBillAmount("yes")
                ->setInitialFailAmountAction("CONTINUE")
                ->setMaxFailAttempts("0")
                ->setSetupFee(new Currency(array('value' => 1, 'currency' => 'USD')));


            $plan->setPaymentDefinitions(array($paymentDefinition));
            $plan->setMerchantPreferences($merchantPreferences);

            // For Sample Purposes Only.
            $request = clone $plan;

            // ### Create Plan
            try {
                $output = $plan->create($apiContext);
            } catch (Exception $ex) {
                // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
                ResultPrinter::printError("Created Plan", "Plan", null, $request, $ex);
                exit(1);
            }
    }
        
    public function refund_payment($paypalid, $amount) {
         
        $context = $this->get_context();        
        
        if (substr($paypalid,0,4) == 'PAY-') {
            // this is a Payment ID (REST API was used)
             $payment = PayPal\Api\Payment::get($paypalid, $context);
             
            // ### Get Sale From Created Payment
            // You can retrieve the sale Id from Related Resources for each transactions.
            $transactions = $payment->getTransactions();
            $relatedResources = $transactions[0]->getRelatedResources();
            $sale = $relatedResources[0]->getSale();
            $paypalid = $sale->getId();
        }
        // else this is an Express Checkout payment id (Classic/SOAP API)
        
        // ### Refund amount
        // Includes both the refunded amount (to Payer) 
        // and refunded fee (to Payee). Use the $amt->details
        // field to mention fees refund details.
        $amt = new PayPal\Api\Amount();
        $amt->setCurrency('USD')
            ->setTotal($amount);

        // ### Refund object
        $refund = new PayPal\Api\Refund();
        $refund->setAmount($amt);

        // ###Sale
        // A sale transaction.
        // Create a Sale object with the
        // given sale transaction id.
        $sale = new PayPal\Api\Sale();
        $sale->setId($paypalid);
        try {           

            // Refund the sale            
            $refundedSale = $sale->refund($refund, $context);
        } catch (Exception $ex) {
            \Hmhship\Logger::getInstance()->log('Paypal error: '.$ex->getMessage(), 'error');
            return null;    
        }
        
        if (!isset($refundedSale)) {
            \Hmhship\Logger::getInstance()->log('Paypal refund failed','error');
            return null;    
        }
        
        return $refundedSale->getId();
        
    }

    


}
