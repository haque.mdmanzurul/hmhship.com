<?php

App::uses('Component', 'Controller');
App::uses('Hmhship\Quickship\Package', 'Vendor');
App::uses('Hmhship\Quickship\Address', 'Vendor');

/**
 * Class ShippingComponent
 */
class ShippingComponent extends Component
{

	/**
	 * @var null
	 */
	private $from = null;

	/**
	 * @param Controller $controller
	 */
	public function initialize(Controller $controller)
	{
		//require_once ROOT . DS . 'Vendor' . DS . 'easypost' . DS . 'easypost-php/lib/easypost.php';
		\EasyPost\EasyPost::setApiKey(Configure::read('Shipping.EasyPost.key'));


		parent::initialize($controller);
	}

	public function init()
	{
		if (is_null($this->from)) {
			$this->from = \EasyPost\Address::create(Configure::read('Shipping.from_address'));
		}
	}

	/**
	 * Get shipping rates based on selected address and package options.
	 *
	 * @param \Hmhship\Quickship\Address $address
	 * @param \Hmhship\Quickship\Package $package
	 * @return array
	 */
	public function get_rates(\Hmhship\Quickship\Address $address, \Hmhship\Quickship\Package $package, $from = null)
	{
        CakeLog::write('debug', 'ShippingComponent.get_rates() top.  package = ' . print_r($package, true));
            
		$this->init();

		$to = \EasyPost\Address::create(array(
			'name' => $address->first_name . ' ' . $address->last_name,
			'street1' => $address->address1,
			'street2' => (isset($address->address2)) ? $address->address2 : '',
			'city' => $address->city,
                        'state' => $address->state,
			'zip' => $address->postal_code,
			'country' => Country::get_code($address->country),
			'phone' => $address->phone
		));

		$shipment_data = array(
			'from_address' => $this->from,
			'to_address' => $to,
			'parcel' => $this->create_parcel($package), // measures should be above 0,
		);

		if(!$from) $shipment_data['customs_info'] = $this->create_customs_info($package);

        CakeLog::write('debug', 'ShippingComponent.get_rates() shipment_data = ' . print_r($shipment_data, true));

		$shipment = \EasyPost\Shipment::create($shipment_data);


		/*if (is_float($insurance)) {
            $shipment->insure(array('amount' => (float)$insurance));
        }*/

		//var_dump($shipment);

		$rates = array();
		foreach ($shipment->rates as $rate) {
			$est = $rate->est_delivery_days;
			if (is_null($est) || empty($est) || $est == 0) {
				$est = __('Varies by destination');
			}

            // split CamelCase like PriorityMailInternational into separate words
            $service = $rate->service;
            preg_match_all('/((?:^|[A-Z])[a-z]+)/',$service,$split_service);
            if (count($split_service[0]) > 1) {
                $service = "";
                foreach($split_service[0] as $word) {
                    if ($split_service != "")
                        $service .= " ";
                    $service .= $word;
                }                    
            }
            $service = str_replace("UPSSaver", "UPS Saver", $service);

            // setlocale(LC_MONETARY,"en_US");

            // per Hirsh - add 13% to EasyPost rate
			$rates[] = array(
				'carrier' => $rate->carrier,                
				'service' => $service,
				'rate_id' => $rate->id,
				'shipment_id' => $shipment->id,
				'est_delivery_days' => $est,
				'est_delivery_days_num' => (isset($rate->est_delivery_days) && is_numeric($rate->est_delivery_days) && $rate->est_delivery_days > 0) ? $rate->est_delivery_days : 99,
				'rate' => number_format($rate->rate * 1.13, 2) . ' ' . $rate->currency,
				'rate_num' => $rate->rate * 1.13,
                'delivery_date' => (isset($rate->delivery_date)) ? date('n/j/y', strtotime($rate->delivery_date)) :
                                     ( (isset($rate->delivery_days)) ? date('n/j/y', strtotime("+".$rate->delivery_days." days")) : "" ),                                    
                'delivery_date_sort' => (isset($rate->delivery_date)) ? strtotime($rate->delivery_date) :
                                     ( (isset($rate->delivery_days)) ? strtotime("+".$rate->delivery_days." days") : strtotime("+99 years") )
			);
		}

		//var_dump(\EasyPost\Shipment::retrieve($shipment->id));

		return $rates;
	}

	/* ***************************************************************************************** */

	/**
	 * @TODO: implement according to Easypost specs
	 * @param $total_value
	 * @return string
	 */
	private function get_eel_pfc($total_value)
	{
		/* value < $2500: 'NOEEI 30.37(a) */
		return 'NOEEI 30.37(a)';
	}

	/**
	 *
	 * NOTE: all items' weight should be converted to ounces (OZ)
	 * @param $item
	 *
	 * @return mixed
	 */
	public function create_customs_item($item)
	{
		$this->init();
		$conversion = (object)Configure::read('Shipping.conversion');
		$weight_factor = $item->weight_unit === 'lb' ? $conversion->lb_oz : $conversion->kg_oz;

		return \EasyPost\CustomsItem::create(array(
			'description' => $item->description,
			'origin_country' => Country::get_code($item->country_id),
			'quantity' => (int)$item->quantity,
			'value' => (float)$this->stripMoneyPrefix($item->price_value),
			'weight' => (float)$item->weight * $weight_factor
		));
	}

	/**
	 * @param \Hmhship\Quickship\Package $package
	 *
	 * @return mixed
	 */
	public function create_customs_info(\Hmhship\Quickship\Package $package)
	{
		$this->init();
		$total_value = 0;
		//$total_weight = 0;
		$contents_type = array();

		$customs_items = array();
		foreach ($package->items as $item) {
			$customs_item = $this->create_customs_item($item);

			$total_value += $customs_item->value;
			//$total_weight += $customs_item->weight;

			$content_type = ItemType::get($item->type_id);
			$contents_type[] = $content_type->description;

			$customs_items[] = $customs_item;
		}

		return \EasyPost\CustomsInfo::create(array(
			'eel_pfc' => $this->get_eel_pfc($total_value),
			'contents_type' => 'other',
			'contents_explanation' => implode(', ', $contents_type),
			'customs_certify' => false,
			'restriction_type' => 'none',
			'restriction_comments' => '',
			'customs_items' => $customs_items
		));
	}

	/**
	 * @param \Hmhship\Quickship\Package $package
	 *
	 * @return mixed
	 */
	private function create_parcel(Hmhship\Quickship\Package $package)
	{
		$this->init();
		//$package = (object)$package->data['Package'];
		$conversion = (object)Configure::read('Shipping.conversion');
		$weight_factor = $package->weight_unit === 'lb' ? $conversion->lb_oz : $conversion->kg_oz;
		$size_factor = $package->size_unit === 'cm' ? $conversion->cm_in : 1;

		return \EasyPost\Parcel::create(array(
			'length' => $package->length * $size_factor,
			'width' => $package->width * $size_factor,
			'height' => $package->height * $size_factor,
			'weight' => $package->weight * $weight_factor
		));
	}

	/**
	 * @param array $address
	 *
	 * @return bool|mixed
	 */
	public function verify_address($address = array())
	{
		$this->init();
		// TODO: validate
		if (($response = Cache::read('easypost-verify-' . md5(json_encode($address)))) === false) {
			try {
				$response = \EasyPost\Address::create_and_verify($address);
				Cache::write('easypost-verify-' . md5(json_encode($address)), $response);

			} catch (\EasyPost\Error $e) {
				// TODO: log me
				return false;
			} catch (Exception $e) {
				// TODO: log me
				return false;
			}

		}
		return $response;
	}

	/**
	 * @param $price
	 *
	 * @return string
	 */
	public function stripMoneyPrefix($price)
	{
		return substr($price, 1, strlen($price));
	}
}
