<?php

App::uses('Component', 'Controller');

class FakeDataComponent extends Component {
    public function getUser() {
        return array(
            'first_name' => $this->data('string'),
            'last_name' => $this->data('string'),
            'email' => $this->data('email'),
            'phone' => $this->data('phone'),
            'username' => $this->data('string'),
            'password' => '',
            'active' => 0
        );
    }
    
    public function getAddress($type_id = 1) {
        return array(
            'address1' => $this->data('street'),
            'address2' => $this->data('apt'),
            'city' => 'Capital Federal',
            'adm_division' => 'Buenos Aires',
            'postal_code' => $this->data('zip'),
            'country_id' => 2,
            'address_type_id' => 1
        );
    }
    
    private function data($type) {
        $data = uniqid();
        
        switch($type) {
            case 'string':
                break;
            case 'email':
                $data = $data . '@' . uniqid() . '.com';
                break;
            case 'phone':
                $data = rand(1111111111, 9999999999);
                break;
            case 'street':
                $data = $this->data('string') . ' ' . rand(1111, 9999);
                break;
            case 'apt':
                $data = 'Dept. ' . rand(11, 99);
                break;
            case 'zip':
                $data = rand(1111, 9999);
                break;
        }
        
        return $data;
    }
}
