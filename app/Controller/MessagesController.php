<?php
App::uses('AppController', 'Controller');

/**
 * Transactions Controller
 *
 * @property Transaction $Transaction
 * @property Country $Country
 * @property ItemType $ItemType
 * @property PaginatorComponent $Paginator
 * @property NotificationComponent $Notification
 * @property FlashComponent $Flash
 * @property SessionComponent $Session
 */
class MessagesController extends AppController
{

    /**
     * @var array
     */
    public $components = array(
        'Notification'
    );

    public function isAuthorized($user)
    {

        return true;
    }

    public function admin_api_read($id = null)
    {
        $this->loadModel('Message');
        $this->loadModel('Note');

        if (!$this->Message->exists($id)) {
            throw new NotFoundException(__('Item not found'));
        }

        $this->Message->id = (int)$id;
        $this->Message->saveField('read', 1);

        $this->response->compress();

        $detail = $this->Message->getMessage((int)$id);
        CakeLog::write('debug', "User Id=" . $detail['Message']['user_id']);


        $notes = $this->Note->find('all', array(
            'conditions' => array('Note.user_id' => $detail['Message']['user_id']),
            'order' => 'Note.created DESC',
            'group' => 'Note.created '
        ));


        $detail['Notes'] = array();
        if (count($notes) > 0) {
            foreach ($notes as $note) {
                array_push($detail['Notes'], $note['Note']);
            }
        }


        $_serialize = array('detail');
        $_jsonOptions = JSON_NUMERIC_CHECK + JSON_FORCE_OBJECT;
        $this->set(compact('detail', '_serialize', '_jsonOptions'));
    }

    public function addReply()
    {
        //CakeLog::write('debug', "addReply().  userid=" . $userid . ", msg=" . $msg);
        $this->Message->createMessage(array(
            'message' => $this->request->data['msg'],
            'user_id' => $this->request->data['userid'],
            'from_hmh' => 1,
            'created' => date("Y-m-d H:i:s")
        ));
    }
}