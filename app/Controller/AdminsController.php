<?php
App::uses('AppController', 'Controller');

/**
 * Admins Controller
 *
 * @property Admin $Admin
 * @property Transaction $Transaction
 * @property FlashComponent $Flash
 * @property PaginatorComponent $Paginator
 * @property ShippingComponent $Shipping
 * @property SessionComponent $Session
 */
class AdminsController extends AppController
{

	public $components = array('Shipping');

    public function isAuthorized($user) {

        return true;
    }

	/**
	 *
	 */
	public function beforeFilter()
	{
		$this->Auth->allow('admin_login');
		$section = '';
		if (isset($this->request->query['section']) && !empty($this->request->query['section'])) {
			$section = $this->request->query['section'];
		}
		$this->set('section', $section);

		parent::beforeFilter();
	}

	/**
	 * Admin login section.
	 */
	public function admin_login()
	{
		$this->layout = 'login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Flash->set(__('Username and/or password are incorrect'), array('key' => 'auth', 'params' => array('class' => 'alert alert-danger')));
			}
		}
	}

	public function admin_logout()
	{
		$this->redirect($this->Auth->logout());
	}

	/**
	 * Admin dashboard.
	 *
	 * @return void
	 */
	public function admin_index()
	{
		$this->layout = 'main';
		if (isset($this->request->query['page'])) {
			$page = $this->request->query['page'];
		} else {
			$page = "";
		}
		$type = (isset($this->request->query['type']) ? $this->request->query['type'] : null);
		$this->set(compact('page', 'type'));
		$this->loadModel('Transaction');
        $this->loadModel('Message');
        $this->loadModel('BusinessSignup');
        $this->loadModel('User');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
		$this->set('quickship_unread_count', $quickship_unread_count);


        $this->set('quickship_registered_unread_count', $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP));
        $this->set('messages_unread_count', $this->Message->countUnread());


        $this->set('count_waiting_package_ghost', $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST));

        $this->set('messages_registered_unread_count', $this->Message->countRegisteredUnread());
        $this->set('business_unread_count', $this->BusinessSignup->countUnread());
		$this->set('count_registered', $this->User->countUnread());
		if (isset($this->request->query['section']) && !empty($this->request->query['section'])) {
			switch ($this->request->query['section']) {
                case 'bizcust':
                    return $this->render('admin_business_customers');
                    break;

                case 'messages':
                    $messages = $this->Message->listMessages();
                    $this->set('messages', $messages);
                    return $this->render('admin_messages');
                    break;

                case 'regcust':
                    return $this->render('admin_registered_customers');
                    break;

                case 'alltransactions':
                    return $this->render('admin_transactions');
                    break;

                case 'allusers':
                    return $this->render('admin_users');
                    break;

                case 'registered':
                    return $this->render('admin_registered');
                    break;

                default:
			}
		}
	}

	/**
	 * admin_view method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_view($id = null)
	{
		if (!$this->Admin->exists($id)) {
			throw new NotFoundException(__('Invalid admin'));
		}
		$options = array('conditions' => array('Admin.' . $this->Admin->primaryKey => $id));
		$this->set('admin', $this->Admin->find('first', $options));
	}

	/**
	 * admin_add method
	 *
	 * @return void
	 */
	public function admin_add()
	{
		if ($this->request->is('post')) {
			$this->Admin->create();
			if ($this->Admin->save($this->request->data)) {
				$this->Flash->set(__('The admin has been saved.'), array('params' => array('class' => 'alert alert-success')));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->set(__('The admin could not be saved. Please, try again.'), array('params' => array('class' => 'alert alert-danger')));
			}
		}
	}

	/**
	 * admin_edit method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_edit($id = null)
	{
		if (!$this->Admin->exists($id)) {
			throw new NotFoundException(__('Invalid admin'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Admin->save($this->request->data)) {
				$this->Flash->set(__('The admin has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				debug($this->Admin->validationErrors);
				$this->Flash->set(__('The admin could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Admin.' . $this->Admin->primaryKey => $id));
			$this->request->data = $this->Admin->find('first', $options);
		}
	}

	/**
	 * admin_delete method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function admin_delete($id = null)
	{
		$this->Admin->id = $id;
		if (!$this->Admin->exists()) {
			throw new NotFoundException(__('Invalid admin'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Admin->delete()) {
			$this->Flash->set(__('The admin has been deleted.'));
		} else {
			$this->Flash->set(__('The admin could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

    // Messages is selected in Left Admin panel
    public function admin_messages()
	{
		$this->layout = 'main';
		$section = 'messages';
		$this->loadModel('Message');
        $messages = $this->Message->listMessages();
		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('messages','messages_unread_count','quickship_unread_count','business_unread_count','section','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
	}

    // Messages - Registered is selected in Left Admin panel
    public function admin_messages_registered()
	{
		$this->layout = 'main';
		$section = 'messages_registered';
		$this->loadModel('Message');
        $messages = $this->Message->listRegisteredMessages();
		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('messages','messages_unread_count','quickship_unread_count','business_unread_count','section', 'messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
	}

    // Business Customers is selected in Left Admin panel
    public function admin_business()
	{
		$this->layout = 'main';
		$section = 'bizcust';
		$this->loadModel('Message');
        $messages = $this->Message->listMessages();
		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('messages','messages_unread_count','quickship_unread_count','business_unread_count','section','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
	}

    public function admin_blogs() {
        // Display Blog Admin in iframe
        $this->layout = 'main';
		$section = 'blogs';

		$this->loadModel('Message');
        $messages = $this->Message->listMessages();
		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('messages','messages_unread_count','quickship_unread_count','business_unread_count','section','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
    }

    public function admin_registered()
	{
		$this->layout = 'main';
		$section = 'registered';
		$this->loadModel('Message');

		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $registereds = $this->User->find('all');
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('registereds','messages_unread_count','quickship_unread_count','business_unread_count','section','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
	}

     public function admin_documents() {
        // Display Documents Admin in iframe
        $this->layout = 'main';
		$section = 'documents';

		$this->loadModel('Message');
        $messages = $this->Message->listMessages();
		$messages_unread_count = $this->Message->countUnread();
        $messages_registered_unread_count = $this->Message->countRegisteredUnread();
        $this->loadModel('Transaction');
		$quickship_unread_count = $this->Transaction->countUnreadByType(Transaction::TYPE_QUICKSHIP);
        $quickship_registered_unread_count = $this->Transaction->countRegisteredUnreadByType(Transaction::TYPE_QUICKSHIP);
        $this->loadModel('BusinessSignup');
        $business_unread_count = $this->BusinessSignup->countUnread();
        $this->loadModel('User');
        $count_registered = $this->User->countUnread();
        $count_waiting_package_ghost = $this->Transaction->countRegisteredUnreadByStatus(Transaction::STATUS_GHOST);
		$this->set(compact('messages','messages_unread_count','quickship_unread_count','business_unread_count','section','messages_registered_unread_count','quickship_registered_unread_count','count_registered', 'count_waiting_package_ghost'));
    }
}
